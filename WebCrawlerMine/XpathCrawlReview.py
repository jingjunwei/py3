#-*- coding:utf-8 -*-
#-------------------------------------------------------------------------------
# Name: XpathCrawlReview.py ver0.95
# Purpose: 数据采集
#
# Author: chenk
#
# Created: 08/02/2016
#-------------------------------------------------------------------------------
# 中文乱码
import sys 
import random
reload(sys) 
sys.setdefaultencoding('utf-8') 

# 数据抓取 分析
import urllib2
from lxml import etree
import requests
import socket
import urlparse
# html编码问题
import decode_html

# 数据处理 文件读取等
import re
import os
from ConfigParser import ConfigParser

# 数据库
import pymssql

# 线程
import time  
import threading

# 日志记录
import logging
import logging.config  

# solr
import solr  

# 代理获取
import getproxy

# json 数据
import json2xml

class MSSQL:
    """
    对pymssql的简单封装
    pymssql库，该库到这里下载：http://www.lfd.uci.edu/~gohlke/pythonlibs/#pymssql
    使用该库时，需要在Sql Server Configuration Manager里面将TCP/IP协议开启

    用法：

    """

    def __init__(self, host, user, pwd, db, port=1433):
        self.host = host
        self.user = user
        self.pwd = pwd
        self.db = db
        self.port = port

    def __GetConnect(self):
        """
        得到连接信息
        返回: conn.cursor()
        """
        if not self.db:
            raise(NameError,"没有设置数据库信息")
        self.conn = pymssql.connect(host=self.host,user=self.user,password=self.pwd,database=self.db,charset="utf8", port = self.port)
        cur = self.conn.cursor()
        if not cur:
            raise(NameError,"连接数据库失败")
        else:
            return cur

    def ExecQuery(self,sql):
        """
        执行查询语句
        返回的是一个包含tuple的list，list的元素是记录行，tuple的元素是每行记录的字段

        调用示例：
                ms = MSSQL(host="localhost",user="sa",pwd="123456",db="PythonWeiboStatistics")
                resList = ms.ExecQuery("SELECT id,NickName FROM WeiBoUser")
                for (id,NickName) in resList:
                    print str(id),NickName
        """
        try:
            cur = self.__GetConnect()
            sql = sql.replace("WebRescourseForVipProject", self.db)
            cur.execute(sql)
            resList = cur.fetchall()
        #查询完毕后必须关闭连接
        finally:
            self.conn.close()
        return resList

    def ExecNonQuery(self,sql):
        """
        执行非查询语句

        调用示例：
            cur = self.__GetConnect()
            cur.execute(sql)
            self.conn.commit()
            self.conn.close()
        """
        try:
            cur = self.__GetConnect()
            sql = sql.replace("WebRescourseForVipProject", self.db)
            cur.execute(sql)
            self.conn.commit()
        finally:
            self.conn.close()
            pass
        pass
    pass

class myThread(threading.Thread):   #继承父类threading.Thread
    '''
    线程
    '''
    def __init__(self, conf, name, taskId, uid, ruleId, siteSectionId, pageUrlWithParam, logger, htmlgetter, supthread=False, func=lambda x: x):
        threading.Thread.__init__(self)
        self.conf = (os.path.join(os.getcwd(),conf))
        self.threadID = taskId
        self.name = name
        self.taskId = taskId
        self.uid = uid
        self.ruleId = ruleId
        self.siteSectionId = siteSectionId
        self.pageUrlWithParam = pageUrlWithParam
        self.logger = logger
        self.htmlgetter = htmlgetter
        self.supthread = supthread
        self.func = func
    def run(self):                   #把要执行的代码写到run函数里面 线程在创建后会直接运行run函数
        # 读取配置
        if not self.supthread:
            self.cf = ConfigParser()
            self.cf.read(self.conf)
            # 读取配置：数据库链接
            db_host = self.cf.get("db", "host")
            if db_host == '':
                db_host == '1433'
            db_port = self.cf.get("db", "port")
            db_user = self.cf.get("db", "user")
            db_pass = self.cf.get("db", "pass")
            db_name = self.cf.get("db", "dbname")
            ms = MSSQL(host=db_host,user=db_user,pwd=db_pass,db=db_name,port=db_port)
            crawl = Crawl(self.conf, ms, self.taskId, self.uid, self.ruleId, self.siteSectionId, self.pageUrlWithParam, self.logger, self.htmlgetter)
            pass
        else:
            self.func()
        pass
    pass

class HtmlGet:
    def __init__(self, conf, logger, proxyuse):
        try:
            self.cf = ConfigParser()
            self.logger = logger
            self.conf = (os.path.join(os.getcwd(),conf))
            self.proxyuse = proxyuse
            self.headerlist = [{'User-Agent':'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6'},
                {'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0'},
                {'User-Agent':'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20130331 Firefox/21.0'},
                {'User-Agent':'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0'},
                {'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36'},
                {'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.11 (KHTML, like Gecko) Ubuntu/11.10 Chromium/27.0.1453.93 Chrome/27.0.1453.93 Safari/537.36'},
                {'User-Agent':'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36'},
                {'User-Agent':'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)'},
                {'User-Agent':'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0)'},
                {'User-Agent':'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)'},
                {'User-Agent':'Mozilla/5.0 (compatible; WOW64; MSIE 10.0; Windows NT 6.2)'},
                {'User-Agent':'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6; en-US) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27'},
                {'User-Agent':'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27'},
                {'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_0) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11'},
                {'User-Agent':'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; 360SE)'},
                {'User-Agent':'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; .NET CLR 2.0.50727; .NET CLR 3.0.30729; .NET CLR 3.5.30729; InfoPath.3; rv:11.0) like Gecko'}]
            self._have_proxy = False
            self.get_config()
            # 读取配置
            pass
        except Exception as inst:
            msg = "[HtmlGet init]" + str(inst.message)
            self.logger.error(msg)
    def get_config(self):
        self.cf.read(self.conf)
        self.crawl_only_proxy_success = self.cf.getint("proxy", "crawl_only_proxy_success")
        self.retry_time = self.cf.getint("proxy", "retry_time")
        self.retry_after = self.cf.getint("proxy", "retry_after")
        self.max_retry_time = self.cf.getint("proxy", "max_retry_time")
        self.retry_flag = 0
        # 最大同一网址抓取次数
        self.url_get_flag = 0
        self.max_get_num = self.cf.getint("crawl", "max_get_num")
        self.solrdeltaurl = self.cf.get("solr", "solrdeltaurl")
        self.solrQry = solr.SolrConnection(self.cf.get("solr", "solrqueryurl"))
        self.auto_deduplication = self.cf.getint("db", "auto_deduplication")
        self.check_exist_by = self.cf.get("crawl", "check_exist_by")
        db_host = self.cf.get("db", "host")
        if db_host == '':
            db_host == '1433'
        db_port = self.cf.get("db", "port")
        db_user = self.cf.get("db", "user")
        db_pass = self.cf.get("db", "pass")
        db_name = self.cf.get("db", "dbname")
        # 数据库链接
        self.ms = MSSQL(host=db_host,user=db_user,pwd=db_pass,db=db_name,port=db_port)
        #if self.proxyuse and not self._have_proxy:
        #    self._get_proxyopener()
        pass
    def _get_proxyopener(self):
        try:
            if not self._have_proxy:
                self.proxy = getproxy.getproxy()
                msg = "[Proxy]" + str(self.proxy) + ", retry time:" + str(self.retry_flag)
                self.logger.info(msg)
            if self.proxy == "":
                self._have_proxy = False
                self.retry_flag += 1
                if self.crawl_only_proxy_success and self.retry_flag < self.max_retry_time:
                    time.sleep(self.retry_after * 60)
                    self._get_proxyopener()
                    return
                elif self.retry_flag > self.retry_time:
                    self.proxyuse = 0
                    msg = "[Proxy Got ERR, Retry time]" + str(self.retry_flag)
                    self.logger.info(msg)
                    return
                time.sleep(self.retry_after * 60)
                self._get_proxyopener()
                return
            
            # 连接超时设置
            defaultsockettime = self.cf.getint("crawl", "defaultsockettime")
            socket.setdefaulttimeout(defaultsockettime)
            proxy_handler = urllib2.ProxyHandler(self.proxy)
            opener = urllib2.build_opener(proxy_handler)
            self.opener = opener
            self._have_proxy = True
            pass
        except Exception as inst:
            msg = "[Proxy]" + str(inst.message)
            self.logger.error(msg)
    def get_htmldatadecoded(self, url):
        try:
            if self.url_get_flag >= self.max_get_num:
                msg = "[BYD MAX GET]" + str(url)
                raise Exception(msg)
            headersuse = random.choice(self.headerlist)
            getdata = ""
            self.url_get_flag += 1
            # 使用代理
            if self.proxyuse:
                try:
                    getdata = self.opener.open(url).read()
                    self.retry_flag = 0
                # 使用代理失败
                except Exception as inst:
                    self._have_proxy = False
                    self._get_proxyopener()  
                    msg = "[HTML(P) GET]"+ str(inst.message) + str(url)
                    self.logger.error(msg)
                    time.sleep(1)
                    return self.get_htmldatadecoded(url)
            # 不使用代理
            else:
                try:
                    req = urllib2.Request(url = url, headers = headersuse)
                    getdata = urllib2.urlopen(req).read()
                except Exception as inst:
                    msg = "[HTML GET]"+ str(inst.message) + str(url)
                    self.logger.error(msg)
                    time.sleep(1)
                    return self.get_htmldatadecoded(url)
            # 编码转成Unicode
            try:
                returnData = decode_html.decode_html(getdata)
                self.url_get_flag = 0
                return returnData
            except:
                time.sleep(1)
                return self.get_htmldatadecoded(url)
        except Exception as inst:
            msg = "[Html Data Decoded]" + str(url)
            raise Exception(msg)
    def check_recordexist(self, title, url):
        try:
            if self.check_exist_by == "solr":
                q = 'Title:"' + title + '" and Url:"' + url + '"'
                result = self.solrQry.query(q, rows=1, fields='id')
                if len(result):
                    return True
                return False
            elif self.check_exist_by == "db":
                checkList = self.ms.ExecQuery("""SELECT 
                            ID
                    FROM [WebRescourseForVipProject].[dbo].[CrawlResult] WHERE [Title] = '%s' AND [Url] = '%s'""" % (title, url))
                if len(checkList):
                    return True
                return False
        except Exception as inst:
            msg = "[Record Exist]" + str(inst.message)
            self.logger.error(msg)
    def deduplication(self):
        if self.auto_deduplication:
            self.ms.ExecNonQuery("""delete tmp from(    
                        select row_num = row_number() over(partition by Title, Url order by Title desc)    
                            from CrawlResult 
                         ) tmp    
                     where row_num > 1""")
            # 去重时等待数据库完成
            time.sleep(1 * 60)
    def send_solrdeltaimport(self):
        """
        插入 solr 增量索引添加
        """
        while True:
            # 查询当前数据库总条目
            toList = self.ms.ExecQuery("""SELECT COUNT(1) AS [TOTAL] FROM [WebRescourseForVipProject].[dbo].[CrawlResult]""")
            for (tl,) in toList:
                totalDb = int(tl)
                pass
            q = '*:*'
            result = self.solrQry.query(q, rows=1, fields='id')
            totalSolr = result.numFound
            # 当前数据库总条目与solr不一致时 去重后solr导入
            if totalDb != totalSolr:
                solrimportper = self.cf.getint("solr", "solrimportper")
                localtime = time.localtime(time.time())
                if localtime.tm_min % solrimportper == 0:
                    try:
                        self.deduplication()
                        urllib2.urlopen(self.solrdeltaurl).read()
                    except Exception as inst:
                        msg = "[Solr Deltaimport]" + str(inst.message)
                        self.logger.error(msg)
                        raise Exception(msg)
            time.sleep(60)
    pass

class New:
    def __init__(self, ms, taskid, uid, webname, sectionname, rule, htmlgetter):
        try:
            self.url = ""
            self.rule = rule
            self._setXpath()
            self.ms = ms
            self.taskid = taskid
            self.uid = uid
            self.webname = webname
            self.sectionname = sectionname
            self.collecttime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
            self.htmlgetter = htmlgetter
            self.flag = 0
        except Exception as inst:
            msg = "[NEW INIT]" + str(inst.message)
            raise Exception(msg)
            pass
    def _getHtmlnews(self):
        try:
            getdata = self.htmlgetter.get_htmldatadecoded(self.url)
            self.htmlnews = etree.HTML(getdata)   
        except Exception as inst:
            msg = "[NEW REQUEST]" + str(inst.message)
            raise Exception(msg)
            pass
    def _setXpath(self):
        try:
            if len(self.rule) == 1:
                self.submittype, self.titlexpath, self.keywordxpath, self.keywordspliter, self.urlxpath, self.urlattr, self.contentxpath, self.createtimexpath, self.createtimereg, self.picturexpath, self.pictureattr, self.sourcexpath, self.sourcereg, self.authorxpath, self.authorreg, self.isfinished = self.rule[0]
            else:
                raise Exception('No Rule Found!') 
        except Exception as inst:
            msg = "[Xpath]" + str(inst.message)
            raise Exception(msg)
            pass
    def _setTitle(self):
            title = ''
            if self.titlexpath:
                try:
                    titletemp = self.htmlnews.xpath(self.titlexpath)
                    if len(titletemp):
                        if isinstance(titletemp[0], etree._ElementUnicodeResult):
                            title = titletemp[0]
                        elif titletemp[0].text:
                            title = titletemp[0].text.strip() or titletemp[0].get("title").strip()
                except Exception as inst:
                    msg = "[Title]" + str(titletemp) + '\n' + str(inst.args) + str(inst.message)
                    raise Exception(msg)
                    pass
            self.title = title
    def _setKeyword(self):
        keyword = ''
        if self.keywordxpath:
            try:
                keywordtemp = self.htmlnews.xpath(self.keywordxpath)
                if len(keywordtemp):
                    for key in keywordtemp:
                        keyword += key.strip()
                        keyword += "; "
                    if len(keywordtemp) == 1:
                        if self.keywordspliter and keywordtemp.strip().find(self.keywordspliter) > -1:
                            keyword = keywordtemp.strip().replace("self.keywordspliter", "; ")
            except Exception as inst:
                msg = "[Keyword]" + str(keywordtemp) + '\n' + str(inst.args) + str(inst.message)
                raise Exception(msg)
                pass
        self.keyword = keyword
    def _setCreatetime(self):
        createtime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
        if self.createtimexpath:
            try:
                createtimetemp = self.htmlnews.xpath(self.createtimexpath)
                if len(createtimetemp):
                    createtimetemp = createtimetemp[0].strip()
                    createtimeregtemp = self.createtimereg.replace("yyyy",u"\d{4}").replace("yy",u"\d{2}").replace("MM",u"\d{1,2}").replace("dd",u"\d{1,2}").replace("HH",u"\d{1,2}").replace("mm",u"\d{1,2}").replace("ss",u"\d{1,2}")
                    match = re.search(createtimeregtemp, createtimetemp)
                    if match:
                        createtime = match.group()
                        self.createtimeregtemp2 = self.createtimereg.replace("yyyy",u"%Y").replace("yy",u"%y").replace("MM",u"%m").replace("dd",u"%d").replace("HH",u"%H").replace("mm",u"%M").replace("ss",u"%S")
                        createtime = time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(createtime,self.createtimeregtemp2))
            except Exception as inst:
                msg = "[Createtime]" + str(createtimetemp) + '\n' + str(inst.args) + str(inst.message)
                raise Exception(msg)
                pass
        self.createtime = createtime
    def _setPicture(self):
        picture = ''
        if self.picturexpath:
            try:
                pic = self.htmlnews.xpath(self.picturexpath)
                coverflag = 0
                for img in pic:
                    picturetemp = img.get(self.pictureattr) or img.get("src")
                    picturetemp = urlparse.urljoin(self.url, picturetemp)
                    img.set("src", picturetemp)
                    if coverflag == 0:
                        picture = picturetemp
                    coverflag += 1
                if re.match(r'^https?:/{2}\w.+$', picture): 
                    pass
                else:
                    picture = ""
            except Exception as inst:
                msg = "[Picture]" + str(pic) + '\n' + str(inst.args) + str(inst.message)
                raise Exception(msg)
        self.picture = picture
    def _setContent(self):
        contents = ''
        if self.contentxpath:
            try:
                content = self.htmlnews.xpath(self.contentxpath)
                if len(content):
                    if len(content) > 1:
                        ctemp = content
                    else:
                        ctemp = content[0]
                    for c in ctemp:
                        if c.tag not in ["meta", "script"]:
                            contents = contents + etree.tostring(c)
            except Exception as inst:
                msg = "[Content]" + str(content) + '\n' + str(inst.args) + str(inst.message)
                raise Exception(msg)
        self.contents = contents
    def _setSource(self):
        source = ''
        if self.sourcexpath:
            try:
                sourcetemp = self.htmlnews.xpath(self.sourcexpath)
                if len(sourcetemp):
                    source = sourcetemp[0].strip()
                    if len(source) and self.sourcereg:
                        self.sourcereg = self.sourcereg.replace("%s",u'([a-zA-Z0-9\u4e00-\u9fff]+)')
                        source = decode_html.findFirstNotEmptyStr(self.sourcereg, source) or source
            except Exception as inst:
                msg = "[Source]" + str(sourcetemp) + '\n' + str(inst.args) + str(inst.message)
                raise Exception(msg)
        self.source = source
    def _setAuthor(self):
        author = ''
        if self.authorxpath:
            try:
                autemp = self.htmlnews.xpath(self.authorxpath)
                if len(autemp):
                    author = autemp[0].strip()
                    if len(author) and self.authorreg:
                        self.authorreg = self.authorreg.replace("%s",u'([a-zA-Z0-9\u4e00-\u9fff、 ]+)')
                        author = decode_html.findFirstNotEmptyStr(self.authorreg, author) or author
            except Exception as inst:
                msg = "[Author]" + str(autemp) + '\n' + str(inst.args) + str(inst.message)
                raise Exception(msg)
        self.author = author
    def set_url(self, url):
        self.url = url
        pass
    def insert_new(self):
        '''
        插入一条采集的信息
        '''
        try:
            self.contents = self.contents.replace("'", "''")
            self.title = self.title.replace("'", "''")
            sql = '''INSERT INTO [WebRescourseForVipProject].[dbo].[CrawlResult](
                                [taskid]
                                ,[Uid]
                                ,[webname]
                                ,[sectionname]
                                ,[Title]
                                ,[Keyword]
                                ,[Url]
                                ,[Content]
                                ,[CreateTime]
                                ,[Picture]
                                ,[Source]
                                ,[Author]
                                ,[CollectTime]
                                ,[IsCleaned]) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')
                    ''' % (self.taskid, 
                           self.uid,
                           self.webname, 
                           self.sectionname, 
                           self.title, 
                           self.keyword, 
                           self.url, 
                           self.contents,
                           self.createtime, 
                           self.picture, 
                           self.source,
                           self.author,
                           self.collecttime,
                           self.flag)
            self.ms.ExecNonQuery(sql)
        except Exception as inst:
            msg = "[Insert]" + str(inst.message)
            raise Exception(msg)
    def get_new_news(self):
        try:
            self._getHtmlnews()
            self._setTitle()
            if self.title == "":
                return False
            if self.htmlgetter.check_recordexist(self.title, self.url):
                return False
            self._setKeyword()
            self._setCreatetime()
            self._setPicture()
            self._setContent()
            self._setSource()
            self._setAuthor()
            self.collecttime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
            return True
        except Exception as inst:
            msg = "[Get New]" + str(inst.message)
            raise Exception(msg)

class Crawl:
    def __init__(self, conf, ms, taskid, uid, ruleid, sitesectionid, pageurlwithparam, logger, htmlgetter, startpage=1):
        #super(Crawl, self).__init__(conf)
        try:
            self.cf = ConfigParser()
            self.conf = (os.path.join(os.getcwd(),conf))
            self.ms = ms
            self.taskid = taskid
            self.uid = uid
            self.sitesectionid = sitesectionid
            self.pageurlwithparam = pageurlwithparam
            self.ruleid = ruleid
            self.logger = logger
            self.htmlgetter = htmlgetter
            # 起始页
            self.startpage = startpage
            # 结束页
            self.endpage = 999999
            self.startTask()
        except Exception as inst:
            msg = "[Crawl init]" + str(inst.message)
            self.logger.error(msg)
        pass
    def _get_config(self):
        try:
            self.cf.read(self.conf)
            self.stop_when_crawlpageerr = self.cf.getint("crawl", "stop_when_crawlpageerr")
            self.continue_when_inserterr = self.cf.getint("crawl", "continue_when_inserterr")
            self.continue_when_recordexist = self.cf.getint("crawl", "continue_when_recordexist")
            self.crawltoyear = self.cf.getint("crawl", "crawltoyear")
            self.thread_onepagewait = self.cf.getint("thread", "onepagewait")
            self.thread_onerecoredwait = self.cf.getint("thread", "onerecoredwait")
        except Exception as inst:
            msg = "[Crawl Config]" + str(inst.message)
            raise Exception(msg)
    def _get_rule(self):
        # 读取规则
        try:
            self.rulelist = self.ms.ExecQuery("""SELECT 
                                       [SubmitType]
                                      ,[TitleXpath]
                                      ,[KeywordXpath]
                                      ,[KeywordSpliter]
                                      ,[UrlXpath]
                                      ,[UrlAttr]
                                      ,[ContentXpath]
                                      ,[CreateTimeXpath]
                                      ,[CreateTimeReg]
                                      ,[PictureXpath]
                                      ,[PictureAttr]
                                      ,[SourceXpath]
                                      ,[SourceReg]
                                      ,[AuthorXpath]
                                      ,[AuthorReg]
                                      ,[IsFinished]
                                FROM [WebRescourseForVipProject].[dbo].[CrawlRules] WHERE [Id] = '%s' AND [DeleteFlag] = 0""" % str(self.ruleid))
        except Exception as inst:
            msg = "[Rule]" + str(inst.message)
            raise Exception(msg)
    def _get_weblist(self):
        # 读取被采集网站信息
        try:
            self.weblist = self.ms.ExecQuery("""SELECT 
                                       [WebName]
                                      ,[WebLink]
                                      ,[SectionName]
                                      ,[IndexDiff]
                                      ,[SectionIndexUrl]
                                      ,[Start]
                                      ,[Step]
                                FROM [WebRescourseForVipProject].[dbo].[CrawlSite] WHERE [Id] = '%s' AND [DeleteFlag] = 0""" % str(self.sitesectionid))
        except Exception as inst:
            msg = "[Weblist]" + str(inst.message)
            raise Exception(msg)
    def _get_links(self):
        try:
            if self.urlxpath[0:5] == "[xml]":
                self.links = self.htmlnewlist.xpath(self.urlxpath[5:])
                lidx = 0
                for l in self.links:
                    self.links[lidx] = self.urlattr.replace("%s", l)
                    lidx += 1
            elif self.urlxpath[0:6] == "[json]":
                self.links = self.htmlnewlist.xpath(self.urlxpath[6:])
                lidx = 0
                for l in self.links:
                    self.links[lidx] = self.urlattr.replace("%s", l)
                    lidx += 1
            elif self.urlxpath[0:7] == "[jsonp]":
                self.links = self.htmlnewlist.xpath(self.urlxpath[7:])
                lidx = 0
                for l in self.links:
                    self.links[lidx] = self.urlattr.replace("%s", l)
                    lidx += 1
            elif self.urlxpath[0:7] == "[jsonh]":
                self.links = self.htmlnewlist.xpath(self.urlattr)
            else:
                self.links = self.htmlnewlist.xpath(self.urlxpath + '/@' + self.urlattr)
        except Exception as inst:
            msg = "[Page Links]" + str(inst.message)
            raise Exception(msg)
        pass
    def _get_linkslength(self):
        try:
            self.linkslength = len(self.links)
        except Exception as inst:
            msg = "[Linkslength]" + str(inst.message)
            raise Exception(msg)
        pass
    def _get_htmlnewlist(self):
        try:
            getdata = self.htmlgetter.get_htmldatadecoded(self.url)
            if self.urlxpath[0:5] == "[xml]":
                self.htmlnewlist = etree.XML(str(getdata))
            elif self.urlxpath[0:6] == "[json]":
                xmlstr = json2xml.json2xml(str(getdata))
                self.htmlnewlist = etree.XML(str(xmlstr))
            elif self.urlxpath[0:7] == "[jsonp]":
                xmlstr = json2xml.jsonp2xml(str(getdata))
                self.htmlnewlist = etree.XML(str(xmlstr))
            elif self.urlxpath[0:7] == "[jsonh]":
                xmlstr = json2xml.json2xml(str(getdata), "del0&1")
                htmlturple = etree.XML(str(xmlstr)).xpath(self.urlxpath[7:])
                if len(htmlturple):
                    self.htmlnewlist = etree.HTML(htmlturple[0])
                else:
                    raise Exception("[Has No Html]")
            else:
                self.htmlnewlist = etree.HTML(getdata)
        except Exception as inst:
            msg = "[Html New List GET ERR]" + str(inst.message)
            raise Exception(msg)
    def _run_crawl(self):
        # 取得规则参数
        if len(self.rulelist) == 1:
            self.submittype, self.titlexpath, self.keywordxpath, self.keywordspliter, self.urlxpath, self.urlattr, self.contentxpath, self.createtimexpath, self.createtimereg, self.picturexpath, self.pictureattr, self.sourcexpath, self.sourcereg, self.authorxpath, self.authorreg, self.isfinished = self.rulelist[0]
            pass
        else:
            raise Exception("No Rule Found")
            pass
        # 取得采集网站信息
        if len(self.weblist) == 1:
            self.webname, self.weblink, self.sectionname, self.indexdiff, self.sectionindexurl, self.start, self.step = self.weblist[0]
            pass
        else:
            raise Exception("No Web Found")
            pass
        # 继续采集标志
        self._crawl_allpages()
    def _get_one_pagelinks(self, url, idx):
        collAll = True
        self.url = url
        self._get_htmlnewlist()             
        # 链接查找
        self._get_links()
        self._get_linkslength()
        # 未找到链接
        if not self._has_links(idx):
            collAll = False
        else:
            # 开始采集这一页
            if self._crawl_pagelinks(int(idx) - int(self.step)):
                collAll = False
            # 每一页暂停，缓解被采集的服务器压力
            time.sleep(self.thread_onepagewait)
        return collAll
    def _crawl_allpages(self):
        collAll = True
        s = self.startpage
        if int(self.start) != 1:
            s = int(self.start)
        self.step = int(self.step) or 1
        self.new = New(self.ms, self.taskid, self.uid, self.webname, self.sectionname, self.rulelist, self.htmlgetter)

        # 首页不同
        if self.indexdiff:
            collAll = self._get_one_pagelinks(self.sectionindexurl, self.start)

        if not collAll:
            pass
            return
        for j in range(s, self.endpage, self.step):
            try:
                # 是否继续采集
                if not collAll:
                    pass
                    return
                # 页面列表的url地址
                collAll = self._get_one_pagelinks(self.pageurlwithparam % j  , j)
            except Exception as inst:
                msg = str(inst.message) + ", [url]" + self.url + ", [new url]" + self.new.url
                self.logger.error(msg)
                if self.stop_when_crawlpageerr:
                    msg = u'task %s err in page %s, link: %s, stop when crawl err,  %s' % (str(self.taskid), str(j), self.url, str(inst.message))
                    self.logger.error(msg)
                    self.ms.ExecNonQuery('''UPDATE [WebRescourseForVipProject].[dbo].[CrawlTask] 
                                   SET [IsStarted] = 0 WHERE [ID] = %s''' % str(self.taskid))
                break
            pass
        pass
    def _crawl_pagelinks(self, page):
        linkslength = self.linkslength
        # 找到链接 开始循环
        try:
            for lidx in range(0, linkslength):
                time.sleep(self.thread_onerecoredwait)
                # 当前任务采集到的条数
                try:
                    link = urlparse.urljoin(self.url, self.links[lidx])
                    self.new.set_url(link)
                    if self.new.get_new_news():
                        if self.new.title == "":
                            continue
                        cretime = time.strptime(self.new.createtime,'%Y-%m-%d %H:%M:%S')
                        if cretime.tm_year >= self.crawltoyear:
                            try:
                                self.new.insert_new()
                                self._set_rule_OK()
                                pass
                            except Exception as inst:
                                msg = "[Insert]%s, %s" % (self.new.url , str(inst.message))
                                self.logger.error(msg)
                                if self.continue_when_inserterr:
                                    continue
                                else:
                                    return False
                        else:
                            return True
                    else:
                        if self.continue_when_recordexist:
                            continue
                        else:
                            return True
                    pass
                except Exception as inst:
                    msg = str(inst.message) + ", [New url]" + link
                    if lidx == 0 and self.isfinished != 2:
                        sql = """UPDATE CrawlRules SET IsFinished = 2 WHERE Id = '%s'""" % self.ruleid
                        self.ms.ExecNonQuery(sql)
                    raise Exception(msg)
            return False
        except Exception as inst:
            msg = str(inst.message)
            raise Exception(msg)
    def _has_links(self, pageidx):
        try:
            if self.linkslength:
                return True
            # 未找到链接 请检查规则 设置规则异常
            if pageidx == self.start:
                msg = u'no links at page 1, link: %s, rule ID: %s' % (self.url, str(self.ruleid))
                self.logger.info(msg)
                #print msg
                if self.isfinished != 2:
                    sql = """UPDATE CrawlRules SET IsFinished = 2 WHERE Id = '%s'""" % self.ruleid
                    self.ms.ExecNonQuery(sql)
            # 非第一页读取0数据
            else:
                self.cf.read("Crawl.conf")
                self.cf.set("crawl", "continue_when_recordexist", 0)
                self.cf.write(open("Crawl.conf", "w"))
            return False
        except Exception as inst:
            msg = "[Links Check]" + str(inst.message)
            raise Exception(msg)
    def _set_rule_OK(self):
        try:
            if self.isfinished != 1:
                #找到链接 并 插入成功 规则可用 设置规则完成
                msg = u'rule OK, ID: ' + str(self.ruleid)
                self.logger.info(msg)
                #print msg
                sql = """UPDATE CrawlRules SET IsFinished = 1 WHERE Id = '%s'""" % str(self.ruleid)
                self.ms.ExecNonQuery(sql)
                self.isfinished = 1
        except Exception as inst:
            msg = "[Rule OK]" + str(inst.message)
            self.logger.error(msg)
    def startTask(self):
        try:
            self._get_rule()
            self._get_weblist()
            self._get_config()
            # 根据规则开始采集
            if len(self.rulelist) == 1 and len(self.weblist) == 1:
                self._run_crawl()
                pass
        except Exception as inst:
            msg = "[StartTask]" + str(inst.message)
            self.logger.error(msg)
        pass
    pass

class Main:
    def __init__(self, conf):
        self.cf = ConfigParser()
        self.conf = (os.path.join(os.getcwd(),conf))
        self._get_logger()
        try:
            self._get_firsttimeparam()
            self.htmlgetter = HtmlGet(self.conf, self.logger, 1)
            self._run()
            pass
        except Exception as inst:
            msg = "[Main]" + str(inst.message)
            self.logger.error(msg)
    def _get_logger(self):
        try:
            self.cf.read(self.conf)
            logging.config.dictConfig({
                'version': 1,
                'disable_existing_loggers': True,
                'formatters': {
                    'verbose': {
                        'format': "##%(asctime)s - %(threadName)s|%(funcName)s|%(lineno)d - %(levelname)s - %(message)s",
                        'datefmt': "%m-%d %H:%M:%S"
                    },
                    'simple': {
                        'format': '%(levelname)s %(message)s'
                    },
                },
                'handlers': {
                    'null': {
                        'level': 'DEBUG',
                        'class': 'logging.NullHandler',
                    },
                    'console': {
                        'level': 'DEBUG',
                        'class': 'logging.StreamHandler',
                        'formatter': 'simple'
                    },
                    'file': {
                            'level': 'DEBUG',
                            # 如果没有使用并发的日志处理类，在多实例的情况下日志会出现缺失
                            #'class':
                            #'cloghandler.ConcurrentRotatingFileHandler',
                            'class': 'logging.handlers.RotatingFileHandler',
                            # 当达到10MB时分割日志
                            'maxBytes': 1024 * 1024 * self.cf.getint("log", "size"),
                            # 最多保留50份文件
                            'backupCount': 50,
                            # If delay is true,
                            # then file opening is deferred until the first
                            # call to emit().
                            'delay': True,
                            'filename': self.cf.get("log", "logfilepath"),
                            'formatter': 'verbose'
                    }
                },
                'loggers': {
                    '': {
                        'handlers': ['file', 'console'],
                        'level': 'DEBUG',
                    },
                }
            })
            self.logger = logging.getLogger(__name__)
        except Exception as inst:
            msg = "[Main Logger]" + str(inst.message)
            self.logger.error(msg)
            pass
    def _start_solr(self):
        # 读取solr配置 启动solr
        if self.cf.getint("solr", "startsolr"):
            os.environ['JAVA_HOME'] = self.cf.get("solr", "javahome")
            command = self.cf.get("solr", "solrstartcmd")
            os.system(command)
    def _get_firsttimeparam(self):
        self.cf.read(self.conf)
        self._start_solr() 
        self.threads = []
        pass
    def _get_tasklist(self):
        # 读取配置：数据库链接
        db_host = self.cf.get("db", "host")
        if db_host == '':
            db_host == '1433'
        db_port = self.cf.get("db", "port")
        db_user = self.cf.get("db", "user")
        db_pass = self.cf.get("db", "pass")
        db_name = self.cf.get("db", "dbname")
        # 数据库链接
        ms = MSSQL(host=db_host,user=db_user,pwd=db_pass,db=db_name,port=db_port)
        # 获取打开的任务列表
        self.tasklist = ms.ExecQuery('''SELECT [Id]
                                        ,[Uid]
                                        ,[RuleId]
                                        ,[SiteSectionId]
                                        ,[PageUrlWithParam]
                                        ,[UseProxy]
                                    FROM [WebRescourseForVipProject].[dbo].[CrawlTask] WHERE [IsStarted] = 1''')
    def _get_threadconf(self):
        # 读取配置：线程最大/探测新任务间隔/每页等待时间
        self.thread_maxThread = self.cf.getint("thread", "maxthread")
        self.thread_checktask = self.cf.getint("thread", "checktask")
        if self.thread_checktask < 1 or self.thread_checktask >= 60:
            self.thread_checktask = 59
    def _create_taskthreads(self):
        '''
        根据任务列表创建所有任务线程，创建时检查是否正在执行，未执行则创建，已执行就跳过
        '''
        # 循环任务 开始采集
        for (TaskId, Uid, RuleId, SiteSectionId, PageUrlWithParam, UseProxy) in self.tasklist:
            try:
                # 创建新线程
                ths = threading.enumerate()
                length = len(ths)
                if(length <= self.thread_maxThread):
                    for i in range(0,length):
                        if ths[i].getName() == "Thread-" + str(TaskId):
                            break
                        if i == length - 1:
                            if(UseProxy):
                                thread2 = myThread(self.conf, "Thread-" + str(TaskId), TaskId, Uid, RuleId, SiteSectionId, PageUrlWithParam, self.logger, self.htmlgetter)
                            else:
                                htmlgetter = HtmlGet(self.conf, self.logger, UseProxy)
                                thread2 = myThread(self.conf, "Thread-" + str(TaskId), TaskId, Uid, RuleId, SiteSectionId, PageUrlWithParam, self.logger, htmlgetter)
                            thread2.start()
                            break
                        time.sleep(1)
                else:
                    msg = u" %s beyound thread limit" % str(TaskId)
                    self.logger.error(msg) 
                    break
            except Exception,e:
                msg = u"[Thread]" + e.message + '\n'
                self.logger.error(msg)
    def _run(self):            
        try:
            os.system("title CRAWLING, Ctrl+C TO EXIT")
            
            # 独立线程： solr导入、数据库去重等
            t = myThread(self.conf, "Thread-solrimport", -1, "", "", "", "", self.logger, self.htmlgetter, True, self.htmlgetter.send_solrdeltaimport)
            t.start()
            self.threads.append(t)

            while True:
                # 读取配置：读取任务执行时间
                self.cf.read(self.conf)
                when_month = self.cf.get("when", "month").split(",")
                when_date = self.cf.get("when", "date").split(",")
                when_day = self.cf.get("when", "day").split(",")
                when_hour = self.cf.get("when", "hour").split(",")
                when_always = self.cf.getint("when", "always")
                # 读取配置：线程最大/探测新任务间隔/每页等待时间
                self._get_threadconf()

                # 判断是否在执行时间内
                localtime = time.localtime(time.time())
                if str(localtime.tm_mon) in when_month or str(localtime.tm_mday) in when_date or str(localtime.tm_wday) in when_day or str(localtime.tm_hour) in when_hour or when_always:
                    # 获取采集 列表
                    self._get_tasklist()
                    self._create_taskthreads()  
                    pass
                time.sleep(60 * self.thread_checktask)
        except KeyboardInterrupt:
            msg = u"exit"
            self.logger.error(msg) 
            print u'exiting: checking threads...'
            for t in self.threads:
                t.join()
            print u'exiting: wait for all sub-threads...'
            time.sleep(3)
            print u'exiting: FINISH.'
            pass
        pass
    pass

if __name__ == '__main__':
    try:
        main = Main("Crawl.conf")
    except Exception as inst:
        msg = "[Main]" + str(inst.message)
        print msg
