#-*- coding:utf-8 -*-
import urllib2
import urllib
#import re
#import requests
#import os
from lxml import etree
#from time import localtime

import sys 
reload(sys) 
sys.setdefaultencoding('utf-8') 

import urllib2
import re
import requests
import os
from lxml import etree
from time import localtime

#这些是要导入的库，代码并没有使用正则，使用的是xpath，正则困难的童鞋可以尝试使用下
#推荐各位先使用基本库来写，这样可以学习到更多

#以当前时间为文件夹
#filegap = 'image' + str(localtime().tm_year) + str(localtime().tm_mon) + str(localtime().tm_mday)
#os.mkdir(filegap)
try:
  os.mkdir('Data')
except:
  pass

domain = 'http://blog.csdn.net/'

print u'请输入最后的页数：'
endPage=int(raw_input())    #最终的页数  (r'\d+(?=\s*页) 这是一个比较通用的正则抓取总页数的代码，当然最后要group

headers = {'User-Agent':'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6'}
req = urllib2.Request(url = 'http://sjk11.e-library.com.cn/kcms/docdown/pubdownload.aspx?dk=U_WEFyWGpWc1VkVGZ1_F_2009116147.nh_P_D_nhdown',headers = headers)
html=urllib2.urlopen(req).read()                #读取首页的内容

#这里是手动输入页数，避免内容太多

for j in range(1,endPage):
    url='http://blog.csdn.net/shenyisyn/article/list/'+str(j)      #页数的url地址
    req = urllib2.Request(url = url,headers = headers)
    html=urllib2.urlopen(req).read()                #读取首页的内容
    #print html
    selector=etree.HTML(html)              #转换为xml，用于在接下来识别
    #print selector
    links=selector.xpath('//span[@class="link_title"]/a/@href')        #抓取当前页面的所有帖子的url 
    #print links
    for l in links:
        url = domain + l
        req = urllib2.Request(url = url,headers = headers)
        html = urllib2.urlopen(req).read()                #读取首页的内容
        html = html.decode('utf-8') 
        selector=etree.HTML(html)              #转换为xml，用于在接下来识别
        title=selector.xpath('//span[@class="link_title"]/a')        #抓取当前页面的title 

        article = ""
        try:
            title = title[0].text.replace('\r\n','').replace(' ', '').replace('"', '').replace("'", '')\
                .replace(",", '').replace("?", '').replace(":", '').replace(";", '').replace("\\", '')\
                .replace("/", '').replace("&", '').replace("*", '').replace("@", '').replace("!", '')\
                .replace("~", '').replace("<", '').replace(">", '').replace("|", '')
        except:
            title = ""
        print title

        article += title

        content = selector.xpath('//div[@class="article_content"]/p | //div[@class="article_content"]/p/strong/span') 
        for c in content:
            if c.text != None:
                article += ('\n' + c.text)
        
        filepath = 'Data\\' + title+'.txt'
        file_object = open(filepath, 'w')
        file_object.write(article)
        file_object.close()

#大家可以使用浏览器自带的源码查看工具，在指定目标处查看元素，这样更快捷

  

print u'下载完成!'