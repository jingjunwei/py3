USE [WebRescourseForVipProject]
GO
/****** Object:  StoredProcedure [dbo].[Page_v1]    Script Date: 08/11/2016 15:25:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[Page_v1]
    @PCount INT OUTPUT ,    --总页数输出
    @RCount INT OUTPUT ,    --总记录数输出
    @sys_Table NVARCHAR(100) ,    --查询表名
    @sys_Key VARCHAR(50) ,        --主键
    @sys_Fields NVARCHAR(500) ,    --查询字段
    @sys_Where NVARCHAR(3000) ,    --查询条件
    @sys_Order NVARCHAR(100) ,    --排序字段
    @sys_Begin INT ,        --开始位置
    @sys_PageIndex INT ,        --当前页数
    @sys_PageSize INT        --页大小
AS 
    SET NOCOUNT ON
    SET ANSI_WARNINGS ON
    IF @sys_PageSize < 0
        OR @sys_PageIndex < 0 
        BEGIN        
            RETURN
        END
    DECLARE @new_where1 NVARCHAR(3000)
    DECLARE @new_order1 NVARCHAR(100)
    DECLARE @new_order2 NVARCHAR(100)
    DECLARE @Sql NVARCHAR(4000)
    DECLARE @SqlCount NVARCHAR(4000)
    DECLARE @Top1 INT
    DECLARE @Top2 INT
    IF ( @sys_Begin <= 0 ) 
        SET @sys_Begin = 0
    ELSE 
        SET @sys_Begin = @sys_Begin - 1
    IF ISNULL(@sys_Where, '') = '' 
        SET @new_where1 = ' '
    ELSE 
        SET @new_where1 = ' WHERE ' + @sys_Where
    IF ISNULL(@sys_Order, '') <> '' 
        BEGIN
            SET @new_order1 = ' ORDER BY ' + REPLACE(@sys_Order, 'desc', '')
            SET @new_order1 = REPLACE(@new_order1, 'asc', 'desc')
            SET @new_order2 = ' ORDER BY ' + @sys_Order
        END
    ELSE 
        BEGIN
            SET @new_order1 = ' ORDER BY ID DESC'
            SET @new_order2 = ' ORDER BY ID ASC'
        END
    SET @SqlCount = 'SELECT @RCount=COUNT(1),@PCount=CEILING((COUNT(1)+0.0)/'
        + CAST(@sys_PageSize AS NVARCHAR) + ') FROM ' + @sys_Table
        + @new_where1
    EXEC SP_EXECUTESQL @SqlCount, N'@RCount INT OUTPUT,@PCount INT OUTPUT',
        @RCount OUTPUT, @PCount OUTPUT
    --IF @sys_PageIndex > CEILING(( @RCount + 0.0 ) / @sys_PageSize)    --如果输入的当前页数大于实际总页数,则把实际总页数赋值给当前页数
    --    BEGIN
    --        SET @sys_PageIndex = CEILING(( @RCount + 0.0 ) / @sys_PageSize)
    --    END
    IF @sys_PageSize * @sys_PageIndex > @RCount
		BEGIN
		    SET @Top1 = (@RCount - @sys_PageSize * (@sys_PageIndex - 1))
		    IF @Top1 < 0
		        SET @Top1 = 0
		END
	ELSE
	    SET @Top1 = @sys_PageSize
	SET @Top2 = @sys_PageSize * @sys_PageIndex + @sys_Begin
    SET @sql = 'select ' + @sys_fields + ' from ' + @sys_Table + ' w1 '
        + ' where ' + @sys_Key + ' in (' + 'select top '
        + LTRIM(STR(@Top1)) + ' ' + @sys_Key + ' from ' + '('
        + 'select top ' + LTRIM(STR(@Top2)) + ' ' + @sys_Key + ' FROM '
        + @sys_Table + @new_where1 + @new_order2 + ') w ' + @new_order1 + ') '
        + @new_order2
    PRINT ( @sql )
    EXEC(@sql)
GO
/****** Object:  StoredProcedure [dbo].[TableSiteTokens]    Script Date: 08/11/2016 15:25:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TableSiteTokens]
        @Id int = 0, 
        @Token nvarchar(50) = null, 
        @SiteName nvarchar(50) = null, 
        @Ip nvarchar(50) = null, 
        @IsEnabled int = 1, 

        @StartIndex int = 0, 
        @PageSize int = 0, 
        @SPReturnType varchar(100) = null, 
        @SPAction varchar(100) = null 
AS
BEGIN
    SET NOCOUNT ON;
    IF @SPAction='Insert'
        BEGIN            
            INSERT INTO [SiteTokens]([Token],[SiteName],[Ip],[IsEnabled]) 
            VALUES(@Token,@SiteName,@Ip,@IsEnabled) 

            Declare @newID varchar(10) 
            Set @newID = SCOPE_IDENTITY() 
            If (@newID is NULL) 
                set @newID = ('-1') 
            select @newID as 'newID'
        END
    IF @SPAction='Delete'
        BEGIN            
            DELETE FROM [SiteTokens] WHERE [Id] = @Id
        END
    IF @SPAction='Update'
        BEGIN            
            UPDATE [SiteTokens] 
            SET [Token] = @Token,[SiteName] = @SiteName,[Ip] = @Ip,[IsEnabled] = @IsEnabled 
            WHERE [Id] = @Id
        END
    IF @SPAction='SelectPaging'
        BEGIN            
            SELECT [Id],[Token],[SiteName],[Ip],[IsEnabled] 
            FROM [SiteTokens]
        END
    IF @SPAction='SelectAll'
        BEGIN            
            SELECT [Id],[Token],[SiteName],[Ip],[IsEnabled] 
            FROM [SiteTokens]
        END
    IF @SPAction='Unique'
        BEGIN            
            Declare @isExist int 
            if @Id = 0 
                Set @isExist = (select count(*) from [SiteTokens] where [Token] = @Token and [SiteName] = @SiteName and [Ip] = @Ip and [IsEnabled] = @IsEnabled) 
            else 
                Set @isExist = (select count(*) from [SiteTokens] where [Token] = @Token and [SiteName] = @SiteName and [Ip] = @Ip and [IsEnabled] = @IsEnabled and [Id] != @Id) 
            if @isExist>0 
                raiserror('data existed already',16,1)
        END
END
GO
/****** Object:  StoredProcedure [dbo].[TableCrawlTask]    Script Date: 08/11/2016 15:25:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TableCrawlTask]
        @Id int = 0, 
        @RuleId int = 0, 
        @SiteSectionId int = null, 
        @PageUrlWithParam nvarchar(2000) = null, 
        @IsStarted int = null, 

        @StartIndex int = 0, 
        @PageSize int = 0, 
        @SPReturnType varchar(100) = null, 
        @SPAction varchar(100) = null 
AS
BEGIN
    SET NOCOUNT ON;
    IF @SPAction='Insert'
        BEGIN            
            INSERT INTO [CrawlTask]([RuleId],[SiteSectionId],[PageUrlWithParam],[IsStarted]) 
            VALUES(@RuleId,@SiteSectionId,@PageUrlWithParam,@IsStarted) 

            Declare @newID varchar(10) 
            Set @newID = SCOPE_IDENTITY() 
            If (@newID is NULL) 
                set @newID = ('-1') 
            select @newID as 'newID'
        END
    IF @SPAction='Delete'
        BEGIN            
            DELETE FROM [CrawlTask] WHERE [Id] = @Id
        END
    IF @SPAction='Update'
        BEGIN            
            UPDATE [CrawlTask] 
            SET [RuleId] = @RuleId,[SiteSectionId] = @SiteSectionId,[PageUrlWithParam] = @PageUrlWithParam,[IsStarted] = @IsStarted 
            WHERE [Id] = @Id
        END
    IF @SPAction='SelectPaging'
        BEGIN            
            SELECT [Id],[RuleId],[SiteSectionId],[PageUrlWithParam],[IsStarted] 
            FROM [CrawlTask]
        END
    IF @SPAction='SelectAll'
        BEGIN            
            SELECT [Id],[RuleId],[SiteSectionId],[PageUrlWithParam],[IsStarted] 
            FROM [CrawlTask]
        END
    IF @SPAction='Unique'
        BEGIN            
            Declare @isExist int 
            if @Id = 0 
                Set @isExist = (select count(*) from [CrawlTask] where [RuleId] = @RuleId and [SiteSectionId] = @SiteSectionId and [PageUrlWithParam] = @PageUrlWithParam and [IsStarted] = @IsStarted) 
            else 
                Set @isExist = (select count(*) from [CrawlTask] where [RuleId] = @RuleId and [SiteSectionId] = @SiteSectionId and [PageUrlWithParam] = @PageUrlWithParam and [IsStarted] = @IsStarted and [Id] != @Id) 
            if @isExist>0 
                raiserror('data existed already',16,1)
        END
END
GO
/****** Object:  StoredProcedure [dbo].[TableCrawlSite]    Script Date: 08/11/2016 15:25:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TableCrawlSite]
        @Id int = 0, 
        @WebName nvarchar(50) = null, 
        @WebLink nvarchar(200) = null, 
        @SectionName nvarchar(50) = null, 
        @SectionIndexUrl nvarchar(200) = null, 
        @SiteFrom nvarchar(50) = null, 
        @DeleteFlag int = null, 

        @StartIndex int = 0, 
        @PageSize int = 0, 
        @SPReturnType varchar(100) = null, 
        @SPAction varchar(100) = null 
AS
BEGIN
    SET NOCOUNT ON;
    IF @SPAction='Insert'
        BEGIN            
            INSERT INTO [CrawlSite]([WebName],[WebLink],[SectionName],[SectionIndexUrl],[SiteFrom],[DeleteFlag]) 
            VALUES(@WebName,@WebLink,@SectionName,@SectionIndexUrl,@SiteFrom,@DeleteFlag) 

            Declare @newID varchar(10) 
            Set @newID = SCOPE_IDENTITY() 
            If (@newID is NULL) 
                set @newID = ('-1') 
            select @newID as 'newID'
        END
    IF @SPAction='Delete'
        BEGIN            
            DELETE FROM [CrawlSite] WHERE [Id] = @Id
        END
    IF @SPAction='Update'
        BEGIN            
            UPDATE [CrawlSite] 
            SET [WebName] = @WebName,[WebLink] = @WebLink,[SectionName] = @SectionName,[SectionIndexUrl] = @SectionIndexUrl,[SiteFrom] = @SiteFrom,[DeleteFlag] = @DeleteFlag 
            WHERE [Id] = @Id
        END
    IF @SPAction='SelectPaging'
        BEGIN            
            SELECT [Id],[WebName],[WebLink],[SectionName],[SectionIndexUrl],[SiteFrom],[DeleteFlag] 
            FROM [CrawlSite]
        END
    IF @SPAction='SelectAll'
        BEGIN            
            SELECT [Id],[WebName],[WebLink],[SectionName],[SectionIndexUrl],[SiteFrom],[DeleteFlag] 
            FROM [CrawlSite]
        END
    IF @SPAction='Unique'
        BEGIN            
            Declare @isExist int 
            if @Id = 0 
                Set @isExist = (select count(*) from [CrawlSite] where [WebName] = @WebName and [WebLink] = @WebLink and [SectionName] = @SectionName and [SectionIndexUrl] = @SectionIndexUrl and [SiteFrom] = @SiteFrom and [DeleteFlag] = @DeleteFlag) 
            else 
                Set @isExist = (select count(*) from [CrawlSite] where [WebName] = @WebName and [WebLink] = @WebLink and [SectionName] = @SectionName and [SectionIndexUrl] = @SectionIndexUrl and [SiteFrom] = @SiteFrom and [DeleteFlag] = @DeleteFlag and [Id] != @Id) 
            if @isExist>0 
                raiserror('data existed already',16,1)
        END
END
GO
/****** Object:  StoredProcedure [dbo].[TableCrawlRules]    Script Date: 08/11/2016 15:25:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TableCrawlRules]
        @Id int = 0, 
        @RuleName nvarchar(50) = null, 
        @IsFinished int = null, 
        @SubmitType nvarchar(10) = null, 
        @TitleXpath nvarchar(200) = null, 
        @UrlXpath nvarchar(200) = null, 
        @UrlAttr nvarchar(200) = null, 
        @ContentXpath nvarchar(200) = null, 
        @CreateTimeXpath nvarchar(200) = null, 
        @CreateTimeReg nvarchar(200) = null, 
        @PictureXpath nvarchar(200) = null, 
        @PictureAttr nvarchar(200) = null, 
        @SourceXpath nvarchar(200) = null, 
        @SourceReg nvarchar(200) = null, 
        @AuthorXpath nvarchar(200) = null, 
        @AuthorReg nvarchar(200) = null, 
        @Version int = 1, 
        @CreateTime datetime = null, 
        @DeleteFlag int = 0, 

        @StartIndex int = 0, 
        @PageSize int = 0, 
        @SPReturnType varchar(100) = null, 
        @SPAction varchar(100) = null 
AS
BEGIN
    SET NOCOUNT ON;
    IF @SPAction='Insert'
        BEGIN            
            INSERT INTO [CrawlRules]([RuleName],[IsFinished],[SubmitType],[TitleXpath],[UrlXpath],[UrlAttr],[ContentXpath],[CreateTimeXpath],[CreateTimeReg],[PictureXpath],[PictureAttr],[SourceXpath],[SourceReg],[AuthorXpath],[AuthorReg],[Version],[CreateTime],[DeleteFlag]) 
            VALUES(@RuleName,@IsFinished,@SubmitType,@TitleXpath,@UrlXpath,@UrlAttr,@ContentXpath,@CreateTimeXpath,@CreateTimeReg,@PictureXpath,@PictureAttr,@SourceXpath,@SourceReg,@AuthorXpath,@AuthorReg,@Version,@CreateTime,@DeleteFlag) 

            Declare @newID varchar(10) 
            Set @newID = SCOPE_IDENTITY() 
            If (@newID is NULL) 
                set @newID = ('-1') 
            select @newID as 'newID'
        END
    IF @SPAction='Delete'
        BEGIN            
            DELETE FROM [CrawlRules] WHERE [Id] = @Id
        END
    IF @SPAction='Update'
        BEGIN            
            UPDATE [CrawlRules] 
            SET [RuleName] = @RuleName,[IsFinished] = @IsFinished,[SubmitType] = @SubmitType,[TitleXpath] = @TitleXpath,[UrlXpath] = @UrlXpath,[UrlAttr] = @UrlAttr,[ContentXpath] = @ContentXpath,[CreateTimeXpath] = @CreateTimeXpath,[CreateTimeReg] = @CreateTimeReg,[PictureXpath] = @PictureXpath,[PictureAttr] = @PictureAttr,[SourceXpath] = @SourceXpath,[SourceReg] = @SourceReg,[AuthorXpath] = @AuthorXpath,[AuthorReg] = @AuthorReg,[Version] = @Version,[CreateTime] = @CreateTime,[DeleteFlag] = @DeleteFlag 
            WHERE [Id] = @Id
        END
    IF @SPAction='SelectPaging'
        BEGIN            
            SELECT [Id],[RuleName],[IsFinished],[SubmitType],[TitleXpath],[UrlXpath],[UrlAttr],[ContentXpath],[CreateTimeXpath],[CreateTimeReg],[PictureXpath],[PictureAttr],[SourceXpath],[SourceReg],[AuthorXpath],[AuthorReg],[Version],[CreateTime],[DeleteFlag] 
            FROM [CrawlRules]
        END
    IF @SPAction='SelectAll'
        BEGIN            
            SELECT [Id],[RuleName],[IsFinished],[SubmitType],[TitleXpath],[UrlXpath],[UrlAttr],[ContentXpath],[CreateTimeXpath],[CreateTimeReg],[PictureXpath],[PictureAttr],[SourceXpath],[SourceReg],[AuthorXpath],[AuthorReg],[Version],[CreateTime],[DeleteFlag] 
            FROM [CrawlRules]
        END
    IF @SPAction='Unique'
        BEGIN            
            Declare @isExist int 
            if @Id = 0 
                Set @isExist = (select count(*) from [CrawlRules] where [RuleName] = @RuleName and [IsFinished] = @IsFinished and [SubmitType] = @SubmitType and [TitleXpath] = @TitleXpath and [UrlXpath] = @UrlXpath and [UrlAttr] = @UrlAttr and [ContentXpath] = @ContentXpath and [CreateTimeXpath] = @CreateTimeXpath and [CreateTimeReg] = @CreateTimeReg and [PictureXpath] = @PictureXpath and [PictureAttr] = @PictureAttr and [SourceXpath] = @SourceXpath and [SourceReg] = @SourceReg and [AuthorXpath] = @AuthorXpath and [AuthorReg] = @AuthorReg and [Version] = @Version and [CreateTime] = @CreateTime and [DeleteFlag] = @DeleteFlag) 
            else 
                Set @isExist = (select count(*) from [CrawlRules] where [RuleName] = @RuleName and [IsFinished] = @IsFinished and [SubmitType] = @SubmitType and [TitleXpath] = @TitleXpath and [UrlXpath] = @UrlXpath and [UrlAttr] = @UrlAttr and [ContentXpath] = @ContentXpath and [CreateTimeXpath] = @CreateTimeXpath and [CreateTimeReg] = @CreateTimeReg and [PictureXpath] = @PictureXpath and [PictureAttr] = @PictureAttr and [SourceXpath] = @SourceXpath and [SourceReg] = @SourceReg and [AuthorXpath] = @AuthorXpath and [AuthorReg] = @AuthorReg and [Version] = @Version and [CreateTime] = @CreateTime and [DeleteFlag] = @DeleteFlag and [Id] != @Id) 
            if @isExist>0 
                raiserror('data existed already',16,1)
        END
END
GO
/****** Object:  StoredProcedure [dbo].[TableCrawlResult]    Script Date: 08/11/2016 15:25:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TableCrawlResult]
        @Id int = 0, 
        @TaskId int = 0, 
        @WebName nvarchar(50) = null, 
        @SectionName nvarchar(50) = null, 
        @Title nvarchar(50) = null, 
        @Url nvarchar(200) = null, 
        @Content nvarchar(max) = null, 
        @CreateTime datetime = null, 
        @Picture nvarchar(200) = null, 
        @Source nvarchar(200) = null, 
        @Author nvarchar(200) = null, 
        @CollectTime datetime = null, 
        @IsCleaned int = null, 

        @StartIndex int = 0, 
        @PageSize int = 0, 
        @SPReturnType varchar(100) = null, 
        @SPAction varchar(100) = null 
AS
BEGIN
    SET NOCOUNT ON;
    IF @SPAction='Insert'
        BEGIN            
            INSERT INTO [CrawlResult]([TaskId],[WebName],[SectionName],[Title],[Url],[Content],[CreateTime],[Picture],[Source],[Author],[CollectTime],[IsCleaned]) 
            VALUES(@TaskId,@WebName,@SectionName,@Title,@Url,@Content,@CreateTime,@Picture,@Source,@Author,@CollectTime,@IsCleaned) 

            Declare @newID varchar(10) 
            Set @newID = SCOPE_IDENTITY() 
            If (@newID is NULL) 
                set @newID = ('-1') 
            select @newID as 'newID'
        END
    IF @SPAction='Delete'
        BEGIN            
            DELETE FROM [CrawlResult] WHERE [Id] = @Id
        END
    IF @SPAction='Update'
        BEGIN            
            UPDATE [CrawlResult] 
            SET [TaskId] = @TaskId,[WebName] = @WebName,[SectionName] = @SectionName,[Title] = @Title,[Url] = @Url,[Content] = @Content,[CreateTime] = @CreateTime,[Picture] = @Picture,[Source] = @Source,[Author] = @Author,[CollectTime] = @CollectTime,[IsCleaned] = @IsCleaned 
            WHERE [Id] = @Id
        END
    IF @SPAction='SelectPaging'
        BEGIN            
            SELECT [Id],[TaskId],[WebName],[SectionName],[Title],[Url],[Content],[CreateTime],[Picture],[Source],[Author],[CollectTime],[IsCleaned] 
            FROM [CrawlResult]
        END
    IF @SPAction='SelectAll'
        BEGIN            
            SELECT [Id],[TaskId],[WebName],[SectionName],[Title],[Url],[Content],[CreateTime],[Picture],[Source],[Author],[CollectTime],[IsCleaned] 
            FROM [CrawlResult]
        END
    IF @SPAction='Unique'
        BEGIN            
            Declare @isExist int 
            if @Id = 0 
                Set @isExist = (select count(*) from [CrawlResult] where [TaskId] = @TaskId and [WebName] = @WebName and [SectionName] = @SectionName and [Title] = @Title and [Url] = @Url and [Content] = @Content and [CreateTime] = @CreateTime and [Picture] = @Picture and [Source] = @Source and [Author] = @Author and [CollectTime] = @CollectTime and [IsCleaned] = @IsCleaned) 
            else 
                Set @isExist = (select count(*) from [CrawlResult] where [TaskId] = @TaskId and [WebName] = @WebName and [SectionName] = @SectionName and [Title] = @Title and [Url] = @Url and [Content] = @Content and [CreateTime] = @CreateTime and [Picture] = @Picture and [Source] = @Source and [Author] = @Author and [CollectTime] = @CollectTime and [IsCleaned] = @IsCleaned and [Id] != @Id) 
            if @isExist>0 
                raiserror('data existed already',16,1)
        END
END
GO
