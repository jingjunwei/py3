#-*- coding:utf-8 -*-
#-------------------------------------------------------------------------------
# Name: XpathCrawlReview.py ver0.95
# Purpose: 数据采集
#
# Author: chenk
#
# Created: 08/02/2016
#-------------------------------------------------------------------------------
# 中文乱码
import sys 
import random
reload(sys) 
sys.setdefaultencoding('utf-8') 

# 数据抓取 分析
import urllib2
from lxml import etree
import requests
import socket
import urlparse
# html编码问题
import decode_html

# 数据处理 文件读取等
import re
import os
from ConfigParser import ConfigParser

# 数据库
import pymssql

# 线程
import time  
import threading

# 日志记录
import logging
import logging.config  

# solr
import solr  

# 代理获取
import getproxy

class MSSQL:
    """
    对pymssql的简单封装
    pymssql库，该库到这里下载：http://www.lfd.uci.edu/~gohlke/pythonlibs/#pymssql
    使用该库时，需要在Sql Server Configuration Manager里面将TCP/IP协议开启

    用法：

    """

    def __init__(self, host, user, pwd, db, port=1433):
        self.host = host
        self.user = user
        self.pwd = pwd
        self.db = db
        self.port = port

    def __GetConnect(self):
        """
        得到连接信息
        返回: conn.cursor()
        """
        if not self.db:
            raise(NameError,"没有设置数据库信息")
        self.conn = pymssql.connect(host=self.host,user=self.user,password=self.pwd,database=self.db,charset="utf8", port = self.port)
        cur = self.conn.cursor()
        if not cur:
            raise(NameError,"连接数据库失败")
        else:
            return cur

    def ExecQuery(self,sql):
        """
        执行查询语句
        返回的是一个包含tuple的list，list的元素是记录行，tuple的元素是每行记录的字段

        调用示例：
                ms = MSSQL(host="localhost",user="sa",pwd="123456",db="PythonWeiboStatistics")
                resList = ms.ExecQuery("SELECT id,NickName FROM WeiBoUser")
                for (id,NickName) in resList:
                    print str(id),NickName
        """
        try:
            cur = self.__GetConnect()
            cur.execute(sql)
            resList = cur.fetchall()
        #查询完毕后必须关闭连接
        finally:
            self.conn.close()
        return resList

    def ExecNonQuery(self,sql):
        """
        执行非查询语句

        调用示例：
            cur = self.__GetConnect()
            cur.execute(sql)
            self.conn.commit()
            self.conn.close()
        """
        try:
            cur = self.__GetConnect()
            cur.execute(sql)
            self.conn.commit()
        finally:
            self.conn.close()
            pass
        pass
    pass

class HtmlGet:
    def __init__(self, conf, logger):
        try:
            self.cf = ConfigParser()
            self.logger = logger
            self.conf = conf
            self.headerlist = [{'User-Agent':'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6'},
                {'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0'},
                {'User-Agent':'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20130331 Firefox/21.0'},
                {'User-Agent':'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0'},
                {'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36'},
                {'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.11 (KHTML, like Gecko) Ubuntu/11.10 Chromium/27.0.1453.93 Chrome/27.0.1453.93 Safari/537.36'},
                {'User-Agent':'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36'},
                {'User-Agent':'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)'},
                {'User-Agent':'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0)'},
                {'User-Agent':'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)'},
                {'User-Agent':'Mozilla/5.0 (compatible; WOW64; MSIE 10.0; Windows NT 6.2)'},
                {'User-Agent':'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6; en-US) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27'},
                {'User-Agent':'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27'},
                {'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_0) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11'},
                {'User-Agent':'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; 360SE)'},
                {'User-Agent':'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; .NET CLR 2.0.50727; .NET CLR 3.0.30729; .NET CLR 3.5.30729; InfoPath.3; rv:11.0) like Gecko'}]
            self._have_proxy = False
            # 读取配置
            pass
        except Exception as inst:
            msg = "HtmlGet init ERR, msg:" + str(inst.args) + str(inst.message)
            self.logger.error(msg)
    def get_config(self):
        self.cf.read(self.conf)
        # 连接超时设置
        defaultsockettime = self.cf.getint("crawl_test", "defaultsockettime")
        socket.setdefaulttimeout(defaultsockettime)
        self.auto_decode = self.cf.getint("crawl_test", "auto_decode")
        self.proxyuse = self.cf.getint("proxy", "useproxy")
        self.crawl_only_proxy_success = self.cf.getint("proxy", "crawl_only_proxy_success")
        self.retry_time = self.cf.getint("proxy", "retry_time")
        self.retry_after = self.cf.getint("proxy", "retry_after")
        self.max_retry_time = self.cf.getint("proxy", "max_retry_time")
        self.retry_flag = 0
        self.solrdeltaurl = self.cf.get("solr", "solrdeltaurl")
        self.solrQry = solr.SolrConnection(self.cf.get("solr", "solrqueryurl"))
        db_host = self.cf.get("db", "host")
        if db_host == '':
            db_host == '1433'
        db_port = self.cf.get("db", "port")
        db_user = self.cf.get("db", "user")
        db_pass = self.cf.get("db", "pass")
        db_name = self.cf.get("db", "dbname")
        # 数据库链接
        self.ms = MSSQL(host=db_host,user=db_user,pwd=db_pass,db=db_name,port=db_port)
        if self.proxyuse and not self._have_proxy:
            self._get_proxyopener()
        pass
    def _get_proxyopener(self):
        try:
            if not self._have_proxy:
                self.proxy = getproxy.getproxy()
                msg = "Proxy Opener Got:" + str(self.proxy) + ",retry time:" + str(self.retry_flag)
                self.logger.info(msg)
            if self.proxy == "":
                self._have_proxy = False
                self.retry_flag += 1
                if self.crawl_only_proxy_success and self.retry_flag < self.max_retry_time:
                    time.sleep(self.retry_after * 60)
                    self._get_proxyopener()
                    return
                elif self.retry_flag > self.retry_time:
                    self.proxyuse = 0
                    msg = "Proxy Got ERR, Retry time: " + str(self.retry_flag)
                    self.logger.info(msg)
                    return
                time.sleep(self.retry_after * 60)
                self._get_proxyopener()
                return
            proxy_handler = urllib2.ProxyHandler(self.proxy)
            opener = urllib2.build_opener(proxy_handler)
            self.opener = opener
            self._have_proxy = True
            pass
        except Exception as inst:
            msg = "Proxy Opener Get ERR, msg:" + str(inst.args) + str(inst.message)
            self.logger.error(msg)
    def get_htmldatadecoded(self, url):
        try:
            headersuse = random.choice(self.headerlist)
            getdata = ""
            # 使用代理
            if self.proxyuse:
                try:
                    getdata = self.opener.open(url).read()
                # 使用代理失败
                except:
                    self._have_proxy = False
                    self._get_proxyopener()  
                    return self.get_htmldatadecoded(url)
            # 不使用代理
            else:
                try:
                    req = urllib2.Request(url = url, headers = headersuse)
                    getdata = urllib2.urlopen(req).read()
                except:
                    return self.get_htmldatadecoded(url)
            # 编码转成Unicode
            if self.auto_decode:
                return decode_html.decode_html(getdata)
            else:
                return getdata
        except Exception as inst:
            msg = "Html Data Decoded ERR, msg:" + str(inst.args) + str(inst.message)
            #self.logger.error(msg)
            raise Exception(msg)
    def check_recordexist(self, title, url):
        try:
            q = 'Title:"' + title + '" and Url:"' + url + '"'
            result = self.solrQry.query(q, rows=1, fields='id')
            if len(result):
                return True
            return False
        except Exception as inst:
            try:
                msg = "Record From Solr GET ERR, msg:" + str(inst.args) + str(inst.message)
                self.logger.error(msg)
                checkList = self.ms.ExecQuery("""SELECT 
                            ID
                    FROM [WebRescourseForVipProject].[dbo].[CrawlResult] WHERE [Title] = '%s' AND [Url] = '%s'""" % (title, url))
                if len(checkList):
                    return True
                return False
            except Exception as inst:
                msg = "Record From SQL GET ERR, msg:" + str(inst.args) + str(inst.message)
                self.logger.error(msg)
                raise Exception(msg)
    def send_solrdeltaimport(self):
        # 插入 solr 增量索引添加
        try:
            headersuse = random.choice(self.headerlist)
            req = urllib2.Request(url = self.solrdeltaurl, headers = headersuse)
            return urllib2.urlopen(req).read()
        except Exception as inst:
            msg = "Send Solr Deltaimport ERR, msg:" + str(inst.args) + str(inst.message)
            self.logger.error(msg)
            raise Exception(msg)
    pass

class New:
    def __init__(self, ms, taskid, webname, sectionname, rule, htmlgetter, url=""):
        try:
            self.rule = rule
            self._setXpath()
            self.ms = ms
            self.taskid = taskid
            self.webname = webname
            self.sectionname = sectionname
            self.collecttime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
            self.htmlgetter = htmlgetter
            self.flag = 0
            self.url = url
        except Exception as inst:
            msg = "New init ERR, msg:" + str(inst.args) + str(inst.message)
            raise Exception(msg)
            pass
    def _getHtmlnews(self):
        try:
            getdata = self.htmlgetter.get_htmldatadecoded(self.url)
            self.htmlnews = etree.HTML(getdata)   
        except Exception as inst:
            msg = "Get Html ERR, msg:" + str(inst.args) + str(inst.message)
            raise Exception(msg)
            pass
    def _setXpath(self):
        try:
            if len(self.rule) == 1:
                self.submittype, self.titlexpath, self.urlxpath, self.urlattr, self.contentxpath, self.createtimexpath, self.createtimereg, self.picturexpath, self.pictureattr, self.sourcexpath, self.sourcereg, self.authorxpath, self.authorreg, self.isfinished = self.rule[0]
            else:
                raise Exception('No Rule Found!') 
        except Exception as inst:
            msg = "Get Xpath ERR, msg:" + str(inst.args) + str(inst.message)
            raise Exception(msg)
            pass
    def _setTitle(self):
        try:
            title = ''
            if self.titlexpath:
                titletemp = self.htmlnews.xpath(self.titlexpath)
                if len(titletemp):
                    title = titletemp[0].text.strip() or titletemp[0].get("title").strip()
            self.title = title
        except Exception as inst:
            msg = "Get Title ERR, msg:" + str(inst.args) + str(inst.message)
            raise Exception(msg)
            pass
    def _setCreatetime(self):
        try:
            createtime = ''
            if self.createtimexpath:
                createtimetemp = self.htmlnews.xpath(self.createtimexpath)
                if len(createtimetemp):
                    createtime = createtimetemp[0].strip()
                    createtimeregtemp = self.createtimereg.replace("yyyy",u"\d{4}").replace("MM",u"\d{1,2}").replace("dd",u"\d{1,2}").replace("HH",u"\d{1,2}").replace("mm",u"\d{1,2}").replace("ss",u"\d{1,2}")
                    match = re.search(createtimeregtemp, createtime)
                    if match:
                        createtime = match.group()
                        self.createtimeregtemp2 = self.createtimereg.replace("yyyy",u"%Y").replace("MM",u"%m").replace("dd",u"%d").replace("HH",u"%H").replace("mm",u"%M").replace("ss",u"%S")
                        createtime = time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(createtime,self.createtimeregtemp2))
            else:
                createtime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
                pass
            self.createtime = createtime
        except Exception as inst:
            msg = "Get Createtime ERR, msg:" + str(inst.args) + str(inst.message)
            raise Exception(msg)
            pass
    def _setPicture(self):
        try:
            picture = ''
            if self.picturexpath:
                pic = self.htmlnews.xpath(self.picturexpath)
                coverflag = 0
                for img in pic:
                    picturetemp = img.get(self.pictureattr) or img.get("src")
                    picturetemp = urlparse.urljoin(self.url, picturetemp)
                    img.set("src", picturetemp)
                    if coverflag == 0:
                        picture = picturetemp
                    coverflag += 1
            if re.match(r'^https?:/{2}\w.+$', picture): 
                pass
            else:
                picture = ""
            self.picture = picture
        except Exception as inst:
            msg = "Get Picture ERR, msg:" + str(inst.args) + str(inst.message)
            raise Exception(msg)
    def _setContent(self):
        try:
            contents = ''
            if self.contentxpath:
                content = self.htmlnews.xpath(self.contentxpath)
                if len(content):
                    if len(content) > 1:
                        ctemp = content
                    else:
                        ctemp = content[0]
                    for c in ctemp:
                        if c.tag not in ["meta", "script"]:
                            contents = contents + etree.tostring(c)
            self.contents = contents
        except Exception as inst:
            msg = "Get Content ERR, msg:" + str(inst.args) + str(inst.message)
            raise Exception(msg)
    def _setSource(self):
        try:
            source = ''
            if self.sourcexpath:
                sourcetemp = self.htmlnews.xpath(self.sourcexpath)
                if len(sourcetemp):
                    source = sourcetemp[0].strip()
                    if len(source) and self.sourcereg:
                        self.sourcereg = self.sourcereg.replace("%s",u'([a-zA-Z0-9\u4e00-\u9fff]+)')
                        source = decode_html.findFirstNotEmptyStr(self.sourcereg, source) or source
            self.source = source
        except Exception as inst:
            msg = "Get Source ERR, msg:" + str(inst.args) + str(inst.message)
            raise Exception(msg)
    def _setAuthor(self):
        try:
            author = ''
            if self.authorxpath:
                autemp = self.htmlnews.xpath(self.authorxpath)
                if len(autemp):
                    author = autemp[0].strip()
                    if len(author) and self.authorreg:
                        self.authorreg = self.authorreg.replace("%s",u'([a-zA-Z0-9\u4e00-\u9fff、 ]+)')
                        author = decode_html.findFirstNotEmptyStr(self.authorreg, author) or author
            self.author = author
        except Exception as inst:
            msg = "Get Author ERR, msg:" + str(inst.args) + str(inst.message)
            raise Exception(msg)
    def set_url(self, url):
        self.url = url
        pass
    def insert_new(self):
        '''
        插入一条采集的信息
        '''
        try:
            self.contents = self.contents.replace("'", "''")
            self.title = self.title.replace("'", "''")
            sql = '''INSERT INTO [WebRescourseForVipProject].[dbo].[CrawlResult](
                                [taskid]
                                ,[webname]
                                ,[sectionname]
                                ,[Title]
                                ,[Url]
                                ,[Content]
                                ,[CreateTime]
                                ,[Picture]
                                ,[Source]
                                ,[Author]
                                ,[CollectTime]
                                ,[IsCleaned]) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')
                    ''' % (self.taskid, 
                           self.webname, 
                           self.sectionname, 
                           self.title, 
                           self.url, 
                           self.contents,
                           self.createtime, 
                           self.picture, 
                           self.source,
                           self.author,
                           self.collecttime,
                           self.flag)
            self.ms.ExecNonQuery(sql)
        except Exception as inst:
            msg = "Insert SQL ERR, msg:" + str(inst.args) + str(inst.message)
            raise Exception(msg)
    def get_attrs_no_exist(self):
        try:
            self._getHtmlnews()
            self._setTitle()
            self._setCreatetime()
            self._setPicture()
            self._setContent()
            self._setSource()
            self._setAuthor()
            self.collecttime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
            return True
        except Exception as inst:
            msg = "Get new check exist ERR, msg:" + str(inst.args) + str(inst.message)
            raise Exception(msg)

class Crawl:
    def __init__(self, conf, ms, taskid, ruleid, sitesectionid, logger, htmlgetter, link=""):
        try:
            self.cf = ConfigParser()
            self.conf = conf
            self.ms = ms
            self.taskid = taskid
            self.sitesectionid = sitesectionid
            self.ruleid = ruleid
            self.logger = logger
            self.htmlgetter = htmlgetter
            self.link = link
            self.startTask()
        except Exception as inst:
            msg = "Crawl init ERR, msg:" + str(inst.args) + str(inst.message)
            self.logger.error(msg)
        pass
    def _get_rule(self):
        # 读取规则
        try:
            self.rulelist = self.ms.ExecQuery("""SELECT 
                                       [SubmitType]
                                      ,[TitleXpath]
                                      ,[UrlXpath]
                                      ,[UrlAttr]
                                      ,[ContentXpath]
                                      ,[CreateTimeXpath]
                                      ,[CreateTimeReg]
                                      ,[PictureXpath]
                                      ,[PictureAttr]
                                      ,[SourceXpath]
                                      ,[SourceReg]
                                      ,[AuthorXpath]
                                      ,[AuthorReg]
                                      ,[IsFinished]
                                FROM [WebRescourseForVipProject].[dbo].[CrawlRules] WHERE [Id] = '%s' AND [DeleteFlag] = 0""" % str(self.ruleid))
        except Exception as inst:
            msg = "Rule GET ERR, msg:" + str(inst.args) + str(inst.message)
            raise Exception(msg)
    def _get_weblist(self):
        # 读取被采集网站信息
        try:
            self.weblist = self.ms.ExecQuery("""SELECT 
                                       [WebName]
                                      ,[WebLink]
                                      ,[SectionName]
                                FROM [WebRescourseForVipProject].[dbo].[CrawlSite] WHERE [Id] = '%s' AND [DeleteFlag] = 0""" % str(self.sitesectionid))
        except Exception as inst:
            msg = "Weblist GET ERR, msg:" + str(inst.args) + str(inst.message)
            raise Exception(msg)
    def _get_htmlnewlist(self):
        try:
            getdata = self.htmlgetter.get_htmldatadecoded(self.url)
            self.htmlnewlist = etree.HTML(getdata)
        except Exception as inst:
            msg = "Html New List GET ERR, msg:" + str(inst.args) + str(inst.message)
            raise Exception(msg)
    def _run_crawl(self):
        # 取得规则参数
        if len(self.rulelist) == 1:
            self.submittype, self.titlexpath, self.urlxpath, self.urlattr, self.contentxpath, self.createtimexpath, self.createtimereg, self.picturexpath, self.pictureattr, self.sourcexpath, self.sourcereg, self.authorxpath, self.authorreg, self.isfinished = self.rulelist[0]
            pass
        else:
            raise Exception("No Rule Found")
            pass
        # 取得采集网站信息
        if len(self.weblist) == 1:
            self.webname, self.weblink, self.sectionname = self.weblist[0]
            pass
        else:
            raise Exception("No Web Found")
            pass
        # 继续采集标志
        self._crawl_link()
    def _crawl_link(self):
        try:
            new = New(self.ms, self.taskid, self.webname, self.sectionname, self.rulelist, self.htmlgetter, self.link)
            try:
                if new.get_attrs_no_exist():
                    cretime = time.strptime(new.createtime,'%Y-%m-%d %H:%M:%S')
                    self.logger.info(u"========== 任务测试结果: %s ==========" % new.collecttime)
                    self.logger.info(u"任务ＩＤ: " + str(new.taskid))
                    self.logger.info(u"网站名称: " + new.webname)
                    self.logger.info(u"模块名称: " + new.sectionname)
                    self.logger.info(u"测试链接: " + new.url)
                    self.logger.info(u"标　　题: " + new.title)
                    self.logger.info(u"发布时间: " + new.createtime)
                    self.logger.info(u"来　　源: " + new.source)
                    self.logger.info(u"作　　者: " + new.author)
                    self.logger.info(u"封　　面: " + new.picture)
                    self.logger.info(u"内　　容: " + new.contents)
                    msg = "New Got, task: %d, link: %s, PLS Check" % (self.taskid, self.link)
                    self.logger.info(msg)
                    self.logger.info(u"==================================================")
            except Exception as inst:
                msg = "Crawl Page Links ERR, msg:" + str(inst.args) + str(inst.message)
                if self.isfinished != 2:
                    sql = """UPDATE CrawlRules SET IsFinished = 2 WHERE Id = '%s'""" % self.ruleid
                    self.ms.ExecNonQuery(sql)
                raise Exception(msg)
        except Exception as inst:
            msg = "Main ERR, msg:" + str(inst.args) + str(inst.message)
            raise Exception(msg)
    def startTask(self):
        try:
            self._get_rule()
            self._get_weblist()
            # 根据规则开始采集
            if len(self.rulelist) == 1 and len(self.weblist) == 1:
                self._run_crawl()
                pass
        except Exception as inst:
            msg = "StartTask ERR, msg:" + str(inst.args) + str(inst.message)
            self.logger.error(msg)
        pass
    pass

class Main:
    def __init__(self, conffilename, taskid, testurl, logfilepath):
        self.cf = ConfigParser()
        self.conf = conffilename
        self.cf.read(self.conf)
        if taskid == 0:
            self.taskid = self.cf.getint("crawl_test", "task_id")
        else:
            self.taskid = taskid
        if testurl == "":
            self.testurl = self.cf.get("crawl_test", "test_url")
        else:
            self.testurl = testurl
        if logfilepath == "":
            self.logfilepath = self.cf.get("log", "logfilepath")
        else:
            self.logfilepath = logfilepath
        self._get_logger()
        try:
            if self.cf.getint("solr", "runsolr"):
                self._run_solr()
            self.htmlgetter = HtmlGet(self.conf, self.logger)
            self._run()
            pass
        except Exception as inst:
            msg = "Main ERR, msg:" + str(inst.args) + str(inst.message)
            self.logger.error(msg)
    def _get_logger(self):
        try:
            self.cf.read(self.conf)
            logging.config.dictConfig({
                'version': 1,
                'disable_existing_loggers': True,
                'formatters': {
                    'verbose': {
                        'format': "##%(asctime)s - %(threadName)s|%(funcName)s|%(lineno)d - %(levelname)s - %(message)s",
                        'datefmt': "%m-%d %H:%M:%S"
                    },
                    'simple': {
                        'format': '##%(asctime)s - %(message)s'
                    },
                },
                'handlers': {
                    'null': {
                        'level': 'DEBUG',
                        'class': 'logging.NullHandler',
                    },
                    'console': {
                        'level': 'DEBUG',
                        'class': 'logging.StreamHandler',
                        'formatter': 'simple'
                    },
                    'file': {
                            'level': 'DEBUG',
                            # 如果没有使用并发的日志处理类，在多实例的情况下日志会出现缺失
                            #'class':
                            #'cloghandler.ConcurrentRotatingFileHandler',
                            'class': 'logging.handlers.RotatingFileHandler',
                            # 当达到10MB时分割日志
                            'maxBytes': 1024 * self.cf.getint("log", "size"),
                            # 最多保留50份文件
                            'backupCount': 50,
                            # If delay is true,
                            # then file opening is deferred until the first
                            # call to emit().
                            'delay': True,
                            'filename': self.logfilepath,
                            'formatter': 'simple'
                    }
                },
                'loggers': {
                    '': {
                        'handlers': ['file'],
                        'level': 'DEBUG',
                    },
                }
            })
            self.logger = logging.getLogger(__name__)
        except Exception, ex:
            msg = ex.message + str(ex)
            self.logger.error(msg)
            pass
    def _run_solr(self):
        # 读取solr配置 启动solr
        os.environ['JAVA_HOME'] = self.cf.get("solr", "javahome")
        command = self.cf.get("solr", "solrstartcmd")
        os.system(command) 
        pass
    def _get_task(self):
        # 读取配置：数据库链接
        db_host = self.cf.get("db", "host")
        if db_host == '':
            db_host == '1433'
        db_port = self.cf.get("db", "port")
        db_user = self.cf.get("db", "user")
        db_pass = self.cf.get("db", "pass")
        db_name = self.cf.get("db", "dbname")
        # 数据库链接
        self.ms = MSSQL(host=db_host,user=db_user,pwd=db_pass,db=db_name,port=db_port)
        # 获取打开的任务列表
        self.tasklist = self.ms.ExecQuery('''SELECT [Id]
                                        ,[RuleId]
                                        ,[SiteSectionId]
                                        ,[PageUrlWithParam]
                                    FROM [WebRescourseForVipProject].[dbo].[CrawlTask] WHERE [IsStarted] = 1 AND [Id] = %d''' % self.taskid)
    def _create_task(self):
        '''
        根据任务列表创建所有任务线程，创建时检查是否正在执行，未执行则创建，已执行就跳过
        '''
        for (TaskId, RuleId, SiteSectionId, PageUrlWithParam) in self.tasklist:
            try:
                crawl = Crawl(self.conf, self.ms, TaskId, RuleId, SiteSectionId, self.logger, self.htmlgetter, self.testurl)
            except Exception,e:
                msg = u"crawl err：" + e.message + '\n'
                self.logger.error(msg)
    def _run(self):
        try:
            os.system("title CRAWLING, Ctrl+C TO EXIT")
            self._get_task()
            # 获取采集数据配置等
            self.htmlgetter.get_config()
            # 循环任务 开始采集
            self._create_task()  
        except Exception as inst:
            msg = "Main ERR, msg:" + str(inst.args) + str(inst.message)
            print msg
        pass
    pass

if __name__ == '__main__':
    try:
        if len(sys.argv) != 4:
            print u"请提供三个参数:第一个为任务id, 第二个为测试链接, 第三个为存储文件."
        else:
            id = int(sys.argv[1])
            link = sys.argv[2]
            filepath = sys.argv[3]
            print id, link, filepath
            main = Main("Crawl_test.conf", id, link, filepath)
    except Exception as inst:
        msg = "Main ERR, msg:" + str(inst.args) + str(inst.message)
        print msg
