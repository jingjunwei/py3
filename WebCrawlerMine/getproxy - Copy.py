#encoding=utf8
import urllib2
import urllib
from bs4 import BeautifulSoup
urllib.socket.setdefaulttimeout(3)
# 中文乱码
import sys 
reload(sys) 
sys.setdefaultencoding('utf-8') 
  
def getproxy(proxyfilename):
    User_Agent = 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0'
    header = {}
    header['User-Agent'] = User_Agent

    url = 'http://www.xicidaili.com/nn/1'
    req = urllib2.Request(url,headers=header)
    res = urllib2.urlopen(req).read()

    soup = BeautifulSoup(res, 'lxml')
    ips = soup.findAll('tr')
    f = open(proxyfilename,"w")

    testurl = "http://ip.chinaz.com/getip.aspx"

    for x in range(1,len(ips)):
        ip = ips[x]
        tds = ip.findAll("td")
        ip_temp = tds[1].contents[0]+"\t"+tds[2].contents[0]+"\n"

        proxy = "http://%s:%s" % (tds[1].contents[0], tds[2].contents[0])
        proxy_temp = {"http":proxy}
        
        try:
            res = urllib.urlopen(testurl,proxies=proxy_temp).read()
            f.write(ip_temp)
            #print res
        except Exception,e:
            #print proxy
            #print e
            continue
    f.close()

if __name__ == '__main__':
    proxyfilename = 'proxy'
    getproxy(proxyfilename)