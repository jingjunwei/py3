#-*- coding:utf-8 -*-
import json
import xmltodict
import re
#import os

def replaceNumTagInXmlS(x):
    return '<_' + x.group('value') + '>'

def replaceNumTagInXmlE(x):
    return '</_' + x.group('value') + '>'

def replaceSymTagInXmlS(x):
    return '<_' + x.group('value') + '>'
def replaceSymTagInXmlE(x):
    return '</_' + x.group('value') + '>'


def replaceErrSymInKeyName(jdic):
    xmlstr = re.sub(r'<(?P<value>[0-9]{1,50})>', replaceNumTagInXmlS, xmltodict.unparse({'root':jdic}, full_document=False))
    xmlstr = re.sub(r'<\/(?P<value>[0-9]{1,50})>', replaceSymTagInXmlE, xmlstr)
    return xmlstr

def json2xml(jsonStr, delnouse = ""):
    jdic = json.loads(jsonStr)
    if len(jdic) and delnouse == "del0&1":
        del jdic[1]
        del jdic[0]
    return replaceErrSymInKeyName(jdic)

def jsonp2xml(jsonStr):
    jsonStr = jsonStr.replace("jQuery(","").replace(");","")
    return json2xml(jsonStr)


if __name__ == "__main__":
    #os.system("chcp 936")
    jsonStr = '{"9":"9"}'
    print json2xml(jsonStr)