#-*- coding:utf-8 -*-
#-------------------------------------------------------------------------------
# Name: Crawl.py ver0.9
# Purpose: 数据采集
#
# Author: chenk
#
# Created: 08/02/2016
#-------------------------------------------------------------------------------
# 数据抓取 分析
import urllib2
import urllib
from bs4 import BeautifulSoup
import requests
import socket
#socket.setdefaulttimeout(10)
import urlparse
# html编码问题
import decode_html

# 数据处理 文件读取等
import re
import os
from ConfigParser import ConfigParser
# 数据库
import pymssql

# 线程
import time  
import threading

# 保存图片用
import hashlib
# 日志记录
import logging  
import logging.handlers  

# 测试代理用
import socket

# 中文乱码
import sys 
reload(sys) 
sys.setdefaultencoding('utf-8') 

# solr
import solr  
s = solr.SolrConnection('http://127.0.0.1:8983/solr/crawl')

# 代理获取
import getproxy
# 代理
proxyuse = 0
# 等待
thread_onepagewait = 0
thread_onerecoredwait = 0
stopwhencantcrawl = 1
# 模拟浏览器请求
headers = [{'User-Agent':'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6'},
    {'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0'},
    {'User-Agent':'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20130331 Firefox/21.0'},
    {'User-Agent':'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0'},
    {'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36'},
    {'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.11 (KHTML, like Gecko) Ubuntu/11.10 Chromium/27.0.1453.93 Chrome/27.0.1453.93 Safari/537.36'},
    {'User-Agent':'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36'},
    {'User-Agent':'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)'},
    {'User-Agent':'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0)'},
    {'User-Agent':'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)'},
    {'User-Agent':'Mozilla/5.0 (compatible; WOW64; MSIE 10.0; Windows NT 6.2)'},
    {'User-Agent':'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6; en-US) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27'},
    {'User-Agent':'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27'},
    {'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_0) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11'},
    {'User-Agent':'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; 360SE)'},
    {'User-Agent':'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; .NET CLR 2.0.50727; .NET CLR 3.0.30729; .NET CLR 3.5.30729; InfoPath.3; rv:11.0) like Gecko'}]
headerslen = len(headers)

try:
    # 创建一个logger
    logger = logging.getLogger('main')  
    logger.setLevel(logging.DEBUG)  
  
    # 创建一个handler，用于写入日志文件
    fh = logging.FileHandler('crawl.log')  
    fh.setLevel(logging.DEBUG)
  
    # 再创建一个handler，用于输出到控制台
    ch = logging.StreamHandler()  
    ch.setLevel(logging.DEBUG)  
  
    # 定义handler的输出格式
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')  
    fh.setFormatter(formatter)  
    ch.setFormatter(formatter)  
  
    # 给logger添加handler
    logger.addHandler(fh)  
    logger.addHandler(ch)  
except Exception, ex:
    print ex.message

class MSSQL:
    """
    对pymssql的简单封装
    pymssql库，该库到这里下载：http://www.lfd.uci.edu/~gohlke/pythonlibs/#pymssql
    使用该库时，需要在Sql Server Configuration Manager里面将TCP/IP协议开启

    用法：

    """

    def __init__(self, host, user, pwd, db, port=1433):
        self.host = host
        self.user = user
        self.pwd = pwd
        self.db = db
        self.port = port

    def __GetConnect(self):
        """
        得到连接信息
        返回: conn.cursor()
        """
        if not self.db:
            raise(NameError,"没有设置数据库信息")
        self.conn = pymssql.connect(host=self.host,user=self.user,password=self.pwd,database=self.db,charset="utf8", port = self.port)
        cur = self.conn.cursor()
        if not cur:
            raise(NameError,"连接数据库失败")
        else:
            return cur

    def ExecQuery(self,sql):
        """
        执行查询语句
        返回的是一个包含tuple的list，list的元素是记录行，tuple的元素是每行记录的字段

        调用示例：
                ms = MSSQL(host="localhost",user="sa",pwd="123456",db="PythonWeiboStatistics")
                resList = ms.ExecQuery("SELECT id,NickName FROM WeiBoUser")
                for (id,NickName) in resList:
                    print str(id),NickName
        """
        try:
            cur = self.__GetConnect()
            cur.execute(sql)
            resList = cur.fetchall()
        #查询完毕后必须关闭连接
        finally:
            self.conn.close()
        return resList

    def ExecNonQuery(self,sql):
        """
        执行非查询语句

        调用示例：
            cur = self.__GetConnect()
            cur.execute(sql)
            self.conn.commit()
            self.conn.close()
        """
        try:
            cur = self.__GetConnect()
            cur.execute(sql)
            self.conn.commit()
        finally:
            self.conn.close()

class myThread(threading.Thread):   #继承父类threading.Thread
    '''
    线程
    '''
    def __init__(self, threadID, name, taskId, ruleId, siteSectionId, pageUrlWithParam,isfirsttime):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.headers = headers
        self.taskId = taskId
        self.ruleId = ruleId
        self.siteSectionId = siteSectionId
        self.pageUrlWithParam = pageUrlWithParam
        self.isfirsttime = isfirsttime
    def run(self):                   #把要执行的代码写到run函数里面 线程在创建后会直接运行run函数
        ms = MSSQL(host=db_host,user=db_user,pwd=db_pass,db=db_name,port=db_port)
        startTask(ms, self.taskId, self.ruleId, self.siteSectionId, self.pageUrlWithParam, self.isfirsttime)

def insertOneNew(ms, TaskId, WebName, SectionName, title, url, contents, createtime, picture, source, author):
    '''
    插入一条采集的信息
    '''
    try:
        contents = contents.replace("'", "''")
        title = title.replace("'", "''")
        sql = '''INSERT INTO [WebRescourseForVipProject].[dbo].[CrawlResult](
                            [TaskId]
                            ,[WebName]
                            ,[SectionName]
                            ,[Title]
                            ,[Url]
                            ,[Content]
                            ,[CreateTime]
                            ,[Picture]
                            ,[Source]
                            ,[Author]
                            ,[CollectTime]
                            ,[IsCleaned]) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')
                ''' % (TaskId, WebName, SectionName, title, url, contents, createtime, picture, source, author, time.strftime('%Y-%m-%d %H:%M:%S', time.localtime()), 0)
        ms.ExecNonQuery(sql)
        return True
    except Exception, e:
        msg = u'insert err:%s, %s' % (url , e.message)
        logger.error(msg)  
        return True

#def makedirectory(filegap):
#    try:
#        os.mkdir(filegap)
#    except Exception, e:
#        msg = u'create dir err:%s' % e.message
#        logger.info(msg)
def CrawlContents(ms, TaskId, PageUrlWithParam, endPage, ruleList, webList, ruleId, isfirsttime):
    '''
    批量采集信息
    '''
    global proxyuse, thread_onepagewait, headers, headerslen, thread_onerecoredwait,stopwhencantcrawl,solrdeltaurl
    # 取得规则参数
    for (SubmitType, TitleXpath, UrlXpath, UrlAttr, ContentXpath, CreateTimeXpath, CreateTimeReg, PictureXpath, PictureAttr, SourceXpath, SourceReg, AuthorXpath, AuthorReg, IsFinished) in ruleList:
        pass
    # 取得采集网站信息
    for (WebName, WebLink, SectionName) in webList:
        pass
    # 主站链接
    domain = WebLink
    # 继续采集标志
    collAll = True

    for j in range(1, endPage):
        url = ""
        try:
            # 是否继续采集
            if not collAll:
                break
            # 页面列表的url地址
            url = PageUrlWithParam % j  
            
            # 数据请求
            getdata = ""
            if proxyuse:
                try:
                    proxy = getproxy.getproxy()
                    proxy_handler = urllib2.ProxyHandler(proxy)
                    opener = urllib2.build_opener(proxy_handler)  
                    f = opener.open(url)  
                    getdata = f.read()
                except:
                    req = urllib2.Request(url = url,headers = headers[j % headerslen])
                    getdata = urllib2.urlopen(req).read()
            else:
                req = urllib2.Request(url = url,headers = headers[j % headerslen])
                getdata = urllib2.urlopen(req).read()
            # 编码转成Unicode
            getdata = decode_html.decode_html(getdata)
            # 装载数据
            html = BeautifulSoup(getdata, "lxml")                
            # 链接查找
            links = html.select(UrlXpath)
            # 未找到链接
            linkslength = len(links)
            if not linkslength:
                if j == 1:
                    # 未找到链接 请检查规则 设置规则异常
                    msg = u'no links at page 1, link: %s, rule ID: %s' % (url, str(ruleId))
                    logger.info(msg)
                    #print msg
                    if IsFinished != 2:
                        sql = """UPDATE CrawlRules SET IsFinished = 2 WHERE Id = '%s'""" % ruleId
                        ms.ExecNonQuery(sql)
                else:
                    # 非第一页读取0数据
                    cf = ConfigParser()
                    cf.read("Crawl.conf")
                    cf.set("crawl", "isfirsttime", 0)
                    cf.write(open("Crawl.conf", "w"))
                break

            if j == 1 and IsFinished != 1:
                #找到链接 规则可用 设置规则完成
                msg = u'rule OK, ID: ' + str(ruleId)
                logger.info(msg)
                #print msg
                sql = """UPDATE CrawlRules SET IsFinished = 1 WHERE Id = '%s'""" % str(ruleId)
                ms.ExecNonQuery(sql)

            # 找到链接 开始循环
            for lidx in range(0, linkslength):
                # 当前人物采集到的条数
                taskidx = j * linkslength + lidx
                url = ""
                try:
                    link = links[lidx][UrlAttr]
                    url = urlparse.urljoin(domain, link)
                    # 数据请求
                    #req = urllib2.Request(url = url,headers = headers)
                    # 数据请求
                    getdata = ""
                    if proxyuse:
                        try:
                            proxy_handler = urllib2.ProxyHandler(proxy)
                            opener = urllib2.build_opener(proxy_handler)  
                            f = opener.open(url)  
                            getdata = f.read()
                        except:
                            req = urllib2.Request(url = url,headers = headers[taskidx % headerslen])
                            getdata = urllib2.urlopen(req).read()
                    else:
                        req = urllib2.Request(url = url,headers = headers[taskidx % headerslen])
                        getdata = urllib2.urlopen(req).read()
                    # 编码转成Unicode
                    getdata = decode_html.decode_html(getdata)
                    # 装载数据
                    htmlnews = BeautifulSoup(getdata, "lxml")
                    # 标题
                    title = ''
                    titletemp = htmlnews.select(TitleXpath)
                    if len(titletemp):
                        title = titletemp[0].get_text()
                
                    #checkList = ms.ExecQuery("""SELECT 
                    #             ID
                    #        FROM [WebRescourseForVipProject].[dbo].[CrawlResult] WHERE [Title] = '%s' AND [Url] = '%s'""" % (title, url))
                    #if len(checkList):

                    q = 'Title:"' + title + '" and Url:"' + url + '"'
                    result = s.query(q, rows=2, fields='id')
                    if len(result):
                        if isfirsttime:
                            time.sleep(thread_onerecoredwait)
                            collAll = True
                            continue
                        else:
                            msg = u'task %s GOT, STOP' % str(TaskId)
                            logger.info(msg)
                            time.sleep(thread_onerecoredwait)
                            collAll = False
                            break

                    # 发布时间
                    createtime = ''
                    createtimetemp = htmlnews.select(CreateTimeXpath)
                    if len(createtimetemp):
                        createtime = createtimetemp[0].get_text().strip()
                        CreateTimeRegtemp = CreateTimeReg.replace("yyyy",u"\d{4}").replace("MM",u"\d{2}").replace("dd",u"\d{2}").replace("HH",u"\d{2}").replace("mm",u"\d{2}").replace("ss",u"\d{2}")
                        match = re.search(CreateTimeRegtemp, createtime)
                        if match:
                            createtime = match.group()
                            CreateTimeRegtemp2 = CreateTimeReg.replace("yyyy",u"%Y").replace("MM",u"%m").replace("dd",u"%d").replace("HH",u"%H").replace("mm",u"%M").replace("ss",u"%S")
                            createtime = time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(createtime,CreateTimeRegtemp2))
                            
                    # 文章图片路径处理
                    picture = ''
                    pic = htmlnews.select(PictureXpath)
                    coverflag = 0
                    for img in pic:
                        try:
                            picturetemp = img[PictureAttr]
                        except:
                            picturetemp = img["src"]
                        picturetemp = urlparse.urljoin(domain, picturetemp)
                        img["src"] = picturetemp
                        if coverflag == 0:
                            picture = picturetemp
                        coverflag += 1

                    # 文章内容
                    content = htmlnews.select(ContentXpath)[0].contents
                    contents = ''
                    for c in content:
                        # 过滤掉 回车符号 script
                        if str(c) != '\n':
                            if c.name not in ["meta", "script"]:
                                contents += decode_html.replaceCharEntity(str(c))

                    # 文章来源
                    source = ''
                    sourcetemp = htmlnews.select(SourceXpath)
                    if len(sourcetemp):
                        source = sourcetemp[0].get_text().strip()
                        if len(source) and SourceReg:
                            SourceReg = SourceReg.replace("%s",u'([a-zA-Z0-9\u4e00-\u9fff]+)')
                            source = decode_html.findFirstNotEmptyStr(SourceReg, source)

                    # 文章作者
                    author = ''
                    autemp = htmlnews.select(AuthorXpath)
                    if len(autemp):
                        author = autemp[0].get_text().strip()
                        if len(author) and AuthorReg:
                            AuthorReg = AuthorReg.replace("%s",u'([a-zA-Z0-9\u4e00-\u9fff、 ]+)')
                            author = decode_html.findFirstNotEmptyStr(AuthorReg, author)                   
                    
                    insertOneNew(ms, TaskId, WebName, SectionName, title, url, contents, createtime, picture, source, author)

                except Exception, ex:
                    msg = u'task %s err in record %s page %s, url: %s, stop' % (str(TaskId), str(lidx), str(j), url)
                    logger.error(msg)
                    continue
                finally:
                    time.sleep(thread_onerecoredwait)

                # solr 增量索引添加
                req = urllib2.Request(url = solrdeltaurl, headers = headers[1])
                getdata = urllib2.urlopen(req).read()

            # 每一页暂停，缓解被采集的服务器压力
            time.sleep(thread_onepagewait)
        except Exception, ex:
            if j == 1:
                msg = u'task %s err in page %s, stop, pls check it if sth wrong, link: %s' % (str(TaskId), str(j), url)
                logger.error(msg)
            else:
                msg = u'task %s err in page %s, link: %s, stop' % (str(TaskId), str(j), url)
                logger.error(msg)
            if stopwhencantcrawl:
                msg = u'task %s err in page %s, link: %s, stop when crawl err' % (str(TaskId), str(j), url)
                logger.error(msg)
                ms.ExecNonQuery('''UPDATE [WebRescourseForVipProject].[dbo].[CrawlTask] 
                               SET [IsStarted] = 0 WHERE [ID] = %s''' % str(TaskId))
            break

def startTask(ms, TaskId, RuleId, SiteSectionId, PageUrlWithParam, isfirsttime):
    # 主站链接 用于相对路径等
    #domain = WebLink
    
    # 最终页
    endPage = 999999
    
    # 读取规则
    ruleList = ms.ExecQuery("""SELECT 
                                   [SubmitType]
                                  ,[TitleXpath]
                                  ,[UrlXpath]
                                  ,[UrlAttr]
                                  ,[ContentXpath]
                                  ,[CreateTimeXpath]
                                  ,[CreateTimeReg]
                                  ,[PictureXpath]
                                  ,[PictureAttr]
                                  ,[SourceXpath]
                                  ,[SourceReg]
                                  ,[AuthorXpath]
                                  ,[AuthorReg]
                                  ,[IsFinished]
                            FROM [WebRescourseForVipProject].[dbo].[CrawlRules] WHERE [Id] = '%s' AND [DeleteFlag] = 0""" % str(RuleId))

    # 读取被采集网站信息
    webList = ms.ExecQuery("""SELECT 
                                   [WebName]
                                  ,[WebLink]
                                  ,[SectionName]
                            FROM [WebRescourseForVipProject].[dbo].[CrawlSite] WHERE [Id] = '%s' AND [DeleteFlag] = 0""" % str(SiteSectionId))
    
    # 根据规则开始采集
    msg = u'task %s start Crawling.' % str(TaskId)
    logger.info(msg)

    CrawlContents(ms, TaskId, PageUrlWithParam, endPage, ruleList, webList, RuleId, isfirsttime)

    msg = u'task %s finish Crawling.' % str(TaskId)
    logger.info(msg)

def main():
    ## ms =
    ## MSSQL(host="localhost",user="sa",pwd="123456",db="PythonWeiboStatistics")
    ## #返回的是一个包含tuple的list，list的元素是记录行，tuple的元素是每行记录的字段
    ## ms.ExecNonQuery("insert into WeiBoUser values('2','3')")
    global proxyuse,db_host,db_port,db_user,db_pass,db_name,thread_onerecoredwait,thread_onepagewait,stopwhencantcrawl,solrdeltaurl
    
    # 配置助手
    cf = ConfigParser()
    cf.read("Crawl.conf")

    # 启动solr
    os.environ['JAVA_HOME'] = cf.get("solr", "javahome")
    command = cf.get("solr", "solrstartcmd")
    os.system(command) 

    # 读取配置：第一次执行
    isfirsttime = cf.getint("crawl", "isfirsttime")
    defaultsockettime = cf.getint("crawl", "defaultsockettime")
    solrdeltaurl = cf.get("solr", "solrdeltaurl")
    # 连接超时设置
    socket.setdefaulttimeout(defaultsockettime)
    
    threads = []
    try:
        print u'Ctrl+C: 退出程序。'
        while True:
            # 读取配置
            cf.read("Crawl.conf")
            # 读取配置：数据库链接
            db_host = cf.get("db", "host")
            if db_host == '':
                db_host == '1433'
            db_port = cf.get("db", "port")
            db_user = cf.get("db", "user")
            db_pass = cf.get("db", "pass")
            db_name = cf.get("db", "dbname")
            # 读取配置：线程最大/探测新任务间隔/每页等待时间
            thread_maxThread = cf.getint("thread", "maxthread")
            thread_onepagewait = cf.getint("thread", "onepagewait")
            thread_onerecoredwait = cf.getint("thread", "onerecoredwait")
            thread_checktask = cf.getint("thread", "checktask")
            if thread_checktask < 1 or thread_checktask >= 60:
                thread_checktask = 59
            # 读取配置：读取任务执行时间
            when_month = cf.getint("when", "month")
            when_date = cf.getint("when", "date")
            when_day = cf.getint("when", "day")
            when_hour = cf.getint("when", "hour")
            when_always = cf.getint("when", "always")
            
            # 读取配置：采集错误时停止与否
            stopwhencantcrawl = cf.getint("crawl", "stopwhencantcrawl")

            # 读取配置：代理
            proxyuse = cf.getint("proxy", "useproxy")
            #if proxyuse:
            #    msg = u"use proxy, may not get any data."
            #    logger.info(msg)
            # 数据库链接
            ms = MSSQL(host=db_host,user=db_user,pwd=db_pass,db=db_name,port=db_port)

            # 判断是否在执行时间内
            localtime = time.localtime(time.time())
            if when_month == localtime.tm_mon or when_date == localtime.tm_hour or when_day == localtime.tm_hour or when_hour == localtime.tm_hour or when_always:
                #msg = u"ready to start"
                #logger.info(msg)

                # 获取打开的任务列表
                resList = ms.ExecQuery('''SELECT [Id]
                                                ,[RuleId]
                                                ,[SiteSectionId]
                                                ,[PageUrlWithParam]
                                            FROM [WebRescourseForVipProject].[dbo].[CrawlTask] WHERE [IsStarted] = 1''')
                # 循环任务 开始采集
                for (TaskId, RuleId, SiteSectionId, PageUrlWithParam) in resList:
                        try:
                            # 创建新线程
                            ths = threading.enumerate()
                            length = len(ths)
                            if(len(ths) <= thread_maxThread):
                                for i in range(0,length):
                                    if ths[i].getName() == "Thread-" + str(TaskId):
                                        #msg = u"task %s running." %
                                        #str(TaskId)
                                        #logger.info(msg)
                                        break
                                    if i == length - 1:
                                        msg = u"task %s starting." % str(TaskId)
                                        logger.info(msg)  
                                        thread2 = myThread(TaskId, "Thread-" + str(TaskId), TaskId, RuleId, SiteSectionId, PageUrlWithParam, isfirsttime)
                                        thread2.start()
                                        threads.append(thread2)
                                        msg = u"task %s started." % str(TaskId)
                                        logger.info(msg)  
                                        break
                            else:
                                msg = u" %s beyound thread limit" % str(TaskId)
                                logger.error(msg) 
                                break
                        except Exception,e:
                            msg = u"thread err：" + e.message + '\n'
                            logger.error(msg)  
            #else:
            #    msg = u"not now, but after %s minutes." %
            #    str(thread_checktask)
            #    logger.info(msg)

            #msg = u"check after %s minutes." % str(thread_checktask)
            #logger.info(msg)
            time.sleep(60 * thread_checktask)
    except KeyboardInterrupt:
        msg = u"exit"
        logger.error(msg) 
        print u'exiting: checking threads...'
        for t in threads:
            t.join()
        print u'exiting: wait for all sub-threads...'
        time.sleep(3)
        print u'exiting: FINISH.'
if __name__ == '__main__':
    main()
