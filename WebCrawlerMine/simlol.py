import random
import time
import copy

class Player:
    def __init__(self, name, power, pos, farm, top, butt):
        self.name = name
        self.power = power
        self.pos = pos
        self.farm = farm
        self.money = 450.0
        self.top = top
        self.butt = butt
        self.kill = 0
        self.dead = 0
        self.cs = 0
        self.sup = 0
        self.isalive = 1
        pass
    def killone(self):
        self.kill += 1
        self.money += 300.0
        print self.name + " got the kill! Money: " + str(self.money)

    def deadone(self):
        self.dead += 1
        self.isalive = 0
        print self.name + " was killed! Money: " + str(self.money)

    def farms(self):
        if self.isalive:
            self.money += self.farm * random.uniform(0.9, 1.1)
        else:
            self.isalive = 1
        print self.name + "'s Money: " + str(self.money)
    def show(self):
        print self.name + ': ' + str(self.kill) + '/' + str(self.dead) + '/' + str(self.sup) + '/' + str(self.cs) + ' Gold: ' + str(self.money)


class Team:
    def __init__(self, tplayers):
        self.tplayers = tplayers
        self._config()
        pass
    def _config(self):
        self.top = [1, 2 ,3]
        self.mid = [1, 2 ,3]
        self.butt = [1, 2 ,3]
        self.jg = [1, 1]
        self.base = 1
        pass
    def isalive(self):
        return self.base == 1
        pass
    def farm(self):
        for p in self.tplayers:
            p.farms()
    def show(self):
        for p in self.tplayers:
            p.show()


class Match:
    def __init__(self, team1, team2):
        self.team1 = team1
        self.team2 = team2
        pass
    def start(self):
        alive = self.team1.isalive() and self.team2.isalive()
        times = 0
        while alive and times <= 40:
            bat = Battle(self.team1.tplayers, self.team2.tplayers)
            bat.start()
            alive = self.team1.isalive() and self.team2.isalive()
            self.team1.farm()
            self.team2.farm()
            times += 2
            time.sleep(1)
            pass
        self.team1.show()
        self.team2.show()
        pass
    pass

class Battle:
    def __init__(self, players1, players2):
        self.players1 = players1
        self.players2 = players2
        self.t1 = 0.0
        self.t2 = 0.0
        pass
    def start(self):
        print "----Battle Start----"
        for player in self.players1:
            self.t1 += (player.power + player.money * 3/1000) * random.uniform(player.top, player.butt)
        for player in self.players2:
            self.t2 += (player.power + player.money * 3/1000) * random.uniform(player.top, player.butt)
        
        if self.t1 == 0.0:
            self.t1 = 1
        if self.t2 == 0.0:
            self.t2 = 1

        self.ps1 = copy.copy(self.players1)
        self.ps2 = copy.copy(self.players2)

        print self.t1, ' ', self.t2

        if self.t1 < self.t2:
            self.t1, self.t2 = self.t2, self.t1
            self.ps1, self.ps2 = self.ps2, self.ps1

        t1Kill = random.uniform(0.25,1) * self.t1 / self.t2
        t2Kill = random.uniform(0.0,0.75) * self.t2 / self.t1

        while t1Kill > 0.5 and len(self.ps2) and len(self.ps1):
            pkill = random.choice(self.ps1)
            pkill.killone()
            pdead = random.choice(self.ps2)
            self.ps2.remove(pdead)
            pdead.deadone()
            t1Kill -= random.uniform(0,1) * 0.5

        while t2Kill > 0.5 and len(self.ps1) and len(self.ps2):
            pkill = random.choice(self.ps2)
            pkill.killone()
            pdead = random.choice(self.ps1)
            self.ps1.remove(pdead)
            pdead.deadone()
            t2Kill -= random.uniform(0,1) * 0.5

        print "----Battle End----"


if __name__ == '__main__':
    mink = Player('Mink', 94.0, 'jg', 2000.0, 0.5, 2.0)
    peanut = Player('Peanut', 94.0, 'jg', 2000.0, 0.5, 2.0)

    edg = Team([mink, ])
    rox = Team([peanut, ])

    mat = Match(edg, rox)
    mat.start()



