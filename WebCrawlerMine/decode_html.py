#-*- coding:utf-8 -*-
import logging
import re
import chardet
from bs4 import UnicodeDammit

LOG = logging.getLogger()


BROTHER_ENCODINGS = [
    ('GB2312', 'GBK', 'GB18030'),
]


def decode_html(html_string):
    """使用BS4的UnicodeDammit来检测网页编码, 并返回unicode文档, 不能保证正确率100%, 因此加上备选方案
    """
    dammit = UnicodeDammit(html_string, ['GB2312', 'GBK', 'GB18030'], smart_quotes_to="html", is_html=True)
    doc = dammit.unicode_markup
    # print u"dammit -- ", dammit.original_encoding
    # FIXME 部分网页判断是'ISO-8859-2', 不知道为什么, 这时使用备选方案进行判断
    if dammit.original_encoding.upper() == 'ISO-8859-2':
        enc = get_encoding(html_string)
        # print("chardet —— ", enc)
        doc = html_string.decode(enc, 'replace')
        #print(enc)
    elif not dammit.unicode_markup:
        raise UnicodeDecodeError("Failed to detect encoding, tried [%s]", ', '.join(dammit.tried_encodings))
    # print(doc.encode('utf-8'))
    return doc


def get_encoding(page):
    """测试编码的备选方案
    """
    def is_enc(enc):
        try:
            diff = text.decode(enc, 'ignore').encode(enc)
            sizes = len(diff), len(text)
            # 99% of utf-8
            if abs(len(text) - len(diff)) < max(sizes) * 0.01:
                return True
        except UnicodeDecodeError:
            return False

    text = re.sub(b'</?[^>]*>\s*', b'', page)
    enc = 'utf-8'
    if not text.strip() or len(text) < 10:
        return enc  # can't guess

    if is_enc(enc):
        return enc

    def get_brothers(ec):
        for i in BROTHER_ENCODINGS:
            if ec in i:
                return i
        return None

    res = chardet.detect(text)
    enc = res['encoding']

    if enc == 'MacCyrillic':
        enc = 'cp1251'

    # print '->', enc, "%.2f" % res['confidence']

    if res['confidence'] < 0.7:
        possible_encodings = get_brothers(enc) or ('GBK', enc)
        for p_enc in possible_encodings:
            if is_enc(p_enc):
                # print '->', p_enc
                return p_enc
    else:
        return enc

    raise UnicodeDecodeError("Failed to detect encoding, tried [%s]", 'raw result from chardet: %s' % res)


def replaceCharEntity(htmlstr):  
    '''
    ##替换常用HTML字符实体.  
    #使用正常的字符替换HTML中特殊的字符实体.  
    #你可以添加新的实体字符到CHAR_ENTITIES中,处理更多HTML字符实体.  
    #@param htmlstr HTML字符串.  
    '''
    CHAR_ENTITIES={'nbsp':' ','160':' ',  
                'lt':'<','60':'<',  
                'gt':'>','62':'>',  
                'amp':'&','38':'&',  
                'quot':'"''"','34':'"',}  
      
    re_charEntity=re.compile(r'&#?(?P<name>\w+);')  
    sz=re_charEntity.search(htmlstr)  
    while sz:  
        entity=sz.group()#entity全称，如>  
        key=sz.group('name')#去除&;后entity,如>为gt  
        try:  
            htmlstr=re_charEntity.sub(CHAR_ENTITIES[key],htmlstr,1)  
            sz=re_charEntity.search(htmlstr)  
        except KeyError:  
            #以空串代替  
            htmlstr=re_charEntity.sub('',htmlstr,1)  
            sz=re_charEntity.search(htmlstr)  
    return htmlstr  

def getMaxLengthStr(lst):
    val = ""
    for i in lst:
        if len(i) > len(val):
            val = i
    return val

def findFirstNotEmptyStr(pat, str):
    '''
    取到相匹配的第一个非空字符串，并去掉前后空格
    pat 正则表达式
    str 待匹配字符串
    '''
    str = str.replace(u" 文/", " ")
    mat = re.findall(pat, str)
    res = []
    if len(mat):
        match = mat[0]
        if type(match) == unicode:
            for ma in mat:
                if ma.strip() and ma not in res:
                    res.append(ma.strip())
        elif type(match) == tuple:
            for ma in match:
                if ma.strip() and ma not in res:
                    res.append(ma.strip())
    if len(res):
        return getMaxLengthStr(mat)
    else:
        return ""
