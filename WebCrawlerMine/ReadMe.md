# python 开发环境 #
## python版本 ##
Python 2.7.12 (v2.7.12:d33e0cf91556, Jun 27 2016, 15:19:22) [MSC v.1500 32 bit (Intel)] on win32

## OS ##
WIN7 X64 SP1

# python 所需扩展包 #
1. pip install pymssql
2. pip install requests
3. pip install BeautifulSoup
4. pip install lxml
4. pip install chardet


#配置文件 Crawl.conf#
## db 数据库链接设置 ##
1. host 主机
2. port 端口， 默认1433
3. user 用户名
4. pass 密码
5. dbname 数据库名

## thread 线程设置##
1. maxthread 最大线程数 有效值: 1-无穷
2. checktask 线程检查间隔时长,单位: 分钟 有效值 1-59
3. onepagewait 每页等待时长,单位: 秒 有效值1-无穷
4. onerecoredwait 每条数据等待时长,单位: 秒 有效值1-无穷

perminute 是指检查新任务、检查配置更新、检查是否在任务执行时刻内的间隔分钟数

## when 任务执行时刻设置 ##
1. month 月，有效值 1-12、-1
2. date 日 有效值 1-31、-1
3. day 星期 有效值 0-6、-1
4. hour 时 有效值 0-23、-1
5. always 随时采集 有效值0/1 (0停止随时采集 1随时采集)

全为-1时不执行任务

## proxy ip代理设置 ##
1. useproxy 使用代理，有效防止一些网站会封禁频繁访问ip，系统默认不使用 有效值 1:使用 0:不使用

## crawl 采集设置 ##
1. isfirsttime 第一次采集设置，有效值0/1 1第一次采集 0不是第一次采集  默认1
2. stopwhencantcrawl 采集列表页面时发生错误后 是否继续设置 有效值0/1 0继续采集 1停止采集并停止当前任务
3. defaultsockettime 超时设置 有效值 正整数

# 提醒 所有选项 请不要填 空值#