# pcikCard.py
# -- coding:utf8 -- 
# chenk 
# 2016 - 09 -19

import random
import os
from ConfigParser import ConfigParser
import time

class Card:
    pickCards = []
    def __init__(self, hasjoker):
        self._setDefault(hasjoker)
        self._setCard()
        pass
    def __eq__(self, other):
        if self.num == other.num:
            return True
        return False
    def __lt__(self, other):
        seidx = self._nums.index(self.num)
        otidx = self._nums.index(other.num)
        if seidx < otidx:
            return True
        return False
    def __str__(self):
        return self.getCard()
        pass
    def _setDefault(self, hasjoker):
        self._nums = ("2","3","4","5","6","7","8","9","10","J","Q","K","A")
        self._huas = ("Diamond ","Club ","Heart ", "Spade ")
        if hasjoker:
            self._allnums = range(1,55)
        else:
            self._allnums = range(1,53)
        self._picknums = []
        pass
    def _setCard(self):
        randNum = random.choice(self._allnums)
        if randNum == 53:
            self.num = "Joker"
            self.hua = "Black "
        elif randNum == 54:
            self.num = "Joker"
            self.hua = "Red "
        else:
            self.num = random.choice(self._nums)
            self.hua = random.choice(self._huas)
        if self.getCard() not in self.pickCards:
            self.pickCards.append(self.getCard())
            return 
        else:
            self._setCard()
    def getCard(self):
        return self.hua + self.num
        pass
class Main:
    def __init__(self, conf):
        self.conf = conf
        self.cf = ConfigParser()
        self.cf.read(self.conf)
        self._run()
    def _get_player(self):
        self.name = raw_input("Pls Enter your name:")
        if not self.name:
            self._get_player()
            return
        if self.name not in self.players:
            self.cf.add_section(self.name)
            self.cf.set(self.name, "gold", 1000)
            self.gold = 1000
            self.playtime = 1
            self.playround = 1
            self.goal = 100000000
            self.cf.write(open(self.conf, "w"))
        else:
            self.gold = self.cf.getint(self.name, "gold")
            self.playtime = self.cf.getint(self.name, "playtime")
            self.playround = self.cf.getint(self.name, "playround")
            self.goal = self.cf.getint(self.name, "goal")
        if self.gold == 0:
            self.gold = 1000
    def _run(self):
        self.players = self.cf.sections()
        print self.players
        self._get_player()
        self.game()
    def setbet(self):
        #bet = input("Pls enter a num:")
        bet = 0
        if self.gold < 100:
            bet = self.gold
        else:
            if self.gold < 1000:
                bet = int(0.25 * self.gold)
            elif self.gold < 2000:
                bet = int(0.20 * self.gold)
            elif self.gold < 4000:
                bet = int(0.10 * self.gold)
            elif self.gold < 8000:
                bet = int(0.05 * self.gold)
            elif self.gold < 10000:
                bet = int(0.03 * self.gold)
            elif self.gold < 50000:
                bet = int(0.02 * self.gold)
            elif self.gold < 100000:
                bet = int(0.01 * self.gold)
            if bet == 0:
                bet = 1
        return bet
    def game(self):
        while self.gold > 0 and self.gold <= self.goal:
            try:
                print self.name + ", Your Gold: %d " % self.gold
                bet = self.setbet()
                print "Your Gold: %d" % bet
                if bet <= 0 or bet > self.gold:
                    continue
                self.gold -= bet
                Card.pickCards = []
                cardMe = Card(False)
                cardCpu = Card(False)
                print "Your Card: "
                print cardMe
                print "Computer's Card: "
                print cardCpu
                if cardMe > cardCpu:
                    self.gold += bet*2
                    print "You Win!"
                elif cardMe == cardCpu:
                    self.gold += bet
                    print "Draw!"
                else:
                    print "You Lose!"
                self.playround += 1
                self.cf.set(self.name, "gold", self.gold)
                self.cf.set(self.name, "playround", self.playround)
                self.cf.write(open(self.conf, "w"))  
            except Exception as inst:
                msg = "Main ERR, msg:" + str(inst.args) + str(inst.message)
                print msg
                continue
            time.sleep(1)
            if self.gold == 0:
                self.gold = 1000
                self.playtime += 1
                self.cf.set(self.name, "playtime", self.playtime)
                self.cf.write(open(self.conf, "w"))  
                print "Play time %d" % self.playtime
                time.sleep(4)
            pass
        print "Finally, Your Gold: %d" % self.gold

if __name__ == "__main__":
    try:
        Main("card.conf")
    except KeyboardInterrupt:
        msg = u"exiting"
        time.sleep(3)
        print u'exiting: FINISH.'
        pass
        
