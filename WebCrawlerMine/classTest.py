#class A(object):    # A must be new-style class
#  def __init__(self, name):
#   print "enter A"
#   print name
#   print "leave A"

#class B(A):     # A --> C
#  def __init__(self):
#   print "enter B"
#   name = "Name by B"
#   super(B, self).__init__(name)
#   print "leave B"

#b = B()
#print dir(B)

class task_queue:
    queue=[]
    
    def append(self,obj):
        self.queue.append(obj)
        
    def print_queue(self):
        print self.queue
        

if __name__=="__main__":
    a=task_queue()
    b=task_queue()
    c=task_queue()
    
    a.append('tc_1')
    
    a.print_queue()
    b.print_queue()
    c.print_queue()