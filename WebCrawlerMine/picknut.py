from ConfigParser import ConfigParser


def checkPick(pick):
    global cf
    minP = min(pick)
    maxP = max(pick)
    length = len(pick)
    for i in range(0, length):
        if pick[i] == minP or pick[i] == maxP:
            if i == 0:
                acount = cf.get("count", "acount")
                cf.set("count", "acount", str(int(acount) + 1))
            if i == 1:
                bcount = cf.get("count", "bcount")
                cf.set("count", "bcount", str(int(bcount) + 1))
            if i == 2:
                ccount = cf.get("count", "ccount")
                cf.set("count", "ccount", str(int(ccount) + 1))
            if i == 3:
                dcount = cf.get("count", "dcount")
                cf.set("count", "dcount", str(int(dcount) + 1))
            if i == 4:
                ecount = cf.get("count", "ecount")
                cf.set("count", "ecount", str(int(ecount) + 1))



def main():
    global cf
    a = 1
    b = 1
    c = 1
    d = 1
    e = 1
    pick = [1,1,1,1,1]
    total = 100
    pickcount = 0
    for a in range(1,total - 3):
        pick[0] = a
        b = 1
        while b < (total - a - 2) and b >= 1:
            pick[1] = b
            c = 1
            while c < (total - a - b - 1) and c >= 1:
                pick[2] = c
                d = 1
                while d < (total - a - b - c) and d >= 1:
                    pick[3] = d
                    e = 1
                    while e < (total - a - b - c - d) and e >= 1:
                        pick[4] = e
                        checkPick(pick)
                        pickcount += 1
                        #print pick
                        e += 1 
                    d += 1
                c += 1
            b += 1
    print pickcount
    cf.set("count", "pickcount", pickcount)

if __name__ == "__main__":
    cf = ConfigParser()
    cf.read("picknut.conf")
    cf.set("count", "acount", str(0))
    cf.set("count", "bcount", str(0))
    cf.set("count", "ccount", str(0))
    cf.set("count", "dcount", str(0))
    cf.set("count", "ecount", str(0))
    main()
    cf.write(open("picknut.conf", "w"))
