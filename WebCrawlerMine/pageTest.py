#-*- coding:utf-8 -*-
import re
import urlparse
import decode_html
import urllib2
from bs4 import BeautifulSoup

domain = 'http://www.coalchina.org.cn/'
url = 'http://www.coalchina.org.cn/detail/09/11/05/00000009/content.html?path=09/11/05/00000009'
# 数据请求
getdata = ""
req = urllib2.Request(url = url,headers ={'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0'})
getdata = urllib2.urlopen(req).read()
# 编码转成Unicode
getdata = decode_html.decode_html(getdata)
# 装载数据
htmlnews = BeautifulSoup(getdata, "lxml")
# 标题
#title = ''
#title = htmlnews.select(TitleXpath)[0].get_text()

# 发布时间
#createtime = ''
#createtime = htmlnews.select(CreateTimeXpath)[0].get_text()
#if len(createtime):
#    CreateTimeRegtemp = CreateTimeReg.replace("yyyy",u"\d{4}").replace("MM",u"\d{2}").replace("dd",u"\d{2}").replace("HH",u"\d{2}").replace("mm",u"\d{2}").replace("ss",u"\d{2}")
#    match = re.search(CreateTimeRegtemp, createtime)
#    if match:
#        createtime = match.group()
#        CreateTimeRegtemp2 = CreateTimeReg.replace("yyyy",u"%Y").replace("MM",u"%m").replace("dd",u"%d").replace("HH",u"%H").replace("mm",u"%M").replace("ss",u"%S")
#        createtime = time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(createtime,CreateTimeRegtemp2))
                            
# 文章图片路径处理
#picture = ''
#pic = htmlnews.select(PictureXpath)
#coverflag = 0
#for img in pic:
#    picturetemp = img[PictureAttr]
#    picturetemp = urlparse.urljoin(domain, picturetemp)
#    img["src"] = picturetemp
#    if coverflag == 0:
#        picture = picturetemp
#    coverflag += 1

# 文章内容
#content = htmlnews.select(ContentXpath)[0].contents
#contents = ''
#for c in content:
#    # 过滤掉 回车符号 script
#    if str(c) != '\n':
#        if c.name not in ["meta", "script"]:
#            contents += decode_html.replaceCharEntity(str(c))

# 文章来源
source = ''
SourceReg = "(:%s)|(《%s》)"
sourcetemp = htmlnews.select('td.content > table:nth-of-type(1) > tbody > tr:nth-of-type(2) > td > table > tbody > tr > td:nth-of-type(3)')
if len(sourcetemp):
    source = sourcetemp[0].get_text().strip()
    if len(source) and SourceReg:
        SourceReg = SourceReg.replace("%s",u'([a-zA-Z0-9\u4e00-\u9fff]+)')
        source = decode_html.findFirstNotEmptyStr(SourceReg, source)
print source
# 文章作者
author = ''
AuthorReg = ": %s"
autemp = htmlnews.select('table.mt-20 > tbody > tr:nth-of-type(3) > td:nth-of-type(1)')
if len(autemp):
    author = autemp[0].get_text().strip()
    if len(author) and AuthorReg:
        AuthorReg = AuthorReg.replace("%s",u'([a-zA-Z0-9\u4e00-\u9fff、 ]+)')
        author = decode_html.findFirstNotEmptyStr(AuthorReg, author)
print author
raw_input("Enter!")
