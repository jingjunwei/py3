USE [WebRescourseForVipProject]
GO
/****** Object:  Table [dbo].[SiteTokens]    Script Date: 08/08/2016 17:04:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SiteTokens](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Token] [nvarchar](50) NULL,
	[SiteName] [nvarchar](50) NULL,
	[Ip] [nvarchar](50) NULL,
	[IsEnabled] [int] NOT NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SiteTokens', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Token接口使用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SiteTokens', @level2type=N'COLUMN',@level2name=N'Token'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'站点名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SiteTokens', @level2type=N'COLUMN',@level2name=N'SiteName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IP限制' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SiteTokens', @level2type=N'COLUMN',@level2name=N'Ip'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否启用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SiteTokens', @level2type=N'COLUMN',@level2name=N'IsEnabled'
GO
SET IDENTITY_INSERT [dbo].[SiteTokens] ON
INSERT [dbo].[SiteTokens] ([Id], [Token], [SiteName], [Ip], [IsEnabled]) VALUES (1, N'JSTOKEN2016', N'江苏省情报所', N'192.168.10.48', 1)
SET IDENTITY_INSERT [dbo].[SiteTokens] OFF
/****** Object:  StoredProcedure [dbo].[Page_v1]    Script Date: 08/08/2016 17:04:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[Page_v1]
    @PCount INT OUTPUT ,    --总页数输出
    @RCount INT OUTPUT ,    --总记录数输出
    @sys_Table NVARCHAR(100) ,    --查询表名
    @sys_Key VARCHAR(50) ,        --主键
    @sys_Fields NVARCHAR(500) ,    --查询字段
    @sys_Where NVARCHAR(3000) ,    --查询条件
    @sys_Order NVARCHAR(100) ,    --排序字段
    @sys_Begin INT ,        --开始位置
    @sys_PageIndex INT ,        --当前页数
    @sys_PageSize INT        --页大小
AS 
    SET NOCOUNT ON
    SET ANSI_WARNINGS ON
    IF @sys_PageSize < 0
        OR @sys_PageIndex < 0 
        BEGIN        
            RETURN
        END
    DECLARE @new_where1 NVARCHAR(3000)
    DECLARE @new_order1 NVARCHAR(100)
    DECLARE @new_order2 NVARCHAR(100)
    DECLARE @Sql NVARCHAR(4000)
    DECLARE @SqlCount NVARCHAR(4000)
    DECLARE @Top1 INT
    DECLARE @Top2 INT
    IF ( @sys_Begin <= 0 ) 
        SET @sys_Begin = 0
    ELSE 
        SET @sys_Begin = @sys_Begin - 1
    IF ISNULL(@sys_Where, '') = '' 
        SET @new_where1 = ' '
    ELSE 
        SET @new_where1 = ' WHERE ' + @sys_Where
    IF ISNULL(@sys_Order, '') <> '' 
        BEGIN
            SET @new_order1 = ' ORDER BY ' + REPLACE(@sys_Order, 'desc', '')
            SET @new_order1 = REPLACE(@new_order1, 'asc', 'desc')
            SET @new_order2 = ' ORDER BY ' + @sys_Order
        END
    ELSE 
        BEGIN
            SET @new_order1 = ' ORDER BY ID DESC'
            SET @new_order2 = ' ORDER BY ID ASC'
        END
    SET @SqlCount = 'SELECT @RCount=COUNT(1),@PCount=CEILING((COUNT(1)+0.0)/'
        + CAST(@sys_PageSize AS NVARCHAR) + ') FROM ' + @sys_Table
        + @new_where1
    EXEC SP_EXECUTESQL @SqlCount, N'@RCount INT OUTPUT,@PCount INT OUTPUT',
        @RCount OUTPUT, @PCount OUTPUT
    --IF @sys_PageIndex > CEILING(( @RCount + 0.0 ) / @sys_PageSize)    --如果输入的当前页数大于实际总页数,则把实际总页数赋值给当前页数
    --    BEGIN
    --        SET @sys_PageIndex = CEILING(( @RCount + 0.0 ) / @sys_PageSize)
    --    END
    IF @sys_PageSize * @sys_PageIndex > @RCount
		BEGIN
		    SET @Top1 = (@RCount - @sys_PageSize * (@sys_PageIndex - 1))
		    IF @Top1 < 0
		        SET @Top1 = 0
		END
	ELSE
	    SET @Top1 = @sys_PageSize
	SET @Top2 = @sys_PageSize * @sys_PageIndex + @sys_Begin
    SET @sql = 'select ' + @sys_fields + ' from ' + @sys_Table + ' w1 '
        + ' where ' + @sys_Key + ' in (' + 'select top '
        + LTRIM(STR(@Top1)) + ' ' + @sys_Key + ' from ' + '('
        + 'select top ' + LTRIM(STR(@Top2)) + ' ' + @sys_Key + ' FROM '
        + @sys_Table + @new_where1 + @new_order2 + ') w ' + @new_order1 + ') '
        + @new_order2
    PRINT ( @sql )
    EXEC(@sql)
GO
/****** Object:  Table [dbo].[CrawlTask]    Script Date: 08/08/2016 17:04:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CrawlTask](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RuleId] [int] NOT NULL,
	[SiteSectionId] [int] NULL,
	[PageUrlWithParam] [nvarchar](2000) NULL,
	[IsStarted] [int] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlTask', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'采集规则' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlTask', @level2type=N'COLUMN',@level2name=N'RuleId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'第一页URL' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlTask', @level2type=N'COLUMN',@level2name=N'PageUrlWithParam'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'任务启动与否' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlTask', @level2type=N'COLUMN',@level2name=N'IsStarted'
GO
SET IDENTITY_INSERT [dbo].[CrawlTask] ON
INSERT [dbo].[CrawlTask] ([Id], [RuleId], [SiteSectionId], [PageUrlWithParam], [IsStarted]) VALUES (1, 1, 1, N'http://www.dsb.cn/list-9-%d.html', 1)
INSERT [dbo].[CrawlTask] ([Id], [RuleId], [SiteSectionId], [PageUrlWithParam], [IsStarted]) VALUES (2, 2, 2, N'http://202.99.195.11/cms/templet/2013new/ShowClassList.jsp?id=966&pn=%d', 1)
SET IDENTITY_INSERT [dbo].[CrawlTask] OFF
/****** Object:  Table [dbo].[CrawlSite]    Script Date: 08/08/2016 17:04:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CrawlSite](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WebName] [nvarchar](50) NULL,
	[WebLink] [nvarchar](200) NULL,
	[SectionName] [nvarchar](50) NULL,
	[SectionIndexUrl] [nvarchar](200) NULL,
	[SiteFrom] [nvarchar](50) NULL,
	[DeleteFlag] [int] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlSite', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'网站名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlSite', @level2type=N'COLUMN',@level2name=N'WebName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'网站首页链接' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlSite', @level2type=N'COLUMN',@level2name=N'WebLink'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模块名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlSite', @level2type=N'COLUMN',@level2name=N'SectionName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模块首页地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlSite', @level2type=N'COLUMN',@level2name=N'SectionIndexUrl'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'采集请求来源' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlSite', @level2type=N'COLUMN',@level2name=N'SiteFrom'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'删除标记' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlSite', @level2type=N'COLUMN',@level2name=N'DeleteFlag'
GO
SET IDENTITY_INSERT [dbo].[CrawlSite] ON
INSERT [dbo].[CrawlSite] ([Id], [WebName], [WebLink], [SectionName], [SectionIndexUrl], [SiteFrom], [DeleteFlag]) VALUES (1, N'电商报', N'http://www.dsb.cn/', N'资讯', N'http://www.dsb.cn/list-9-1.html', N'江苏省情报所', 0)
INSERT [dbo].[CrawlSite] ([Id], [WebName], [WebLink], [SectionName], [SectionIndexUrl], [SiteFrom], [DeleteFlag]) VALUES (2, N'山西煤炭信息网', N'http://202.99.195.11/', N'行业要闻', N'http://202.99.195.11/cms/templet/2013new/ShowClassList.jsp?id=966&pn=1', N'江苏省情报所', 0)
SET IDENTITY_INSERT [dbo].[CrawlSite] OFF
/****** Object:  Table [dbo].[CrawlRules]    Script Date: 08/08/2016 17:04:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CrawlRules](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RuleName] [nvarchar](50) NULL,
	[IsFinished] [int] NULL,
	[SubmitType] [nvarchar](10) NULL,
	[TitleXpath] [nvarchar](200) NULL,
	[UrlXpath] [nvarchar](200) NULL,
	[UrlAttr] [nvarchar](200) NULL,
	[ContentXpath] [nvarchar](200) NULL,
	[CreateTimeXpath] [nvarchar](200) NULL,
	[CreateTimeReg] [nvarchar](200) NULL,
	[PictureXpath] [nvarchar](200) NULL,
	[PictureAttr] [nvarchar](200) NULL,
	[SourceXpath] [nvarchar](200) NULL,
	[SourceReg] [nvarchar](200) NULL,
	[AuthorXpath] [nvarchar](200) NULL,
	[AuthorReg] [nvarchar](200) NULL,
	[Version] [int] NOT NULL,
	[CreateTime] [datetime] NULL,
	[DeleteFlag] [int] NOT NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlRules', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规则名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlRules', @level2type=N'COLUMN',@level2name=N'RuleName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规则完成完成1 完成 0未完成 2规则异常(第一次采集数据列表为空)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlRules', @level2type=N'COLUMN',@level2name=N'IsFinished'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'采集方式:POST GET' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlRules', @level2type=N'COLUMN',@level2name=N'SubmitType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标题Xpath' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlRules', @level2type=N'COLUMN',@level2name=N'TitleXpath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'来源链接Xpath' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlRules', @level2type=N'COLUMN',@level2name=N'UrlXpath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'内容Xpath' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlRules', @level2type=N'COLUMN',@level2name=N'ContentXpath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发布时间Xpath' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlRules', @level2type=N'COLUMN',@level2name=N'CreateTimeXpath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'内页图片Xpath' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlRules', @level2type=N'COLUMN',@level2name=N'PictureXpath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'来源Xpath' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlRules', @level2type=N'COLUMN',@level2name=N'SourceXpath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规则版本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlRules', @level2type=N'COLUMN',@level2name=N'Version'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'删除标记' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlRules', @level2type=N'COLUMN',@level2name=N'DeleteFlag'
GO
SET IDENTITY_INSERT [dbo].[CrawlRules] ON
INSERT [dbo].[CrawlRules] ([Id], [RuleName], [IsFinished], [SubmitType], [TitleXpath], [UrlXpath], [UrlAttr], [ContentXpath], [CreateTimeXpath], [CreateTimeReg], [PictureXpath], [PictureAttr], [SourceXpath], [SourceReg], [AuthorXpath], [AuthorReg], [Version], [CreateTime], [DeleteFlag]) VALUES (1, N'江苏电商报资讯采集', 1, N'GET', N'div.contents h1', N'.indexArticle-list-con .news_title a', N'href', N'div.contents-con', N'span.contents-time', N'yyyy-MM-dd HH:mm', N'div.contents-con img', N'data-original', N'span.contents-time', N'%s_', N'span.contents-time', N'_%s', 1, CAST(0x0000A6590101C3F2 AS DateTime), 0)
INSERT [dbo].[CrawlRules] ([Id], [RuleName], [IsFinished], [SubmitType], [TitleXpath], [UrlXpath], [UrlAttr], [ContentXpath], [CreateTimeXpath], [CreateTimeReg], [PictureXpath], [PictureAttr], [SourceXpath], [SourceReg], [AuthorXpath], [AuthorReg], [Version], [CreateTime], [DeleteFlag]) VALUES (2, N'山西煤炭信息网行业新闻', 1, N'GET', N'table h1', N'#main1 > div > ul > li > a', N'href', N'div.zhengwei1', N'body > div > div:nth-of-type(3) > table > tbody > tr > td > table:nth-of-type(2) > tbody > tr:nth-of-type(2) > td > span:nth-of-type(2)', N'yyyy年MM月dd日', N'div.zhengwei1 img', N'src', N'font.style04_1', N'%s', N'body > div > div:nth-of-type(3) > table > tbody > tr > td > table:nth-of-type(2) > tbody > tr:nth-of-type(5) > td > span:nth-of-type(2)', N'%s', 0, CAST(0x0000A659011CD671 AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[CrawlRules] OFF
/****** Object:  Table [dbo].[CrawlResult]    Script Date: 08/08/2016 17:04:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CrawlResult](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TaskId] [int] NOT NULL,
	[WebName] [nvarchar](50) NULL,
	[SectionName] [nvarchar](50) NULL,
	[Title] [nvarchar](50) NULL,
	[Url] [nvarchar](200) NULL,
	[Content] [nvarchar](max) NULL,
	[CreateTime] [datetime] NULL,
	[Picture] [nvarchar](200) NULL,
	[Source] [nvarchar](200) NULL,
	[Author] [nvarchar](200) NULL,
	[CollectTime] [datetime] NULL,
	[IsCleaned] [int] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自增主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlResult', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'采集规则ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlResult', @level2type=N'COLUMN',@level2name=N'TaskId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'网站名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlResult', @level2type=N'COLUMN',@level2name=N'WebName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'网站模块' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlResult', @level2type=N'COLUMN',@level2name=N'SectionName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标题' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlResult', @level2type=N'COLUMN',@level2name=N'Title'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'来源链接' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlResult', @level2type=N'COLUMN',@level2name=N'Url'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'内容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlResult', @level2type=N'COLUMN',@level2name=N'Content'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发布时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlResult', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'内容图片' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlResult', @level2type=N'COLUMN',@level2name=N'Picture'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'来源' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlResult', @level2type=N'COLUMN',@level2name=N'Source'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'作者' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlResult', @level2type=N'COLUMN',@level2name=N'Author'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'采集时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlResult', @level2type=N'COLUMN',@level2name=N'CollectTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已编辑' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CrawlResult', @level2type=N'COLUMN',@level2name=N'IsCleaned'
GO
/****** Object:  StoredProcedure [dbo].[TableSiteTokens]    Script Date: 08/08/2016 17:04:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TableSiteTokens]
        @Id int = 0, 
        @Token nvarchar(50) = null, 
        @SiteName nvarchar(50) = null, 
        @Ip nvarchar(50) = null, 

        @StartIndex int = 0, 
        @PageSize int = 0, 
        @SPReturnType varchar(100) = null, 
        @SPAction varchar(100) = null 
AS
BEGIN
    SET NOCOUNT ON;
    IF @SPAction='Insert'
        BEGIN            
            INSERT INTO [SiteTokens]([Token],[SiteName],[Ip]) 
            VALUES(@Token,@SiteName,@Ip) 

            Declare @newID varchar(10) 
            Set @newID = SCOPE_IDENTITY() 
            If (@newID is NULL) 
                set @newID = ('-1') 
            select @newID as 'newID'
        END
    IF @SPAction='Delete'
        BEGIN            
            DELETE FROM [SiteTokens] WHERE [Id] = @Id
        END
    IF @SPAction='Update'
        BEGIN            
            UPDATE [SiteTokens] 
            SET [Token] = @Token,[SiteName] = @SiteName,[Ip] = @Ip 
            WHERE [Id] = @Id
        END
    IF @SPAction='SelectPaging'
        BEGIN            
            SELECT [Id],[Token],[SiteName],[Ip] 
            FROM [SiteTokens]
        END
    IF @SPAction='SelectAll'
        BEGIN            
            SELECT [Id],[Token],[SiteName],[Ip] 
            FROM [SiteTokens]
        END
    IF @SPAction='Unique'
        BEGIN            
            Declare @isExist int 
            if @Id = 0 
                Set @isExist = (select count(*) from [SiteTokens] where [Token] = @Token and [SiteName] = @SiteName and [Ip] = @Ip) 
            else 
                Set @isExist = (select count(*) from [SiteTokens] where [Token] = @Token and [SiteName] = @SiteName and [Ip] = @Ip and [Id] != @Id) 
            if @isExist>0 
                raiserror('data existed already',16,1)
        END
END
GO
/****** Object:  StoredProcedure [dbo].[TableCrawlTask]    Script Date: 08/08/2016 17:04:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TableCrawlTask]
        @Id int = 0, 
        @RuleId int = 0, 
        @SiteSectionId int = null, 
        @PageUrlWithParam nvarchar(2000) = null, 
        @IsStarted int = null, 

        @StartIndex int = 0, 
        @PageSize int = 0, 
        @SPReturnType varchar(100) = null, 
        @SPAction varchar(100) = null 
AS
BEGIN
    SET NOCOUNT ON;
    IF @SPAction='Insert'
        BEGIN            
            INSERT INTO [CrawlTask]([RuleId],[SiteSectionId],[PageUrlWithParam],[IsStarted]) 
            VALUES(@RuleId,@SiteSectionId,@PageUrlWithParam,@IsStarted) 

            Declare @newID varchar(10) 
            Set @newID = SCOPE_IDENTITY() 
            If (@newID is NULL) 
                set @newID = ('-1') 
            select @newID as 'newID'
        END
    IF @SPAction='Delete'
        BEGIN            
            DELETE FROM [CrawlTask] WHERE [Id] = @Id
        END
    IF @SPAction='Update'
        BEGIN            
            UPDATE [CrawlTask] 
            SET [RuleId] = @RuleId,[SiteSectionId] = @SiteSectionId,[PageUrlWithParam] = @PageUrlWithParam,[IsStarted] = @IsStarted 
            WHERE [Id] = @Id
        END
    IF @SPAction='SelectPaging'
        BEGIN            
            SELECT [Id],[RuleId],[SiteSectionId],[PageUrlWithParam],[IsStarted] 
            FROM [CrawlTask]
        END
    IF @SPAction='SelectAll'
        BEGIN            
            SELECT [Id],[RuleId],[SiteSectionId],[PageUrlWithParam],[IsStarted] 
            FROM [CrawlTask]
        END
    IF @SPAction='Unique'
        BEGIN            
            Declare @isExist int 
            if @Id = 0 
                Set @isExist = (select count(*) from [CrawlTask] where [RuleId] = @RuleId and [SiteSectionId] = @SiteSectionId and [PageUrlWithParam] = @PageUrlWithParam and [IsStarted] = @IsStarted) 
            else 
                Set @isExist = (select count(*) from [CrawlTask] where [RuleId] = @RuleId and [SiteSectionId] = @SiteSectionId and [PageUrlWithParam] = @PageUrlWithParam and [IsStarted] = @IsStarted and [Id] != @Id) 
            if @isExist>0 
                raiserror('data existed already',16,1)
        END
END
GO
/****** Object:  StoredProcedure [dbo].[TableCrawlSite]    Script Date: 08/08/2016 17:04:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TableCrawlSite]
        @Id int = 0, 
        @WebName nvarchar(50) = null, 
        @WebLink nvarchar(200) = null, 
        @SectionName nvarchar(50) = null, 
        @SectionIndexUrl nvarchar(200) = null, 
        @SiteFrom nvarchar(50) = null, 
        @DeleteFlag int = null, 

        @StartIndex int = 0, 
        @PageSize int = 0, 
        @SPReturnType varchar(100) = null, 
        @SPAction varchar(100) = null 
AS
BEGIN
    SET NOCOUNT ON;
    IF @SPAction='Insert'
        BEGIN            
            INSERT INTO [CrawlSite]([WebName],[WebLink],[SectionName],[SectionIndexUrl],[SiteFrom],[DeleteFlag]) 
            VALUES(@WebName,@WebLink,@SectionName,@SectionIndexUrl,@SiteFrom,@DeleteFlag) 

            Declare @newID varchar(10) 
            Set @newID = SCOPE_IDENTITY() 
            If (@newID is NULL) 
                set @newID = ('-1') 
            select @newID as 'newID'
        END
    IF @SPAction='Delete'
        BEGIN            
            DELETE FROM [CrawlSite] WHERE [Id] = @Id
        END
    IF @SPAction='Update'
        BEGIN            
            UPDATE [CrawlSite] 
            SET [WebName] = @WebName,[WebLink] = @WebLink,[SectionName] = @SectionName,[SectionIndexUrl] = @SectionIndexUrl,[SiteFrom] = @SiteFrom,[DeleteFlag] = @DeleteFlag 
            WHERE [Id] = @Id
        END
    IF @SPAction='SelectPaging'
        BEGIN            
            SELECT [Id],[WebName],[WebLink],[SectionName],[SectionIndexUrl],[SiteFrom],[DeleteFlag] 
            FROM [CrawlSite]
        END
    IF @SPAction='SelectAll'
        BEGIN            
            SELECT [Id],[WebName],[WebLink],[SectionName],[SectionIndexUrl],[SiteFrom],[DeleteFlag] 
            FROM [CrawlSite]
        END
    IF @SPAction='Unique'
        BEGIN            
            Declare @isExist int 
            if @Id = 0 
                Set @isExist = (select count(*) from [CrawlSite] where [WebName] = @WebName and [WebLink] = @WebLink and [SectionName] = @SectionName and [SectionIndexUrl] = @SectionIndexUrl and [SiteFrom] = @SiteFrom and [DeleteFlag] = @DeleteFlag) 
            else 
                Set @isExist = (select count(*) from [CrawlSite] where [WebName] = @WebName and [WebLink] = @WebLink and [SectionName] = @SectionName and [SectionIndexUrl] = @SectionIndexUrl and [SiteFrom] = @SiteFrom and [DeleteFlag] = @DeleteFlag and [Id] != @Id) 
            if @isExist>0 
                raiserror('data existed already',16,1)
        END
END
GO
/****** Object:  StoredProcedure [dbo].[TableCrawlRules]    Script Date: 08/08/2016 17:04:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TableCrawlRules]
        @Id int = 0, 
        @RuleName nvarchar(50) = null, 
        @IsFinished int = null, 
        @SubmitType nvarchar(10) = null, 
        @TitleXpath nvarchar(200) = null, 
        @UrlXpath nvarchar(200) = null, 
        @UrlAttr nvarchar(200) = null, 
        @ContentXpath nvarchar(200) = null, 
        @CreateTimeXpath nvarchar(200) = null, 
        @CreateTimeReg nvarchar(200) = null, 
        @PictureXpath nvarchar(200) = null, 
        @PictureAttr nvarchar(200) = null, 
        @SourceXpath nvarchar(200) = null, 
        @SourceReg nvarchar(200) = null, 
        @AuthorXpath nvarchar(200) = null, 
        @AuthorReg nvarchar(200) = null, 
        @Version int = 1, 
        @CreateTime datetime = null, 
        @DeleteFlag int = 0, 

        @StartIndex int = 0, 
        @PageSize int = 0, 
        @SPReturnType varchar(100) = null, 
        @SPAction varchar(100) = null 
AS
BEGIN
    SET NOCOUNT ON;
    IF @SPAction='Insert'
        BEGIN            
            INSERT INTO [CrawlRules]([RuleName],[IsFinished],[SubmitType],[TitleXpath],[UrlXpath],[UrlAttr],[ContentXpath],[CreateTimeXpath],[CreateTimeReg],[PictureXpath],[PictureAttr],[SourceXpath],[SourceReg],[AuthorXpath],[AuthorReg],[Version],[CreateTime],[DeleteFlag]) 
            VALUES(@RuleName,@IsFinished,@SubmitType,@TitleXpath,@UrlXpath,@UrlAttr,@ContentXpath,@CreateTimeXpath,@CreateTimeReg,@PictureXpath,@PictureAttr,@SourceXpath,@SourceReg,@AuthorXpath,@AuthorReg,@Version,@CreateTime,@DeleteFlag) 

            Declare @newID varchar(10) 
            Set @newID = SCOPE_IDENTITY() 
            If (@newID is NULL) 
                set @newID = ('-1') 
            select @newID as 'newID'
        END
    IF @SPAction='Delete'
        BEGIN            
            DELETE FROM [CrawlRules] WHERE [Id] = @Id
        END
    IF @SPAction='Update'
        BEGIN            
            UPDATE [CrawlRules] 
            SET [RuleName] = @RuleName,[IsFinished] = @IsFinished,[SubmitType] = @SubmitType,[TitleXpath] = @TitleXpath,[UrlXpath] = @UrlXpath,[UrlAttr] = @UrlAttr,[ContentXpath] = @ContentXpath,[CreateTimeXpath] = @CreateTimeXpath,[CreateTimeReg] = @CreateTimeReg,[PictureXpath] = @PictureXpath,[PictureAttr] = @PictureAttr,[SourceXpath] = @SourceXpath,[SourceReg] = @SourceReg,[AuthorXpath] = @AuthorXpath,[AuthorReg] = @AuthorReg,[Version] = @Version,[CreateTime] = @CreateTime,[DeleteFlag] = @DeleteFlag 
            WHERE [Id] = @Id
        END
    IF @SPAction='SelectPaging'
        BEGIN            
            SELECT [Id],[RuleName],[IsFinished],[SubmitType],[TitleXpath],[UrlXpath],[UrlAttr],[ContentXpath],[CreateTimeXpath],[CreateTimeReg],[PictureXpath],[PictureAttr],[SourceXpath],[SourceReg],[AuthorXpath],[AuthorReg],[Version],[CreateTime],[DeleteFlag] 
            FROM [CrawlRules]
        END
    IF @SPAction='SelectAll'
        BEGIN            
            SELECT [Id],[RuleName],[IsFinished],[SubmitType],[TitleXpath],[UrlXpath],[UrlAttr],[ContentXpath],[CreateTimeXpath],[CreateTimeReg],[PictureXpath],[PictureAttr],[SourceXpath],[SourceReg],[AuthorXpath],[AuthorReg],[Version],[CreateTime],[DeleteFlag] 
            FROM [CrawlRules]
        END
    IF @SPAction='Unique'
        BEGIN            
            Declare @isExist int 
            if @Id = 0 
                Set @isExist = (select count(*) from [CrawlRules] where [RuleName] = @RuleName and [IsFinished] = @IsFinished and [SubmitType] = @SubmitType and [TitleXpath] = @TitleXpath and [UrlXpath] = @UrlXpath and [UrlAttr] = @UrlAttr and [ContentXpath] = @ContentXpath and [CreateTimeXpath] = @CreateTimeXpath and [CreateTimeReg] = @CreateTimeReg and [PictureXpath] = @PictureXpath and [PictureAttr] = @PictureAttr and [SourceXpath] = @SourceXpath and [SourceReg] = @SourceReg and [AuthorXpath] = @AuthorXpath and [AuthorReg] = @AuthorReg and [Version] = @Version and [CreateTime] = @CreateTime and [DeleteFlag] = @DeleteFlag) 
            else 
                Set @isExist = (select count(*) from [CrawlRules] where [RuleName] = @RuleName and [IsFinished] = @IsFinished and [SubmitType] = @SubmitType and [TitleXpath] = @TitleXpath and [UrlXpath] = @UrlXpath and [UrlAttr] = @UrlAttr and [ContentXpath] = @ContentXpath and [CreateTimeXpath] = @CreateTimeXpath and [CreateTimeReg] = @CreateTimeReg and [PictureXpath] = @PictureXpath and [PictureAttr] = @PictureAttr and [SourceXpath] = @SourceXpath and [SourceReg] = @SourceReg and [AuthorXpath] = @AuthorXpath and [AuthorReg] = @AuthorReg and [Version] = @Version and [CreateTime] = @CreateTime and [DeleteFlag] = @DeleteFlag and [Id] != @Id) 
            if @isExist>0 
                raiserror('data existed already',16,1)
        END
END
GO
/****** Object:  StoredProcedure [dbo].[TableCrawlResult]    Script Date: 08/08/2016 17:04:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TableCrawlResult]
        @Id int = 0, 
        @TaskId int = 0, 
        @WebName nvarchar(50) = null, 
        @SectionName nvarchar(50) = null, 
        @Title nvarchar(50) = null, 
        @Url nvarchar(200) = null, 
        @Content nvarchar(max) = null, 
        @CreateTime datetime = null, 
        @Picture nvarchar(200) = null, 
        @Source nvarchar(200) = null, 
        @Author nvarchar(200) = null, 
        @CollectTime datetime = null, 
        @IsCleaned int = null, 

        @StartIndex int = 0, 
        @PageSize int = 0, 
        @SPReturnType varchar(100) = null, 
        @SPAction varchar(100) = null 
AS
BEGIN
    SET NOCOUNT ON;
    IF @SPAction='Insert'
        BEGIN            
            INSERT INTO [CrawlResult]([TaskId],[WebName],[SectionName],[Title],[Url],[Content],[CreateTime],[Picture],[Source],[Author],[CollectTime],[IsCleaned]) 
            VALUES(@TaskId,@WebName,@SectionName,@Title,@Url,@Content,@CreateTime,@Picture,@Source,@Author,@CollectTime,@IsCleaned) 

            Declare @newID varchar(10) 
            Set @newID = SCOPE_IDENTITY() 
            If (@newID is NULL) 
                set @newID = ('-1') 
            select @newID as 'newID'
        END
    IF @SPAction='Delete'
        BEGIN            
            DELETE FROM [CrawlResult] WHERE [Id] = @Id
        END
    IF @SPAction='Update'
        BEGIN            
            UPDATE [CrawlResult] 
            SET [TaskId] = @TaskId,[WebName] = @WebName,[SectionName] = @SectionName,[Title] = @Title,[Url] = @Url,[Content] = @Content,[CreateTime] = @CreateTime,[Picture] = @Picture,[Source] = @Source,[Author] = @Author,[CollectTime] = @CollectTime,[IsCleaned] = @IsCleaned 
            WHERE [Id] = @Id
        END
    IF @SPAction='SelectPaging'
        BEGIN            
            SELECT [Id],[TaskId],[WebName],[SectionName],[Title],[Url],[Content],[CreateTime],[Picture],[Source],[Author],[CollectTime],[IsCleaned] 
            FROM [CrawlResult]
        END
    IF @SPAction='SelectAll'
        BEGIN            
            SELECT [Id],[TaskId],[WebName],[SectionName],[Title],[Url],[Content],[CreateTime],[Picture],[Source],[Author],[CollectTime],[IsCleaned] 
            FROM [CrawlResult]
        END
    IF @SPAction='Unique'
        BEGIN            
            Declare @isExist int 
            if @Id = 0 
                Set @isExist = (select count(*) from [CrawlResult] where [TaskId] = @TaskId and [WebName] = @WebName and [SectionName] = @SectionName and [Title] = @Title and [Url] = @Url and [Content] = @Content and [CreateTime] = @CreateTime and [Picture] = @Picture and [Source] = @Source and [Author] = @Author and [CollectTime] = @CollectTime and [IsCleaned] = @IsCleaned) 
            else 
                Set @isExist = (select count(*) from [CrawlResult] where [TaskId] = @TaskId and [WebName] = @WebName and [SectionName] = @SectionName and [Title] = @Title and [Url] = @Url and [Content] = @Content and [CreateTime] = @CreateTime and [Picture] = @Picture and [Source] = @Source and [Author] = @Author and [CollectTime] = @CollectTime and [IsCleaned] = @IsCleaned and [Id] != @Id) 
            if @isExist>0 
                raiserror('data existed already',16,1)
        END
END
GO
/****** Object:  Default [DF_CrawlResult_IsCleaned]    Script Date: 08/08/2016 17:04:35 ******/
ALTER TABLE [dbo].[CrawlResult] ADD  CONSTRAINT [DF_CrawlResult_IsCleaned]  DEFAULT ((0)) FOR [IsCleaned]
GO
/****** Object:  Default [DF_CrawlRules_Version]    Script Date: 08/08/2016 17:04:35 ******/
ALTER TABLE [dbo].[CrawlRules] ADD  CONSTRAINT [DF_CrawlRules_Version]  DEFAULT ((1)) FOR [Version]
GO
/****** Object:  Default [DF_CrawlRules_DeleteFlag]    Script Date: 08/08/2016 17:04:35 ******/
ALTER TABLE [dbo].[CrawlRules] ADD  CONSTRAINT [DF_CrawlRules_DeleteFlag]  DEFAULT ((0)) FOR [DeleteFlag]
GO
/****** Object:  Default [DF_SiteTokens_IsEnabled]    Script Date: 08/08/2016 17:04:35 ******/
ALTER TABLE [dbo].[SiteTokens] ADD  CONSTRAINT [DF_SiteTokens_IsEnabled]  DEFAULT ((1)) FOR [IsEnabled]
GO
