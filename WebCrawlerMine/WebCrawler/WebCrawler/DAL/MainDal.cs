﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Utility;
using Model;

namespace DAL
{
    public static class MainDal
    {
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["WebRescourseForVipProject"].ConnectionString;

        public static object DoDal(object obj, string spName, List<object> spActions, out string errMsg)
        {
            errMsg = "";
            var ds = new DataSet();
            var dt = new DataTable();
            ds.Tables.Add(dt);
            var o = new object();
            var conn = new SqlConnection(ConnectionString);
            conn.Open();
            SqlCommand cmd;
            SetSqlParameters.GetCmd(conn, out cmd, obj, spName);
            
            SqlTransaction myTran = conn.BeginTransaction();
            cmd.Transaction = myTran;
            try
            {
                foreach (var act in spActions)
                {
                    if (!cmd.Parameters.Contains("@SPAction"))
                    {
                        cmd.Parameters.Add(new SqlParameter("@SPAction", DbType.String));
                    }
                    cmd.Parameters["@SPAction"].Value = act.ToString();
                    switch (
                        (AppEnum.SPReturnTypes)(Enum.Parse(typeof(AppEnum.SPReturnTypes), cmd.Parameters["@SPReturnType"].Value.ToString())))
                    {
                        case AppEnum.SPReturnTypes.Object:
                            o = cmd.ExecuteScalar();
                            break;
                        case AppEnum.SPReturnTypes.Null:
                            cmd.ExecuteNonQuery();
                            break;
                        case AppEnum.SPReturnTypes.DataSet:
                            var select = new SqlDataAdapter(cmd);
                            if (((int)cmd.Parameters["@StartIndex"].Value).Equals(0) && ((int)cmd.Parameters["@PageSize"].Value).Equals(0))
                            {
                                ds.Tables.RemoveAt(0);
                                select.Fill(ds, "dt");
                            }
                            else
                            {
                                select.Fill((int)cmd.Parameters["@StartIndex"].Value, (int)cmd.Parameters["@PageSize"].Value, ds.Tables[0]);
                            }
                            break;
                    }
                }
                myTran.Commit();
                long newID;
                if (o != null && !Int64.TryParse(o.ToString(), out newID))
                {
                    return ds.Tables[0].Rows.Count > 0 ? ds : null;
                }
                return o;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
            }
            finally
            {
                conn.Close();
            }
            return null;
        }


        public static DataSet RunProcedure(string storedProcName, IDataParameter[] parameters, string tableName, int Times)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                DataSet dataSet = new DataSet();
                connection.Open();
                SqlDataAdapter sqlDA = new SqlDataAdapter();
                sqlDA.SelectCommand = BuildQueryCommand(connection, storedProcName, parameters);
                sqlDA.SelectCommand.CommandTimeout = Times;
                sqlDA.Fill(dataSet, tableName);
                connection.Close();
                return dataSet;
            }
        }

        private static SqlCommand BuildQueryCommand(SqlConnection connection, string storedProcName, IDataParameter[] parameters)
        {
            SqlCommand command = new SqlCommand(storedProcName, connection);
            command.CommandType = CommandType.StoredProcedure;
            foreach (SqlParameter parameter in parameters)
            {
                if (parameter != null)
                {
                    // 检查未分配值的输出参数,将其分配以DBNull.Value.
                    if ((parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Input) &&
                        (parameter.Value == null))
                    {
                        parameter.Value = DBNull.Value;
                    }
                    command.Parameters.Add(parameter);
                }
            }

            return command;
        }

        public static DataSet GetPager(int pageIndex, int pageSize, out int rcount, out int pcount, string table, string condition = "1=1", string orderBy = "Id desc", string fields = "*")
        {
            try
            {
                var param = new IDataParameter[]
                {
                    new SqlParameter("@PCount", SqlDbType.Int),
                    new SqlParameter("@RCount", SqlDbType.Int),
                    new SqlParameter("@sys_Table", table),
                    new SqlParameter("@sys_Key", "Id"),
                    new SqlParameter("@sys_Fields", fields),
                    new SqlParameter("@sys_Where", condition),
                    new SqlParameter("@sys_Order", orderBy),
                    new SqlParameter("@sys_Begin", 1),
                    new SqlParameter("@sys_PageIndex", pageIndex),
                    new SqlParameter("@sys_PageSize", pageSize)
                };
                param[0].Direction = ParameterDirection.Output;
                param[1].Direction = ParameterDirection.Output;

                var ds = RunProcedure("[dbo].[Page_v1]", param, "table", 15);
                pcount = int.Parse(param[0].Value.ToString());
                rcount = int.Parse(param[1].Value.ToString());
                return ds;
            }
            catch
            {
                rcount = 0;
                pcount = 0;
                return new DataSet();
            }
        }

        /// <summary>
        /// 执行SQL语句，返回影响的记录数
        /// </summary>
        /// <param name="sqlString">SQL语句</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSql(string sqlString)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(sqlString, connection))
                {
                    try
                    {
                        connection.Open();
                        int rows = cmd.ExecuteNonQuery();
                        return rows;
                    }
                    catch (System.Data.SqlClient.SqlException e)
                    {
                        connection.Close();
                        throw e;
                    }
                }
            }
        }
    }
}
