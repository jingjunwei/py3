﻿using System;

namespace Model
{ 
    public enum SPActionsCrawlSite 
    {
        Insert,
        Update,
        Delete,
        SelectPaging,
        SelectAll,
        Unique
    }

    public class ModelCrawlSite 
    { 
        /// <summary> 
        /// 自增主键  
        /// </summary> 
        public int Id { get; set; }

        /// <summary> 
        ///   
        /// </summary> 
        public int Uid { get; set; }

        /// <summary> 
        ///   
        /// </summary> 
        public string Type { get; set; }

        /// <summary> 
        /// 网站名称  
        /// </summary> 
        public string WebName {get; set;}

        /// <summary> 
        /// 网站首页链接  
        /// </summary> 
        public string WebLink {get; set;}

        /// <summary> 
        /// 模块名称  
        /// </summary> 
        public string SectionName {get; set;}

        /// <summary> 
        ///   
        /// </summary> 
        public int IndexDiff { get; set; }
        /// <summary> 
        /// 模块首页地址  
        /// </summary> 
        public string SectionIndexUrl { get; set; }

        /// <summary> 
        ///   
        /// </summary> 
        public string Start { get; set; }

        /// <summary> 
        ///   
        /// </summary> 
        public string Step { get; set; }

        /// <summary> 
        /// 采集请求来源  
        /// </summary> 
        public string SiteFrom {get; set;}

        /// <summary> 
        /// 删除标记  
        /// </summary> 
        public int DeleteFlag {get; set;}


        /// <summary>
        /// 读取数据的开始索引
        /// </summary>
        public int StartIndex { get; set; }

        /// <summary>
        /// 读取数据每页的条数
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 存储过程返回值类型
        /// </summary>
        public AppEnum.SPReturnTypes SPReturnType { get; set; }

        /// <summary>
        /// 存储过程动作
        /// </summary>
        public string SPAction {get; set;}
    }
} 

