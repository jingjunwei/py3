﻿using System;
using SolrNet.Attributes;

namespace Model
{
    public enum SPActionsCrawlResult
    {
        Insert,
        Update,
        Delete,
        SelectPaging,
        SelectAll,
        Unique
    }

    public class ModelCrawlResult
    {
        /// <summary> 
        /// 自增主键  
        /// </summary> 
        [SolrField("Id")]
        public int Id { get; set; }

        /// <summary> 
        ///   
        /// </summary> 
        [SolrField("Uid")]
        public int Uid { get; set; }

        /// <summary> 
        /// 采集规则ID  
        /// </summary> 
        [SolrField("TaskId")]
        public int TaskId { get; set; }

        /// <summary> 
        /// 网站名称  
        /// </summary> 
        [SolrField("WebName")]
        public string WebName { get; set; }

        /// <summary> 
        /// 网站模块  
        /// </summary> 
        [SolrField("SectionName")]
        public string SectionName { get; set; }

        /// <summary> 
        /// 资源类型  
        /// </summary> 
        [SolrField("Type")]
        public string Type { get; set; }

        /// <summary> 
        /// 标题  
        /// </summary> 
        [SolrField("Title")]
        public string Title { get; set; }

        /// <summary> 
        ///   
        /// </summary> 
        [SolrField("Keyword")]
        public string Keyword { get; set; }

        /// <summary> 
        /// 来源链接  
        /// </summary> 
        [SolrField("Url")]
        public string Url { get; set; }

        /// <summary> 
        /// 内容  
        /// </summary> 
        [SolrField("Content")]
        public string Content { get; set; }

        /// <summary> 
        /// 发布时间  
        /// </summary> 
        [SolrField("CreateTime")]
        public DateTime? CreateTime { get; set; }

        /// <summary> 
        /// 内容图片  
        /// </summary> 
        [SolrField("Picture")]
        public string Picture { get; set; }

        /// <summary> 
        /// 来源  
        /// </summary> 
        [SolrField("Source")]
        public string Source { get; set; }

        /// <summary> 
        /// 作者  
        /// </summary> 
        [SolrField("Author")]
        public string Author { get; set; }

        /// <summary> 
        /// 采集时间  
        /// </summary> 
        [SolrField("CollectTime")]
        public DateTime? CollectTime { get; set; }

        /// <summary> 
        /// 是否已编辑  
        /// </summary> 
        [SolrField("IsCleaned")]
        public int IsCleaned { get; set; }


        /// <summary>
        /// 读取数据的开始索引
        /// </summary>
        public int StartIndex { get; set; }

        /// <summary>
        /// 读取数据每页的条数
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 存储过程返回值类型
        /// </summary>
        public AppEnum.SPReturnTypes SPReturnType { get; set; }

        /// <summary>
        /// 存储过程动作
        /// </summary>
        public string SPAction { get; set; }
    }
}
