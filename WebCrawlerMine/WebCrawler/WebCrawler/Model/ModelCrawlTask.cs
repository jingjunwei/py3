﻿using System;

namespace Model
{ 
    public enum SPActionsCrawlTask 
    {
        Insert,
        Update,
        Delete,
        SelectPaging,
        SelectAll,
        Unique
    }

    public class ModelCrawlTask 
    { 
        /// <summary> 
        /// 自增主键  
        /// </summary> 
        public int Id {get; set;}

        /// <summary> 
        ///   
        /// </summary> 
        public int Uid { get; set; }

        /// <summary> 
        /// 采集规则  
        /// </summary> 
        public int RuleId {get; set;}

        /// <summary> 
        ///   
        /// </summary> 
        public int SiteSectionId {get; set;}

        /// <summary> 
        /// 第一页URL  
        /// </summary> 
        public string PageUrlWithParam {get; set;}

        /// <summary> 
        /// 任务启动与否  
        /// </summary> 
        public int IsStarted { get; set; }

        /// <summary> 
        ///   
        /// </summary> 
        public int UseProxy { get; set; }

        /// <summary>
        /// 读取数据的开始索引
        /// </summary>
        public int StartIndex { get; set; }

        /// <summary>
        /// 读取数据每页的条数
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 存储过程返回值类型
        /// </summary>
        public AppEnum.SPReturnTypes SPReturnType { get; set; }

        /// <summary>
        /// 存储过程动作
        /// </summary>
        public string SPAction {get; set;}
    }
} 

