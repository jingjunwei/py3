﻿using System;

namespace Model
{ 
    public enum SPActionsSiteTokens 
    {
        Insert,
        Update,
        Delete,
        SelectPaging,
        SelectAll,
        Unique
    }

    public class ModelSiteTokens 
    { 
        /// <summary> 
        /// 自增主键  
        /// </summary> 
        public int Id {get; set;}

        /// <summary> 
        /// Token接口使用  
        /// </summary> 
        public string Token {get; set;}

        /// <summary> 
        /// 站点名称  
        /// </summary> 
        public string SiteName {get; set;}

        /// <summary> 
        /// IP限制  
        /// </summary> 
        public string Ip {get; set;}

        /// <summary> 
        /// 是否启用  
        /// </summary> 
        public int IsEnabled { get; set; }


        /// <summary>
        /// 读取数据的开始索引
        /// </summary>
        public int StartIndex { get; set; }

        /// <summary>
        /// 读取数据每页的条数
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 存储过程返回值类型
        /// </summary>
        public AppEnum.SPReturnTypes SPReturnType { get; set; }

        /// <summary>
        /// 存储过程动作
        /// </summary>
        public string SPAction {get; set;}
    }
} 

