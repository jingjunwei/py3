﻿using System;

namespace Model
{
    public enum SPActionsCrawlRules
    {
        Insert,
        Update,
        Delete,
        SelectPaging,
        SelectAll,
        Unique
    }

    public class ModelCrawlRules
    {
        /// <summary> 
        /// 自增主键  
        /// </summary> 
        public int Id { get; set; }

        /// <summary> 
        ///   
        /// </summary> 
        public int Sid { get; set; }

        /// <summary> 
        /// 规则名称  
        /// </summary> 
        public string RuleName { get; set; }

        /// <summary> 
        /// 规则完成完成1 完成 0未完成 2规则异常(第一次采集数据列表为空)  
        /// </summary> 
        public int IsFinished { get; set; }

        /// <summary> 
        /// 采集方式:POST GET  
        /// </summary> 
        public string SubmitType { get; set; }

        /// <summary> 
        /// 标题Xpath  
        /// </summary> 
        public string TitleXpath { get; set; }

        /// <summary> 
        /// 来源链接Xpath  
        /// </summary> 
        public string UrlXpath { get; set; }

        /// <summary> 
        ///   
        /// </summary> 
        public string UrlAttr { get; set; }

        /// <summary> 
        /// 内容Xpath  
        /// </summary> 
        public string ContentXpath { get; set; }

        /// <summary> 
        ///   
        /// </summary> 
        public string KeywordXpath { get; set; }

        /// <summary> 
        ///   
        /// </summary> 
        public string KeywordSpliter { get; set; }

        /// <summary> 
        /// 发布时间Xpath  
        /// </summary> 
        public string CreateTimeXpath { get; set; }

        /// <summary> 
        ///   
        /// </summary> 
        public string CreateTimeReg { get; set; }

        /// <summary> 
        /// 内页图片Xpath  
        /// </summary> 
        public string PictureXpath { get; set; }

        /// <summary> 
        ///   
        /// </summary> 
        public string PictureAttr { get; set; }

        /// <summary> 
        /// 来源Xpath  
        /// </summary> 
        public string SourceXpath { get; set; }

        /// <summary> 
        ///   
        /// </summary> 
        public string SourceReg { get; set; }

        /// <summary> 
        ///   
        /// </summary> 
        public string AuthorXpath { get; set; }

        /// <summary> 
        ///   
        /// </summary> 
        public string AuthorReg { get; set; }

        /// <summary> 
        /// 规则版本  
        /// </summary> 
        public int Version { get; set; }

        /// <summary> 
        ///   
        /// </summary> 
        public DateTime? CreateTime { get; set; }

        /// <summary> 
        /// 删除标记  
        /// </summary> 
        public int DeleteFlag { get; set; }


        /// <summary>
        /// 读取数据的开始索引
        /// </summary>
        public int StartIndex { get; set; }

        /// <summary>
        /// 读取数据每页的条数
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 存储过程返回值类型
        /// </summary>
        public AppEnum.SPReturnTypes SPReturnType { get; set; }

        /// <summary>
        /// 存储过程动作
        /// </summary>
        public string SPAction { get; set; }
    }
}
