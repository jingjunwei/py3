﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Web.UI.WebControls;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using HtmlAgilityPack;

namespace Utility
{
    /// <summary>
    /// 名    称：HTTP请求帮助类
    /// 作    者：付正勇
    /// 创建时间：2014-1-3 12:17:10
    /// 联系方式：电话[15086925108],邮件[fxy6781349@qq.com]
    /// 描    述：
    /// </summary>
    public class HttpHelper
    {
        private CookieContainer _cookie = new CookieContainer();
        private int _delayTime;
        private string _lastUrl = String.Empty;
        private WebProxy _proxy;
        private int _timeOut = 0x1d4c0;
        private int _tryTimes = 3;
        private string reqUserAgent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; SLCC1; .NET CLR 2.0.50727; .NET CLR 3.0.04506; InfoPath.2)";


        public HttpHelper()
        {
            //SetProxy("192.168.2.2", 80, "", "");
        }

        public HttpHelper(string name)
        {
            // 读取coolie 信息
            string path = @"cookies\" + name + ".cookie";
            if (File.Exists(path))
            {
                using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    this._cookie = (CookieContainer)formatter.Deserialize(stream);
                }
            }
        }

        // 设置延迟时间
        private void DelaySomeTime()
        {
            if (this._delayTime > 0)
            {
                Random random = new Random();
                int millisecondsTimeout = (this._delayTime * 0x3e8) + random.Next(0x3e8);
                Thread.Sleep(millisecondsTimeout);
            }
        }

        // 下载资源文件
        public void DownLoad(string url, string localfile)
        {
            new WebClient().DownloadFile(url, localfile);
        }

        // 使用指定的编码对象对url字符串进行编码
        public static string EncodePostDate(string data, string codename)
        {
            data = HttpUtility.UrlEncode(data, Encoding.GetEncoding(codename));
            return data;
        }

        // 将字符串从gb2312 转换成unicode
        public static string GB2312ToUnicode(string inputStr)
        {
            StringBuilder builder = new StringBuilder();
            foreach (char c in inputStr)
            {
                builder.Append(GetUnicode(c.ToString()));
            }
            return builder.ToString();
        }

        // 获取字符的unicode 格式
        private static string GetUnicode(string s)
        {
            byte[] bytes = new byte[2];
            bytes = Encoding.Unicode.GetBytes(s);
            int num = bytes[0];
            int num2 = bytes[1];
            return (@"\u" + num2.ToString("x") + num.ToString("x"));
        }

        // get方式访问
        public string Get(string url)
        {
            string str = this.Get(url, "utf-8", this._lastUrl);
            this._lastUrl = url;
            return str;
        }

        // get方式访问
        public string Get(string url, string encode)
        {
            string str = this.Get(url, encode, this._lastUrl);
            this._lastUrl = url;
            return str;
        }

        // Get方式访问
        public string Get(string url, string encode, string referer)
        {
            int num = this._tryTimes;
            HttpWebRequest request = null;
            HttpWebResponse response = null;
            StreamReader reader = null;
            while (num-- > 0)
            {
                try
                {
                    this.DelaySomeTime();
                    request = (HttpWebRequest)WebRequest.Create(url);
                    request.UserAgent = this.reqUserAgent;
                    request.CookieContainer = this._cookie;
                    request.Referer = referer;
                    request.Method = "GET";
                    request.Timeout = this._timeOut;
                    if (this._proxy != null && this._proxy.Credentials != null)
                    {
                        request.UseDefaultCredentials = true;
                    }
                    request.Proxy = this._proxy;
                    response = (HttpWebResponse)request.GetResponse();
                    reader = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding(encode));
                    return reader.ReadToEnd();
                }
                catch (Exception)
                {
                    continue;
                }
                finally
                {
                    if (request != null)
                    {
                        request.Abort();
                    }
                    if (response != null)
                    {
                        response.Close();
                    }
                    if (reader != null)
                    {
                        reader.Close();
                    }
                }

            }
            return String.Empty;
        }

        public string GetStatusCode(string url)
        {
            int num = this._tryTimes;
            HttpWebRequest request = null;
            HttpWebResponse response = null;
            StreamReader reader = null;
            while (num-- > 0)
            {
                try
                {
                    this.DelaySomeTime();
                    request = (HttpWebRequest)WebRequest.Create(url);
                    request.UserAgent = this.reqUserAgent;
                    request.CookieContainer = this._cookie;
                    request.Referer = "";
                    request.Method = "GET";
                    request.Timeout = this._timeOut;
                    if (this._proxy != null && this._proxy.Credentials != null)
                    {
                        request.UseDefaultCredentials = true;
                    }
                    request.Proxy = this._proxy;
                    response = (HttpWebResponse)request.GetResponse();
                    return response.StatusCode.ToString();
                }
                catch (Exception)
                {
                    continue;
                }
                finally
                {
                    if (request != null)
                    {
                        request.Abort();
                    }
                    if (response != null)
                    {
                        response.Close();
                    }
                    if (reader != null)
                    {
                        reader.Close();
                    }
                }

            }
            return String.Empty;
        }


        // 取出_cookie
        public CookieContainer GetCookie()
        {
            return this._cookie;
        }

        public string Post(string url, string content)
        {
            string str = this.Post(url, content, "utf-8", this._lastUrl);
            this._lastUrl = url;
            return str;
        }

        public string Post(string url, string content, string encode)
        {
            string str = this.Post(url, content, encode, this._lastUrl);
            this._lastUrl = url;
            return str;
        }

        // post访问
        public string Post(string url, string content, string encode, string referer)
        {

            int num = this._tryTimes;
            HttpWebRequest request = null;
            HttpWebResponse response = null;
            StreamReader reader = null;
            Stream requestStream = null;
            while (num-- > 0)
            {
                try
                {
                    this.DelaySomeTime(); // sleep 一会
                    request = (HttpWebRequest)WebRequest.Create(url);
                    request.UserAgent = this.reqUserAgent;
                    request.CookieContainer = this._cookie;
                    request.Referer = referer;
                    byte[] bytes = Encoding.GetEncoding(encode).GetBytes(content);
                    request.Method = "POST";
                    request.Timeout = this._timeOut;
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.ContentLength = bytes.Length;
                    if (this._proxy != null && this._proxy.Credentials != null)
                    {
                        request.UseDefaultCredentials = true;
                    }
                    request.Proxy = this._proxy;
                    requestStream = request.GetRequestStream();
                    requestStream.Write(bytes, 0, bytes.Length);
                    requestStream.Close();
                    response = (HttpWebResponse)request.GetResponse();
                    reader = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding(encode));
                    return reader.ReadToEnd();
                }
                catch (Exception)
                {
                    continue;
                }
                finally
                {
                    if (request != null)
                    {
                        request.Abort();
                    }
                    if (response != null)
                    {
                        response.Close();
                    }
                    if (reader != null)
                    {
                        reader.Close();
                    }
                    if (requestStream != null)
                    {
                        requestStream.Close();
                    }
                }

            }
            return String.Empty;
        }

        // 保存cookie
        public void SaveCookie(string name)
        {

            string path = @"cookies\" + name + ".cookie";
            if (!File.Exists("cookies"))
            {
                Directory.CreateDirectory("cookies");
            }
            using (FileStream steam = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                new BinaryFormatter().Serialize(steam, this._cookie);
            }
        }


        // 设置延迟时间
        public void SetDelayConnect(int delayTime)
        {
            this._delayTime = delayTime;
        }

        // 设置代理服务器
        public void SetProxy(string server, int port, string username, string password)
        {
            if (!String.IsNullOrEmpty(server) && port > 0)
            {

                this._proxy = new WebProxy(server, port);
                if (!String.IsNullOrEmpty(username) && !String.IsNullOrEmpty(password))
                {
                    this._proxy.Credentials = new NetworkCredential(username, password);
                    this._proxy.BypassProxyOnLocal = true;
                }
            }
        }

        // 设置过期时间 
        public void SetTimeOut(int timeout)
        {
            if (timeout > 0)
            {
                this._timeOut = timeout;
            }
        }

        public void SetTryTimes(int times)
        {
            if (times > 0)
            {
                this._tryTimes = times;
            }
        }
        private static readonly Regex IpRegex = new Regex(@"^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$", RegexOptions.Compiled);

        public static string GetIP()
        {
            string result = HttpContext.Current.Request.Headers["CDN-SRC-IP"];
            if (string.IsNullOrEmpty(result))
            {
                result = HttpContext.Current.Request.ServerVariables["HTTP_X_REAL_IP"];
            }
            if (string.IsNullOrEmpty(result))
            {
                result = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (!string.IsNullOrEmpty(result))
                {
                    if (result.IndexOf(",") > 0)
                    {
                        string[] ips = result.Split(',');
                        result = ips[0].Trim();
                    }
                    if (!IpRegex.IsMatch(result) || result.IndexOf("127.") == 0 || result.IndexOf("192.168") == 0 || result.IndexOf("172.") == 0 || result.IndexOf("10.") == 0)
                    {
                        result = HttpContext.Current.Request.ServerVariables["HTTP_X_REAL_IP"];
                    }
                }
            }

            if (string.IsNullOrEmpty(result))
                result = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

            if (string.IsNullOrEmpty(result) || !IpRegex.IsMatch(result))
                return "127.0.0.1";

            return result;
        }
        public static string GetAgent()
        {
            return HttpContext.Current.Request.UserAgent;
        }

        public static string GetSessionId()
        {
            return HttpContext.Current.Session.SessionID;
        }

        public static string URLGBEncode(string url)
        {
            string result = String.Empty;
            Encoding gb2312 = Encoding.GetEncoding("gb2312");
            result = HttpUtility.UrlEncode(url, gb2312);
            return result;
        }

        ///   <summary>
        ///   去除HTML标记
        ///   </summary>
        ///   <returns>已经去除后的文字</returns>
        public static string NoHtml(string htmlstring)
        {
            //删除脚本
            htmlstring = Regex.Replace(htmlstring, @"<script[^>]*?>(.|\n)*?</script>", "",
            RegexOptions.IgnoreCase);
            htmlstring = Regex.Replace(htmlstring, @"<script[^>]*?>(.|\n)*?</script>", "",
            RegexOptions.IgnoreCase);
            htmlstring = Regex.Replace(htmlstring, @"<link[^>]*?/>", "",
            RegexOptions.IgnoreCase);
            htmlstring = Regex.Replace(htmlstring, @"<head[^>]*?>(.|\n)*?</head>", "",
            RegexOptions.IgnoreCase);
            htmlstring = Regex.Replace(htmlstring, @"<meta[^>]*?/>", "",
            RegexOptions.IgnoreCase);
            htmlstring = Regex.Replace(htmlstring, @"<img[^>]*?/>", "<img style='background:#000;color:#fff;' alt='此处图片' />",
            RegexOptions.IgnoreCase);
            ////删除HTML 
            //htmlstring = Regex.Replace(htmlstring, @"<(.[^>]*)>", "",
            //RegexOptions.IgnoreCase);
            htmlstring = Regex.Replace(htmlstring, @"([\r\n])[\s]+", "",
            RegexOptions.IgnoreCase);
            htmlstring = Regex.Replace(htmlstring, @"–>", "", RegexOptions.IgnoreCase);
            htmlstring = Regex.Replace(htmlstring, @"<!–.*", "", RegexOptions.IgnoreCase);
            //htmlstring = Regex.Replace(htmlstring, @"&(quot|#34);", "\"",
            //RegexOptions.IgnoreCase);
            //htmlstring = Regex.Replace(htmlstring, @"&(amp|#38);", "&",
            //RegexOptions.IgnoreCase);
            //htmlstring = Regex.Replace(htmlstring, @"&(lt|#60);", "<",
            //RegexOptions.IgnoreCase);
            //htmlstring = Regex.Replace(htmlstring, @"&(gt|#62);", ">",
            //RegexOptions.IgnoreCase);
            //htmlstring = Regex.Replace(htmlstring, @"&(nbsp|#160);", "   ",
            //RegexOptions.IgnoreCase);
            //htmlstring = Regex.Replace(htmlstring, @"&(iexcl|#161);", "\xa1", RegexOptions.IgnoreCase);
            //htmlstring = Regex.Replace(htmlstring, @"&(cent|#162);", "\xa2", RegexOptions.IgnoreCase);
            //htmlstring = Regex.Replace(htmlstring, @"&(pound|#163);", "\xa3", RegexOptions.IgnoreCase);
            //htmlstring = Regex.Replace(htmlstring, @"&(copy|#169);", "\xa9", RegexOptions.IgnoreCase);
            //htmlstring = Regex.Replace(htmlstring, @"&#(\d+);", "", RegexOptions.IgnoreCase);
            //htmlstring.Replace("<", "");
            //htmlstring.Replace(">", "");
            //htmlstring.Replace("\r\n", "");
            //htmlstring = HttpContext.Current.Server.HtmlEncode(htmlstring).Trim();
            return htmlstring;
        }

        public static string GetBodyWithoutScript(string htmlstring, bool removeTag = false, bool removeDesc = true, bool removeScript = true)
        {
            if (removeTag)
            {//删除脚本
                if (removeScript) htmlstring = Regex.Replace(htmlstring, @"<script[^>]*?>(.|\n)*?</script>", "",
                RegexOptions.IgnoreCase);
                htmlstring = Regex.Replace(htmlstring, @"<style[^>]*?>(.|\n)*?</style>", "",
                RegexOptions.IgnoreCase);
                htmlstring = Regex.Replace(htmlstring, @"<link[^>]*?/?>", "",
                RegexOptions.IgnoreCase);
                //htmlstring = Regex.Replace(htmlstring, @"<head[^>]*?>(.|\n)*?</head>", "",
                //RegexOptions.IgnoreCase);
                htmlstring = Regex.Replace(htmlstring, @"<meta[^>]*?/?>", "",
                RegexOptions.IgnoreCase);
                htmlstring = Regex.Replace(htmlstring, @"<img[^>]*?/?>", "<img style='background:#000;color:#fff;' alt='此处图片' />",
                RegexOptions.IgnoreCase);
                htmlstring = Regex.Replace(htmlstring, @"<iframe[^>]*?>.*?</iframe>", "",
                RegexOptions.IgnoreCase);
                htmlstring = Regex.Replace(htmlstring, @"<head[^>]*?>(.|\n)*?</head>", "",
                 RegexOptions.IgnoreCase);
                htmlstring = Regex.Replace(htmlstring, @"<title[^>]*?>.*?</title>", "",
                 RegexOptions.IgnoreCase);
                if (removeDesc) htmlstring = Regex.Replace(htmlstring, @"<!--[\w\W\r\n]*?-->", "",
                 RegexOptions.IgnoreCase);
            }
            HtmlDocument htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(htmlstring);

            if (htmlDoc.DocumentNode != null)
            {
                HtmlNode bodyNode = htmlDoc.DocumentNode.SelectSingleNode("//body");

                if (bodyNode != null)
                {
                    return bodyNode.InnerHtml;
                }
            }

            return htmlstring;
        }
        /// <summary>
        /// Check Html Page Encoding
        /// site: http://www.cnphp.info/howto-solve-encoding-problem-in-csharp.html
        /// author: <a href="http://www.cnphp.info">freemouse</a>
        /// </summary>
        /// <param name="s">NetWork Stream</param>
        /// <param name="enc">Ecnoding in</param>
        /// <returns>return read content from stream s</returns>
        public static string CheckEncoding(Stream s, ref Encoding enc)
        {
            string pattern = "<meta.+?charset\\s*=\\s*(?<charset>[-\\w]+)";
            Regex charSetPattern = new Regex(pattern, RegexOptions.IgnoreCase);
            StringBuilder strBuilder = new StringBuilder();
            StringBuilder retBuilder = new StringBuilder();
            string line = "";
            while (ReadLine(s, strBuilder))
            {
                line = strBuilder.ToString();
                if (line.Trim().StartsWith("<body")) break;
                strBuilder.Remove(0, strBuilder.Length);
                retBuilder.AppendLine(line);
                Match m = charSetPattern.Match(line);
                if (m.Success)
                {
                    string strEnc = m.Groups["charset"].Value;
                    try
                    {
                        enc = Encoding.GetEncoding(strEnc);
                        break;
                    }
                    catch (Exception)
                    {
                        //throw new Exception(err.Message);
                        return retBuilder.ToString();
                    }
                }
            }
            if (enc != null)
            {
                return enc.GetString(Encoding.GetEncoding("ISO-8859-1").GetBytes(retBuilder.ToString()));
            }

            return retBuilder.ToString();
        }

        /// <summary>
        /// Read a line from NetwrokStream
        /// site: http://www.cnphp.info/howto-solve-encoding-problem-in-csharp.html
        /// author: <a href="http://www.cnphp.info">freemouse</a>
        /// </summary>
        /// <param name="s">Stream to Read</param>
        /// <param name="strBuilder">Line storage</param>
        /// <returns>if is end of Stream return false</returns>
        private static bool ReadLine(Stream s, StringBuilder strBuilder)
        {
            int iChar = -1;
            do
            {
                if (iChar != -1)
                {
                    if (iChar <= 65 && iChar >= 90)
                    {
                        strBuilder.Append(iChar + 32);
                    }
                    else
                        strBuilder.Append((char)iChar);
                }

            } while ((iChar = s.ReadByte()) != (int)'\n' && iChar != -1);

            if (iChar != -1) return true;

            return false;
        }

        public static string EncodingConvert(string fromString, Encoding fromEncoding, Encoding toEncoding)
        {
            byte[] fromBytes = fromEncoding.GetBytes(fromString);
            byte[] toBytes = Encoding.Convert(fromEncoding, toEncoding, fromBytes);

            string toString = toEncoding.GetString(toBytes);
            return toString;
        }
    }
}
