﻿using System;
using System.Collections.Generic;
using System.Data;
using DAL;
using Model;

namespace BLL
{
    public static class BllCrawlSite 
    {
        /// <summary> 
        /// Select Paging Data 
        /// </summary> 
        /// <param name="obj">TableCrawlSite Model</param> 
        /// <param name="errMsg">错误信息</param> 
        /// <returns>DataSet</returns> 
        public static DataSet SelectPaging(ModelCrawlSite obj, out string errMsg) 
        {
            object returnObj = MainDal.DoDal(obj, "TableCrawlSite", new List<object> { SPActionsCrawlSite.SelectPaging }, out errMsg); 
            if (!string.IsNullOrEmpty(errMsg) || returnObj == null) 
            {
                return null;
            }
            return (DataSet)returnObj;
        }

        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="pageIdx"></param>
        /// <param name="pageSize"></param>
        /// <param name="rcount"></param>
        /// <param name="pcount"></param>
        /// <returns></returns>
        public static DataSet GetPager(int pageIdx, int pageSize, out int rcount, out int pcount)
        {
            return MainDal.GetPager(pageIdx, pageSize, out rcount, out pcount, "CrawlSite");
        }

        /// <summary> 
        /// Select All Data 
        /// </summary> 
        /// <param name="obj">TableCrawlSite Model</param> 
        /// <param name="errMsg">错误信息</param> 
        /// <returns>DataSet</returns> 
        public static DataSet SelectAll(ModelCrawlSite obj, out string errMsg) 
        {
            object returnObj = MainDal.DoDal(obj, "TableCrawlSite", new List<object> { SPActionsCrawlSite.SelectAll }, out errMsg); 
            if (!string.IsNullOrEmpty(errMsg)) 
            {
                return null;
            }
            return (DataSet)returnObj;
        }

        /// <summary> 
        /// Insert Unique Data 
        /// </summary> 
        /// <param name="obj">TableCrawlSite Model</param> 
        /// <param name="errMsg">错误信息</param> 
        /// <returns>long</returns> 
        public static long InsertUnique(ModelCrawlSite obj, out string errMsg) 
        {
            object returnObj = MainDal.DoDal(obj, "TableCrawlSite", new List<object> { SPActionsCrawlSite.Unique, SPActionsCrawlSite.Insert }, out errMsg); 
            if (!string.IsNullOrEmpty(errMsg)) 
            {
                return -1;
            }
            return Convert.ToInt64(returnObj);
        }

        /// <summary> 
        /// Update Unique Data 
        /// </summary> 
        /// <param name="obj">TableCrawlSite Model</param> 
        /// <param name="errMsg">错误信息</param> 
        public static void UpdateUnique(ModelCrawlSite obj, out string errMsg) 
        {
            MainDal.DoDal(obj, "TableCrawlSite", new List<object> { SPActionsCrawlSite.Unique, SPActionsCrawlSite.Update }, out errMsg); 
        }

        /// <summary> 
        /// Delete Data 
        /// </summary> 
        /// <param name="obj">TableCrawlSite Model</param> 
        /// <param name="errMsg">错误信息</param> 
        public static void Delete(ModelCrawlSite obj, out string errMsg) 
        {
            MainDal.DoDal(obj, "TableCrawlSite", new List<object> { SPActionsCrawlSite.Delete }, out errMsg); 
        }

        /// <summary> 
        /// get by id
        /// </summary> 
        /// <param name="obj">TableCrawlResult Model</param> 
        /// <param name="errMsg">错误信息</param> 
        public static ModelCrawlSite GeById(string id)
        {
            int p;
            int r;
            var list = MainDal.GetPager(1, 1, out r, out p, "CrawlSite", "Id = " + id);
            var tb = list.Tables[0];
            if (tb.Rows.Count > 0)
            {
                return
                    Newtonsoft.Json.JsonConvert.DeserializeObject<List<ModelCrawlSite>>(
                        Newtonsoft.Json.JsonConvert.SerializeObject(tb))[0];
            }
            return null;
        }
    }
}

