﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DAL;

namespace BLL
{
    public static class BllBaseService
    {
        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="pageIdx"></param>
        /// <param name="pageSize"></param>
        /// <param name="rcount"></param>
        /// <param name="pcount"></param>
        /// <param name="table"></param>
        /// <param name="condition"></param>
        /// <param name="orderBy"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public static DataSet GetPager(int pageIdx, int pageSize, out int rcount, out int pcount, string table, string condition = "1=1", string orderBy = "Id desc", string fields = "*")
        {
            return MainDal.GetPager(pageIdx, pageSize, out rcount, out pcount, table, condition, orderBy, fields);
        }

        /// <summary>
        /// 执行SQL 返回影响行数
        /// </summary>
        /// <param name="sqlString"></param>
        /// <returns></returns>
        public static int ExecuteSql(string sqlString)
        {
            return MainDal.ExecuteSql(sqlString);
        }
    }
}
