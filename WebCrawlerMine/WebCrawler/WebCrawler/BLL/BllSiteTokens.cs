﻿using System;
using System.Collections.Generic;
using System.Data;
using DAL;
using Model;

namespace BLL
{
    public static class BllSiteTokens
    {
        /// <summary> 
        /// Select Paging Data 
        /// </summary> 
        /// <param name="obj">TableSiteTokens Model</param> 
        /// <param name="errMsg">错误信息</param> 
        /// <returns>DataSet</returns> 
        public static DataSet SelectPaging(ModelSiteTokens obj, out string errMsg)
        {
            object returnObj = MainDal.DoDal(obj, "TableSiteTokens", new List<object> { SPActionsSiteTokens.SelectPaging }, out errMsg);
            if (!string.IsNullOrEmpty(errMsg) || returnObj == null)
            {
                return null;
            }
            return (DataSet)returnObj;
        }

        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="pageIdx"></param>
        /// <param name="pageSize"></param>
        /// <param name="rcount"></param>
        /// <param name="pcount"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        public static DataSet GetPager(int pageIdx, int pageSize, out int rcount, out int pcount, string condition)
        {
            return condition == "" ? MainDal.GetPager(pageIdx, pageSize, out rcount, out pcount, "SiteTokens") : MainDal.GetPager(pageIdx, pageSize, out rcount, out pcount, "SiteTokens", condition);
        }

        /// <summary> 
        /// Select All Data 
        /// </summary> 
        /// <param name="obj">TableSiteTokens Model</param> 
        /// <param name="errMsg">错误信息</param> 
        /// <returns>DataSet</returns> 
        public static DataSet SelectAll(ModelSiteTokens obj, out string errMsg)
        {
            object returnObj = MainDal.DoDal(obj, "TableSiteTokens", new List<object> { SPActionsSiteTokens.SelectAll }, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                return null;
            }
            return (DataSet)returnObj;
        }

        /// <summary> 
        /// Insert Unique Data 
        /// </summary> 
        /// <param name="obj">TableSiteTokens Model</param> 
        /// <param name="errMsg">错误信息</param> 
        /// <returns>long</returns> 
        public static long InsertUnique(ModelSiteTokens obj, out string errMsg)
        {
            object returnObj = MainDal.DoDal(obj, "TableSiteTokens", new List<object> { SPActionsSiteTokens.Unique, SPActionsSiteTokens.Insert }, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                return -1;
            }
            return Convert.ToInt64(returnObj);
        }

        /// <summary> 
        /// Update Unique Data 
        /// </summary> 
        /// <param name="obj">TableSiteTokens Model</param> 
        /// <param name="errMsg">错误信息</param> 
        public static void UpdateUnique(ModelSiteTokens obj, out string errMsg)
        {
            MainDal.DoDal(obj, "TableSiteTokens", new List<object> { SPActionsSiteTokens.Unique, SPActionsSiteTokens.Update }, out errMsg);
        }

        /// <summary> 
        /// Delete Data 
        /// </summary> 
        /// <param name="obj">TableSiteTokens Model</param> 
        /// <param name="errMsg">错误信息</param> 
        public static void Delete(ModelSiteTokens obj, out string errMsg)
        {
            MainDal.DoDal(obj, "TableSiteTokens", new List<object> { SPActionsSiteTokens.Delete }, out errMsg);
        }

        /// <summary> 
        /// 获取by id
        /// </summary> 
        /// <param name="obj">TableCrawlResult Model</param> 
        /// <param name="errMsg">错误信息</param> 
        public static ModelSiteTokens GeById(string id)
        {
            int p;
            int r;
            var list = MainDal.GetPager(1, 1, out r, out p, "SiteTokens", "Id = " + id);
            var tb = list.Tables[0];
            if (tb.Rows.Count > 0)
            {
                return
                    Newtonsoft.Json.JsonConvert.DeserializeObject<List<ModelSiteTokens>>(
                        Newtonsoft.Json.JsonConvert.SerializeObject(tb))[0];
            }
            return null;
        }

        public static ModelSiteTokens LoginCheck(string name, string token)
        {
            try
            {
                int p;
                int r;
                var list = MainDal.GetPager(1, 1, out r, out p, "SiteTokens", string.Format("[SiteName] = '{0}' AND [Token] = '{1}' AND [IsEnabled] = 1", name, token));
                var tb = list.Tables[0];
                if (tb.Rows.Count > 0)
                {
                    return
                        Newtonsoft.Json.JsonConvert.DeserializeObject<List<ModelSiteTokens>>(
                            Newtonsoft.Json.JsonConvert.SerializeObject(tb))[0];
                }

            }
            catch (Exception ex)
            {
                // ignored
            }
            return null;
        }
    }
}

