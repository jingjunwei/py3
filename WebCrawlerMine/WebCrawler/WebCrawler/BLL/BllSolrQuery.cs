﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Microsoft.Practices.ServiceLocation;
using Model;
using SolrNet;
using SolrNet.Commands.Parameters;
using SolrNet.DSL;
using SolrNet.Exceptions;

namespace BLL
{
    public static class BllSolrQuery
    {
        /// <summary>
        /// 期刊高级检索
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="key"></param>
        /// <param name="title"></param>
        /// <param name="sectionName"></param>
        /// <param name="type"></param>
        /// <param name="isclean"></param>
        /// <param name="stime"></param>
        /// <param name="etime"></param>
        /// <param name="rcount"></param>
        /// <param name="pcount"></param>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <param name="sortField"></param>
        /// <param name="sort"></param>
        /// <param name="keyword"></param>
        /// <param name="content"></param>
        /// <param name="source"></param>
        /// <param name="author"></param>
        /// <param name="webName"></param>
        /// <param name="uid"></param>
        /// <param name="ctimes"></param>
        /// <param name="ctimee"></param>
        /// <param name="solrstr"></param>
        /// <returns></returns>
        public static List<ModelCrawlResult> SolrQueryResult(string ids, string key, string title, string keyword, string content, string source, string author, string webName, string sectionName, string type, string isclean, string stime, string etime, out int rcount, out int pcount, int page = 1, int pagesize = 10, string sortField = "Id", string sort = "desc", string uid = "", string ctimes = "1990-01-01 00:00:00", string ctimee = "2099-12-31 23:59:59", string solrstr = "")
        {
            //定义solr
            var solr = ServiceLocator.Current.GetInstance<ISolrOperations<ModelCrawlResult>>();

            #region 字段条件检索
            // 建立排序，条件.
            var options = new QueryOptions
            {
                Rows = pagesize,//数据条数
                Start = (page - 1) * pagesize//开始项
            };
            // 创建条件集合
            var query = new List<ISolrQuery>();
            // id字段
            if (ids != "")
            {
                var iSolrQueryList = new List<ISolrQuery>();
                //创建查询存储列表
                var keys = ids.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var a in keys)
                {
                    //存储地区
                    var q = new SolrQueryByField("Id", "\"" + a.ToLower() + "\"") { Quoted = false };
                    iSolrQueryList.Add(q);
                }
                //创建查询存储之间的关系,是OR还是AND
                var qArea = new SolrMultipleCriteriaQuery(iSolrQueryList, "OR");
                //添加至条件集合
                query.Add(qArea);
            }
            // uid字段
            if (uid != "")
            {
                var iSolrQueryList = new List<ISolrQuery>();
                //创建查询存储列表
                var keys = uid.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var a in keys)
                {
                    //存储地区
                    var q = new SolrQueryByField("Uid", "\"" + a.ToLower() + "\"") { Quoted = false };
                    iSolrQueryList.Add(q);
                }
                //创建查询存储之间的关系,是OR还是AND
                var qArea = new SolrMultipleCriteriaQuery(iSolrQueryList, "OR");
                //添加至条件集合
                query.Add(qArea);
            }
            // 任意字段
            if (key != "")
            {
                var iSolrQueryList = new List<ISolrQuery>();
                //创建查询存储列表
                var keys = key.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var a in keys)
                {
                    //存储地区
                    iSolrQueryList.Add(new SolrQueryByField("Title", a));
                    iSolrQueryList.Add(new SolrQueryByField("Content", a));
                    iSolrQueryList.Add(new SolrQueryByField("Source", a));
                    iSolrQueryList.Add(new SolrQueryByField("Keyword", a));
                    iSolrQueryList.Add(new SolrQueryByField("Author", a));
                    iSolrQueryList.Add(new SolrQueryByField("WebName", a));
                    iSolrQueryList.Add(new SolrQueryByField("SectionName", a));
                }
                //创建查询存储之间的关系,是OR还是AND
                var qArea = new SolrMultipleCriteriaQuery(iSolrQueryList, "OR");
                //添加至条件集合
                query.Add(qArea);
            }
            // 题名
            if (title != "")
            {
                //创建查询存储列表
                var iSolrQueryList = new List<ISolrQuery>();
                var keys = title.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var a in keys)
                {
                    //存储地区
                    iSolrQueryList.Add(new SolrQueryByField("Title", a));
                }
                //创建查询存储之间的关系,是OR还是AND
                var qArea = new SolrMultipleCriteriaQuery(iSolrQueryList, "OR");
                //添加至条件集合
                query.Add(qArea);
            }
            // 关键字
            if (keyword != "")
            {
                //创建查询存储列表
                var iSolrQueryList = new List<ISolrQuery>();
                var keys = keyword.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var a in keys)
                {
                    //关键字
                    iSolrQueryList.Add(new SolrQueryByField("Keyword", a));
                }
                //创建查询关键字之间的关系,是OR还是AND
                var qArea = new SolrMultipleCriteriaQuery(iSolrQueryList, "OR");
                //添加至条件集合
                query.Add(qArea);
            }
            // 内容
            if (content != "")
            {
                //创建查询存储列表
                var iSolrQueryList = new List<ISolrQuery>();
                var keys = content.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var a in keys)
                {
                    //存储地区
                    iSolrQueryList.Add(new SolrQueryByField("Content", a));
                }
                //创建查询存储之间的关系,是OR还是AND
                var qArea = new SolrMultipleCriteriaQuery(iSolrQueryList, "OR");
                //添加至条件集合
                query.Add(qArea);
            }
            // Source
            if (source != "")
            {
                //创建查询存储列表
                var iSolrQueryList = new List<ISolrQuery>();
                var keys = source.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var a in keys)
                {
                    //存储地区
                    var q = new SolrQueryByField("Source", "\"" + a.ToLower() + "\"") { Quoted = false };
                    iSolrQueryList.Add(q);
                }
                //创建查询存储之间的关系,是OR还是AND
                var qArea = new SolrMultipleCriteriaQuery(iSolrQueryList, "OR");
                //添加至条件集合
                query.Add(qArea);
            }
            // Author
            if (author != "")
            {
                //创建查询存储列表
                var iSolrQueryList = new List<ISolrQuery>();
                var keys = author.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var a in keys)
                {
                    //存储地区
                    var q = new SolrQueryByField("Author", "\"" + a.ToLower() + "\"") { Quoted = false };
                    iSolrQueryList.Add(q);
                }
                //创建查询存储之间的关系,是OR还是AND
                var qArea = new SolrMultipleCriteriaQuery(iSolrQueryList, "OR");
                //添加至条件集合
                query.Add(qArea);
            }
            // WebName
            if (webName != "")
            {
                //创建查询存储列表
                var iSolrQueryList = new List<ISolrQuery>();
                var keys = webName.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var a in keys)
                {
                    //存储地区
                    var q = new SolrQueryByField("WebName", "\"" + a.ToLower() + "\"") { Quoted = false };
                    iSolrQueryList.Add(q);
                }
                //创建查询存储之间的关系,是OR还是AND
                var qArea = new SolrMultipleCriteriaQuery(iSolrQueryList, "OR");
                //添加至条件集合
                query.Add(qArea);
            }
            // SectionName
            if (sectionName != "")
            {
                //创建查询存储列表
                var iSolrQueryList = new List<ISolrQuery>();
                var keys = sectionName.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var a in keys)
                {
                    //存储地区
                    var q = new SolrQueryByField("SectionName", "\"" + a.ToLower() + "\"") { Quoted = false };
                    iSolrQueryList.Add(q);
                }
                //创建查询存储之间的关系,是OR还是AND
                var qArea = new SolrMultipleCriteriaQuery(iSolrQueryList, "OR");
                //添加至条件集合
                query.Add(qArea);
            }
            // type
            if (type != "")
            {
                //创建查询存储列表
                var iSolrQueryList = new List<ISolrQuery>();
                var keys = type.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var a in keys)
                {
                    //存储地区
                    var q = new SolrQueryByField("Type", "\"" + a.ToLower() + "\"") { Quoted = false };
                    iSolrQueryList.Add(q);
                }
                //创建查询存储之间的关系,是OR还是AND
                var qArea = new SolrMultipleCriteriaQuery(iSolrQueryList, "OR");
                //添加至条件集合
                query.Add(qArea);
            }
            // isclean
            if (isclean != "")
            {
                //创建查询存储列表
                var iSolrQueryList = new List<ISolrQuery>();
                var keys = isclean.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var a in keys)
                {
                    //存储地区
                    var q = new SolrQueryByField("IsCleaned", a.ToLower());
                    iSolrQueryList.Add(q);
                }
                //创建查询存储之间的关系,是OR还是AND
                var qArea = new SolrMultipleCriteriaQuery(iSolrQueryList, "OR");
                //添加至条件集合
                query.Add(qArea);
            }
            #endregion

            #region solr条件查询
            // 直接查询 其他条件可用
            if (solrstr != "")
            {
                query.Add(new SolrQuery(solrstr));
            }
            #endregion

            #region 时间范围查询条件
            //创建时间范围实例
            SolrQueryByRange<DateTime> qDateRange = null;
            var sdate = DateTime.Parse(stime);
            var edate = DateTime.Parse(etime);
            qDateRange = new SolrQueryByRange<DateTime>("CreateTime", sdate, edate);
            query.Add(qDateRange);

            //创建时间范围实例
            SolrQueryByRange<DateTime> qDateRangeCtime = null;
            var scdate = DateTime.Parse(ctimes);
            var ecdate = DateTime.Parse(ctimee);
            qDateRangeCtime = new SolrQueryByRange<DateTime>("CreateTime", scdate, ecdate);
            //加入集合
            query.Add(qDateRangeCtime);
            #endregion

            #region 条件间关系
            //条件集合之间的关系
            var qTbo = new SolrMultipleCriteriaQuery(query, "AND");
            #endregion

            #region 排序字段
            if (sortField != "")
            {
                options.AddOrder(new SolrNet.SortOrder("score", Order.DESC));
                options.AddOrder(new SolrNet.SortOrder(sortField, sort.ToLower() == "asc" ? Order.ASC : Order.DESC));
            }
            else
            {
                if (query.Count == 0)
                {
                    //按照时间倒排序.
                    options.AddOrder(new SolrNet.SortOrder("score", Order.DESC));
                    options.AddOrder(new SolrNet.SortOrder("CreateTime", Order.DESC));
                }
                else
                {
                    options.AddOrder(new SolrNet.SortOrder("CreateTime", Order.DESC));
                }
            }

            #endregion

            #region 执行查询
            //执行查询,有5个重载
            try
            {
                var results = solr.Query(query.Count == 2 ? SolrQuery.All : qTbo, options);

                #region 总条目、总页数
                rcount = results.NumFound;
                if (rcount % pagesize == 0)
                {
                    pcount = rcount / pagesize;
                }
                else
                {
                    pcount = rcount / pagesize + 1;
                }
                #endregion
                return results;
            }
            #endregion
            catch (Exception ex)
            {
                rcount = 0;
                pcount = 0;
                return null;
            }
        }

    }
}
