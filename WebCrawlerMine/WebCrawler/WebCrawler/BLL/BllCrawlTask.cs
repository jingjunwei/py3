﻿using System;
using System.Collections.Generic;
using System.Data;
using DAL;
using Model;

namespace BLL
{
    public static class BllCrawlTask 
    {
        /// <summary> 
        /// Select Paging Data 
        /// </summary> 
        /// <param name="obj">TableCrawlTask Model</param> 
        /// <param name="errMsg">错误信息</param> 
        /// <returns>DataSet</returns> 
        public static DataSet SelectPaging(ModelCrawlTask obj, out string errMsg) 
        {
            object returnObj = MainDal.DoDal(obj, "TableCrawlTask", new List<object> { SPActionsCrawlTask.SelectPaging }, out errMsg); 
            if (!string.IsNullOrEmpty(errMsg) || returnObj == null) 
            {
                return null;
            }
            return (DataSet)returnObj;
        }

        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="pageIdx"></param>
        /// <param name="pageSize"></param>
        /// <param name="rcount"></param>
        /// <param name="pcount"></param>
        /// <returns></returns>
        public static DataSet GetPager(int pageIdx, int pageSize, out int rcount, out int pcount)
        {
            return MainDal.GetPager(pageIdx, pageSize, out rcount, out pcount, "CrawlTask");
        }

        /// <summary> 
        /// Select All Data 
        /// </summary> 
        /// <param name="obj">TableCrawlTask Model</param> 
        /// <param name="errMsg">错误信息</param> 
        /// <returns>DataSet</returns> 
        public static DataSet SelectAll(ModelCrawlTask obj, out string errMsg) 
        {
            object returnObj = MainDal.DoDal(obj, "TableCrawlTask", new List<object> { SPActionsCrawlTask.SelectAll }, out errMsg); 
            if (!string.IsNullOrEmpty(errMsg)) 
            {
                return null;
            }
            return (DataSet)returnObj;
        }

        /// <summary> 
        /// Insert Unique Data 
        /// </summary> 
        /// <param name="obj">TableCrawlTask Model</param> 
        /// <param name="errMsg">错误信息</param> 
        /// <returns>long</returns> 
        public static long InsertUnique(ModelCrawlTask obj, out string errMsg) 
        {
            object returnObj = MainDal.DoDal(obj, "TableCrawlTask", new List<object> { SPActionsCrawlTask.Unique, SPActionsCrawlTask.Insert }, out errMsg); 
            if (!string.IsNullOrEmpty(errMsg)) 
            {
                return -1;
            }
            return Convert.ToInt64(returnObj);
        }

        /// <summary> 
        /// Update Unique Data 
        /// </summary> 
        /// <param name="obj">TableCrawlTask Model</param> 
        /// <param name="errMsg">错误信息</param> 
        public static void UpdateUnique(ModelCrawlTask obj, out string errMsg) 
        {
            MainDal.DoDal(obj, "TableCrawlTask", new List<object> { SPActionsCrawlTask.Unique, SPActionsCrawlTask.Update }, out errMsg); 
        }

        /// <summary> 
        /// Delete Data 
        /// </summary> 
        /// <param name="obj">TableCrawlTask Model</param> 
        /// <param name="errMsg">错误信息</param> 
        public static void Delete(ModelCrawlTask obj, out string errMsg) 
        {
            MainDal.DoDal(obj, "TableCrawlTask", new List<object> { SPActionsCrawlTask.Delete }, out errMsg); 
        }

        /// <summary> 
        /// 获取by id
        /// </summary> 
        public static ModelCrawlTask GeById(string id)
        {
            int p;
            int r;
            var list = MainDal.GetPager(1, 1, out r, out p, "CrawlTask", "Id = " + id);
            var tb = list.Tables[0];
            if (tb.Rows.Count > 0)
            {
                return
                    Newtonsoft.Json.JsonConvert.DeserializeObject<List<ModelCrawlTask>>(
                        Newtonsoft.Json.JsonConvert.SerializeObject(tb))[0];
            }
            return null;
        }
    }
}

