﻿using System;
using System.Collections.Generic;
using System.Data;
using DAL;
using Model;

namespace BLL
{
    public static class BllCrawlResult
    {
        /// <summary> 
        /// Select Paging Data 
        /// </summary> 
        /// <param name="obj">TableCrawlResult Model</param> 
        /// <param name="errMsg">错误信息</param> 
        /// <returns>DataSet</returns> 
        public static DataSet SelectPaging(ModelCrawlResult obj, out string errMsg)
        {
            object returnObj = MainDal.DoDal(obj, "TableCrawlResult", new List<object> { SPActionsCrawlResult.SelectPaging }, out errMsg);
            if (!string.IsNullOrEmpty(errMsg) || returnObj == null)
            {
                return null;
            }
            return (DataSet)returnObj;
        }

        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="pageIdx"></param>
        /// <param name="pageSize"></param>
        /// <param name="rcount"></param>
        /// <param name="pcount"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        public static DataSet GetPager(int pageIdx, int pageSize, out int rcount, out int pcount, string condition)
        {
            return condition == "" ? MainDal.GetPager(pageIdx, pageSize, out rcount, out pcount, "CrawlResult") : MainDal.GetPager(pageIdx, pageSize, out rcount, out pcount, "CrawlResult", condition);
        }

        /// <summary> 
        /// Select All Data 
        /// </summary> 
        /// <param name="obj">TableCrawlResult Model</param> 
        /// <param name="errMsg">错误信息</param> 
        /// <returns>DataSet</returns> 
        public static DataSet SelectAll(ModelCrawlResult obj, out string errMsg)
        {
            object returnObj = MainDal.DoDal(obj, "TableCrawlResult", new List<object> { SPActionsCrawlResult.SelectAll }, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                return null;
            }
            return (DataSet)returnObj;
        }

        /// <summary> 
        /// Insert Unique Data 
        /// </summary> 
        /// <param name="obj">TableCrawlResult Model</param> 
        /// <param name="errMsg">错误信息</param> 
        /// <returns>long</returns> 
        public static long InsertUnique(ModelCrawlResult obj, out string errMsg)
        {
            object returnObj = MainDal.DoDal(obj, "TableCrawlResult", new List<object> { SPActionsCrawlResult.Unique, SPActionsCrawlResult.Insert }, out errMsg);
            if (!string.IsNullOrEmpty(errMsg))
            {
                return -1;
            }
            return Convert.ToInt64(returnObj);
        }

        /// <summary> 
        /// Update Unique Data 
        /// </summary> 
        /// <param name="obj">TableCrawlResult Model</param> 
        /// <param name="errMsg">错误信息</param> 
        public static void UpdateUnique(ModelCrawlResult obj, out string errMsg)
        {
            MainDal.DoDal(obj, "TableCrawlResult", new List<object> { SPActionsCrawlResult.Unique, SPActionsCrawlResult.Update }, out errMsg);
        }

        /// <summary> 
        /// Delete Data 
        /// </summary> 
        /// <param name="obj">TableCrawlResult Model</param> 
        /// <param name="errMsg">错误信息</param> 
        public static void Delete(ModelCrawlResult obj, out string errMsg)
        {
            MainDal.DoDal(obj, "TableCrawlResult", new List<object> { SPActionsCrawlResult.Delete }, out errMsg);
        }

        /// <summary> 
        /// get by id
        /// </summary> 
        /// <param name="obj">TableCrawlResult Model</param> 
        /// <param name="errMsg">错误信息</param> 
        public static ModelCrawlResult GeById(string id)
        {
            int p;
            int r;
            var list = MainDal.GetPager(1, 1, out r, out p, "CrawlResult", "Id = " + id);
            var tb = list.Tables[0];
            if (tb.Rows.Count > 0)
            {
                return
                    Newtonsoft.Json.JsonConvert.DeserializeObject<List<ModelCrawlResult>>(
                        Newtonsoft.Json.JsonConvert.SerializeObject(tb))[0];
            }
            return null;
        }
    }
}

