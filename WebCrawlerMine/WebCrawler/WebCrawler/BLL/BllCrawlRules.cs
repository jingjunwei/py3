﻿using System;
using System.Collections.Generic;
using System.Data;
using DAL;
using Model;

namespace BLL
{
    public static class BllCrawlRules 
    {
        /// <summary> 
        /// Select Paging Data 
        /// </summary> 
        /// <param name="obj">TableCrawlRules Model</param> 
        /// <param name="errMsg">错误信息</param> 
        /// <returns>DataSet</returns> 
        public static DataSet SelectPaging(ModelCrawlRules obj, out string errMsg) 
        {
            object returnObj = MainDal.DoDal(obj, "TableCrawlRules", new List<object> { SPActionsCrawlRules.SelectPaging }, out errMsg); 
            if (!string.IsNullOrEmpty(errMsg) || returnObj == null) 
            {
                return null;
            }
            return (DataSet)returnObj;
        }

        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="pageIdx"></param>
        /// <param name="pageSize"></param>
        /// <param name="rcount"></param>
        /// <param name="pcount"></param>
        /// <returns></returns>
        public static DataSet GetPager(int pageIdx, int pageSize, out int rcount, out int pcount)
        {
            return MainDal.GetPager(pageIdx, pageSize, out rcount, out pcount, "CrawlRules");
        }

        /// <summary> 
        /// Select All Data 
        /// </summary> 
        /// <param name="obj">TableCrawlRules Model</param> 
        /// <param name="errMsg">错误信息</param> 
        /// <returns>DataSet</returns> 
        public static DataSet SelectAll(ModelCrawlRules obj, out string errMsg) 
        {
            object returnObj = MainDal.DoDal(obj, "TableCrawlRules", new List<object> { SPActionsCrawlRules.SelectAll }, out errMsg); 
            if (!string.IsNullOrEmpty(errMsg)) 
            {
                return null;
            }
            return (DataSet)returnObj;
        }

        /// <summary> 
        /// Insert Unique Data 
        /// </summary> 
        /// <param name="obj">TableCrawlRules Model</param> 
        /// <param name="errMsg">错误信息</param> 
        /// <returns>long</returns> 
        public static long InsertUnique(ModelCrawlRules obj, out string errMsg) 
        {
            object returnObj = MainDal.DoDal(obj, "TableCrawlRules", new List<object> { SPActionsCrawlRules.Unique, SPActionsCrawlRules.Insert }, out errMsg); 
            if (!string.IsNullOrEmpty(errMsg)) 
            {
                return -1;
            }
            return Convert.ToInt64(returnObj);
        }

        /// <summary> 
        /// Update Unique Data 
        /// </summary> 
        /// <param name="obj">TableCrawlRules Model</param> 
        /// <param name="errMsg">错误信息</param> 
        public static void UpdateUnique(ModelCrawlRules obj, out string errMsg) 
        {
            MainDal.DoDal(obj, "TableCrawlRules", new List<object> { SPActionsCrawlRules.Unique, SPActionsCrawlRules.Update }, out errMsg); 
        }

        /// <summary> 
        /// Delete Data 
        /// </summary> 
        /// <param name="obj">TableCrawlRules Model</param> 
        /// <param name="errMsg">错误信息</param> 
        public static void Delete(ModelCrawlRules obj, out string errMsg) 
        {
            MainDal.DoDal(obj, "TableCrawlRules", new List<object> { SPActionsCrawlRules.Delete }, out errMsg); 
        }

        /// <summary> 
        /// 获取by id
        /// </summary> 
        public static ModelCrawlRules GeById(string id)
        {
            int p;
            int r;
            var list = MainDal.GetPager(1, 1, out r, out p, "CrawlRules", "Id = " + id);
            var tb = list.Tables[0];
            if (tb.Rows.Count > 0)
            {
                return
                    Newtonsoft.Json.JsonConvert.DeserializeObject<List<ModelCrawlRules>>(
                        Newtonsoft.Json.JsonConvert.SerializeObject(tb))[0];
            }
            return null;
        }
    }
}

