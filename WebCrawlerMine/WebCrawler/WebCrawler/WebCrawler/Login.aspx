﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebCrawler.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>登录</title>
    <link href="/Content/bootstrap.min.css" rel="stylesheet" />
    <link href="/Content/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="/Content/themes/base/jquery-ui.min.css" rel="stylesheet" />
    <link href="/Content/ui.jqgrid.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="center-block" style="margin: 0 auto; width: 50%; margin-top: 100px;">
            <div class="col-md-12">
                <div class="search-label col-md-2">名</div>
                <div class="col-md-10">
                    <asp:TextBox ID="Name" class="form-control " placeholder="站点名称" name="Name" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-12">
                <div class="search-label col-md-2">密</div>
                <div class="col-md-10">
                    <asp:TextBox ID="Token" class="form-control " TextMode="Password" placeholder="Token" name="Token" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-12 center-block">
                <asp:Button ID="BTNLogin" class="form-control col-md-12" runat="server" Text="Link Start" OnClick="BTNLogin_Click" />
            </div>
        </div>
    </form>
    <script src="/Scripts/jquery-1.12.4.min.js"></script>
    <script src="/Scripts/bootstrap.min.js"></script>
</body>
</html>
