﻿<%@ Page Language="C#" ValidateRequest="false" AutoEventWireup="true" CodeBehind="Log.aspx.cs" Inherits="WebCrawler.Log" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Log</title>
    <link href="/Content/bootstrap.min.css" rel="stylesheet" />
    <link href="/Content/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="/Content/themes/base/jquery-ui.min.css" rel="stylesheet" />
    <link href="/Content/ui.jqgrid.css" rel="stylesheet" />
</head>
<body>
    <form runat="server" id="form1">
        <asp:Label ID="Label1" runat="server" Visible="False" Text="Log目录："></asp:Label>
        <asp:DropDownList ID="DDLLogPath" Visible="False" runat="server">
        </asp:DropDownList>
        <asp:Button ID="BTNGetFiles" Visible="False" runat="server" OnClick="BTNGetFiles_Click" Text="显示所有文件" /><br />
        <asp:Label ID="Label2" runat="server" Text="Log文件："></asp:Label>
        <asp:DropDownList ID="DDLLogFiles" runat="server"></asp:DropDownList>
        <asp:Button ID="BTNGetLog" runat="server" OnClick="BTNGetLog_Click" Text="显示文件内容" />
        <asp:Button ID="BTNClearLog" runat="server" OnClick="BTNClearLog_Click" Text="清空当前日志" /><br />
        <asp:Label runat="server" ID="LBMsg"></asp:Label>
        <asp:TextBox ID="TBLog" Style="width: 100%; height: 200px" TextMode="MultiLine" ReadOnly="True" runat="server"></asp:TextBox>
    </form>
    <script src="/Scripts/jquery-1.12.4.min.js"></script>
    <script src="/Scripts/common.js"></script>
    <script src="/Scripts/bootstrap.min.js"></script>
</body>
</html>
