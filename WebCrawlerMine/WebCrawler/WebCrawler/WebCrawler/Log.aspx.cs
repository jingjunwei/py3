﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebCrawler
{
    public partial class Log : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ShowFiles();
            }
            if (RightCheck.CookieLoginCheck(Request)) return;
            Response.Redirect("/Login.aspx");
        }

        protected void BTNGetFiles_Click(object sender, EventArgs e)
        {
            ShowFiles();
        }

        protected void BTNGetLog_Click(object sender, EventArgs e)
        {
            GetLog();
        }
        protected void BTNClearLog_Click(object sender, EventArgs e)
        {
            var targetPath = DDLLogFiles.Text;
            FileStream fs = null;
            try
            {
                fs = new FileStream(targetPath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read);
                fs.SetLength(0);
            }
            catch (Exception ex)
            {
                LBMsg.Text = "清空日志文件失败：" + ex.Message;
            }
            finally
            {
                if (fs != null) fs.Close();
            }
            GetLog();
        }

        private void GetLog()
        {
            try
            {
                if (File.Exists(DDLLogFiles.SelectedValue))
                {
                    // 日志文件可能无法直接读取 被采集程序占用
                    // 所以此处将 需要查看的日志文件 拷贝至 日志文件夹的子目录 sub下 然后再查看
                    // 以此实现 跳过被占用的 情况
                    var fileName = DDLLogFiles.SelectedValue;
                    var sourcePath = DDLLogPath.Text;
                    var targetPath = DDLLogPath.Text + "/sub";
                    var sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                    var destFile = System.IO.Path.Combine(targetPath, "log.log");
                    if (!System.IO.Directory.Exists(targetPath))
                    {
                        System.IO.Directory.CreateDirectory(targetPath);
                    }
                    System.IO.File.Copy(sourceFile, destFile, true);
                    TBLog.Text = File.ReadAllText(destFile);
                }
            }
            catch (Exception ex)
            {
                LBMsg.Text = "查看日志文件失败：" + ex.Message + "，请稍后再试！";
            }
        }

        private void ShowFiles()
        {
            var logpath = ConfigurationManager.AppSettings["LogPath"];
            DDLLogPath.Items.Add(new ListItem("目录", logpath));
            DDLLogPath.SelectedIndex = 0;
            if (Directory.Exists(DDLLogPath.SelectedValue))
            {
                var files = Directory.GetFiles(DDLLogPath.Text);
                DDLLogFiles.Items.Clear();
                foreach (var file in files)
                {
                    DDLLogFiles.Items.Add(new ListItem(file.Substring(file.LastIndexOf("\\") + 1, file.Length - file.LastIndexOf("\\") - 1), file));
                }
            }
        }


    }
}