﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebCrawler
{
    public partial class LogOut : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["name"] == null || Request.Cookies["token"] == null || Request.Cookies["secret"] == null)
            {
                Response.Redirect("/Login.aspx");
                return;
            }
            var name = Request.Cookies["name"];
            name.Value = "";
            name.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Set(name);

            var token = Request.Cookies["token"];
            token.Value = "";
            token.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Set(token);

            var secret = Request.Cookies["secret"];
            secret.Value = "";
            secret.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Set(secret);
            Response.Redirect("/Login.aspx");
        }
    }
}