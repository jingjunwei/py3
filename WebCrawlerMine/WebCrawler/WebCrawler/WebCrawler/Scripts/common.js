﻿// 清除BUG
jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
    $("body").prepend('<link href="/Content/css.css" rel="stylesheet" />' +
        '<div class="row menu">\
            <div class="col-md-12">\
                <a class="btn" href="LogOut.aspx">退出登录</a>\
                <a class="btn" href="SiteTokens.aspx">用户站点</a>\
            </div>\
        <div class="form-group col-md-5">\
            <a class="btn" href="CrawlSite.aspx">1.站源</a>\
            <a class="btn" href="CrawlRules.aspx">2.规则</a>\
            <a class="btn" href="CrawlTask.aspx">3.任务</a>\
            <a class="btn" href="CrawlResult.aspx">4.结果</a>\
            <a class="btn" href="Log.aspx">5.日志</a>\
        </div>\
    </div>');
    $(".menu a[href='" + $("title").html().trim() + ".aspx']").addClass("current");
})();
//清除表单数据
$.fn.clearData = function () {
    $(this).each(function (i, input) {
        $(input).val("");
    });
}
//表单数据绑定
$.fn.bindData = function (data) {
    if (!data) return;

    $(this).each(function (i, input) {
        var name = $(input).attr("name");
        if (!name) return true;
        var fieldArray = name.split(".");
        if (!fieldArray) return true;
        var fieldName = fieldArray[fieldArray.length - 1];

        //普通select控件
        if ($(input).prop("tagName").toLowerCase() == "select" && !$(input).hasClass("select2-offscreen")) {
            $(input).val(data[fieldName] == null ? "" : data[fieldName].toString());
        }
            //select2控件
        else if ($(input).hasClass("select2-offscreen")) {
            //用select标签初始化的select2控件
            if ($(input).prop("tagName").toLowerCase() == "select") {
                $(input).select2("val", data[fieldName] == null ? "" : data[fieldName].toString());
            }
        }
        else if ($(input).hasClass("date-picker")) {
            //用select标签初始化的select2控件
            $(input).closest("div.date-picker").datepicker("update", data[fieldName]);
        }
        else {
            if (data[fieldName]) {
                $(input).val(data[fieldName]);
            } else {
                $(input).val(data[fieldName]);
            }
        }
    }
    );
}
