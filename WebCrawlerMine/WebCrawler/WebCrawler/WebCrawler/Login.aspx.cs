﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;

namespace WebCrawler
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (RightCheck.CookieLoginCheck(Request))
                {
                    Response.Redirect("/CrawlResult.aspx");
                }
            }
        }

        protected void BTNLogin_Click(object sender, EventArgs e)
        {
            if (Name.Text != "" && Token.Text != "")
            {
                if (BllSiteTokens.LoginCheck(Name.Text, Token.Text) != null)
                {
                    RightCheck.AddCookies(Name.Text, Token.Text);
                    Response.Redirect("/CrawlResult.aspx");
                }
                else
                {
                    Response.Write("<script>alert('您的名或密不正确。');</script>");
                } 
            }
            else
            {
                Response.Write(Name.Text == ""
                    ? "<script>alert('请键入您的名。');</script>"
                    : "<script>alert('请键入您的密。');</script>");
            }
        }

        protected void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            if (HttpContext.Current.Server.GetLastError() is HttpRequestValidationException)
            {
                HttpContext.Current.Response.Write("请输入正确的字符串，谢谢合作。【<a href=\"javascript:history.back(0);\">返回</a>】");
                HttpContext.Current.Server.ClearError();
            }
        } 
    }
}