﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using BLL;
using Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace WebCrawler
{
    /// <summary>
    /// Service 的摘要说明
    /// </summary>
    public class Service : IHttpHandler
    {
        private readonly string[] _adminName = ConfigurationManager.AppSettings["Admin"].Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
        private ModelSiteTokens _user;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            context.Response.ContentType = "application/json";

            var res = "";
            if (RightCheck.CookieLoginCheck(context.Request, out _user))
            {
                res = GoResLocal(context);
            }
            else switch (context.Request.HttpMethod.ToUpper())
                {
                    case "GET":
                        res = RightCheck.ServiceRightCheck(context.Request.QueryString, out _user) ? GoResService(context) : "\nerr: 没有访问权限。";
                        break;
                    case "POST":
                        res = RightCheck.ServiceRightCheck(context.Request.Form, out _user) ? GoResService(context) : "\nerr: 没有访问权限。";
                        break;
                    default:
                        res = "{ \"CODE\": 403, \"message\": \"请求已接收，但您没有权限访问。\"}";
                        context.Response.StatusCode = 403;
                        context.Response.StatusDescription = res;
                        break;
                }

            context.Response.Write(res);
        }

        private string GoResLocal(HttpContext context)
        {
            var res = "";
            if (context.Request.HttpMethod.ToUpper() == "GET")
            {
                var m = context.Request.QueryString["m"];
                if (m != null)
                {
                    switch (m)
                    {
                        case "GetNewsPager":
                            res = GetNewsPager(context.Request.QueryString);
                            break;
                        case "GetNewsById":
                            res = GetNewsById(context.Request.QueryString);
                            break;
                        case "GetSiteTokensPager":
                            res = GetSiteTokensPager(context.Request.QueryString);
                            break;
                        case "GetCrawlRulesPager":
                            res = GetCrawlRulesPager(context.Request.QueryString);
                            break;
                        case "GetCrawlSitePager":
                            res = GetCrawlSitePager(context.Request.QueryString);
                            break;
                        case "GetCrawlTaskPager":
                            res = GetCrawlTaskPager(context.Request.QueryString);
                            break;
                        case "GetAllWebName":
                            res = GetAllWebName(context.Request.QueryString);
                            break;
                        default:
                            res = "err: 未找到请求方法。";
                            break;
                    }
                }
                else
                {
                    res = "err: 未找到请求方法。";
                }
            }
            else if (context.Request.HttpMethod.ToUpper() == "POST")
            {
                var m = context.Request.Form["m"] ?? context.Request.QueryString["m"];
                if (m != null)
                {
                    switch (m)
                    {
                        case "GetNewsPager":
                            res = GetNewsPager(context.Request.Form);
                            break;
                        case "GetNewsById":
                            res = GetNewsById(context.Request.Form);
                            break;
                        case "GetSiteTokensPager":
                            res = GetSiteTokensPager(context.Request.Form);
                            break;
                        case "GetCrawlRulesPager":
                            res = GetCrawlRulesPager(context.Request.Form);
                            break;
                        case "GetCrawlSitePager":
                            res = GetCrawlSitePager(context.Request.Form);
                            break;
                        case "GetCrawlTaskPager":
                            res = GetCrawlTaskPager(context.Request.Form);
                            break;
                        case "GetAllWebName":
                            res = GetAllWebName(context.Request.Form);
                            break;
                        default:
                            res = "err: 未找到请求方法。";
                            break;
                    }
                }
                else
                {
                    res = "CODE: 207请求已接收，但未找到请求方法。";
                }
            }
            else
            {
                res = "CODE: 208请求已接收，但未识别的请求方式。";
            }
            return res;
        }

        private string GoResService(HttpContext context)
        {
            var res = "";
            if (context.Request.HttpMethod.ToUpper() == "GET")
            {
                var m = context.Request.QueryString["m"];
                if (m != null)
                {
                    switch (m)
                    {
                        case "GetNewsPager":
                            res = GetNewsPager(context.Request.QueryString);
                            break;
                        case "GetNewsById":
                            res = GetNewsById(context.Request.QueryString);
                            break;
                        case "GetCrawlSitePager":
                            res = GetCrawlSitePager(context.Request.QueryString);
                            break;
                        case "GetAllWebName":
                            res = GetAllWebName(context.Request.QueryString);
                            break;
                        case "InsertNewCrawlSite":
                            res = InsertNewCrawlSite(context.Request.QueryString);
                            break;
                        default:
                            res = "err: 未找到请求方法。";
                            break;
                    }
                }
                else
                {
                    res = "err: 未找到请求方法。";
                }
            }
            else if (context.Request.HttpMethod.ToUpper() == "POST")
            {
                var m = context.Request.Form["m"] ?? context.Request.QueryString["m"];
                if (m != null)
                {
                    switch (m)
                    {
                        case "GetNewsPager":
                            res = GetNewsPager(context.Request.Form);
                            break;
                        case "GetNewsById":
                            res = GetNewsById(context.Request.Form);
                            break;
                        case "GetCrawlSitePager":
                            res = GetCrawlSitePager(context.Request.Form);
                            break;
                        case "GetAllWebName":
                            res = GetAllWebName(context.Request.Form);
                            break;
                        case "InsertNewCrawlSite":
                            res = InsertNewCrawlSite(context.Request.Form);
                            break;
                        default:
                            res = "err: 未找到请求方法。";
                            break;
                    }
                }
                else
                {
                    res = "CODE: 207请求已接收，但未找到请求方法。";
                }
            }
            else
            {
                res = "CODE: 208请求已接收，但未识别的请求方式。";
            }
            return res;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// 查询采集结果分页记录
        /// </summary>
        /// <param name="nvc"></param>
        /// <returns>DataSet</returns> 
        protected string GetNewsPager(System.Collections.Specialized.NameValueCollection nvc)
        {
            var res = new Dictionary<string, object>();

            // 统一字段
            var sidx = nvc["sidx"] ?? "Id";
            var sord = nvc["sord"] ?? "desc";
            var page = nvc["page"] == null ? 1 : int.Parse(nvc["page"]);
            var rows = nvc["rows"] == null ? 1 : int.Parse(nvc["rows"]);

            // 条件字段
            var pcount = 0;
            var rcount = 0;
            var id = nvc["id"] ?? "";
            var key = nvc["key"] ?? "";
            var title = nvc["title"] ?? "";
            var keyword = nvc["keyword"] ?? "";
            var content = nvc["content"] ?? "";
            var source = nvc["source"] ?? "";
            var author = nvc["author"] ?? "";
            var wname = nvc["wname"] ?? "";
            var sname = nvc["sname"] ?? "";
            var type = nvc["type"] ?? "";
            var isclean = nvc["IsCleaned"] ?? "";
            var q = nvc["q"] ?? "";
            var sUid = nvc["sUid"] ?? "";
            if (!((IList)_adminName).Contains(_user.SiteName))
            {
                sUid = _user.Id.ToString();
            }
            var range = nvc["range"] ?? "self";
            if (range == "all")
            {
                sUid = "";
            }
            var stime = nvc["stime"] ?? DateTime.MinValue.ToString("yyyy-MM-dd HH:mm:ss");
            var etime = nvc["etime"] ?? DateTime.MaxValue.ToString("yyyy-MM-dd HH:mm:ss");
            var scollecttime = nvc["scollecttime"] ?? DateTime.MinValue.ToString("yyyy-MM-dd HH:mm:ss");
            var ecollecttime = nvc["ecollecttime"] ?? DateTime.MaxValue.ToString("yyyy-MM-dd HH:mm:ss");
            var ds = BllSolrQuery.SolrQueryResult(id, key, title, keyword, content, source, author, wname, sname, type, isclean, stime, etime, out rcount, out pcount, page, rows, sidx, sord, sUid, scollecttime, ecollecttime, q);
            res.Add("page", page);
            res.Add("total", pcount);
            res.Add("records", rcount);
            res.Add("rows", ds);
            return JsonConvert.SerializeObject(res);
        }

        /// <summary>
        /// 查询采集结果分页记录
        /// </summary>
        /// <param name="nvc"></param>
        /// <returns>DataSet</returns> 
        protected string GetNewsById(System.Collections.Specialized.NameValueCollection nvc)
        {
            var res = new Dictionary<string, object>();

            // 统一字段
            var sidx = nvc["sidx"] ?? "Id";
            var sord = nvc["sord"] ?? "desc";
            var page = nvc["page"] == null ? 1 : int.Parse(nvc["page"]);
            var rows = nvc["rows"] == null ? 1 : int.Parse(nvc["rows"]);

            // 条件字段
            var condition = "";
            var wname = nvc["id"] ?? "";
            if (wname != "")
            {
                condition = string.Format("[Id] = '{0}'", wname);
            }
            if (condition == "") condition = "1=1";

            // 信息返回
            int rcount;
            int pcount;
            var ds = BllBaseService.GetPager(page, rows, out rcount, out pcount, "CrawlResult", condition, sidx + " " + sord);
            res.Add("page", page);
            res.Add("total", pcount);
            res.Add("records", rcount);
            res.Add("rows", ds.Tables[0]);
            return JsonConvert.SerializeObject(res);
        }

        /// <summary>
        /// 查询网站tokens分页记录
        /// </summary>
        /// <param name="nvc"></param>
        /// <returns>DataSet</returns> 
        protected string GetSiteTokensPager(System.Collections.Specialized.NameValueCollection nvc)
        {
            var res = new Dictionary<string, object>();

            // 统一字段
            var sidx = nvc["sidx"] ?? "Id";
            var sord = nvc["sord"] ?? "desc";
            var page = nvc["page"] == null ? 1 : int.Parse(nvc["page"]);
            var rows = nvc["rows"] == null ? 1 : int.Parse(nvc["rows"]);

            // 条件字段
            var condition = "";
            var sname = nvc["sname"] ?? "";
            if (sname != "")
            {
                condition = string.Format("[SiteName] like '%{0}%'", sname);
            }
            if (condition == "") condition = "1=1";
            if (!((IList)_adminName).Contains(_user.SiteName))
            {
                condition += " AND [Id] = '" + _user.Id + "'";
            }

            // 信息返回
            int rcount;
            int pcount;
            var ds = BllBaseService.GetPager(page, rows, out rcount, out pcount, "SiteTokens", condition, sidx + " " + sord);
            res.Add("page", page);
            res.Add("total", pcount);
            res.Add("records", rcount);
            res.Add("rows", ds.Tables[0]);
            return JsonConvert.SerializeObject(res);
        }

        /// <summary>
        /// 查询采集规则分页记录
        /// </summary>
        /// <param name="nvc"></param>
        /// <returns>DataSet</returns> 
        protected string GetCrawlRulesPager(System.Collections.Specialized.NameValueCollection nvc)
        {
            var res = new Dictionary<string, object>();

            // 统一字段
            var sidx = nvc["sidx"] ?? "Id";
            var sord = nvc["sord"] ?? "desc";
            var page = nvc["page"] == null ? 1 : int.Parse(nvc["page"]);
            var rows = nvc["rows"] == null ? 1 : int.Parse(nvc["rows"]);

            // 条件字段
            var condition = "";
            var sname = nvc["sname"] ?? "";
            if (sname != "")
            {
                condition = string.Format("[RuleName] like '%{0}%'", sname);
            }
            if (condition == "") condition = "1=1";

            // 信息返回
            int rcount;
            int pcount;
            var ds = BllBaseService.GetPager(page, rows, out rcount, out pcount, "CrawlRules", condition, sidx + " " + sord);
            res.Add("page", page);
            res.Add("total", pcount);
            res.Add("records", rcount);
            res.Add("rows", ds.Tables[0]);
            return JsonConvert.SerializeObject(res);
        }

        /// <summary>
        /// 查询采集网站模块分页记录
        /// </summary>
        /// <param name="nvc"></param>
        /// <returns>DataSet</returns> 
        protected string GetCrawlSitePager(System.Collections.Specialized.NameValueCollection nvc)
        {
            var res = new Dictionary<string, object>();

            // 统一字段
            var sidx = nvc["sidx"] ?? "Id";
            var sord = nvc["sord"] ?? "desc";
            var page = nvc["page"] == null ? 1 : int.Parse(nvc["page"]);
            var rows = nvc["rows"] == null ? 1 : int.Parse(nvc["rows"]);

            // 条件字段
            var condition = "";
            var wname = nvc["wname"] ?? "";
            var wlink = nvc["link"] ?? "";
            var sname = nvc["sname"] ?? "";
            var sfrom = nvc["sfrom"] ?? "";
            var type = nvc["sType"] ?? "";
            var sUid = nvc["sUid"] ?? "";
            var delflag = (object)nvc["delflag"] ?? "";
            if (type != "")
            {
                condition =
                       string.Format(
                           "[WebName] LIKE '%{0}%' AND [TYPE] = '{5}' AND [WebLink] LIKE '%{1}%'  AND [SectionName] LIKE '%{2}%' AND [SiteFrom] LIKE '%{3}%' AND [DELETEFLAG] LIKE '%{4}%' ",
                           wname, wlink, sname, sfrom, delflag, type);
            }
            else
            {
                condition = string.Format("[WebName] LIKE '%{0}%' AND [WebLink] LIKE '%{1}%'  AND [SectionName] LIKE '%{2}%' AND [SiteFrom] LIKE '%{3}%' AND [DELETEFLAG] LIKE '%{4}%' ", wname, wlink, sname, sfrom, delflag);
            }
            if (sUid != "")
            {
                condition += " AND [Uid] = '" + sUid + "'";
            }
            if (!((IList)_adminName).Contains(_user.SiteName))
            {
                condition += " AND [Uid] = '" + _user.Id + "'";
            }

            if (condition == "") condition = "1=1";

            // 信息返回
            int rcount;
            int pcount;
            var ds = BllBaseService.GetPager(page, rows, out rcount, out pcount, "CrawlSite", condition, sidx + " " + sord);
            res.Add("page", page);
            res.Add("total", pcount);
            res.Add("records", rcount);
            res.Add("rows", ds.Tables[0]);
            return JsonConvert.SerializeObject(res);
        }

        /// <summary>
        /// 查询采集任务分页记录
        /// </summary>
        /// <param name="nvc"></param>
        /// <returns>DataSet</returns> 
        protected string GetCrawlTaskPager(System.Collections.Specialized.NameValueCollection nvc)
        {
            var res = new Dictionary<string, object>();

            // 统一字段
            var sidx = nvc["sidx"] ?? "Id";
            var sord = nvc["sord"] ?? "desc";
            var page = nvc["page"] == null ? 1 : int.Parse(nvc["page"]);
            var rows = nvc["rows"] == null ? 1 : int.Parse(nvc["rows"]);

            // 条件字段
            var condition = "";
            var rid = nvc["rid"] ?? "";
            var wsid = nvc["wsid"] ?? "";
            var sUid = nvc["sUid"] ?? ""; //delete
            var started = nvc["started"] ?? ""; //delete
            if (rid != "")
            {
                condition = string.Format("[RuleId] = '{0}'", rid);
            }
            if (wsid != "")
            {
                condition = string.Format("[SiteSectionId] = '{0}'", rid);
            }
            if (wsid != "" && rid != "")
            {
                condition = string.Format("[RuleId] = '{0}' AND [SiteSectionId] = '{1}'", rid, wsid);
            }
            if (condition == "") condition = "1=1";
            if (sUid != "")
            {
                condition += " AND [Uid] = '" + sUid + "'";
            }
            if (!((IList)_adminName).Contains(_user.SiteName))
            {
                condition += " AND [Uid] = '" + _user.Id + "'";
            }
            if (started != "")
            {
                condition += " AND [IsStarted] = '" + started + "'";
            }
            // 信息返回
            int rcount;
            int pcount;
            var ds = BllBaseService.GetPager(page, rows, out rcount, out pcount, "CrawlTask", condition, sidx + " " + sord);
            res.Add("page", page);
            res.Add("total", pcount);
            res.Add("records", rcount);
            res.Add("rows", ds.Tables[0]);
            return JsonConvert.SerializeObject(res);
        }

        /// <summary>
        /// 查询采集任务分页记录
        /// </summary>
        /// <param name="nvc"></param>
        /// <returns>DataSet</returns> 
        protected string GetAllWebName(System.Collections.Specialized.NameValueCollection nvc)
        {
            var res = new Dictionary<string, object>();

            // 统一字段
            var sidx = nvc["sidx"] ?? "Id";
            var sord = nvc["sord"] ?? "desc";
            var page = nvc["page"] == null ? 1 : int.Parse(nvc["page"]);
            var rows = nvc["rows"] == null ? int.MaxValue : int.Parse(nvc["rows"]);

            // 条件字段
            var condition = "";
            var wname = nvc["wname"] ?? "";
            if (wname != "")
            {
                condition = string.Format("[WebName] = '{0}'", wname);
            }
            if (!((IList)_adminName).Contains(_user.SiteName))
            {
                if (condition != "")
                {
                    condition += " AND [Uid] = '" + _user.Id + "'";
                }
                else
                {
                    condition = " [Uid] = '" + _user.Id + "'";
                }
            }
            if (condition == "") condition = "1=1";

            // 信息返回
            int rcount;
            int pcount;
            var ds = BllBaseService.GetPager(page, rows, out rcount, out pcount, "[CrawlSite]", condition, sidx + " " + sord);
            var dt = ds.Tables[0];

            var weblist = new List<Dictionary<string, object>>();
            foreach (DataRow dataRow in dt.Rows)
            {
                var webname = dataRow["WebName"].ToString();
                var weblink = dataRow["WebLink"].ToString();

                var id = dataRow["Id"].ToString();
                var sectionname = dataRow["SectionName"].ToString();
                var sectionindexurl = dataRow["SectionIndexUrl"].ToString();
                var deleteflag = dataRow["DeleteFlag"].ToString();
                var section = new Dictionary<string, string>
                {
                    { "Id", id }, 
                    { "SectionName", sectionname }, 
                    { "SectionIndexUrl", sectionindexurl },
                    { "Status", deleteflag }
                };
                if (!weblist.Exists(w => w["WebName"].ToString() == webname))
                {
                    var web = new Dictionary<string, object>()
                    {
                        {"WebName", webname},
                        {"WebLink", weblink},
                        {"Sections", new List<Dictionary<string, string>> { section }}
                    };
                    weblist.Add(web);
                }
                else
                {
                    var firstOrDefault = weblist.FirstOrDefault(w => w["WebName"].ToString() == webname);
                    if (firstOrDefault != null)
                    {
                        var nsection = (List<Dictionary<string, string>>)firstOrDefault["Sections"];
                        nsection.Add(section);
                    }
                }
            }
            res.Add("records", weblist.Count);
            res.Add("rows", weblist);
            return JsonConvert.SerializeObject(res);
        }

        /// <summary>
        /// 新增 申请采集源
        /// </summary>
        /// <param name="nvc"></param>
        /// <returns></returns>
        protected string InsertNewCrawlSite(System.Collections.Specialized.NameValueCollection nvc)
        {
            var res = new Dictionary<string, object>();
            try
            {
                var obj = new ModelCrawlSite
                {
                    Id = 0,
                    WebName = nvc["WebName"],
                    WebLink = nvc["WebLink"],
                    SectionName = nvc["SectionName"],
                    SectionIndexUrl = nvc["SectionIndexUrl"],
                    SiteFrom = nvc["name"],
                    DeleteFlag = 1,
                    SPReturnType = AppEnum.SPReturnTypes.Object,
                    SPAction = "Insert"
                };
                string errMsg;
                BllCrawlSite.InsertUnique(obj, out errMsg);
                res.Add("result", true);
            }
            catch (Exception ex)
            {
                res.Add("result", false);
                res.Add("message", ex.Message);
            }
            return JsonConvert.SerializeObject(res);
        }
    }



}