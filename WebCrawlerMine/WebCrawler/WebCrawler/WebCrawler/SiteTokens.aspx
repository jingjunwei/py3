﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SiteTokens.aspx.cs" Inherits="WebCrawler.SiteTokens" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>SiteTokens</title>
    <link href="/Content/bootstrap.min.css" rel="stylesheet" />
    <link href="/Content/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="/Content/themes/base/jquery-ui.min.css" rel="stylesheet" />
    <link href="/Content/ui.jqgrid.css" rel="stylesheet" />
</head>
<body>
    <div id="search">
        <div class="row">
            <div class="form-group col-md-4">
                <div class="search-label col-md-4">站点名称</div>
                <div class="col-md-8">
                    <input type="text" class="form-control " name="sname" placeholder="站点名称">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="search-label col-md-2">&nbsp;</div>
                <div class="col-md-9" onclick="search_user()">
                    <button class="btn green">
                        检索
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-scrollable col-md-12">
                <table id="grid"></table>
                <div id="pager"></div>
            </div>
        </div>
    </div>
    <form runat="server">
        <div class="row" id="editform">
            <asp:TextBox ID="Id" class="form-control hidden " name="Id" runat="server"></asp:TextBox>
            <div class="col-md-4">
                <div class="search-label col-md-4">站点Token</div>
                <div class="col-md-8">
                    <asp:TextBox ID="Token" class="form-control " placeholder="站点Token" name="Token" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-4">
                <div class="search-label col-md-4">站点名称</div>
                <div class="col-md-8">
                    <asp:TextBox ID="SiteName" class="form-control " placeholder="站点名称" name="SiteName" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-4">
                <div class="search-label col-md-4">是否启用登录</div>
                <div class="col-md-8">
                    <asp:DropDownList ID="IsEnabled" class="form-control" runat="server" name="IsEnabled" placeholder="是否启用登录">
                        <asp:ListItem Value="1">启用</asp:ListItem>
                        <asp:ListItem Value="0">禁用</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-4">
                <div class="search-label col-md-4">站点IP限制</div>
                <div class="col-md-8">
                    <asp:TextBox ID="Ip" class="form-control " placeholder="无" name="Ip" runat="server"></asp:TextBox>
                </div>
            </div>
        </div>
        <asp:Button ID="BTNNew" class="btn" runat="server" Text="新建" OnClick="BTNNew_Click" />
        <asp:Button ID="BTNEdit" class="btn" runat="server" Text="修改" OnClick="BTNEdit_Click" />
        <asp:Button ID="BTNDisbale" class="btn" runat="server" Text="禁用" OnClick="BTNDisbale_Click" />
        <asp:Button ID="BTNEnable" class="btn" runat="server" Text="启用" OnClick="BTNEnable_Click" />
    </form>
    <script src="/Scripts/jquery-1.12.4.min.js"></script>
    <script src="/Scripts/common.js"></script>
    <script src="/Scripts/bootstrap.min.js"></script>
    <script src="/Scripts/jquery-ui-1.12.0.min.js"></script>
    <script src="/Scripts/jqGrid/jquery.jqGrid.min.js"></script>
    <script src="/Scripts/jqGrid/grid.locale-cn.js"></script>
    <script>
        // 数据查询
        jQuery("#grid").jqGrid({
            url: '/Service.ashx?m=GetSiteTokensPager',
            mtype: "GET",
            datatype: "json",
            height: 'auto',
            autowidth: true, //自动宽
            colNames: ['Id', 'Token', '站点名称', "是否启用登录", '限制Ip'],
            colModel: [
                { name: 'Id', index: 'Id', width: 55 },
                { name: 'Token', index: 'Token', align: "center", width: 90 },
                { name: 'SiteName', index: 'SiteName', align: "center", width: 100 },
                {
                    name: 'IsEnabled', index: 'IsEnabled', align: "center", width: 100,
                    formatter: function (cellvalue, options, rowObject) {
                        if (cellvalue == "1") {
                            return "启用";
                        }
                        return "禁用";
                    }
                },
                { name: 'Ip', index: 'Ip', width: 80, align: "center" }
            ],
            rowNum: 30,
            rowList: [10, 20, 30, 100],
            pager: '#pager',
            sortname: 'Id',
            viewrecords: true,
            rownumbers: true, //添加左侧行号
            sortorder: "desc",
            caption: "可用站点",
            jsonReader: { repeatitems: false },
            onSelectRow: function (rowid) {
                var row = $("#grid").jqGrid("getRowData", rowid);
                $("#editform .form-control").bindData(row);
                if (row["IsEnabled"] === "启用") {
                    $("#IsEnabled").val("1");
                } else {
                    $("#IsEnabled").val("0");
                }
            }
        });

        //查询
        function search_user() {
            var queryParams = {};
            $($("#search .form-group .form-control").serializeArray()).each(function (i, obj) {
                queryParams[obj.name] = obj.value;
            });
            $("#grid").jqGrid("setGridParam", { postData: queryParams }).trigger("reloadGrid");
        }

    </script>
</body>
</html>

