﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using Model;
using Newtonsoft.Json;
using Utility;

namespace WebCrawler
{
    public partial class CrawlResult : Page
    {
        private DataSet _dtCrawlSite = new DataSet();
        private readonly string[] _adminName = ConfigurationManager.AppSettings["Admin"].Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
        private ModelSiteTokens _user;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!RightCheck.CookieLoginCheck(Request, out _user))
            {
                Response.Redirect("/Login.aspx");
            }
            int rcount;
            int pcount;
            var condition = "1=1";

            if (!((IList) _adminName).Contains(_user.SiteName))
            {
                condition = " [Uid] = " + _user.Id;
                BTNCount.Visible = false;
                BTNDeltaImp.Visible = false;
                BTNQc.Visible = false;
            }
            _dtCrawlSite = BllBaseService.GetPager(1, int.MaxValue, out rcount, out pcount, "CrawlSite", condition);
            if (!IsPostBack)
            {
                if (!((IList) _adminName).Contains(_user.SiteName))
                {
                    condition = " [Id] = " + _user.Id;
                }
                else
                {
                    sUid.Items.Add(new ListItem
                    {
                        Value = "",
                        Text = "选择一个"
                    });
                }
                wname.Items.Add(new ListItem
                {
                    Value = "",
                    Text = "选择一个"
                });
                sname.Items.Add(new ListItem
                {
                    Value = "",
                    Text = "选择一个站点"
                });
                type.Items.Add(new ListItem
                {
                    Value = "",
                    Text = "选择一个"
                });
                var ds = BllBaseService.GetPager(1, int.MaxValue, out rcount, out pcount, "SiteTokens", condition);
                if (ds.Tables.Count > 0)
                {
                    var dt = ds.Tables[0];

                    foreach (DataRow dataRow in dt.Rows)
                    {
                        sUid.Items.Add(new ListItem
                        {
                            Value = dataRow["Id"].ToString(),
                            Text = dataRow["Id"] + ":" + dataRow["SiteName"]
                        });
                    }
                }

                if (_dtCrawlSite.Tables.Count > 0)
                {
                    var dt = _dtCrawlSite.Tables[0];
                    foreach (DataRow dataRow in dt.Rows)
                    {
                        var val = dataRow["WebName"].ToString();
                        if (wname.Items.FindByValue(val) == null)
                        {
                            wname.Items.Add(new ListItem
                            {
                                Value = dataRow["WebName"].ToString(),
                                Text = dataRow["WebName"].ToString()
                            });
                        }

                        val = dataRow["Type"].ToString();
                        if (type.Items.FindByValue(val) == null)
                        {
                            type.Items.Add(new ListItem
                            {
                                Value = dataRow["Type"].ToString(),
                                Text = dataRow["Type"].ToString()
                            });
                        }

                    }
                }

                BllCrawlResult.GetPager(1, 1, out rcount, out pcount, "");
                BTNCount.Text = "数据库数据条数: " + rcount + "条, 点击执行全量更新";
            }
        }

        /// <summary>
        /// 增加记录
        /// </summary>
        /// <returns>int</returns> 
        private long Insert(ModelCrawlResult obj)
        {
            string errMsg;

            long newID = BllCrawlResult.InsertUnique(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                //Error Logic
            }
            return newID;
        }

        /// <summary>
        /// 删除记录
        /// </summary>
        private void Delete(ModelCrawlResult obj)
        {
            string errMsg;
            BllCrawlResult.Delete(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                //Error Logic
                return;
            }
        }

        /// <summary>
        /// 修改记录
        /// </summary>
        private void Update(ModelCrawlResult obj)
        {
            string errMsg;
            BllCrawlResult.UpdateUnique(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                //Error Logic
                return;
            }
        }

        /// <summary>
        /// 查询分页记录
        /// </summary>
        /// <param name="startIndex">数据开始索引值</param> 
        /// <param name="pageSize">数据每页条数</param> 
        /// <returns>DataSet</returns> 
        protected DataSet SelectPaging(int startIndex, int pageSize)
        {
            string errMsg;
            var obj = new ModelCrawlResult
                {
                    StartIndex = startIndex,
                    PageSize = pageSize,
                    SPReturnType = AppEnum.SPReturnTypes.DataSet
                };
            DataSet ds = BllCrawlResult.SelectPaging(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                //Error Logic
                return null;
            }
            return ds;
        }

        ///// <summary>
        ///// 查询分页记录
        ///// </summary>
        ///// <param name="startIndex">数据开始索引值</param> 
        ///// <param name="pageSize">数据每页条数</param>
        ///// <param name="rcount"></param>
        ///// <param name="pcount"></param>
        ///// <returns>DataSet</returns> 
        //protected DataSet GetPager(int startIndex, int pageSize, out int rcount, out int pcount)
        //{
        //    return BllCrawlResult.GetPager(startIndex, pageSize, out rcount, out pcount);
        //}

        /// <summary>
        /// 查询分页记录
        /// </summary>
        /// <param name="startIndex">数据开始索引值</param> 
        /// <param name="pageSize">数据每页条数</param>
        /// <param name="rcount"></param>
        /// <param name="pcount"></param>
        /// <param name="webName"></param>
        /// <param name="sectionName"></param>
        /// <returns>DataSet</returns> 
        protected string GetPager(int startIndex, int pageSize, out int rcount, out int pcount, string webName, string sectionName)
        {
            var res = new Dictionary<string, object>();
            //var condition = "";
            pcount = 0;
            rcount = 0;
            var ds = new List<ModelCrawlResult>();
            if (webName != "")
            {
                ds = BllSolrQuery.SolrQueryResult("", "", "", "", "", "", "", webName, "", "", "", DateTime.MinValue.ToString("yyyy-MM-dd HH:mm:ss"), DateTime.MaxValue.ToString("yyyy-MM-dd HH:mm:ss "), out rcount, out pcount);
            }
            if (webName != "" && sectionName != "")
            {
                ds = BllSolrQuery.SolrQueryResult("", "", "", "", "", "", "", webName, sectionName, "", "", DateTime.MinValue.ToString("yyyy-MM-dd HH:mm:ss"), DateTime.MaxValue.ToString("yyyy-MM-dd HH:mm:ss "), out rcount, out pcount);
            }
            res.Add("page", startIndex);
            res.Add("total", pcount);
            res.Add("records", rcount);
            res.Add("rows", ds);
            return JsonConvert.SerializeObject(res);
        }

        /// <summary>
        /// 查询全部记录
        /// </summary>
        /// <returns>DataSet</returns> 
        protected DataSet SelectAll()
        {
            string errMsg;
            var obj = new ModelCrawlResult
                {
                    SPReturnType = AppEnum.SPReturnTypes.DataSet
                };
            DataSet ds = BllCrawlResult.SelectAll(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                //Error Logic
                return null;
            }
            return ds;
        }

        /// <summary>
        /// 判断是否出错
        /// </summary>
        /// <param name="errMsg">错误信息</param>
        private bool CheckIsError(string errMsg)
        {
            if (!string.IsNullOrEmpty(errMsg))
            {
                //Catch Error 
                return true;
            };
            return false;
        }


        protected void BTNNew_Click(object sender, EventArgs e)
        {
            var obj = new ModelCrawlResult
            {
                Id = 0,
                Uid = _user.Id,
                TaskId = 0,
                WebName = WebName.Text,
                SectionName = SectionName.Text,
                Title = Title.Text,
                Url = Url.Text,
                Content = Content.Text,
                CreateTime = DateTime.Now,
                Picture = Picture.Text,
                Source = Source.Text,
                Keyword = Keyword.Text,
                Author = Author.Text,
                CollectTime = DateTime.Now,
                IsCleaned = 1,
                SPReturnType = AppEnum.SPReturnTypes.Object,
                SPAction = "Insert"
            };
            Insert(obj);

            var wc = new XWebClient();
            wc.DownloadData(ConfigurationManager.AppSettings["SolrCoreCrawlDeltaImp"]);
        }

        protected void BTNEdit_Click(object sender, EventArgs e)
        {
            var ob = BllCrawlResult.GeById(Id.Text);
            if (ob == null) return;
            ob.Title = Title.Text;
            ob.Content = Content.Text;
            ob.Picture = Picture.Text;
            ob.Source = Source.Text;
            ob.Keyword = Keyword.Text;
            ob.Author = Author.Text;
            ob.IsCleaned = 1;
            ob.CollectTime = DateTime.Now;
            ob.SPReturnType = AppEnum.SPReturnTypes.Null;
            ob.SPAction = "Update";
            Update(ob);

            var wc = new XWebClient();
            wc.DownloadData(ConfigurationManager.AppSettings["SolrCoreCrawlDeltaImp"]);
        }

        protected void BTNDel_Click(object sender, EventArgs e)
        {
            var ob = BllCrawlResult.GeById(Id.Text);
            if (ob == null) return;
            ob.IsCleaned = 2;
            ob.CollectTime = DateTime.Now;
            ob.SPReturnType = AppEnum.SPReturnTypes.Null;
            ob.SPAction = "Update";
            Update(ob);

            var wc = new XWebClient();
            wc.DownloadData(ConfigurationManager.AppSettings["SolrCoreCrawlDeltaImp"]);
        }

        protected void wname_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_dtCrawlSite.Tables.Count > 0)
            {
                sname.Items.Clear();
                var webname = wname.SelectedValue;
                if (webname != "")
                {
                    sname.Items.Add(new ListItem
                    {
                        Value = "",
                        Text = "选择一个"
                    });
                    var dt = _dtCrawlSite.Tables[0];
                    foreach (DataRow dataRow in dt.Rows)
                    {
                        var val = dataRow["WebName"].ToString();
                        if (webname == val && sname.Items.FindByValue(val) == null)
                        {
                            sname.Items.Add(new ListItem
                            {
                                Value = dataRow["SectionName"].ToString(),
                                Text = dataRow["SectionName"].ToString()
                            });
                        }
                    }
                }
                else
                {
                    sname.Items.Add(new ListItem
                    {
                        Value = "",
                        Text = "选择一个站点"
                    });
                }
            }
        }

        protected void BTNCount_Click(object sender, EventArgs e)
        {
            var wc = new XWebClient();
            wc.DownloadData(ConfigurationManager.AppSettings["SolrCoreCrawlFullImp"]);
        }
        protected void BTNQc_Click(object sender, EventArgs e)
        {
            string sql = @"delete tmp from(    
                            select row_num = row_number() over(partition by Title, Url order by Title desc)    
                                from CrawlResult 
                             ) tmp    
                         where row_num > 1   ";
            BLL.BllBaseService.ExecuteSql(sql);
        }
        protected void BTNDeltaImp_Click(object sender, EventArgs e)
        {
            var wc = new XWebClient();
            wc.DownloadData(ConfigurationManager.AppSettings["SolrCoreCrawlDeltaImp"]);
        }
    }
}

