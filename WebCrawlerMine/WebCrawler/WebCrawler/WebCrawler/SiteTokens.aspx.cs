﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using BLL;
using Model;
using Newtonsoft.Json;
using Utility;

namespace WebCrawler
{
    public partial class SiteTokens : Page
    {
        private ModelSiteTokens _user;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!RightCheck.CookieLoginCheck(Request, out _user))
            {
                Response.Redirect("/Login.aspx");
            }
        }
        /// <summary>
        /// 增加记录
        /// </summary>
        /// <returns>int</returns> 
        private long Insert(ModelSiteTokens obj)
        {
            string errMsg;
            long newID = BllSiteTokens.InsertUnique(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                //Error Logic
            }
            return newID;
        }

        /// <summary>
        /// 删除记录
        /// </summary>
        private void Delete(ModelSiteTokens obj)
        {
            string errMsg;
            BllSiteTokens.Delete(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                //Error Logic
                return;
            }
        }

        /// <summary>
        /// 修改记录
        /// </summary>
        private void Update(ModelSiteTokens obj)
        {
            string errMsg;
            BllSiteTokens.UpdateUnique(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                //Error Logic
                return;
            }
        }

        /// <summary>
        /// 查询分页记录
        /// </summary>
        /// <param name="startIndex">数据开始索引值</param> 
        /// <param name="pageSize">数据每页条数</param> 
        /// <returns>DataSet</returns> 
        protected DataSet SelectPaging(int startIndex, int pageSize)
        {
            string errMsg;
            var obj = new ModelSiteTokens
                {
                    StartIndex = startIndex,
                    PageSize = pageSize,
                    SPReturnType = AppEnum.SPReturnTypes.DataSet,
                    SPAction = "SelectPaging"
                };
            DataSet ds = BllSiteTokens.SelectPaging(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                //Error Logic
                return null;
            }
            return ds;
        }

        ///// <summary>
        ///// 查询分页记录
        ///// </summary>
        ///// <param name="startIndex">数据开始索引值</param> 
        ///// <param name="pageSize">数据每页条数</param>
        ///// <param name="rcount"></param>
        ///// <param name="pcount"></param>
        ///// <returns>DataSet</returns> 
        //protected DataSet GetPager(int startIndex, int pageSize, out int rcount, out int pcount)
        //{
        //    return BllSiteTokens.GetPager(startIndex, pageSize, out rcount, out pcount);
        //}

        /// <summary>
        /// 查询分页记录
        /// </summary>
        /// <param name="startIndex">数据开始索引值</param> 
        /// <param name="pageSize">数据每页条数</param>
        /// <param name="rcount"></param>
        /// <param name="pcount"></param>
        /// <param name="webName"></param>
        /// <param name="sectionName"></param>
        /// <returns>DataSet</returns> 
        protected string GetPager(int startIndex, int pageSize, out int rcount, out int pcount, string webName, string sectionName)
        {
            var res = new Dictionary<string, object>();
            var condition = "";
            if (webName != "")
            {
                condition = string.Format("[WebName] = {0}", webName);
            }
            if (webName != "" && sectionName != "")
            {
                condition = string.Format("[WebName] = {0} AND [SectionName] = {1}", webName, sectionName);
            }
            var ds = BllSiteTokens.GetPager(startIndex, pageSize, out rcount, out pcount, condition);
            res.Add("page", startIndex);
            res.Add("total", pcount);
            res.Add("records", rcount);
            res.Add("rows", ds.Tables[0]);
            return JsonConvert.SerializeObject(res);
        }

        /// <summary>
        /// 查询全部记录
        /// </summary>
        /// <returns>DataSet</returns> 
        protected DataSet SelectAll()
        {
            string errMsg;
            var obj = new ModelSiteTokens
                {
                    SPReturnType = AppEnum.SPReturnTypes.DataSet,
                    SPAction = "SelectAll"
                };
            DataSet ds = BllSiteTokens.SelectAll(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                //Error Logic
                return null;
            }
            return ds;
        }

        /// <summary>
        /// 判断是否出错
        /// </summary>
        /// <param name="errMsg">错误信息</param>
        private bool CheckIsError(string errMsg)
        {
            if (!string.IsNullOrEmpty(errMsg))
            {
                //Catch Error 
                return true;
            };
            return false;
        }

        protected void BTNNew_Click(object sender, EventArgs e)
        {
            var obj = new ModelSiteTokens
            {
                Token = Token.Text,
                SiteName = SiteName.Text,
                IsEnabled = int.Parse(IsEnabled.SelectedValue),
                Ip = Ip.Text,
                SPReturnType = AppEnum.SPReturnTypes.Object,
                SPAction = "Insert"
            };
            Insert(obj);
        }

        protected void BTNEdit_Click(object sender, EventArgs e)
        {
            var ob = BllSiteTokens.GeById(Id.Text);
            if (ob == null) return;
            ob.Token = Token.Text;
            ob.SiteName = SiteName.Text;
            ob.IsEnabled = int.Parse(IsEnabled.SelectedValue);
            ob.Ip = Ip.Text;
            ob.SPReturnType = AppEnum.SPReturnTypes.Null;
            ob.SPAction = "Update";
            Update(ob);
        }

        protected void BTNDisbale_Click(object sender, EventArgs e)
        {
            var ob = BllSiteTokens.GeById(Id.Text);
            if (ob == null) return;
            ob.SPAction = "Update";
            ob.IsEnabled = 0;
            Update(ob);
        }

        protected void BTNEnable_Click(object sender, EventArgs e)
        {
            var ob = BllSiteTokens.GeById(Id.Text);
            if (ob == null) return;
            ob.SPAction = "Update";
            ob.IsEnabled = 1;
            Update(ob);
        }
        protected void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            if (HttpContext.Current.Server.GetLastError() is HttpRequestValidationException)
            {
                HttpContext.Current.Response.Write("请输入正确的字符串，谢谢合作。【<a href=\"javascript:history.back(0);\">返回</a>】");
                HttpContext.Current.Server.ClearError();
            }
        } 
    }
}

