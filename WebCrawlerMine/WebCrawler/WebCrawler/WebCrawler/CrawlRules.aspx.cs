﻿using System;
using System.Data;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using Model;
using Utility;

namespace WebCrawler
{
    public class XWebClient : WebClient
    {
        protected override WebRequest GetWebRequest(Uri address)
        {
            HttpWebRequest request = base.GetWebRequest(address) as HttpWebRequest;
            request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            return request;
        }
    }
    public partial class CrawlRules : Page
    {
        public string Content = "";
        private ModelSiteTokens _user;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!RightCheck.CookieLoginCheck(Request, out _user))
            {
                Response.Redirect("/Login.aspx");
            }
            if (!IsPostBack)
            {
                var url = HttpContext.Current.Request.QueryString["crawlurl"];
                if (url != null)
                {
                    TBUrl.Text = url;
                    //GetDataAndShow();
                    IsFinished.Text = "0";
                    RuleName.Text = HttpContext.Current.Request.QueryString["name"];
                    Version.Text = "1";
                }
            }
        }

        private void GetDataAndShow()
        {
            string url = TBUrl.Text;
            if (url == "") return;
            try
            {
                url = HttpContext.Current.Server.UrlDecode(url);

                var wc = new XWebClient();
                var btWeb = wc.DownloadData(url);
                var strWeb = System.Text.Encoding.GetEncoding(DDLCode.Text).GetString(btWeb);
                LTContent.Text = HttpHelper.GetBodyWithoutScript(strWeb, true, !CBKeepDesc.Checked, !CBKeepScript.Checked);
            }
            catch (Exception ex)
            {
                LTContent.Text = "请检查url链接是否无误，message:" + ex.Message;
            }
        }

        protected void BTNGo_Click(object sender, EventArgs e)
        {
            GetDataAndShow();
        }
        /// <summary>
        /// 增加记录
        /// </summary>
        /// <returns>int</returns> 
        private long Insert(ModelCrawlRules obj)
        {
            string errMsg;

            long newID = BllCrawlRules.InsertUnique(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                //Error Logic
            }
            return newID;
        }

        /// <summary>
        /// 删除记录
        /// </summary>
        private void Delete(ModelCrawlRules obj)
        {
            string errMsg;
            BllCrawlRules.Delete(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                //Error Logic
                return;
            }
        }

        /// <summary>
        /// 修改记录
        /// </summary>
        private void Update(ModelCrawlRules obj)
        {
            string errMsg;

            BllCrawlRules.UpdateUnique(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                //Error Logic
                return;
            }
        }

        /// <summary>
        /// 查询分页记录
        /// </summary>
        /// <param name="startIndex">数据开始索引值</param> 
        /// <param name="pageSize">数据每页条数</param> 
        /// <returns>DataSet</returns> 
        protected DataSet SelectPaging(int startIndex, int pageSize)
        {
            string errMsg;
            var obj = new ModelCrawlRules
                {
                    StartIndex = startIndex,
                    PageSize = pageSize,
                    SPReturnType = AppEnum.SPReturnTypes.DataSet
                };
            DataSet ds = BllCrawlRules.SelectPaging(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                //Error Logic
                return null;
            }
            return ds;
        }

        /// <summary>
        /// 查询分页记录
        /// </summary>
        /// <param name="startIndex">数据开始索引值</param> 
        /// <param name="pageSize">数据每页条数</param>
        /// <param name="rcount"></param>
        /// <param name="pcount"></param>
        /// <returns>DataSet</returns> 
        protected DataSet GetPager(int startIndex, int pageSize, out int rcount, out int pcount)
        {
            return BllCrawlRules.GetPager(startIndex, pageSize, out rcount, out pcount);
        }

        /// <summary>
        /// 查询全部记录
        /// </summary>
        /// <returns>DataSet</returns> 
        protected DataSet SelectAll()
        {
            string errMsg;
            var obj = new ModelCrawlRules
                {
                    SPReturnType = AppEnum.SPReturnTypes.DataSet
                };
            DataSet ds = BllCrawlRules.SelectAll(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                //Error Logic
                return null;
            }
            return ds;
        }

        /// <summary>
        /// 判断是否出错
        /// </summary>
        /// <param name="errMsg">错误信息</param>
        private bool CheckIsError(string errMsg)
        {
            if (!string.IsNullOrEmpty(errMsg))
            {
                //Catch Error 
                return true;
            };
            return false;
        }


        protected void BTNNew_Click(object sender, EventArgs e)
        {
            var obj = new ModelCrawlRules
            {
                Id = 0,
                RuleName = RuleName.Text,
                IsFinished = 0,
                SubmitType = SubmitType.Text,
                TitleXpath = TitleXpath.Text,
                KeywordSpliter = KeywordSpliter.Text,
                KeywordXpath = KeywordXpath.Text,
                UrlXpath = UrlXpath.Text,
                UrlAttr = UrlAttr.Text,
                ContentXpath = ContentXpath.Text,
                CreateTimeXpath = CreateTimeXpath.Text,
                CreateTimeReg = CreateTimeReg.Text,
                PictureXpath = PictureXpath.Text,
                PictureAttr = PictureAttr.Text,
                SourceXpath = SourceXpath.Text,
                SourceReg = SourceReg.Text,
                AuthorXpath = AuthorXpath.Text,
                AuthorReg = AuthorReg.Text,
                Version = 0,
                CreateTime = DateTime.Now,
                DeleteFlag = 0,
                SPReturnType = AppEnum.SPReturnTypes.Object,
                SPAction = "Insert"
            };

            Insert(obj);
        }

        protected void BTNEdit_Click(object sender, EventArgs e)
        {
            var obj = BllCrawlRules.GeById(Id.Text);
            if (obj == null) return;
            obj.RuleName = RuleName.Text;
            obj.SubmitType = SubmitType.Text;
            obj.TitleXpath = TitleXpath.Text;
            obj.KeywordSpliter = KeywordSpliter.Text;
            obj.KeywordXpath = KeywordXpath.Text;
            obj.UrlXpath = UrlXpath.Text;
            obj.UrlAttr = UrlAttr.Text;
            obj.ContentXpath = ContentXpath.Text;
            obj.CreateTimeXpath = CreateTimeXpath.Text;
            obj.CreateTimeReg = CreateTimeReg.Text;
            obj.PictureXpath = PictureXpath.Text;
            obj.PictureAttr = PictureAttr.Text;
            obj.SourceXpath = SourceXpath.Text;
            obj.SourceReg = SourceReg.Text;
            obj.AuthorXpath = AuthorXpath.Text;
            obj.AuthorReg = AuthorReg.Text;
            obj.Version = int.Parse(Version.Text);
            obj.CreateTime = DateTime.Now;
            obj.DeleteFlag = 0;
            obj.SPReturnType = AppEnum.SPReturnTypes.Null;
            obj.SPAction = "Update";
            Update(obj);
        }

        protected void BTNDel_Click(object sender, EventArgs e)
        {
            var obj = BllCrawlRules.GeById(Id.Text);
            if (obj == null) return;
            obj.DeleteFlag = 1;
            obj.SPReturnType = AppEnum.SPReturnTypes.Null;
            obj.SPAction = "Update";
            Update(obj);
        }
        protected void BTNStart_Click(object sender, EventArgs e)
        {
            var obj = BllCrawlRules.GeById(Id.Text);
            if (obj == null) return;
            obj.DeleteFlag = 0;
            obj.SPReturnType = AppEnum.SPReturnTypes.Null;
            obj.SPAction = "Update";
            Update(obj);
        }
        protected void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            if (HttpContext.Current.Server.GetLastError() is HttpRequestValidationException)
            {
                HttpContext.Current.Response.Write("请输入正确的字符串，谢谢合作。【<a href=\"javascript:history.back(0);\">返回</a>】");
                HttpContext.Current.Server.ClearError();
            }
        }

        protected void BTNDelete_Click(object sender, EventArgs e)
        {
            var obj = BllCrawlRules.GeById(Id.Text);
            if (obj == null) return;
            Delete(obj);
        }
    }
}

