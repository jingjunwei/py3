﻿<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" CodeBehind="CrawlTask.aspx.cs" Inherits="WebCrawler.CrawlTask" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>CrawlTask</title>
    <link href="/Content/bootstrap.min.css" rel="stylesheet" />
    <link href="/Content/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="/Content/themes/base/jquery-ui.min.css" rel="stylesheet" />
    <link href="/Content/ui.jqgrid.css" rel="stylesheet" />
    <link href="/Content/select.min.css" rel="stylesheet" />
</head>
<body>
    <div class="row">
        <div class="col-md-4">
            <div class="col-md-9">
                <button class="btn green" onclick="search_user()">
                    <i class="fa fa-search"></i>
                    检索
                </button>
            </div>
        </div>
    </div>
    <form runat="server">
        <div id="search">
            <div class="row">
                <div class="form-group col-md-4">
                    <div class="search-label col-md-4">规则</div>
                    <div class="col-md-8">
                        <%--<input type="text" class="form-control " name="rid" placeholder="规则主键">--%>
                        <asp:DropDownList ID="rid" class="form-control" runat="server" name="rid" placeholder="规则主键"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <div class="search-label col-md-4">用户站点</div>
                    <div class="col-md-8">
                        <asp:DropDownList ID="sUid" class="form-control" runat="server" name="sUid" placeholder="用户站点"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <div class="search-label col-md-4">站点模块</div>
                    <div class="col-md-8">
                        <%--<input type="text" class="form-control " name="wsid" placeholder="站点模块主键">--%>
                        <asp:DropDownList ID="wsid" class="form-control" runat="server" name="wsid" placeholder="站点模块主键"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <div class="search-label col-md-4">是否启动</div>
                    <div class="col-md-8">
                        <select class="form-control" name="started" id="started">
                            <option value="">选择一个</option>
                            <option value="1">已启动</option>
                            <option value="0">未启动</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-scrollable col-md-12">
                    <table id="grid"></table>
                    <div id="pager"></div>
                </div>
            </div>
        </div>
        <br />
        <div class="row" id="editform">
            <asp:TextBox ID="Id" class="form-control hidden " name="Id" runat="server"></asp:TextBox>
            <div class="form-group col-md-4">
                <div class="search-label col-md-4">用户站点</div>
                <div class="col-md-8">
                    <asp:DropDownList ID="Uid" class="form-control" runat="server" name="Uid" placeholder="用户站点"></asp:DropDownList>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="search-label col-md-4">规则</div>
                <div class="col-md-8">
                    <asp:DropDownList ID="RuleId" class="form-control" runat="server" name="RuleId" placeholder="规则"></asp:DropDownList>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="search-label col-md-4">站点模块</div>
                <div class="col-md-8">
                    <asp:DropDownList ID="SiteSectionId" class="form-control" runat="server" name="SiteSectionId" placeholder="站点模块"></asp:DropDownList>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="search-label col-md-4">带参数的链接（新建时请检查）</div>
                <div class="col-md-8">
                    <asp:TextBox ID="PageUrlWithParam" class="form-control " placeholder="带参数的链接" name="PageUrlWithParam" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="search-label col-md-4">是否启动</div>
                <div class="col-md-8">
                    <asp:DropDownList ID="IsStarted" class="form-control" runat="server" name="IsStarted" placeholder="站点模块">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="search-label col-md-4">是否代理</div>
                <div class="col-md-8">
                    <asp:DropDownList ID="UseProxy" class="form-control" runat="server" name="UseProxy" placeholder="是否代理">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <asp:Button ID="BTNNew" class="btn" runat="server" Text="新建任务" OnClick="BTNNew_Click" />
        <asp:Button ID="BTNEdit" class="btn" runat="server" Text="修改任务" OnClick="BTNEdit_Click" />
        <br />
        <br />
        <asp:Button ID="BTNStart" class="btn" runat="server" Text="启动选中任务" OnClick="BTNStart_Click" />
        <asp:Button ID="BTNDel" class="btn" runat="server" Text="停止选中任务" OnClick="BTNDel_Click" />
        <asp:Button ID="BTNStartAll" class="btn" runat="server" Text="启动所有任务" OnClick="BTNStartAll_Click" />
        <asp:Button ID="BTNStopAll" class="btn" runat="server" Text="停止所有任务" OnClick="BTNStopAll_Click" />
        <br />
        <br />
        <asp:Button ID="BTNDelete" class="btn" runat="server" Text="删除任务" OnClick="BTNDelete_Click" />
        <br />
        <br />
        <asp:Button ID="BTNTest" class="btn" runat="server" Text="测试选中任务" OnClick="BTNTest_Click" />
        <asp:Button ID="BTNDelLog" class="btn" runat="server" Text="清空测试日志" OnClick="BTNDelLog_Click" />
        <asp:TextBox runat="server" TextMode="MultiLine" Style="width: 100%; height: 200px" ID="TBLog"></asp:TextBox>
    </form>
    <script src="/Scripts/jquery-1.12.4.min.js"></script>
    <script src="/Scripts/common.js"></script>
    <script src="/Scripts/bootstrap.min.js"></script>
    <script src="/Scripts/jquery-ui-1.12.0.min.js"></script>
    <script src="/Scripts/jqGrid/jquery.jqGrid.min.js"></script>
    <script src="/Scripts/jqGrid/grid.locale-cn.js"></script>
    <script src="/Scripts/select.min.js"></script>
    <script>
        function getQueryString(name) {
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) { return unescape(r[2]); } return null;
        }

        var crawlsites = JSON.parse('<%=CrawlSites%>');
        // 数据查询
        jQuery("#grid").jqGrid({
            url: '/Service.ashx?m=GetCrawlTaskPager',
            mtype: "GET",
            datatype: "json",
            height: 'auto',
            autowidth: true, //自动宽
            colNames: ['Id', '用户站点', '规则主键', '站点模块主键', '带参数的链接', '是否启动', '代理使用'],
            colModel: [
                { name: 'Id', index: 'Id', width: 55 },
                { name: 'Uid', index: 'Uid', align: "center", hidden: true },
                { name: 'RuleId', index: 'RuleId', align: "center", hidden: true },
                {
                    name: 'SiteSectionId', index: 'SiteSectionId', align: "center",
                    formatter: function (cellvalue, options, rowObject) {
                        var returnval = "";
                        for (var i = 0; i < crawlsites.length; i++) {
                            var obj = crawlsites[i];
                            if (cellvalue === obj['Id']) {
                                return obj['Id'] + ":" + obj["WebName"] + ":" + obj["SectionName"];
                            }
                        }
                        return returnval;
                    }
                },
                { name: 'PageUrlWithParam', index: 'PageUrlWithParam', align: "center" },
                {
                    name: 'IsStarted', index: 'IsStarted', align: "center",
                    formatter: function (cellvalue, options, rowObject) {
                        if (cellvalue) {
                            return "已启动";
                        }
                        return "未启动";
                    }
                },
                {
                    name: 'UseProxy', index: 'UseProxy', align: "center",
                    formatter: function (cellvalue, options, rowObject) {
                        if (cellvalue) {
                            return "使用代理";
                        }
                        return "不使用代理";
                    }
                }
            ],
            rowNum: 30,
            rowList: [10, 20, 30, 100],
            pager: '#pager',
            sortname: 'Id',
            viewrecords: true,
            rownumbers: true, //添加左侧行号
            sortorder: "desc",
            caption: "可用站点",
            jsonReader: { repeatitems: false },
            onSelectRow: function (rowid) {
                var row = $("#grid").jqGrid("getRowData", rowid);
                $("#editform .form-control").bindData(row);
                $("#RuleId").val(row["RuleId"]);
                var sid = row["SiteSectionId"];
                $("#SiteSectionId").val(sid.substr(0,sid.indexOf(":")));
                $("#IsStarted").val(row["IsStarted"] === "已启动" ? "1" : "0");
                $("#UseProxy").val(row["UseProxy"] === "使用代理" ? "1" : "0");
            }
        });

        //查询
        function search_user() {
            var queryParams = {};
            $($("#search .form-group .form-control").serializeArray()).each(function (i, obj) {
                queryParams[obj.name] = obj.value;
            });
            $("#grid").jqGrid("setGridParam", { postData: queryParams }).trigger("reloadGrid");
        }

        function formatRepoProvince(repo) {
            if (repo.loading) return repo.text;
            var markup = "<div>" + repo.name + "</div>";
            return markup;
        }

    </script>
</body>
</html>

