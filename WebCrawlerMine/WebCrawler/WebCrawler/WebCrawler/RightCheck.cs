﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using BLL;
using Model;

namespace WebCrawler
{
    public class RightCheck
    {
        /// <summary>
        /// 本地登录检查
        /// </summary>
        /// <param name="request"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        internal static bool CookieLoginCheck(HttpRequest request, out Model.ModelSiteTokens user)
        {
            if (request.Cookies["name"] == null || request.Cookies["token"] == null || request.Cookies["secret"] == null)
            {
                user = null;
                return false;
            }
            var name = HttpContext.Current.Server.UrlDecode(request.Cookies["name"].Value);
            var token = HttpContext.Current.Server.UrlDecode(request.Cookies["token"].Value);
            user = BllSiteTokens.LoginCheck(name, token);
            if (user != null)
            {
                var ip = GetHostAddress();
                if (IsInIpsList(ip, user.Ip))
                {
                    var urlDecode = HttpContext.Current.Server.UrlDecode(request.Cookies["secret"].Value);
                    if (urlDecode != null)
                    {
                        var secret = urlDecode.ToUpper();
                        if (Utility.Common.MD5Encrypt(name + token + DateTime.Now.ToString("yyyy-MM-dd")).ToUpper() == secret)
                        {
                            AddCookies(name, token);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 返回Cookies
        /// </summary>
        /// <param name="name"></param>
        /// <param name="token"></param>
        internal static void AddCookies(string name, string token)
        {
            var nameCookie = new HttpCookie("name")
            {
                Value = HttpContext.Current.Server.UrlEncode(name),
                Expires = DateTime.Now.AddDays(1)
            };
            HttpContext.Current.Response.Cookies.Add(nameCookie);
            var tokenCookie = new HttpCookie("token")
            {
                Value = HttpContext.Current.Server.UrlDecode(token),
                Expires = DateTime.Now.AddDays(1)
            };
            HttpContext.Current.Response.Cookies.Add(tokenCookie);
            var urlDecode =
                HttpContext.Current.Server.UrlDecode(
                    Utility.Common.MD5Encrypt(name + token + DateTime.Now.ToString("yyyy-MM-dd")));
            if (urlDecode != null)
            {
                var secretCookie = new HttpCookie("secret")
                {
                    Value = urlDecode.ToUpper(),
                    Expires = DateTime.Now.AddDays(1)
                };
                HttpContext.Current.Response.Cookies.Add(secretCookie);
            }
        }

        /// <summary>
        /// 本地登录检查
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        internal static bool CookieLoginCheck(HttpRequest request)
        {
            ModelSiteTokens user;
            return CookieLoginCheck(request, out user);
        }

        /// <summary>
        /// 接口权限检查
        /// </summary>
        /// <param name="request"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        internal static bool ServiceRightCheck(System.Collections.Specialized.NameValueCollection request, out Model.ModelSiteTokens user)
        {
            if (request["name"] == null || request["token"] == null || request["secret"] == null)
            {
                user = null;
                return false;
            }

            var name = HttpContext.Current.Server.UrlDecode(request["name"]);
            var token = HttpContext.Current.Server.UrlDecode(request["token"]);
            user = BllSiteTokens.LoginCheck(name, token);
            if (user != null)
            {
                var ip = GetHostAddress();
                Trace.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(user),
                    "IP获取为:" + ip + "; IsInIpsList:" + RightCheck.IsInIpsList(ip, user.Ip) + ";");
                if (IsInIpsList(ip, user.Ip))
                {
                    var urlDecode = HttpContext.Current.Server.UrlDecode(request["secret"]);
                    if (urlDecode != null)
                    {
                        var secret = urlDecode.ToUpper();
                        if (Utility.Common.MD5Encrypt(name + token + DateTime.Now.ToString("yyyy-MM-dd")).ToUpper() ==
                            secret)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 接口权限检查
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        internal static bool ServiceRightCheck(System.Collections.Specialized.NameValueCollection request)
        {
            ModelSiteTokens user;
            return ServiceRightCheck(request, out user);
        }

        /// <summary>
        /// 获取客户端IP地址（无视代理）
        /// </summary>
        /// <returns>若失败则返回回送地址</returns>
        public static string GetHostAddress()
        {
            string userHostAddress = HttpContext.Current.Request.UserHostAddress;

            if (string.IsNullOrEmpty(userHostAddress))
            {
                userHostAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }

            //最后判断获取是否成功，并检查IP地址的格式（检查其格式非常重要）
            if (!string.IsNullOrEmpty(userHostAddress) && IsIp(userHostAddress))
            {
                return userHostAddress;
            }
            return "127.0.0.1";
        }

        /// <summary>
        /// 检查IP地址格式
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public static bool IsIp(string ip)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(ip, @"^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$");
        }

        public static bool IsInIpsList(string ip, string ipsListStr)
        {
            var ipsList = ipsListStr.Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            return ipsList.Any(s => IsInIps(ip, s));
        }

        public static bool IsInIps(string ip, string ips)
        {
            long ipsS;
            long ipsE;
            if (ips.Contains("-"))
            {
                var ipsList = ips.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
                ipsS = Ip2Int(ipsList[0]);
                ipsE = Ip2Int(ipsList[1]);
            }
            else if (ips.Contains("*"))
            {
                ipsS = Ip2Int(ips.Replace("*", "000"));
                ipsE = Ip2Int(ips.Replace("*", "255"));
            }
            else
            {
                return ip == ips;
            }
            var ipI = Ip2Int(ip);
            return ipI >= ipsS && ipI <= ipsE;
        }

        private static long Ip2Int(string ip)
        {
            var ipnumList = ip.Split(new[] { "." }, StringSplitOptions.RemoveEmptyEntries);
            var str = ipnumList.Aggregate("", (current, s) => current + string.Format("{0:D3}", short.Parse(s)));
            return long.Parse(str);
        }
    }
}