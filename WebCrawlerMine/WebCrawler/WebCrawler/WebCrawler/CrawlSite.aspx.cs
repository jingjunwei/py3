﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using Model;
using Utility;

namespace WebCrawler
{
    public partial class CrawlSite : Page
    {
        private readonly string[] _adminName = ConfigurationManager.AppSettings["Admin"].Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
        private ModelSiteTokens _user;
        private DataSet _dtCrawlSite = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!RightCheck.CookieLoginCheck(Request, out _user))
            {
                Response.Redirect("/Login.aspx");
            }
            int rcount;
            int pcount;
            var condition = "1=1";
            if (!((IList) _adminName).Contains(_user.SiteName))
            {
                condition += " AND [Uid] = " + _user.Id;
            }
            _dtCrawlSite = BllBaseService.GetPager(1, int.MaxValue, out rcount, out pcount, "CrawlSite", condition);
            if (!IsPostBack)
            {
                sType.Items.Add(new ListItem
                {
                    Value = "",
                    Text = "选择一个"
                });
                wname.Items.Add(new ListItem
                {
                    Value = "",
                    Text = "选择一个"
                });
                sname.Items.Add(new ListItem
                {
                    Value = "",
                    Text = "选择一个站点"
                });
                if (!((IList) _adminName).Contains(_user.SiteName))
                {
                    condition = " [Id] = " + _user.Id;
                }
                else
                {
                    Uid.Items.Add(new ListItem
                    {
                        Value = "",
                        Text = "选择一个"
                    });
                    sUid.Items.Add(new ListItem
                    {
                        Value = "",
                        Text = "选择一个"
                    });
                }
                var ds = BllBaseService.GetPager(1, int.MaxValue, out rcount, out pcount, "SiteTokens", condition);
                if (ds.Tables.Count > 0)
                {
                    var dt = ds.Tables[0];

                    foreach (DataRow dataRow in dt.Rows)
                    {
                        sUid.Items.Add(new ListItem
                        {
                            Value = dataRow["Id"].ToString(),
                            Text = dataRow["Id"] + ":" + dataRow["SiteName"]
                        });
                        Uid.Items.Add(new ListItem
                        {
                            Value = dataRow["Id"].ToString(),
                            Text = dataRow["Id"] + ":" + dataRow["SiteName"]
                        });
                    }
                }

                if (_dtCrawlSite.Tables.Count > 0)
                {
                    var dt = _dtCrawlSite.Tables[0];
                    foreach (DataRow dataRow in dt.Rows)
                    {
                        var val = dataRow["Type"].ToString();
                        if (sType.Items.FindByValue(val) == null)
                        {
                            sType.Items.Add(new ListItem
                            {
                                Value = dataRow["Type"].ToString(),
                                Text = dataRow["Type"].ToString()
                            });
                        }

                        val = dataRow["WebName"].ToString();
                        if (wname.Items.FindByValue(val) == null)
                        {
                            wname.Items.Add(new ListItem
                            {
                                Value = dataRow["WebName"].ToString(),
                                Text = dataRow["WebName"].ToString()
                            });
                        }

                    }
                }

            }
        }

        /// <summary>
        /// 增加记录
        /// </summary>
        /// <returns>int</returns> 
        private long Insert(ModelCrawlSite obj)
        {
            string errMsg;

            long newID = BllCrawlSite.InsertUnique(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                //Error Logic
            }
            return newID;
        }

        /// <summary>
        /// 删除记录
        /// </summary>
        private void Delete()
        {
            string errMsg;
            var obj = new ModelCrawlSite
                {
                    Id = 0,
                    SPReturnType = AppEnum.SPReturnTypes.Null
                };
            BllCrawlSite.Delete(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                //Error Logic
                return;
            }
        }

        /// <summary>
        /// 修改记录
        /// </summary>
        private void Update(ModelCrawlSite obj)
        {
            string errMsg;

            BllCrawlSite.UpdateUnique(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                //Error Logic
                return;
            }
        }

        /// <summary>
        /// 查询分页记录
        /// </summary>
        /// <param name="startIndex">数据开始索引值</param> 
        /// <param name="pageSize">数据每页条数</param> 
        /// <returns>DataSet</returns> 
        protected DataSet SelectPaging(int startIndex, int pageSize)
        {
            string errMsg;
            var obj = new ModelCrawlSite
                {
                    StartIndex = startIndex,
                    PageSize = pageSize,
                    SPReturnType = AppEnum.SPReturnTypes.DataSet
                };
            DataSet ds = BllCrawlSite.SelectPaging(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                //Error Logic
                return null;
            }
            return ds;
        }

        /// <summary>
        /// 查询分页记录
        /// </summary>
        /// <param name="startIndex">数据开始索引值</param> 
        /// <param name="pageSize">数据每页条数</param>
        /// <param name="rcount"></param>
        /// <param name="pcount"></param>
        /// <returns>DataSet</returns> 
        protected DataSet GetPager(int startIndex, int pageSize, out int rcount, out int pcount)
        {
            return BllCrawlSite.GetPager(startIndex, pageSize, out rcount, out pcount);
        }

        /// <summary>
        /// 查询全部记录
        /// </summary>
        /// <returns>DataSet</returns> 
        protected DataSet SelectAll()
        {
            string errMsg;
            var obj = new ModelCrawlSite
                {
                    SPReturnType = AppEnum.SPReturnTypes.DataSet
                };
            DataSet ds = BllCrawlSite.SelectAll(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                //Error Logic
                return null;
            }
            return ds;
        }

        /// <summary>
        /// 判断是否出错
        /// </summary>
        /// <param name="errMsg">错误信息</param>
        private bool CheckIsError(string errMsg)
        {
            if (!string.IsNullOrEmpty(errMsg))
            {
                //Catch Error 
                return true;
            };
            return false;
        }

        protected void BTNNew_Click(object sender, EventArgs e)
        {
            if (WebName.Text != "" && WebLink.Text != "" && SectionName.Text != "" && SectionIndexUrl.Text != "" && SiteFrom.Text != "" && DeleteFlag.Text != "")
            {
                var obj = new ModelCrawlSite
                {
                    Id = 0,
                    Uid = int.Parse(Uid.SelectedValue),
                    Type = Type.Text,
                    WebName = WebName.Text,
                    WebLink = WebLink.Text,
                    SectionName = SectionName.Text,
                    IndexDiff = int.Parse(IndexDiff.SelectedValue),
                    SectionIndexUrl = SectionIndexUrl.Text,
                    Start = Start.Text,
                    Step = Step.Text,
                    SiteFrom = SiteFrom.Text,
                    DeleteFlag = int.Parse(DeleteFlag.SelectedValue),
                    SPReturnType = AppEnum.SPReturnTypes.Object,
                    SPAction = "Insert"
                };
                Insert(obj);
            }
            else
            {
                Response.Write("<script>alert('不能有空值！');</script>");
            }
        }

        protected void BTNEdit_Click(object sender, EventArgs e)
        {
            var ob = BllCrawlSite.GeById(Id.Text);
            if (ob == null) return;
            ob.Uid = int.Parse(Uid.SelectedValue);
            ob.Type = Type.Text;
            ob.WebName = WebName.Text;
            ob.WebLink = WebLink.Text;
            ob.SectionName = SectionName.Text;
            ob.IndexDiff = int.Parse(IndexDiff.SelectedValue);
            ob.SectionIndexUrl = SectionIndexUrl.Text;
            ob.Start = Start.Text;
            ob.Step = Step.Text;
            ob.SiteFrom = SiteFrom.Text;
            ob.DeleteFlag = int.Parse(DeleteFlag.SelectedValue);
            ob.SPReturnType = AppEnum.SPReturnTypes.Null;
            ob.SPAction = "Update";
            Update(ob);
        }

        protected void BTNDel_Click(object sender, EventArgs e)
        {
            var ob = BllCrawlSite.GeById(Id.Text);
            if (ob == null) return;
            ob.DeleteFlag = 1;
            ob.SPReturnType = AppEnum.SPReturnTypes.Null;
            ob.SPAction = "Update";
            Update(ob);
        }
        protected void BTNDone_Click(object sender, EventArgs e)
        {
            var ob = BllCrawlSite.GeById(Id.Text);
            if (ob == null) return;
            ob.DeleteFlag = 0;
            ob.SPReturnType = AppEnum.SPReturnTypes.Null;
            ob.SPAction = "Update";
            Update(ob);
        }

        protected void Page_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            if (HttpContext.Current.Server.GetLastError() is HttpRequestValidationException)
            {
                HttpContext.Current.Response.Write("请输入正确的字符串，谢谢合作。【<a href=\"javascript:history.back(0);\">返回</a>】");
                HttpContext.Current.Server.ClearError();
            }
        }

        protected void wname_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_dtCrawlSite.Tables.Count > 0)
            {
                sname.Items.Clear();
                var webname = wname.SelectedValue;
                if (webname == "")
                {
                    sname.Items.Add(new ListItem
                    {
                        Value = "",
                        Text = "选择一个站点"
                    });
                }
                else
                {
                    var dt = _dtCrawlSite.Tables[0];
                    foreach (DataRow dataRow in dt.Rows)
                    {
                        var val = dataRow["WebName"].ToString();
                        if (webname == val && sname.Items.FindByValue(val) == null)
                        {
                            sname.Items.Add(new ListItem
                            {
                                Value = dataRow["SectionName"].ToString(),
                                Text = dataRow["SectionName"].ToString()
                            });
                        }
                    }
                }
            }
        }

    }
}

