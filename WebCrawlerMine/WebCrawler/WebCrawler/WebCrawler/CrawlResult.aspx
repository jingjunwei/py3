﻿<%@ Page Language="C#" ValidateRequest="false" AutoEventWireup="true" CodeBehind="CrawlResult.aspx.cs" Inherits="WebCrawler.CrawlResult" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>CrawlResult</title>
    <link href="/Content/bootstrap.min.css" rel="stylesheet" />
    <link href="/Content/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="/Content/themes/base/jquery-ui.min.css" rel="stylesheet" />
    <link href="/Content/ui.jqgrid.css" rel="stylesheet" />
</head>
<body>
    <div class="form-group col-md-9">
        <div class="col-md-8">
            <button class="btn green " onclick="search_user()">
                检索
            </button>
            <br/>
            <br/>
            <input type="checkbox" id="autoreflash" />勾选每300s自动刷新
        </div>
    </div>
    <form runat="server">
        <div class="form-group col-md-12">
            <asp:Button ID="BTNCount" name="BTNCount" runat="server" Text="全量更新" OnClick="BTNCount_Click" />
            <asp:Button ID="BTNDeltaImp" name="BTNCount" runat="server" Text="增量更新" OnClick="BTNDeltaImp_Click" />
            <asp:Button ID="BTNQc" name="BTNCount" runat="server" Text="数据库去重" OnClick="BTNQc_Click" />
        </div>
        <div id="search">
            <div class="row">
                <div class="form-group col-md-4">
                    <div class="search-label col-md-4">站点名称</div>
                    <div class="col-md-8">
                        <asp:DropDownList ID="wname" class="form-control" AutoPostBack="True" runat="server" name="wname" placeholder="站点名称" OnSelectedIndexChanged="wname_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <div class="search-label col-md-4">模块名称</div>
                    <div class="col-md-8">
                        <asp:DropDownList ID="sname" class="form-control" runat="server" name="sname" placeholder="模块名称"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <div class="search-label col-md-4">资源类型</div>
                    <div class="col-md-8">
                        <asp:DropDownList ID="type" class="form-control" runat="server" name="type" placeholder="资源类型"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <div class="search-label col-md-4">用户站点</div>
                    <div class="col-md-8">
                        <asp:DropDownList ID="sUid" class="form-control" runat="server" name="sUid" placeholder="用户站点"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <div class="search-label col-md-4">模糊检索</div>
                    <div class="col-md-8">
                        <input type="text" class="form-control " name="key" placeholder="模糊检索">
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <div class="search-label col-md-4">是否清洗</div>
                    <div class="col-md-8">
                        <asp:DropDownList ID="IsCleaned" class="form-control" runat="server" name="IsCleaned" >
                            <asp:ListItem Value="">选择一个</asp:ListItem>
                            <asp:ListItem Value="1">已清洗</asp:ListItem>
                            <asp:ListItem Value="0">未清洗</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-scrollable col-md-12">
                    <table id="grid"></table>
                    <div id="pager"></div>
                </div>
            </div>
        </div>
        <div class="row" id="editform">
            <asp:TextBox ID="Id" class="form-control hidden " placeholder="" name="Id" runat="server"></asp:TextBox>
            <asp:TextBox ID="Uid" class="form-control hidden " placeholder="" name="Uid" runat="server"></asp:TextBox>
            <div class="col-md-4">
                <div class="search-label col-md-4">站点名称</div>
                <div class="col-md-8">
                    <asp:TextBox ID="WebName" class="form-control " disabled="disabled" placeholder="站点名称" name="WebName" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-4">
                <div class="search-label col-md-4">模块名称</div>
                <div class="col-md-8">
                    <asp:TextBox ID="SectionName" class="form-control " disabled="disabled" placeholder="模块名称" name="SectionName" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-4">
                <div class="search-label col-md-4">链接</div>
                <div class="col-md-8">
                    <asp:TextBox ID="Url" class="form-control " disabled="disabled" placeholder="链接" name="Url" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-4">
                <div class="search-label col-md-4">发布时间</div>
                <div class="col-md-8">
                    <asp:TextBox ID="CreateTime" class="form-control " disabled="disabled" placeholder="发布时间" name="CreateTime" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-8">
                <div class="search-label col-md-2">封面地址</div>
                <div class="col-md-10">
                    <asp:TextBox ID="Picture" class="form-control " placeholder="封面地址" name="Picture" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-8">
                <div class="search-label col-md-2">标题</div>
                <div class="col-md-10">
                    <asp:TextBox ID="Title" class="form-control " placeholder="标题" name="Title" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-4">
                <div class="search-label col-md-4">来源</div>
                <div class="col-md-8">
                    <asp:TextBox ID="Source" class="form-control " placeholder="来源" name="Source" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-4">
                <div class="search-label col-md-4">关键字</div>
                <div class="col-md-8">
                    <asp:TextBox ID="Keyword" class="form-control " placeholder="关键字" name="Keyword" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-4">
                <div class="search-label col-md-4">作者</div>
                <div class="col-md-8">
                    <asp:TextBox ID="Author" class="form-control " placeholder="作者" name="Author" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-12">
                <div class="search-label col-md-1">内容</div>
                <div class="col-md-11">
                    <asp:TextBox ID="Content" TextMode="MultiLine" class="form-control " Style="height: 400px;" placeholder="内容" name="Content" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-12">
                <div class="search-label col-md-2">&nbsp;</div>
                <div class="col-md-10">
                    <asp:Button ID="BTNNew" class="btn" runat="server" Text="新建" OnClick="BTNNew_Click" />
                    <asp:Button ID="BTNEdit" class="btn" runat="server" Text="修改" OnClientClick="kindeditersync()" OnClick="BTNEdit_Click" />
                    <asp:Button ID="BTNDel" class="btn" runat="server" Text="删除" OnClick="BTNDel_Click" />
                </div>
            </div>
        </div>
    </form>
    <script src="/Scripts/jquery-1.12.4.min.js"></script>
    <script src="/Scripts/common.js"></script>
    <script src="/Scripts/bootstrap.min.js"></script>
    <script src="/Scripts/jquery-ui-1.12.0.min.js"></script>
    <script src="/Scripts/jqGrid/jquery.jqGrid.min.js"></script>
    <script src="/Scripts/jqGrid/grid.locale-cn.js"></script>
    <script src="/KindEditor/kindeditor.js"></script>
    <script src="/KindEditor/lang/zh_CN.js"></script>
    <script>
        KindEditor.ready(function (K) {
            window.editorContent = K.create('#Content');
        });

        function kindeditersync() {
            editorContent.sync();
        }

        // 数据查询
        jQuery("#grid").jqGrid({
            url: '/Service.ashx?m=GetNewsPager',
            mtype: "GET",
            datatype: "json",
            height: 'auto',
            autowidth: true, //自动宽
            colNames: ['Id', '任务', '网站名', '模块名', '资源类型', '标题', '关键字', '链接', '发布时间', '封面', '来源', '作者', '采集时间', '是否清洗'],
            colModel: [
                { name: 'Id', index: 'Id', width: 55 },
                { name: 'TaskId', index: 'TaskId', align: "center", width: 90 },
                { name: 'WebName', index: 'WebName', align: "center", width: 100 },
                { name: 'SectionName', index: 'SectionName', width: 80, align: "center" },
                { name: 'Type', index: 'Type', width: 80, align: "center" },
                { name: 'Title', index: 'Title', width: 80, align: "center" },
                { name: 'Keyword', index: 'Keyword', width: 80, align: "center" },
                { name: 'Url', index: 'Url', width: 80, align: "center" },
                { name: 'CreateTime', index: 'CreateTime', width: 80, align: "center" },
                { name: 'Picture', index: 'Picture', width: 80, align: "center" },
                { name: 'Source', index: 'Source', width: 80, align: "center" },
                { name: 'Author', index: 'Author', width: 80, align: "center" },
                { name: 'CollectTime', index: 'CollectTime', width: 80, align: "center" },
                {
                    name: 'IsCleaned', index: 'IsCleaned', width: 80, align: "center", formatter: function (val) {
                        if (val == 1) {
                            return "已清洗";
                        } else if (val == 2) {
                            return "已删除";
                        } else {
                            return "未清洗";
                        }
                    }
                }
            ],
            rowNum: 30,
            rowList: [10, 20, 30, 100],
            pager: '#pager',
            sortname: 'CreateTime',
            viewrecords: true,
            rownumbers: true, //添加左侧行号
            sortorder: "desc",
            caption: "可用站点",
            jsonReader: { repeatitems: false },
            ondblClickRow: function (rowid) {
                var row = $("#grid").jqGrid("getRowData", rowid);
                $.get("/Service.ashx?m=GetNewsPager&id=" + row["Id"], function (result) {
                    $("#editform .form-control").bindData(result.rows[0]);
                    editorContent.html(result.rows[0]["Content"]);
                });
            }
        });

        //查询
        function search_user() {
            var queryParams = {};
            $($("#search .form-group  .form-control").serializeArray()).each(function (i, obj) {
                queryParams[obj.name] = obj.value;
            });
            $("#grid").jqGrid("setGridParam", { postData: queryParams }).trigger("reloadGrid");
        }

        $(function () {
            $("#editform .form-control").clearData();
            if (window.location.href.indexOf("autoreflash") > 0) {
                $("#autoreflash").attr("checked", !$("#autoreflash").attr("checked"));
            } else {
                $("#autoreflash").attr("checked", !!$("#autoreflash").attr("checked"));
            }
            $("#autoreflash").click(function () {
                $(this).attr("checked", !$("#autoreflash").attr("checked"));
            });
            function myrefresh() {
                if ($("#autoreflash").attr("checked"))
                    if (window.location.href.indexOf("autoreflash") > 0) {
                        $("#BTNCount").click();
                    } else {
                        window.location.href = window.location.href + "?autoreflash";
                    }
                else {
                    window.location.href = window.location.href.substr(0, window.location.href.indexOf("?"));
                }
            }
            setTimeout(myrefresh, 300 * 1000); //指定1秒刷新一次 
        });
    </script>
</body>
</html>

