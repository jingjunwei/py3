﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CrawlRules.aspx.cs" Inherits="WebCrawler.CrawlRules" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>CrawlRules</title>
    <link href="/Content/bootstrap.min.css" rel="stylesheet" />
    <link href="/Content/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="/Content/themes/base/jquery-ui.min.css" rel="stylesheet" />
    <link href="/Content/ui.jqgrid.css" rel="stylesheet" />
</head>
<body>
    <div id="search">
        <div class="row">
            <div class="form-group col-md-4">
                <div class="search-label col-md-4">规则名称</div>
                <div class="col-md-8">
                    <input type="text" class="form-control " name="sname" placeholder="规则名称">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="search-label col-md-2">&nbsp;</div>
                <div class="col-md-9">
                    <button class="btn green" onclick="search_user()">
                        <i class="fa fa-search"></i>
                        检索
                    </button>
                </div>
            </div>
        </div>
    </div>
    <form runat="server">
        <div class="row">
            <div class="col-md-12">
                <div class="table-scrollable col-md-6">
                    <table id="grid"></table>
                    <div id="pager"></div>
                    <div class="row" id="editform">
                        <asp:TextBox ID="Id" class="form-control hidden" placeholder="规则名称" name="Id" runat="server"></asp:TextBox>
                        <div class="form-group col-md-12">
                            <div class="search-label col-md-4">站点名称</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="RuleName" class="form-control " placeholder="规则名称" name="RuleName" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-12 hidden">
                            <div class="search-label col-md-4">是否完成</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="IsFinished" class="form-control " placeholder="是否完成" name="IsFinished" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-12 hidden">
                            <div class="search-label col-md-4">提交方式</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="SubmitType" class="form-control " placeholder="提交方式" name="SubmitType" Text="GET" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="search-label col-md-4">列表链接</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="UrlXpath" class="form-control " placeholder="新闻列表其中一条链接" name="UrlXpath" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="search-label col-md-4">链接属性字段</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="UrlAttr" class="form-control " placeholder="链接属性字段（一般为href,自动填充）" name="UrlAttr" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="search-label col-md-4">标题</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="TitleXpath" class="form-control " placeholder="新闻标题" name="TitleXpath" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="search-label col-md-4">关键字</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="KeywordXpath" class="form-control " placeholder="关键字" name="KeywordXpath" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="search-label col-md-4">关键字分割符</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="KeywordSpliter" class="form-control " placeholder="关键字分割符" name="KeywordSpliter" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="search-label col-md-4">内容</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="ContentXpath" class="form-control " placeholder="选择正文所在的外层标签（可能需要手动修改）" name="ContentXpath" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="search-label col-md-4">发布时间</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="CreateTimeXpath" class="form-control " placeholder="发布时间（可能需要手动修改）" name="CreateTimeXpath" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="search-label col-md-4">时间格式</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="CreateTimeReg" class="form-control " placeholder="yyyy-MM-dd/yyyy年MM月dd" name="CreateTimeReg" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="search-label col-md-4">图片/封面</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="PictureXpath" class="form-control " placeholder="一般是正文加//img" name="PictureXpath" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="search-label col-md-4">图片链接属性</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="PictureAttr" class="form-control " placeholder="图片链接属性（一般为src,自动填充）" name="PictureAttr" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="search-label col-md-4">来源</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="SourceXpath" class="form-control " placeholder="来源（可能需要手动修改）" name="SourceXpath" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="search-label col-md-4">来源规则(请将来源全称用%s替换)</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="SourceReg" class="form-control " placeholder="来源:%s(%s即来源的名称)" name="SourceReg" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="search-label col-md-4">作者</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="AuthorXpath" class="form-control " placeholder="作者（可能需要手动修改）" name="AuthorXpath" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="search-label col-md-4">作者规则(请将作者全称用%s替换)</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="AuthorReg" class="form-control " placeholder="作者:%s(%s即作者的名称)" name="AuthorReg" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="search-label col-md-4">规则版本</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="Version" class="form-control " placeholder="规则版本（自定义数字，新建时为0）" name="Version" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <asp:Button ID="BTNNew" class="btn col-md-12" runat="server" OnClientClick="addnew()" Text="确认新建规则" OnClick="BTNNew_Click" />
                    <asp:Button ID="BTNEdit" class="btn col-md-12" runat="server" Text="修改" OnClick="BTNEdit_Click" />
                    <asp:Button ID="BTNStart" class="btn col-md-12" runat="server" Text="启用" OnClick="BTNStart_Click" />
                    <asp:Button ID="BTNDel" class="btn col-md-12" runat="server" Text="禁用" OnClick="BTNDel_Click" />
                    <asp:Button ID="BTNDelete" class="btn col-md-12" runat="server" Text="彻底删除" OnClick="BTNDelete_Click" />
                    <input id="addtask" type="button" class="btn col-md-12" value="新建任务" />
                </div>
                <div class="col-md-6" style="background-color: #ccc">
                    <asp:Label ID="Label1" runat="server" Text="地址"></asp:Label>
                    <asp:TextBox ID="TBUrl" runat="server" Width="480"></asp:TextBox><asp:DropDownList ID="DDLCode" runat="server">
                        <asp:ListItem>UTF-8</asp:ListItem>
                        <asp:ListItem>GB2312</asp:ListItem>
                        <asp:ListItem>GBK</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Button ID="BTNGo" runat="server" Text="查看" OnClick="BTNGo_Click" /><asp:CheckBox ID="CBKeepDesc" Text="保留注释" runat="server" /><asp:CheckBox ID="CBKeepScript" Text="保留脚本" runat="server" />
                    <div id="xpathCrawlContent" style="height: 1000px; overflow-y: scroll;">
                        <asp:Literal ID="LTContent" Text="" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script src="/Scripts/jquery-1.12.4.min.js"></script>
    <script src="/Scripts/common.js"></script>
    <script src="/Scripts/bootstrap.min.js"></script>
    <script src="/Scripts/jquery-ui-1.12.0.min.js"></script>
    <script src="/Scripts/jqGrid/jquery.jqGrid.min.js"></script>
    <script src="/Scripts/jqGrid/grid.locale-cn.js"></script>
    <script src="/Scripts/jQueryGetXpath.js"></script>
    <script>
        var row = {};
        var getAbsoluteUrl = (function () {
            return function (hostName, url) {
                return hostName + url;
            };
        })();
        function getQueryString(name) {
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) { return unescape(r[2]); } return null;
        }
        function getHost(url) {
            var host = "null";
            if (!url)
                url = window.location.href;
            var regex = /(.*\:\/\/[^\/]*).*/;
            var match = url.match(regex);
            if (typeof match != "undefined"
                            && null != match)
                host = match[1];
            return host;
        }
        function canonical_uri(src) {
            var img = $("<img />").attr("src", src);
            return $(img).attr("src");
        }
        var hostName = "";
        var action = "new";
        if (getQueryString("crawlurl")) {
            action = "new";
            hostName = getHost(getQueryString("crawlurl"));
        }
        var xpathToSel = "";
        // 数据查询
        jQuery("#grid").jqGrid({
            url: '/Service.ashx?m=GetCrawlRulesPager',
            mtype: "GET",
            datatype: "json",
            height: 'auto',
            autowidth: true, //自动宽
            colNames: ['Id', '规则名称', '规则状态', '提交方式', '链接选择器', '链接属性字段', '标题样式选择器', '关键字选择器', '关键字分割符', '内容选择器', '发布时间选择器', '时间格式', '图片选择器', '图片链接属性', '来源选择器', '来源规则', '作者选择器', '作者规则', '规则版本', '创建时间', '启用禁用'],
            colModel: [
                { name: 'Id', index: 'Id', width: 55 },
                { name: 'RuleName', index: 'RuleName', align: "center" },
                {
                    name: 'IsFinished', index: 'IsFinished', align: "center",
                    formatter: function (cellvalue, options, rowObject) {
                        if (cellvalue == 1) {
                            return "可用";
                        }
                        else if (cellvalue == 2) {
                            return "异常";
                        }
                        return "未使用";
                    }
                },
                { name: 'SubmitType', index: 'SubmitType', align: "center", hidden: true },
                { name: 'UrlXpath', index: 'UrlXpath', align: "center", hidden: true },
                { name: 'UrlAttr', index: 'UrlAttr', align: "center", hidden: true },
                { name: 'TitleXpath', index: 'TitleXpath', align: "center", hidden: true },
                { name: 'KeywordXpath', index: 'KeywordXpath', align: "center", hidden: true },
                { name: 'KeywordSpliter', index: 'KeywordSpliter', align: "center", hidden: true },
                { name: 'ContentXpath', index: 'ContentXpath', align: "center", hidden: true },
                { name: 'CreateTimeXpath', index: 'CreateTimeXpath', align: "center", hidden: true },
                { name: 'CreateTimeReg', index: 'CreateTimeReg', align: "center", hidden: true },
                { name: 'PictureXpath', index: 'PictureXpath', align: "center", hidden: true },
                { name: 'PictureAttr', index: 'PictureAttr', align: "center", hidden: true },
                { name: 'SourceXpath', index: 'SourceXpath', align: "center", hidden: true },
                { name: 'SourceReg', index: 'SourceReg', align: "center", hidden: true },
                { name: 'AuthorXpath', index: 'AuthorXpath', align: "center", hidden: true },
                { name: 'AuthorReg', index: 'AuthorReg', align: "center", hidden: true },
                { name: 'Version', index: 'Version', align: "center", hidden: true },
                { name: 'CreateTime', index: 'CreateTime', align: "center", hidden: true },
                {
                    name: 'DeleteFlag', index: 'DeleteFlag', align: "center",
                    formatter: function (cellvalue, options, rowObject) {
                        if (cellvalue) {
                            return "禁用";
                        }
                        return "启用";
                    }
                }
            ],
            rowNum: 30,
            rowList: [10, 20, 30, 100],
            pager: '#pager',
            sortname: 'Id',
            viewrecords: true,
            rownumbers: true, //添加左侧行号
            sortorder: "desc",
            caption: "可用站点",
            jsonReader: { repeatitems: false },
            onSelectRow: function (rowid) {
                editrule(rowid);
            }
        });

        //查询
        function search_user() {
            var queryParams = {};
            $($("#search .form-group .form-control").serializeArray()).each(function (i, obj) {
                queryParams[obj.name] = obj.value;
            });
            $("#grid").jqGrid("setGridParam", { postData: queryParams }).trigger("reloadGrid");
        }

        function addnew() {
            if (action === "edit") {
                if (window.confirm("正在编辑规则，确认拷贝并新增规则吗？")) {
                    return true;
                }
                else
                    return false;
            } else {
                return true;
            }
        }

        function editrule(rowid) {
            row = $("#grid").jqGrid("getRowData", rowid);
            if (action === "new") {
                if (window.confirm("正在新建规则，是否切换至编辑？")) {
                    $("#editform .form-control").bindData(row);
                    action = "edit";
                }
            } else {
                $("#editform .form-control").bindData(row);
                action = "edit";
            }
        }

        function setXpath(selector, selfIdMode) {
            var path = $(selector).jQueryGetXpath("getXpath", {
                keepTbodys: true,
                mode: "idclass"
            });
            var headings = document.evaluate(path, document, null, XPathResult.ANY_TYPE, null);
            var thisHeading = headings.iterateNext();
            var xpathText = "";
            while (thisHeading) {
                xpathText += thisHeading.textContent + "\n";
                thisHeading = headings.iterateNext();
            }
            var jQueryText = $(selector).text();
            if (xpathText.trim() === jQueryText.trim()) {
                alert("校验通过：\n" + xpathText.trim());
            }
            else {
                alert("校验未通过：\n xpath:\n" + xpathText + "\n" + "jQuery:\n" + jQueryText);
            }

            path = path.replace(/\/tbody[0-9\[\]^/]*\//g, "//").replace('//div[@id="xpathCrawlContent"]', '//body');
            $("#" + xpathToSel).val(path);
        }

        $(function () {
            $("#editform input").click(function () {
                if ($(this).attr("name")) {
                    xpathToSel = $(this).attr("name");
                }
            });
            $("#xpathCrawlContent input").val("");

            $('#xpathCrawlContent *').click(function (event) {
                switch (xpathToSel) {
                    case "UrlXpath": // 链接
                        setXpath(this, false);
                        event.stopPropagation();
                        break;
                    case "TitleXpath":
                        setXpath(this);
                        event.stopPropagation();
                        break;
                    case "KeywordXpath":
                        setXpath(this);
                        $("#KeywordXpath").val($("#KeywordXpath").val() + "/text()");
                        $("#KeywordSpliter").val($(this).text().trim());
                        event.stopPropagation();
                        break;
                    case "ContentXpath":
                        setXpath(this);
                        $("#PictureXpath").val($("#ContentXpath").val() + "//img");
                        $("#PictureAttr").val("src");
                        event.stopPropagation();
                        break;
                    case "CreateTimeXpath":
                        setXpath(this);
                        $("#CreateTimeXpath").val($("#CreateTimeXpath").val() + "/text()");
                        $("#CreateTimeReg").val($(this).text().trim());
                        event.stopPropagation();
                        break;
                    case "PictureXpath":
                        if ($(this)[0].tagName === "IMG" && ($(this).attr("src") || $(this).attr("data-original"))) {
                            if ($(this).attr("src"))
                                $("#PictureAttr").val("src");
                            else if ($(this).attr("data-original"))
                                $("#PictureAttr").val("data-original");
                        }
                        setXpath(this);
                        event.stopPropagation();
                        break;
                    case "SourceXpath":
                        $("#SourceReg").val($(this).text());
                        setXpath(this);
                        $("#SourceXpath").val($("#SourceXpath").val() + "/text()");
                        event.stopPropagation();
                        break;
                    case "AuthorXpath":
                        $("#AuthorReg").val($(this).text());
                        setXpath(this);
                        $("#AuthorXpath").val($("#AuthorXpath").val() + "/text()");
                        event.stopPropagation();
                        break;
                    default:
                        event.stopPropagation();
                        break;
                }
                if ($(this)[0].tagName === "A" && $(this).attr("href")) {
                    var absurl = getAbsoluteUrl(hostName, $(this).attr("href"));
                    $("#TBUrl").val(absurl);
                    $("#UrlAttr").val("href");
                }
                return false;
            });
        });

        $("#addtask").click(function () {
            if (row["Id"]) {
                var url = "/CrawlTask.aspx?ruleid=" + row["Id"];
                if (getQueryString("siteid")) {
                    url += ("&siteid=" + getQueryString("siteid"));
                }
                if (getQueryString("idxurl")) {
                    url += ("&idxurl=" + encodeURIComponent(getQueryString("idxurl")));
                }
                location.href = url;
            }
            else {
                alert("请选择规则。");
            }
        });
    </script>
</body>
</html>

