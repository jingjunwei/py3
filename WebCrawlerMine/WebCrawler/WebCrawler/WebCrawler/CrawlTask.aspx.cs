﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using Model;
using Newtonsoft.Json;
using Utility;

namespace WebCrawler
{
    public partial class CrawlTask : Page
    {
        private readonly string[] _adminName = ConfigurationManager.AppSettings["Admin"].Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
        private ModelSiteTokens _user;
        public string CrawlSites;
        private DataSet _CrawlSites;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!RightCheck.CookieLoginCheck(Request, out _user))
            {
                Response.Redirect("/Login.aspx");
            }
            int rcount;
            int pcount;
            var wscondition = "1=1";
            var condition = "1=1";
            if (!((IList) _adminName).Contains(_user.SiteName))
            {
                condition += " AND [Id] = " + _user.Id;
                wscondition += " AND [Uid] = " + _user.Id;
            }

            _CrawlSites = BllBaseService.GetPager(1, int.MaxValue, out rcount, out pcount, "CrawlSite", wscondition);
            if (_CrawlSites.Tables.Count > 0)
            {
                var dt = _CrawlSites.Tables[0];
                CrawlSites = JsonConvert.SerializeObject(dt);
            }
            if (!IsPostBack)
            {
                if (((IList) _adminName).Contains(_user.SiteName))
                {
                    Uid.Items.Add(new ListItem
                    {
                        Value = "",
                        Text = "选择一个"
                    });
                    sUid.Items.Add(new ListItem
                    {
                        Value = "",
                        Text = "选择一个"
                    });
                }
                RuleId.Items.Add(new ListItem
                {
                    Value = "",
                    Text = "选择一个"
                });
                SiteSectionId.Items.Add(new ListItem
                {
                    Value = "",
                    Text = "选择一个"
                });
                rid.Items.Add(new ListItem
                {
                    Value = "",
                    Text = "选择一个"
                });
                wsid.Items.Add(new ListItem
                {
                    Value = "",
                    Text = "选择一个"
                });
                var ds = BllBaseService.GetPager(1, int.MaxValue, out rcount, out pcount, "SiteTokens", condition);
                if (ds.Tables.Count > 0)
                {
                    var dt = ds.Tables[0];

                    foreach (DataRow dataRow in dt.Rows)
                    {
                        sUid.Items.Add(new ListItem
                        {
                            Value = dataRow["Id"].ToString(),
                            Text = dataRow["Id"] + ":" + dataRow["SiteName"]
                        });
                        Uid.Items.Add(new ListItem
                        {
                            Value = dataRow["Id"].ToString(),
                            Text = dataRow["Id"] + ":" + dataRow["SiteName"]
                        });
                    }
                }
                if (_CrawlSites.Tables.Count > 0)
                {
                    var dt = _CrawlSites.Tables[0];
                    foreach (DataRow dataRow in dt.Rows)
                    {
                        SiteSectionId.Items.Add(new ListItem
                        {
                            Value = dataRow["Id"].ToString(),
                            Text = dataRow["Id"] + ":" + dataRow["WebName"] + ":" + dataRow["SectionName"]
                        });
                        wsid.Items.Add(new ListItem
                        {
                            Value = dataRow["Id"].ToString(),
                            Text = dataRow["Id"] + ":" + dataRow["WebName"] + ":" + dataRow["SectionName"]
                        });
                    }
                }
                if (Request.QueryString["siteid"] != null)
                {
                    SiteSectionId.SelectedValue = Request.QueryString["siteid"];
                }
                ds = BllBaseService.GetPager(1, int.MaxValue, out rcount, out pcount, "CrawlRules");
                if (ds.Tables.Count > 0)
                {
                    var dt = ds.Tables[0];
                    foreach (DataRow dataRow in dt.Rows)
                    {
                        RuleId.Items.Add(new ListItem
                        {
                            Value = dataRow["Id"].ToString(),
                            Text = dataRow["Id"] + ":" + dataRow["RuleName"]
                        });
                        rid.Items.Add(new ListItem
                        {
                            Value = dataRow["Id"].ToString(),
                            Text = dataRow["Id"] + ":" + dataRow["RuleName"]
                        });
                    }
                }
                if (Request.QueryString["ruleid"] != null)
                {
                    RuleId.SelectedValue = Request.QueryString["ruleid"];
                }
                if (Request.QueryString["idxurl"] != null)
                {
                    var urlDecode = HttpContext.Current.Server.UrlDecode(Request.QueryString["idxurl"]);
                    if (urlDecode != null)
                    {
                        urlDecode = urlDecode.Replace("%", "%%");
                        urlDecode = urlDecode.Replace("=1", "=%d");
                        PageUrlWithParam.Text = urlDecode ;
                    }
                }

                IsStarted.Items.Add(new ListItem()
                {
                    Value = "0",
                    Text = "未启动"
                });
                IsStarted.Items.Add(new ListItem()
                {
                    Value = "1",
                    Text = "已启动"
                });

                UseProxy.Items.Add(new ListItem()
                {
                    Value = "0",
                    Text = "不使用代理"
                });
                UseProxy.Items.Add(new ListItem()
                {
                    Value = "1",
                    Text = "使用代理"
                });
            }
        }

        /// <summary>
        /// 增加记录
        /// </summary>
        /// <returns>int</returns> 
        private long Insert(ModelCrawlTask obj)
        {
            string errMsg;

            long newID = BllCrawlTask.InsertUnique(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                Response.Write("<script>alert('" + errMsg + "');</script>");
            }
            return newID;
        }

        /// <summary>
        /// 删除记录
        /// </summary>
        private void Delete(ModelCrawlTask obj)
        {
            string errMsg;
            BllCrawlTask.Delete(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                Response.Write("<script>alert('" + errMsg + "');</script>");
                return;
            }
        }

        /// <summary>
        /// 修改记录
        /// </summary>
        private void Update(ModelCrawlTask obj)
        {
            string errMsg;

            BllCrawlTask.UpdateUnique(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                Response.Write("<script>alert('" + errMsg + "');</script>");
                return;
            }
        }

        /// <summary>
        /// 查询分页记录
        /// </summary>
        /// <param name="startIndex">数据开始索引值</param> 
        /// <param name="pageSize">数据每页条数</param> 
        /// <returns>DataSet</returns> 
        protected DataSet SelectPaging(int startIndex, int pageSize)
        {
            string errMsg;
            var obj = new ModelCrawlTask
                {
                    StartIndex = startIndex,
                    PageSize = pageSize,
                    SPReturnType = AppEnum.SPReturnTypes.DataSet
                };
            DataSet ds = BllCrawlTask.SelectPaging(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                Response.Write("<script>alert('" + errMsg + "');</script>");
                return null;
            }
            return ds;
        }

        /// <summary>
        /// 查询分页记录
        /// </summary>
        /// <param name="startIndex">数据开始索引值</param> 
        /// <param name="pageSize">数据每页条数</param>
        /// <param name="rcount"></param>
        /// <param name="pcount"></param>
        /// <returns>DataSet</returns> 
        protected DataSet GetPager(int startIndex, int pageSize, out int rcount, out int pcount)
        {
            return BllCrawlTask.GetPager(startIndex, pageSize, out rcount, out pcount);
        }

        /// <summary>
        /// 查询全部记录
        /// </summary>
        /// <returns>DataSet</returns> 
        protected DataSet SelectAll()
        {
            string errMsg;
            var obj = new ModelCrawlTask
                {
                    SPReturnType = AppEnum.SPReturnTypes.DataSet
                };
            DataSet ds = BllCrawlTask.SelectAll(obj, out errMsg);
            if (CheckIsError(errMsg))
            {
                Response.Write("<script>alert('" + errMsg + "');</script>");
                return null;
            }
            return ds;
        }

        /// <summary>
        /// 判断是否出错
        /// </summary>
        /// <param name="errMsg">错误信息</param>
        private bool CheckIsError(string errMsg)
        {
            if (!string.IsNullOrEmpty(errMsg))
            {
                //Catch Error 
                return true;
            };
            return false;
        }

        protected void BTNNew_Click(object sender, EventArgs e)
        {
            var obj = new ModelCrawlTask
            {
                Id = 0,
                Uid = int.Parse(Uid.SelectedValue),
                RuleId = int.Parse(RuleId.Text),
                SiteSectionId = int.Parse(SiteSectionId.Text),
                PageUrlWithParam = PageUrlWithParam.Text,
                IsStarted = int.Parse(IsStarted.SelectedValue),
                UseProxy = int.Parse(UseProxy.SelectedValue),
                SPReturnType = AppEnum.SPReturnTypes.Object
            };
            Insert(obj);
        }

        protected void BTNEdit_Click(object sender, EventArgs e)
        {
            var obj = BllCrawlTask.GeById(Id.Text);
            if (obj == null) return;
            obj.Id = int.Parse(Id.Text);
            obj.Uid = int.Parse(Uid.SelectedValue);
            obj.RuleId = int.Parse(RuleId.Text);
            obj.SiteSectionId = int.Parse(SiteSectionId.Text);
            obj.PageUrlWithParam = PageUrlWithParam.Text;
            obj.IsStarted = int.Parse(IsStarted.SelectedValue);
            obj.UseProxy = int.Parse(UseProxy.SelectedValue);
            obj.SPReturnType = AppEnum.SPReturnTypes.Null;
            Update(obj);
        }

        protected void BTNDel_Click(object sender, EventArgs e)
        {
            var ob = BllCrawlTask.GeById(Id.Text);
            if (ob == null) return;
            ob.SPAction = "Update";
            ob.IsStarted = 0;
            Update(ob);
        }
        protected void BTNDelete_Click(object sender, EventArgs e)
        {
            var obj = BllCrawlTask.GeById(Id.Text);
            Delete(obj);
        }
        protected void BTNStart_Click(object sender, EventArgs e)
        {
            var ob = BllCrawlTask.GeById(Id.Text);
            if (ob == null) return;
            ob.SPAction = "Update";
            ob.IsStarted = 1;
            Update(ob);
        }

        protected void BTNTest_Click(object sender, EventArgs e)
        {
            if (Id.Text != "")
                RunTest();
        }
        protected void BTNDelLog_Click(object sender, EventArgs e)
        {
            DelLog();
        }

        protected void BTNStopAll_Click(object sender, EventArgs e)
        {
            var where = " 1=1 ";
            if (!((IList) _adminName).Contains(_user.SiteName))
            {
                where += " AND [Uid]=" + _user.Id;
            }
            var sql = @"UPDATE CrawlTask SET IsStarted = 0 WHERE " + where;
            BllBaseService.ExecuteSql(sql);
        }

        protected void BTNStartAll_Click(object sender, EventArgs e)
        {
            var where = " 1=1 ";
            if (!((IList) _adminName).Contains(_user.SiteName))
            {
                where += " AND [Uid]=" + _user.Id;
            }
            var sql = @"UPDATE CrawlTask SET IsStarted = 1 WHERE " + where;
            BllBaseService.ExecuteSql(sql);
        }

        private void DelLog()
        {
            var filepath = ConfigurationManager.AppSettings["LogPath"] + "\\crawl_test.log";
            try
            {
                if (File.Exists(filepath))
                {
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(filepath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read);
                        fs.SetLength(0);
                    }
                    catch (Exception ex)
                    {
                        TBLog.Text = "清空日志文件失败：" + ex.Message;
                    }
                    finally
                    {
                        if (fs != null) fs.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                TBLog.Text = "查看日志文件失败：" + ex.Message + "，请稍后再试！";
            }
            TBLog.Text = "";
        }

        /// <summary>
        /// 执行LDA模型
        /// </summary>
        private void RunTest()
        {
            string cmdstr = ConfigurationManager.AppSettings["TestTaskCmd"];
            var pro = new System.Diagnostics.ProcessStartInfo("cmd.exe");
            pro.UseShellExecute = false;
            pro.RedirectStandardOutput = true;
            pro.RedirectStandardError = true;
            pro.Arguments = "/C " + cmdstr + " " + Id.Text;
            pro.WorkingDirectory = ConfigurationManager.AppSettings["CmdPath"];
            var proc = System.Diagnostics.Process.Start(pro);
            proc.WaitForExit();
            //System.IO.StreamReader sOut = proc.StandardOutput;
            //string results = sOut.ReadToEnd().Trim();
            //sOut.Close();
            proc.Close();
            //string fmtStdOut = "<font face=courier size=0>{0}</font>";
            //Response.Write(String.Format(fmtStdOut, results.Replace(System.Environment.NewLine, "<br>")));
            //Response.Write("<script>alert('修复成功!'); </script>");

            var filepath = ConfigurationManager.AppSettings["LogPath"] + "\\crawl_test.log";
            try
            {
                if (File.Exists(filepath))
                {
                    TBLog.Text = File.ReadAllText(filepath);
                }
            }
            catch (Exception ex)
            {
                TBLog.Text = "查看日志文件失败：" + ex.Message + "，请稍后再试！";
            }
        }
    }
}

