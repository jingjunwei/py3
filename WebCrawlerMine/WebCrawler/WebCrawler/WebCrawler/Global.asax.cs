﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Model;
using SolrNet;

namespace WebCrawler
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            Startup.Init<ModelCrawlResult>(ConfigurationManager.AppSettings["SolrCoreCrawl"]);
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            //Startup.Init<ModelCrawlResult>(ConfigurationManager.AppSettings["SolrCoreCrawl"]);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}