﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CrawlSite.aspx.cs" Inherits="WebCrawler.CrawlSite" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>CrawlSite</title>
    <link href="/Content/bootstrap.min.css" rel="stylesheet" />
    <link href="/Content/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="/Content/themes/base/jquery-ui.min.css" rel="stylesheet" />
    <link href="/Content/ui.jqgrid.css" rel="stylesheet" />
</head>
<body>
    <div class="row">
        <div class="col-md-4">
            <div class="col-md-9">
                <button class="btn green" onclick="search_user()">
                    <i class="fa fa-search"></i>
                    检索
                </button>
            </div>
        </div>
    </div>
    <form runat="server">
        <div id="search">
            <div class="row">
                <div class="form-group col-md-4">
                    <div class="search-label col-md-4">采集站点</div>
                    <div class="col-md-8">
                        <asp:DropDownList ID="wname" class="form-control" runat="server" name="wname" AutoPostBack="True" placeholder="采集站点" OnSelectedIndexChanged="wname_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group col-md-4 hidden">
                    <div class="search-label col-md-4">站点链接</div>
                    <div class="col-md-8">
                        <input type="text" class="form-control " name="link" placeholder="站点链接">
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <div class="search-label col-md-4">模块名称</div>
                    <div class="col-md-8">
                        <asp:DropDownList ID="sname" class="form-control" runat="server" name="sname" placeholder="模块名称"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <div class="search-label col-md-4">用户</div>
                    <div class="col-md-8">
                        <asp:DropDownList ID="sUid" class="form-control" runat="server" name="sUid" placeholder="用户"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <div class="search-label col-md-4">资源类型</div>
                    <div class="col-md-8">
                        <asp:DropDownList ID="sType" class="form-control" runat="server" name="sType" placeholder="资源类型"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <div class="search-label col-md-4">申请站点</div>
                    <div class="col-md-8">
                        <input type="text" class="form-control " name="sfrom" placeholder="申请站点">
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <div class="search-label col-md-4">等待确认</div>
                    <div class="col-md-8">
                        <select class="form-control " name="delflag">
                            <option value="">选择一个</option>
                            <option value="1">等待处理</option>
                            <option value="0">已确认并处理</option>
                        </select>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-scrollable col-md-12">
                    <table id="grid"></table>
                    <div id="pager"></div>
                </div>
            </div>
        </div>
        <div class="row" id="editform">
            <%--<input type="hidden" name="Id">--%>
            <asp:TextBox ID="Id" class="form-control hidden " placeholder="站点名称" name="Id" runat="server"></asp:TextBox>
            <div class="form-group col-md-4">
                <div class="search-label col-md-4">站点名称</div>
                <div class="col-md-8">
                    <asp:TextBox ID="WebName" class="form-control " placeholder="站点名称" name="WebName" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="search-label col-md-4">资源类型</div>
                <div class="col-md-8">
                    <asp:TextBox ID="Type" class="form-control " placeholder="资源类型" name="Type" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="search-label col-md-4">用户站点</div>
                <div class="col-md-8">
                    <asp:DropDownList ID="Uid" class="form-control" runat="server" name="Uid" placeholder="用户站点"></asp:DropDownList>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="search-label col-md-4">站点链接</div>
                <div class="col-md-8">
                    <asp:TextBox ID="WebLink" class="form-control " placeholder="站点链接" name="WebLink" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="search-label col-md-4">模块名称</div>
                <div class="col-md-8">
                    <asp:TextBox ID="SectionName" class="form-control " placeholder="模块名称" name="SectionName" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="search-label col-md-4">模块第一页</div>
                <div class="col-md-8">
                    <asp:TextBox ID="SectionIndexUrl" class="form-control " placeholder="模块第一页" name="SectionIndexUrl" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="search-label col-md-4">首页一致</div>
                <div class="col-md-8">
                    <%--<asp:TextBox ID="DeleteFlag" class="form-control " placeholder="等待1/已处理0" name="DeleteFlag" runat="server"></asp:TextBox>--%>
                    <asp:DropDownList ID="IndexDiff" class="form-control" runat="server" name="IndexDiff" placeholder="是否启用登录">
                        <asp:ListItem Value="0">相同</asp:ListItem>
                        <asp:ListItem Value="1">不同</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="search-label col-md-4">起始值</div>
                <div class="col-md-8">
                    <asp:TextBox ID="Start" class="form-control " placeholder="起始值" name="Start" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="search-label col-md-4">步进</div>
                <div class="col-md-8">
                    <asp:TextBox ID="Step" class="form-control " placeholder="步进" name="Step" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="search-label col-md-4">申请站点</div>
                <div class="col-md-8">
                    <asp:TextBox ID="SiteFrom" class="form-control " placeholder="申请站点" name="SiteFrom" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="search-label col-md-4">是否处理</div>
                <div class="col-md-8">
                    <%--<asp:TextBox ID="DeleteFlag" class="form-control " placeholder="等待1/已处理0" name="DeleteFlag" runat="server"></asp:TextBox>--%>
                    <asp:DropDownList ID="DeleteFlag" class="form-control" runat="server" name="DeleteFlag" placeholder="是否启用登录">
                        <asp:ListItem Value="0">已处理并确认</asp:ListItem>
                        <asp:ListItem Value="1">等待处理</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <asp:Button ID="BTNNew" class="btn" runat="server" Text="新建站源" OnClick="BTNNew_Click" />
        <asp:Button ID="BTNEdit" class="btn" runat="server" Text="修改" OnClick="BTNEdit_Click" />
        <asp:Button ID="BTNDone" class="btn" runat="server" Text="确认并处理" OnClick="BTNDone_Click" />
        <asp:Button ID="BTNDel" class="btn" runat="server" Text="置于等待处理" OnClick="BTNDel_Click" />
        <input id="addrule" type="button" class="btn" value="新建规则" />
        <input id="addtask" type="button" class="btn" value="新建任务" />
    </form>

    <script src="/Scripts/jquery-1.12.4.min.js"></script>
    <script src="/Scripts/common.js"></script>
    <script src="/Scripts/bootstrap.min.js"></script>
    <script src="/Scripts/jquery-ui-1.12.0.min.js"></script>
    <script src="/Scripts/jqGrid/jquery.jqGrid.min.js"></script>
    <script src="/Scripts/jqGrid/grid.locale-cn.js"></script>
    <script>
        var row = {};
        // 数据查询
        jQuery("#grid").jqGrid({
            url: '/Service.ashx?m=GetCrawlSitePager',
            mtype: "GET",
            datatype: "json",
            height: 'auto',
            autowidth: true, //自动宽
            colNames: ['Id', '用户站点', '资源类型', '站点名称', '站点链接', '模块名称', '首页不同', '模块首页地址', "起始值", "步进", '申请站点', '等待确认'],
            colModel: [
                { name: 'Id', index: 'Id', width: 55 },
                { name: 'Uid', index: 'Uid', align: "center", hidden: true },
                { name: 'Type', index: 'Type', align: "center" },
                { name: 'WebName', index: 'WebName', align: "center" },
                { name: 'WebLink', index: 'WebLink', align: "center", hidden: true },
                { name: 'SectionName', index: 'SectionName', align: "center" },
                {
                    name: 'IndexDiff', index: 'IndexDiff', align: "center", hidden: true,
                    formatter: function (cellvalue, options, rowObject) {
                        if (cellvalue) {
                            return "不同";
                        }
                        return "相同";
                    }
                },
                { name: 'SectionIndexUrl', index: 'SectionIndexUrl', align: "center" },
                { name: 'Start', index: 'Id', align: "center", hidden: true },
                { name: 'Step', index: 'Id', align: "center", hidden: true },
                { name: 'SiteFrom', index: 'SiteFrom', align: "center" },
                {
                    name: 'DeleteFlag', index: 'DeleteFlag', width: 80, align: "center",
                    formatter: function (cellvalue, options, rowObject) {
                        if (cellvalue) {
                            return "等待处理";
                        }
                        return "已确认并处理";
                    }
                }
            ],
            rowNum: 30,
            rowList: [10, 20, 30, 100],
            pager: '#pager',
            sortname: 'Id',
            viewrecords: true,
            rownumbers: true, //添加左侧行号
            sortorder: "desc",
            caption: "可用站点",
            jsonReader: { repeatitems: false },
            onSelectRow: function (rowid) {
                row = $("#grid").jqGrid("getRowData", rowid);
                $("#editform .form-control").bindData(row);
                $("#DeleteFlag").val(row["DeleteFlag"] === "等待处理" ? "1" : "0");
                $("#IndexDiff").val(row["IndexDiff"] === "相同" ? "0" : "1");
            }
        });

        // 查询
        function search_user() {
            var queryParams = {};
            $($("#search .form-group .form-control").serializeArray()).each(function (i, obj) {
                queryParams[obj.name] = obj.value;
            });
            $("#grid").jqGrid("setGridParam", { postData: queryParams }).trigger("reloadGrid");
        }

        $("#addrule").click(function () {
            if (row["SectionIndexUrl"])
                location.href = "/CrawlRules.aspx?crawlurl=" + encodeURIComponent(row["SectionIndexUrl"]) + "&name=" + encodeURIComponent(row["SiteFrom"] + ":" + row["WebName"] + row["SectionName"]) + "&siteid=" + row["Id"] + "&idxurl=" + encodeURIComponent(row["SectionIndexUrl"]);
            else
                alert("请选择站源。");
        });
        $("#addtask").click(function () {
            if (row["SectionIndexUrl"])
                location.href = "/CrawlTask.aspx?siteid=" + row["Id"] + "&idxurl=" + encodeURIComponent(row["SectionIndexUrl"]);
            else
                alert("请选择站源。");
        });
    </script>
</body>
</html>

