﻿USE WebRescourseForVipProject
GO 
SET ANSI_NULLS ON
GO 
SET QUOTED_IDENTIFIER ON
GO 

if exists (select * from dbo.sysobjects where id = object_id(N'[TableCrawlResult]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [TableCrawlResult] 

go 

CREATE PROCEDURE TableCrawlResult
        @Id int = 0, 
        @TaskId int = 0, 
        @WebName nvarchar(50) = null, 
        @SectionName nvarchar(50) = null, 
        @Title nvarchar(50) = null, 
        @Url nvarchar(200) = null, 
        @Content nvarchar(max) = null, 
        @CreateTime datetime = null, 
        @Picture nvarchar(200) = null, 
        @Source nvarchar(200) = null, 
        @Author nvarchar(200) = null, 
        @CollectTime datetime = null, 
        @IsCleaned int = null, 

        @StartIndex int = 0, 
        @PageSize int = 0, 
        @SPReturnType varchar(100) = null, 
        @SPAction varchar(100) = null 
AS
BEGIN
    SET NOCOUNT ON;
    IF @SPAction='Insert'
        BEGIN            
            INSERT INTO [CrawlResult]([TaskId],[WebName],[SectionName],[Title],[Url],[Content],[CreateTime],[Picture],[Source],[Author],[CollectTime],[IsCleaned]) 
            VALUES(@TaskId,@WebName,@SectionName,@Title,@Url,@Content,@CreateTime,@Picture,@Source,@Author,@CollectTime,@IsCleaned) 

            Declare @newID varchar(10) 
            Set @newID = SCOPE_IDENTITY() 
            If (@newID is NULL) 
                set @newID = ('-1') 
            select @newID as 'newID'
        END
    IF @SPAction='Delete'
        BEGIN            
            DELETE FROM [CrawlResult] WHERE [Id] = @Id
        END
    IF @SPAction='Update'
        BEGIN            
            UPDATE [CrawlResult] 
            SET [TaskId] = @TaskId,[WebName] = @WebName,[SectionName] = @SectionName,[Title] = @Title,[Url] = @Url,[Content] = @Content,[CreateTime] = @CreateTime,[Picture] = @Picture,[Source] = @Source,[Author] = @Author,[CollectTime] = @CollectTime,[IsCleaned] = @IsCleaned 
            WHERE [Id] = @Id
        END
    IF @SPAction='SelectPaging'
        BEGIN            
            SELECT [Id],[TaskId],[WebName],[SectionName],[Title],[Url],[Content],[CreateTime],[Picture],[Source],[Author],[CollectTime],[IsCleaned] 
            FROM [CrawlResult]
        END
    IF @SPAction='SelectAll'
        BEGIN            
            SELECT [Id],[TaskId],[WebName],[SectionName],[Title],[Url],[Content],[CreateTime],[Picture],[Source],[Author],[CollectTime],[IsCleaned] 
            FROM [CrawlResult]
        END
    IF @SPAction='Unique'
        BEGIN            
            Declare @isExist int 
            if @Id = 0 
                Set @isExist = (select count(*) from [CrawlResult] where [TaskId] = @TaskId and [WebName] = @WebName and [SectionName] = @SectionName and [Title] = @Title and [Url] = @Url and [Content] = @Content and [CreateTime] = @CreateTime and [Picture] = @Picture and [Source] = @Source and [Author] = @Author and [CollectTime] = @CollectTime and [IsCleaned] = @IsCleaned) 
            else 
                Set @isExist = (select count(*) from [CrawlResult] where [TaskId] = @TaskId and [WebName] = @WebName and [SectionName] = @SectionName and [Title] = @Title and [Url] = @Url and [Content] = @Content and [CreateTime] = @CreateTime and [Picture] = @Picture and [Source] = @Source and [Author] = @Author and [CollectTime] = @CollectTime and [IsCleaned] = @IsCleaned and [Id] != @Id) 
            if @isExist>0 
                raiserror('data existed already',16,1)
        END
END

GO

USE WebRescourseForVipProject
GO 
SET ANSI_NULLS ON
GO 
SET QUOTED_IDENTIFIER ON
GO 

if exists (select * from dbo.sysobjects where id = object_id(N'[TableCrawlRules]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [TableCrawlRules] 

go 

CREATE PROCEDURE TableCrawlRules
        @Id int = 0, 
        @RuleName nvarchar(50) = null, 
        @IsFinished int = null, 
        @SubmitType nvarchar(10) = null, 
        @TitleXpath nvarchar(200) = null, 
        @UrlXpath nvarchar(200) = null, 
        @UrlAttr nvarchar(200) = null, 
        @ContentXpath nvarchar(200) = null, 
        @CreateTimeXpath nvarchar(200) = null, 
        @CreateTimeReg nvarchar(200) = null, 
        @PictureXpath nvarchar(200) = null, 
        @PictureAttr nvarchar(200) = null, 
        @SourceXpath nvarchar(200) = null, 
        @SourceReg nvarchar(200) = null, 
        @AuthorXpath nvarchar(200) = null, 
        @AuthorReg nvarchar(200) = null, 
        @Version int = 1, 
        @CreateTime datetime = null, 
        @DeleteFlag int = 0, 

        @StartIndex int = 0, 
        @PageSize int = 0, 
        @SPReturnType varchar(100) = null, 
        @SPAction varchar(100) = null 
AS
BEGIN
    SET NOCOUNT ON;
    IF @SPAction='Insert'
        BEGIN            
            INSERT INTO [CrawlRules]([RuleName],[IsFinished],[SubmitType],[TitleXpath],[UrlXpath],[UrlAttr],[ContentXpath],[CreateTimeXpath],[CreateTimeReg],[PictureXpath],[PictureAttr],[SourceXpath],[SourceReg],[AuthorXpath],[AuthorReg],[Version],[CreateTime],[DeleteFlag]) 
            VALUES(@RuleName,@IsFinished,@SubmitType,@TitleXpath,@UrlXpath,@UrlAttr,@ContentXpath,@CreateTimeXpath,@CreateTimeReg,@PictureXpath,@PictureAttr,@SourceXpath,@SourceReg,@AuthorXpath,@AuthorReg,@Version,@CreateTime,@DeleteFlag) 

            Declare @newID varchar(10) 
            Set @newID = SCOPE_IDENTITY() 
            If (@newID is NULL) 
                set @newID = ('-1') 
            select @newID as 'newID'
        END
    IF @SPAction='Delete'
        BEGIN            
            DELETE FROM [CrawlRules] WHERE [Id] = @Id
        END
    IF @SPAction='Update'
        BEGIN            
            UPDATE [CrawlRules] 
            SET [RuleName] = @RuleName,[IsFinished] = @IsFinished,[SubmitType] = @SubmitType,[TitleXpath] = @TitleXpath,[UrlXpath] = @UrlXpath,[UrlAttr] = @UrlAttr,[ContentXpath] = @ContentXpath,[CreateTimeXpath] = @CreateTimeXpath,[CreateTimeReg] = @CreateTimeReg,[PictureXpath] = @PictureXpath,[PictureAttr] = @PictureAttr,[SourceXpath] = @SourceXpath,[SourceReg] = @SourceReg,[AuthorXpath] = @AuthorXpath,[AuthorReg] = @AuthorReg,[Version] = @Version,[CreateTime] = @CreateTime,[DeleteFlag] = @DeleteFlag 
            WHERE [Id] = @Id
        END
    IF @SPAction='SelectPaging'
        BEGIN            
            SELECT [Id],[RuleName],[IsFinished],[SubmitType],[TitleXpath],[UrlXpath],[UrlAttr],[ContentXpath],[CreateTimeXpath],[CreateTimeReg],[PictureXpath],[PictureAttr],[SourceXpath],[SourceReg],[AuthorXpath],[AuthorReg],[Version],[CreateTime],[DeleteFlag] 
            FROM [CrawlRules]
        END
    IF @SPAction='SelectAll'
        BEGIN            
            SELECT [Id],[RuleName],[IsFinished],[SubmitType],[TitleXpath],[UrlXpath],[UrlAttr],[ContentXpath],[CreateTimeXpath],[CreateTimeReg],[PictureXpath],[PictureAttr],[SourceXpath],[SourceReg],[AuthorXpath],[AuthorReg],[Version],[CreateTime],[DeleteFlag] 
            FROM [CrawlRules]
        END
    IF @SPAction='Unique'
        BEGIN            
            Declare @isExist int 
            if @Id = 0 
                Set @isExist = (select count(*) from [CrawlRules] where [RuleName] = @RuleName and [IsFinished] = @IsFinished and [SubmitType] = @SubmitType and [TitleXpath] = @TitleXpath and [UrlXpath] = @UrlXpath and [UrlAttr] = @UrlAttr and [ContentXpath] = @ContentXpath and [CreateTimeXpath] = @CreateTimeXpath and [CreateTimeReg] = @CreateTimeReg and [PictureXpath] = @PictureXpath and [PictureAttr] = @PictureAttr and [SourceXpath] = @SourceXpath and [SourceReg] = @SourceReg and [AuthorXpath] = @AuthorXpath and [AuthorReg] = @AuthorReg and [Version] = @Version and [CreateTime] = @CreateTime and [DeleteFlag] = @DeleteFlag) 
            else 
                Set @isExist = (select count(*) from [CrawlRules] where [RuleName] = @RuleName and [IsFinished] = @IsFinished and [SubmitType] = @SubmitType and [TitleXpath] = @TitleXpath and [UrlXpath] = @UrlXpath and [UrlAttr] = @UrlAttr and [ContentXpath] = @ContentXpath and [CreateTimeXpath] = @CreateTimeXpath and [CreateTimeReg] = @CreateTimeReg and [PictureXpath] = @PictureXpath and [PictureAttr] = @PictureAttr and [SourceXpath] = @SourceXpath and [SourceReg] = @SourceReg and [AuthorXpath] = @AuthorXpath and [AuthorReg] = @AuthorReg and [Version] = @Version and [CreateTime] = @CreateTime and [DeleteFlag] = @DeleteFlag and [Id] != @Id) 
            if @isExist>0 
                raiserror('data existed already',16,1)
        END
END

GO

USE WebRescourseForVipProject
GO 
SET ANSI_NULLS ON
GO 
SET QUOTED_IDENTIFIER ON
GO 

if exists (select * from dbo.sysobjects where id = object_id(N'[TableCrawlSite]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [TableCrawlSite] 

go 

CREATE PROCEDURE TableCrawlSite
        @Id int = 0, 
        @WebName nvarchar(50) = null, 
        @WebLink nvarchar(200) = null, 
        @SectionName nvarchar(50) = null, 
        @SectionIndexUrl nvarchar(200) = null, 
        @SiteFrom nvarchar(50) = null, 
        @DeleteFlag int = null, 

        @StartIndex int = 0, 
        @PageSize int = 0, 
        @SPReturnType varchar(100) = null, 
        @SPAction varchar(100) = null 
AS
BEGIN
    SET NOCOUNT ON;
    IF @SPAction='Insert'
        BEGIN            
            INSERT INTO [CrawlSite]([WebName],[WebLink],[SectionName],[SectionIndexUrl],[SiteFrom],[DeleteFlag]) 
            VALUES(@WebName,@WebLink,@SectionName,@SectionIndexUrl,@SiteFrom,@DeleteFlag) 

            Declare @newID varchar(10) 
            Set @newID = SCOPE_IDENTITY() 
            If (@newID is NULL) 
                set @newID = ('-1') 
            select @newID as 'newID'
        END
    IF @SPAction='Delete'
        BEGIN            
            DELETE FROM [CrawlSite] WHERE [Id] = @Id
        END
    IF @SPAction='Update'
        BEGIN            
            UPDATE [CrawlSite] 
            SET [WebName] = @WebName,[WebLink] = @WebLink,[SectionName] = @SectionName,[SectionIndexUrl] = @SectionIndexUrl,[SiteFrom] = @SiteFrom,[DeleteFlag] = @DeleteFlag 
            WHERE [Id] = @Id
        END
    IF @SPAction='SelectPaging'
        BEGIN            
            SELECT [Id],[WebName],[WebLink],[SectionName],[SectionIndexUrl],[SiteFrom],[DeleteFlag] 
            FROM [CrawlSite]
        END
    IF @SPAction='SelectAll'
        BEGIN            
            SELECT [Id],[WebName],[WebLink],[SectionName],[SectionIndexUrl],[SiteFrom],[DeleteFlag] 
            FROM [CrawlSite]
        END
    IF @SPAction='Unique'
        BEGIN            
            Declare @isExist int 
            if @Id = 0 
                Set @isExist = (select count(*) from [CrawlSite] where [WebName] = @WebName and [WebLink] = @WebLink and [SectionName] = @SectionName and [SectionIndexUrl] = @SectionIndexUrl and [SiteFrom] = @SiteFrom and [DeleteFlag] = @DeleteFlag) 
            else 
                Set @isExist = (select count(*) from [CrawlSite] where [WebName] = @WebName and [WebLink] = @WebLink and [SectionName] = @SectionName and [SectionIndexUrl] = @SectionIndexUrl and [SiteFrom] = @SiteFrom and [DeleteFlag] = @DeleteFlag and [Id] != @Id) 
            if @isExist>0 
                raiserror('data existed already',16,1)
        END
END

GO

USE WebRescourseForVipProject
GO 
SET ANSI_NULLS ON
GO 
SET QUOTED_IDENTIFIER ON
GO 

if exists (select * from dbo.sysobjects where id = object_id(N'[TableCrawlTask]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [TableCrawlTask] 

go 

CREATE PROCEDURE TableCrawlTask
        @Id int = 0, 
        @RuleId int = 0, 
        @SiteSectionId int = null, 
        @PageUrlWithParam nvarchar(2000) = null, 
        @IsStarted int = null, 

        @StartIndex int = 0, 
        @PageSize int = 0, 
        @SPReturnType varchar(100) = null, 
        @SPAction varchar(100) = null 
AS
BEGIN
    SET NOCOUNT ON;
    IF @SPAction='Insert'
        BEGIN            
            INSERT INTO [CrawlTask]([RuleId],[SiteSectionId],[PageUrlWithParam],[IsStarted]) 
            VALUES(@RuleId,@SiteSectionId,@PageUrlWithParam,@IsStarted) 

            Declare @newID varchar(10) 
            Set @newID = SCOPE_IDENTITY() 
            If (@newID is NULL) 
                set @newID = ('-1') 
            select @newID as 'newID'
        END
    IF @SPAction='Delete'
        BEGIN            
            DELETE FROM [CrawlTask] WHERE [Id] = @Id
        END
    IF @SPAction='Update'
        BEGIN            
            UPDATE [CrawlTask] 
            SET [RuleId] = @RuleId,[SiteSectionId] = @SiteSectionId,[PageUrlWithParam] = @PageUrlWithParam,[IsStarted] = @IsStarted 
            WHERE [Id] = @Id
        END
    IF @SPAction='SelectPaging'
        BEGIN            
            SELECT [Id],[RuleId],[SiteSectionId],[PageUrlWithParam],[IsStarted] 
            FROM [CrawlTask]
        END
    IF @SPAction='SelectAll'
        BEGIN            
            SELECT [Id],[RuleId],[SiteSectionId],[PageUrlWithParam],[IsStarted] 
            FROM [CrawlTask]
        END
    IF @SPAction='Unique'
        BEGIN            
            Declare @isExist int 
            if @Id = 0 
                Set @isExist = (select count(*) from [CrawlTask] where [RuleId] = @RuleId and [SiteSectionId] = @SiteSectionId and [PageUrlWithParam] = @PageUrlWithParam and [IsStarted] = @IsStarted) 
            else 
                Set @isExist = (select count(*) from [CrawlTask] where [RuleId] = @RuleId and [SiteSectionId] = @SiteSectionId and [PageUrlWithParam] = @PageUrlWithParam and [IsStarted] = @IsStarted and [Id] != @Id) 
            if @isExist>0 
                raiserror('data existed already',16,1)
        END
END

GO

USE WebRescourseForVipProject
GO 
SET ANSI_NULLS ON
GO 
SET QUOTED_IDENTIFIER ON
GO 

if exists (select * from dbo.sysobjects where id = object_id(N'[TableSiteTokens]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [TableSiteTokens] 

go 

CREATE PROCEDURE TableSiteTokens
        @Id int = 0, 
        @Token nvarchar(50) = null, 
        @SiteName nvarchar(50) = null, 
        @Ip nvarchar(50) = null, 

        @StartIndex int = 0, 
        @PageSize int = 0, 
        @SPReturnType varchar(100) = null, 
        @SPAction varchar(100) = null 
AS
BEGIN
    SET NOCOUNT ON;
    IF @SPAction='Insert'
        BEGIN            
            INSERT INTO [SiteTokens]([Token],[SiteName],[Ip]) 
            VALUES(@Token,@SiteName,@Ip) 

            Declare @newID varchar(10) 
            Set @newID = SCOPE_IDENTITY() 
            If (@newID is NULL) 
                set @newID = ('-1') 
            select @newID as 'newID'
        END
    IF @SPAction='Delete'
        BEGIN            
            DELETE FROM [SiteTokens] WHERE [Id] = @Id
        END
    IF @SPAction='Update'
        BEGIN            
            UPDATE [SiteTokens] 
            SET [Token] = @Token,[SiteName] = @SiteName,[Ip] = @Ip 
            WHERE [Id] = @Id
        END
    IF @SPAction='SelectPaging'
        BEGIN            
            SELECT [Id],[Token],[SiteName],[Ip] 
            FROM [SiteTokens]
        END
    IF @SPAction='SelectAll'
        BEGIN            
            SELECT [Id],[Token],[SiteName],[Ip] 
            FROM [SiteTokens]
        END
    IF @SPAction='Unique'
        BEGIN            
            Declare @isExist int 
            if @Id = 0 
                Set @isExist = (select count(*) from [SiteTokens] where [Token] = @Token and [SiteName] = @SiteName and [Ip] = @Ip) 
            else 
                Set @isExist = (select count(*) from [SiteTokens] where [Token] = @Token and [SiteName] = @SiteName and [Ip] = @Ip and [Id] != @Id) 
            if @isExist>0 
                raiserror('data existed already',16,1)
        END
END

GO

