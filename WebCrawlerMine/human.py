class Human:
    name = ""
    def __init__(self, name):
        self.name = name
        print "I'm a man. My name is " + self.name + "."
    def walk(self):
        print "I'm walking!"
    def called(self):
        print "They call me " + self.name + "!"
    def showname(self):
        print "My name is " + self.name + "."
    def rename(self, name):
        if(name != self.name):
            self.name = name
            print "My name is " + self.name + " now!"
        else:
            print "My name is still " + self.name + "!"

if __name__ == "__main__":
    human = Human("Bob");
    human.called();
    human.showname();
    human.rename("Bob");
    human.rename("Shit");
    human.walk();

