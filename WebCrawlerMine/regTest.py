#-*- coding:utf-8 -*-
import re


def findallignoreempty(pat, str):
    mat = re.findall(pat, str)
    res = []
    if len(mat):
        match = mat[0]
        for ma in match:
            if ma.strip() and ma not in res:
                res.append(ma.strip())
    if len(res):
        return res[0]
    else:
        return str


strs = [u'新浪科技 周雪昳 徐利 2016-04-08 08:29	',
        u'证券日报 黄 荣 2016-04-08 08:30	',
        u'21世纪经济报道 汪传鸿 2016-04-08 08:32	',
        u'新华社 文/韩洁、关桂峰、孙琪 2016-04-08 08:36	',
        u'华西都市报 文/杨尚智 王婷 2016-04-08 08:59	',
        u'大洋网-广州日报 2016-04-08 09:24	',
        u'金羊网 2016-04-08 09:28	',
        u'第一财经日报 郭丽琴 关健 赵陈婷 2016-04-08 10:38	',
        u'足亦 2016-04-08 10:53	',
        u'证券市场周刊 2016-04-11 14:36	',
        u' 2016-04-11 15:23',
        u'大洋网_广州日报 2016-04-08 09:24	'
        ]
reg = "[\ \-\_\/]+(%s) "
pat = reg.replace("%s",u'([a-zA-Z0-9\u4e00-\u9fff、 ]+)')
for str in strs:
    print findallignoreempty(pat, str)

reg = "(%s)[\ \-\_]+"
pat = reg.replace("%s",u'([a-zA-Z0-9\u4e00-\u9fff]+)')
for str in strs:
    print findallignoreempty(pat, str)