import urllib
import urllib2
from lxml import etree
from ConfigParser import ConfigParser
cf = ConfigParser()
cf.read("Crawl.conf")
proxyway = cf.get("proxy", "proxyway")

def getproxy():
    try:
        url = cf.get(proxyway, "url")
        import urllib2
        req = urllib2.Request(url)
        req.add_header('Referer', url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6')
        resp = urllib2.urlopen(req)
        res = resp.read()
        htmlnews = etree.HTML(res) 
        ips = htmlnews.xpath(cf.get(proxyway, "ips"))
        ports = htmlnews.xpath(cf.get(proxyway, "ports"))
        testurl = cf.get("proxy", "proxytesturl")
        for x in range(1,len(ips)):
            ip = ips[x]
            port = ports[x]
            proxy = "http://%s:%s" % (ip, port)
            proxy_temp = {"http":proxy}
            try:
                urllib.socket.setdefaulttimeout(15)
                urllib.urlopen(testurl,proxies=proxy_temp).read()
                return proxy_temp
            except Exception as ex:
                continue
        return ""
    except:
        return ""
if __name__ == '__main__':
    print getproxy()