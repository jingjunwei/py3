#coding=utf-8 
#!/usr/bin/env python
#-------------------------------------------------------------------------------
# Name: pymssqlTest.py
# Purpose: 测试 pymssql库，该库到这里下载：http://www.lfd.uci.edu/~gohlke/pythonlibs/#pymssql
#
# Author: scott
#
# Created: 04/02/2012
#-------------------------------------------------------------------------------
#import sys
#reload(sys)
#sys.setdefaultencoding("utf-8")

import pymssql
import time
import solr  
s = solr.SolrConnection('http://192.168.10.53:8080/solr/core_zk')

ISOTIMEFORMAT = '%Y-%m-%d %X'
class MSSQL:
    """
    对pymssql的简单封装
    pymssql库，该库到这里下载：http://www.lfd.uci.edu/~gohlke/pythonlibs/#pymssql
    使用该库时，需要在Sql Server Configuration Manager里面将TCP/IP协议开启

    用法：

    """

    def __init__(self,host,user,pwd,db):
        self.host = host
        self.user = user
        self.pwd = pwd
        self.db = db

    def __GetConnect(self):
        """
        得到连接信息
        返回: conn.cursor()
        """
        if not self.db:
            raise(NameError,"没有设置数据库信息")
        self.conn = pymssql.connect(host=self.host,user=self.user,password=self.pwd,database=self.db,charset="utf8")
        cur = self.conn.cursor()
        if not cur:
            raise(NameError,"连接数据库失败")
        else:
            return cur

    def ExecQuery(self,sql):
        """
        执行查询语句
        返回的是一个包含tuple的list，list的元素是记录行，tuple的元素是每行记录的字段

        调用示例：
                ms = MSSQL(host="localhost",user="sa",pwd="123456",db="PythonWeiboStatistics")
                resList = ms.ExecQuery("SELECT id,NickName FROM WeiBoUser")
                for (id,NickName) in resList:
                    print str(id),NickName
        """
        cur = self.__GetConnect()
        cur.execute(sql)
        resList = cur.fetchall()

        #查询完毕后必须关闭连接
        self.conn.close()
        return resList

    def ExecNonQuery(self,sql):
        """
        执行非查询语句

        调用示例：
            cur = self.__GetConnect()
            cur.execute(sql)
            self.conn.commit()
            self.conn.close()
        """
        cur = self.__GetConnect()
        cur.execute(sql)
        self.conn.commit()
        self.conn.close()

def main():
## ms =
## MSSQL(host="localhost",user="sa",pwd="123456",db="PythonWeiboStatistics")
## #返回的是一个包含tuple的list，list的元素是记录行，tuple的元素是每行记录的字段
## ms.ExecNonQuery("insert into WeiBoUser values('2','3')")
    ms = MSSQL(host="192.168.10.53",user="sa",pwd="vipproject",db="newMetrail")
    resList = ms.ExecQuery('''SELECT count(*) as count  FROM [newMetrail].[dbo].[main0_writer]''')
    
    total = 1
    pagesize = 50000
    for (count,) in resList:
        total = count
    print u'总条目：' + str(total)
    #total = 1
    if total != 0:
        for i in range(2, (total) / pagesize + 1):
            print u'第%d页' % i
            if i == (total) / pagesize:
                sql = u'''DECLARE	@return_value int,
                        @PCount int,
                        @RCount int

                EXEC	@return_value = [dbo].[sys_Page_v3_forpy]
                                @PCount = @PCount OUTPUT,
                                @RCount = @RCount OUTPUT,
                                @sys_Table = N'main0_writer',
                                @sys_Key = N'Authorid',
                                @sys_Fields = N'Authorid,docid',
                                @sys_Where = N'1=1',
                                @sys_Order = N'Authorid ASC',
                                @sys_Begin = %d,
                                @sys_PageIndex = %d,
                                @sys_PageSize = %d''' % ((i * pagesize + 1), 1, (total - i * pagesize))
            else:
                sql = u'''DECLARE	@return_value int,
                        @PCount int,
                        @RCount int

                EXEC	@return_value = [dbo].[sys_Page_v3_forpy]
                                @PCount = @PCount OUTPUT,
                                @RCount = @RCount OUTPUT,
                                @sys_Table = N'main0_writer',
                                @sys_Key = N'Authorid',
                                @sys_Fields = N'Authorid,docid',
                                @sys_Where = N'1=1',
                                @sys_Order = N'Authorid ASC',
                                @sys_Begin = 1,
                                @sys_PageIndex = %d,
                                @sys_PageSize = %d''' % (i + 1, pagesize)
            try:
                resList = ms.ExecQuery(sql)
                j = 1
                for (authorid, docid) in resList:
                    if authorid < "100000000591561":
                        continue
                    strs = u"'" + docid.replace(';',"','")
                    strs = '(' + strs[0:len(strs) - 2] + ',)'
                    strs = eval(strs)
                    if len(strs) > 0:
                        #print strs
                        q = ''
                        for ids in strs:
                            q += "id:%s OR " % ids
                        q = q[0:len(q) - 4] 
                        q = '(' + q + ')'
                        try:
                            result = s.query(q, rows=5)
                            num = len(result)
                            if num == 0:
                                continue
                            docids = ''
                            for r in result:
                                docids += (r['id'] + ';')
                            sql = u"""update main0_writer set docids = '%s',docNum = '%d' WHERE authorid = '%s'""" % (docids, num, authorid)
                            
                            if (j - 1) % 1000 == 0 or (i * pagesize + j) >= total - 1:
                                print u'currentId: ' + authorid
                                print u'进度:%.2f%% ' % ((i * pagesize + j) / (total * 1.0) * 100) + time.strftime(ISOTIMEFORMAT, time.localtime())
                            j += 1
                            try:
                                organList = ms.ExecNonQuery(sql)
                                #print sql
                                #raw_input()
                            except Exception,ex:
                                print Exception,":",ex
                                print u'currentPage: %s' % str(i)
                                print u'currentId: %s' % authorid
                                raw_input()
                                #continue
                        except Exception,ex:
                            print Exception,":",ex
                            print u'currentPage: %s' % str(i)
                            print u'currentId: %s' % authorid
                            raw_input()
                            #continue
            except Exception,ex:
                print Exception,":",ex
                print u'currentPage: %s' % str(i)
                raw_input()

if __name__ == '__main__':
    print time.strftime(ISOTIMEFORMAT, time.localtime()) + u"开始"
    main()
    print time.strftime(ISOTIMEFORMAT, time.localtime()) + u"结束完成"
    raw_input(u"回车退出。")
