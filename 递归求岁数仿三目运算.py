def GetAge(age, manPos):
    return ((manPos + 1) == 8) and (age + 2) or GetAge(age + 2, manPos + 1)

if __name__ == "__main__":
    print GetAge(10, 1)
    raw_input("Press Enter to exit.")
    
