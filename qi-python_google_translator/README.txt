谷歌翻译Python接口

本库未使用谷歌翻译API，而是直接通过谷歌翻译Web接口抓取数据。

为什么要做本库？
谷歌官方提供的翻译API价格太贵，大约是20刀/MB，用不起啊！！！
本库直接通过谷歌翻译的Web接口进行数据抓取，不会产生任何费用。

本库的优势：省钱，不会产生任何费用！

存在的问题和解决方法：
由于谷歌反采集做的很严格，如果同一IP访问频率过高，查询请求就可能被拦截，甚至在一段时间内IP被加入黑名单。
本库支持代理，可以通过指定代理进行轮换IP查询，降低被拦截的风险。

用法举例：
>>> from python_google_translator import translator
>>> print translator.translate(sl='en', tl='zh-CN', content='Hi,\nWhat time is it?', proxy='qi:ws2013@127.0.0.1:8080')
您好！
现在是什么时候？

参数说明：
sl - 原语言。
tl - 翻译成的目标语言。
content - 要翻译的内容。
proxy - 访问谷歌翻译使用的代理。


语言标识符（RFC 3066）：
http://www.i18nguy.com/unicode/language-identifiers.html

关于我们：
鲲鹏数据（西安鲲之鹏网络信息技术有限公司）从2010年开始专注于Web数据抓取领域。致力于为广大中国客户提供准确、快捷的Web数据采集相关服务。
您只需告诉我们您所要搜索的数据是什么，您要取得的是什么，你想要的数据是哪种格式，我们将为您做所有的工作，并直接把最终数据提交给你。数据的格式可以是CSV、JSON、XML、ACCESS、MSSQL、MYSQL等。
网站：http://www.webscraping.cn
邮箱：hello@webscraping.cn