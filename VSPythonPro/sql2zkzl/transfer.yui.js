﻿function wxtransfer(url, type, lid, date, co, Title_C, Writers, Name_C, Years, Num, Volumn, GCH, Vol, BookID, SubjectNum, SpecialNum, mirrorName, UserID, UserName, sourceID, key) {
    // 判断是否登录                
    var userInfo = loginUserInfo();
    if (userInfo !== null) {
        // 期刊保持原有逻辑
        if (type != "1") {
            // 通过请求获取可否购买
            // 1 需要获取文献的价格、提供文献数据库的id获取文献的价格
            // 2 查看 下载日志所需的几个字段值key
            // 3 返回的是文献传递FUN
            yzdownload(key, url, lid, date, co, Title_C, Writers, Name_C, Years, Num, Volumn, GCH, Vol, BookID, SubjectNum, SpecialNum, mirrorName, UserID, UserName, sourceID);
            //gettransferurl(url, lid, date, co, Title_C, Writers, Name_C, Years, Num, Volumn, GCH, Vol, BookID, SubjectNum, SpecialNum, mirrorName, UserID, UserName, sourceID);
        }
        else {
            var paramstr = "ts=" + new Date().getTime();
            $.ajax
                ({
                    type: "get",
                    url: "/ajax/transferurl.ashx?gch=" + GCH + "&ts=" + new Date().getTime(),
                    dataType: "text",
                    data: paramstr,
                    async: false,
                    success: function (msg) {
                        // 通过请求获取可否购买
                        yzdownload(key, msg, lid, date, co, Title_C, Writers, Name_C, Years, Num, Volumn, GCH, Vol, BookID, SubjectNum, SpecialNum, mirrorName, UserID, UserName, sourceID);
                        //gettransferurl(msg, lid, date, co, Title_C, Writers, Name_C, Years, Num, Volumn, GCH, Vol, BookID, SubjectNum, SpecialNum, mirrorName, UserID, UserName, sourceID);
                    },
                    error: function () {
                        alert("该功能暂不可用!");
                    }
                });
        }
    } else {
        alert("您未登录，请登录后重试。");
        location.href = "/Main/Home/Login?furl=" + encodeURIComponent(location.href);
    }
}

//下载
function geturl(_key, msg, lid, date, co, Title_C, Writers, Name_C, Years, Num, Volumn, GCH, Vol, BookID, SubjectNum, SpecialNum, mirrorName, UserID, UserName, sourceID, isselfpay) {
    var key = _key;
    if (!isselfpay)
        $.ajax({
            async: true,
            type: "POST",
            dataType: "json",
            data: { action: "GetDownloadUrl", datak: key },
            url: "/ajax/getdownloadurl.ashx",
            success: function (data) {
                if (data.downloadurl) {
                    eval(data.downloadurl);
                }
                else if (data.error) {
                    alert("请求下载失败: " + data.error);
                } else {
                    alert("请求下载失败: 未知原因。");
                }
            }
        });
    else {
        $.ajax({
            async: true,
            type: "POST",
            dataType: "json",
            data: { action: "GetDownloadUrlByself", datak: key },
            url: "/ajax/getdownloadurl.ashx",
            success: function (data) {
                if (data.downloadurl) {
                    eval(data.downloadurl);
                }
                else if (data.error) {
                    alert("请求下载失败: " + data.error);
                } else {
                    alert("请求下载失败: 未知原因。");
                }
            }
        });
    }
    return true;
}

function yzdownload(key, msg, lid, date, co, Title_C, Writers, Name_C, Years, Num, Volumn, GCH, Vol, BookID, SubjectNum, SpecialNum, mirrorName, UserID, UserName, sourceID) {
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        data: { action: "GetUserDeductionType", key: key },
        url: "/ajax/getdownloadurl.ashx",
        success: function (data) {
            if (typeof (data.type) !== 'undefined') {
                var money = data.money;
                //value="0" 包库计费
                //value="1" 流量计费
                //value="2" 按篇计费
                if (data.type === 2) {
                    if (data.isrole3) {
                        if (money === "0.00") {
                            if (window.confirm('本篇文献免费，点击确认并下载此文献！')) {
                                geturl(key, msg, lid, date, co, Title_C, Writers, Name_C, Years, Num, Volumn, GCH, Vol, BookID, SubjectNum, SpecialNum, mirrorName, UserID, UserName, sourceID);
                            }
                        } else if (parseFloat(data.balance) >= parseFloat(money.replace(/\)|\(|元/g, ""))) {
                            if (window.confirm('您的企业分配的余额: ' + data.balance + ' 元, 你确定要使用' + money + '购买并下载此文献吗？')) {
                                geturl(key, msg, lid, date, co, Title_C, Writers, Name_C, Years, Num, Volumn, GCH, Vol, BookID, SubjectNum, SpecialNum, mirrorName, UserID, UserName, sourceID);
                            }
                        } else if (parseFloat(data.balanceself) >= parseFloat(money.replace(/\)|\(|元/g, ""))) {
                            if (window.confirm('您的企业分配的余额: ' + data.balance + ' 元, 当前文献需' + money + '购买，企业分配的余额不足，\n您的自主账户余额 ' + data.balanceself + ' 元,是否使用自主账户购买？')) {
                                geturl(key, msg, lid, date, co, Title_C, Writers, Name_C, Years, Num, Volumn, GCH, Vol, BookID, SubjectNum, SpecialNum, mirrorName, UserID, UserName, sourceID, true);
                            }
                        } else {
                            alert('您的企业分配的余额: ' + data.balance + ' 元，\n您的自主账户余额 ' + data.balanceself + ' 元,\n当前文献需' + money + '购买，您的余额不足，请充值。');
                        }
                    } else {
                        if (money === "0.00") {
                            if (window.confirm('本篇文献免费，点击确认并下载此文献！')) {
                                geturl(key, msg, lid, date, co, Title_C, Writers, Name_C, Years, Num, Volumn, GCH, Vol, BookID, SubjectNum, SpecialNum, mirrorName, UserID, UserName, sourceID);
                            }
                        } else if (parseFloat(data.balance) >= parseFloat(money.replace(/\)|\(|元/g, ""))) {
                            if (window.confirm('您的余额: ' + data.balance + ' 元, 你确定要使用' + money + '购买并下载此文献吗？')) {
                                geturl(key, msg, lid, date, co, Title_C, Writers, Name_C, Years, Num, Volumn, GCH, Vol, BookID, SubjectNum, SpecialNum, mirrorName, UserID, UserName, sourceID);
                            }
                        } else {
                            alert('您的余额: ' + data.balance + ' 元, 当前文献需' + money + '购买，您的余额不足，请充值。');
                        }
                    }

                } else if (data.type === 1) {
                    if (data.isrole3) {
                        if (parseInt(data.remaining) >= 1) {
                            if (window.confirm('您的企业分配的流量余额: ' + data.remaining + ' 篇, 你确定要购买并下载此文献吗？')) {
                                geturl(key, msg, lid, date, co, Title_C, Writers, Name_C, Years, Num, Volumn, GCH, Vol, BookID, SubjectNum, SpecialNum, mirrorName, UserID, UserName, sourceID);
                            }
                        } else if (parseInt(data.remainingself) >= 1) {
                            if (window.confirm('您的企业分配的余额: ' + data.remaining + ' 篇, 当前文献需1流量购买，企业分配的余额不足，\n您的自主账户流量余额 ' + data.remainingself + ' 篇,是否使用自主账户购买？')) {
                                geturl(key, msg, lid, date, co, Title_C, Writers, Name_C, Years, Num, Volumn, GCH, Vol, BookID, SubjectNum, SpecialNum, mirrorName, UserID, UserName, sourceID, true);
                            }
                        } else {
                            alert('您的企业分配的流量余额: ' + data.remaining + ' 篇，\n您的自主账户流量余额 ' + data.remainingself + ' 篇,\n当前文献需1流量购买，您的余额不足，请充值。');
                        }
                    } else {
                        if (parseFloat(data.remaining) >= 1) {
                            if (window.confirm('您的流量余额: ' + data.remaining + ' 篇, 你确定要购买并下载此文献吗？')) {
                                geturl(key, msg, lid, date, co, Title_C, Writers, Name_C, Years, Num, Volumn, GCH, Vol, BookID, SubjectNum, SpecialNum, mirrorName, UserID, UserName, sourceID);
                            }
                        } else {
                            alert('您的流量余额: ' + data.remaining + ' 篇, 当前文献需1流量购买，您的余额不足，请充值。');
                        }
                    }
                } else if (data.type === 0) {
                    if (window.confirm('您是包库用户，可免费下载，确认下载？')) {
                        geturl(key, msg, lid, date, co, Title_C, Writers, Name_C, Years, Num, Volumn, GCH, Vol, BookID, SubjectNum, SpecialNum, mirrorName, UserID, UserName, sourceID);
                    }
                } else {
                    alert("查询用户消费类型失败,请联系管理员处理.");
                }
            } else if (data.error) {
                alert("查询用户消费类型失败: " + data.error);
            } else {
                alert("查询用户消费类型失败,请联系管理员处理.");
            }
        }
    });
}

function gettransferurl(url, lid, date, co, Title_C, Writers, Name_C, Years, Num, Volumn, GCH, Vol, BookID, SubjectNum, SpecialNum, mirrorName, UserID, UserName, sourceID) {
    if (url.length > 0) {
        document.wxTransfer.lid.value = lid;
        document.wxTransfer.date.value = date;
        document.wxTransfer.co.value = co;
        document.wxTransfer.Title_C.value = Title_C;
        document.wxTransfer.Writers.value = Writers;
        document.wxTransfer.Name_C.value = Name_C;
        document.wxTransfer.Num.value = Num;
        document.wxTransfer.Years.value = Years;
        document.wxTransfer.Volumn.value = Volumn;
        document.wxTransfer.GCH.value = GCH;
        document.wxTransfer.Vol.value = Vol;
        document.wxTransfer.BookID.value = BookID;
        document.wxTransfer.SubjectNum.value = SubjectNum;
        document.wxTransfer.SpecialNum.value = SpecialNum;
        document.wxTransfer.mirrorName.value = mirrorName;
        document.wxTransfer.UserID.value = UserID;
        document.wxTransfer.UserName.value = UserName;
        document.wxTransfer.sourceID.value = sourceID;
        document.wxTransfer.action = url;

        //document.wxTransfer.target = "_blank";
        document.wxTransfer.submit();
    }
    else {
        alert("该功能暂不可用!");
    }
}