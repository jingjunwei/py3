﻿/**聚类的展开和折叠动作*/
var btnClusterToolClickTimes = 0;
(function () {
    var jqGroupBlocks;
    //左侧聚类折叠按钮，默认是关闭状态
    function toggleLeftCluster() {
        //if (!jqGroupBlocks)
        //    jqGroupBlocks = $(".cluster-group");
        //jqGroupBlocks.on("click.expand", "tt a", function (event) {
        //    var jqtt = $(this);
        //    if (jqtt.hasClass("close")) {
        //        //当前点击a的DIV祖先元素的同辈DIV元素的UL列表关闭
        //        jqtt.closest("div.cluster-group").siblings("div").find("div > ul").each(function () {
        //            if ($(this).children("li").length > 5)
        //                $(this).css("height", "118px").closest("div").prev().find("tt a").removeClass("open").addClass("close");
        //        });
        //        //添加当前聚类列表打开样式
        //        jqtt.removeClass("close").addClass("open");
        //        //移出当前聚类列表的样式
        //        jqtt.closest("h3").next().children("ul").removeAttr("style");
        //    }
        //    else {
        //        //移出当前聚类列表关闭样式
        //        jqtt.removeClass("open").addClass("close");
        //        //高度设置为118
        //        jqtt.closest("h3").next().children("ul").css("height", "118px");
        //    }
        //});
    }
    function offClick() {
        //移出那些列表少于5项的脚本事件
        //jqGroupBlocks.each(function () {
        //    var jqGroupBlock = $(this);
        //    if (jqGroupBlock.find("ul li").length <= 5) {
        //        jqGroupBlock.find("tt a").removeClass("close").addClass("open");
        //        jqGroupBlock.off();
        //    }
        //});
    }

    $(function () {
        //检索报告、对象比较显示与隐藏
        //if ($("#hifOutObjectType").val() == "Articles") {
        //    $("#btnRetrieveReport").show();
        //    $("#btnGoToCompare").hide();
        //} else {
        //    $("#btnRetrieveReport").hide();
        //    $("#btnGoToCompare").show();
        //}
        //toggleLeftCluster();
        //offClick();
    });

})();
/**聚类工具的动作*/
(function () {
    var fieldMap = {
        ZY: "学科",
        OCC: "机构",
        ALC: "地区"
    };
    var contNum = 0;      //聚类查询条件选中个数
    /**在指定的字段名中搜索
    @param {string} name 下拉框中的字段名
    @param {string} value 文本框中的值
    */
    function buildSearchRulesByFieldName(name, value, originalValue) {
        var fields = name.split("#");
        var fieldsNames = fields[1].split("#");
        var rule = "{0}={1}".format(fieldsNames[0], value);
        return getSearchRules(rule, originalValue);
    }

    function buildSearhcRulesNoFieldName(value, originalValue) {
        value = "U={0}".format(value);
        return getSearchRules(value, originalValue);
    }
    /**返回最终拼接好的表达式*/
    function getSearchRules(value, originalValue) {
        if (!value || !value.trim()) return originalValue;
        if (originalValue)
            originalValue += "[*]" + value;
        else
            originalValue = value;
        return originalValue;
    }

    /**验证年份是否正确，如果正确直接返回表达式，不正确返回null*/
    function validateYearRange(objType) {
        if (!window.slidingDatepicker.validate())
            return null;

        var formatYears = window.slidingDatepicker.toString();
        if (objType != "articles") {
            return "year=" + formatYears;
        }
        return "YY=" + formatYears;
    }
    /**结果中搜索*/
    window.g_resultAdd = function () {
        var request = g_getUrlParms();
        var resultFilter = "";
        if (request["rf"]) {
            resultFilter = request["rf"];
        }

        $("input[name='txtResultAdd']").each(function () {
            var downkValue = this.value.replace(/(\;|\,|\<|\>)/g, "").replace(/(\))/g, "）").replace(/(\()/g, "（").replace(/(\[)/g, "0x005b").replace(/(\])/g, "0x005d").replace(/(\')/g, "‘").replace(/(\')/g, "’").replace(/(\")/g, "“").replace(/(\")/g, "”").replace(/(\=)/g, "0x005a");
            if (downkValue) {
                var field = $(this).closest(".refine-item").find("select").val();
                if (!field) {
                    field = $(this).closest(".refine-item").find("select").find("option:eq(0)").val();
                }
                if (field === "U") {
                    resultFilter = buildSearhcRulesNoFieldName(downkValue, resultFilter);
                } else {
                    resultFilter = buildSearchRulesByFieldName(field, downkValue, resultFilter);
                }
            }
        });
        var year = "";
        var objType = $("#hfldObjectType").val();
        var rule = "";
        //处理年份表达式
        if (objType != "articles") {
            //year = validateYearRange(objType);
        } else {
            rule = validateYearRange(objType);
        }

        if (rule != "") {
            resultFilter = getSearchRules(rule, resultFilter);
        }
        var newUrl = g_addQueryString(/page=\d*/i, "page=1");
        if (resultFilter) {
            newUrl = g_addQueryString(/rf=[^&#]*/i, "rf=" + encodeURIComponent(resultFilter), newUrl);
        }
        if (year) {
            if (g_getUrlParms("year")) {
                newUrl = g_addQueryString(/year=[^&#]*/i, year, newUrl);
            } else {
                if (newUrl.indexOf("?") > 0) {
                    newUrl += "&" + year;
                } else {
                    newUrl += "?" + year;
                }
            }
        }
        if (newUrl.length > 1000) {
            alert("检索表达式位数超出");
            return false;
        }
        if (!resultFilter && !year) return false;

        searchHistoryRecord(newUrl); // 记录查询历史
        location.href = newUrl;
        if (window.event)
            window.event.returnValue = false; //ie6fix
    };
    //移除所选查询条件
    window.g_RemoveCondition = function (thisCon) {
        //排除查询条件中的非选中值	
        var selectConditionDom = document.getElementById("selectCondition");
        var totalSelectConds = selectConditionDom.value.split("[_]");
        var currentCondtion = $(thisCon).parent().attr("cfield");
        var tempCond = "";
        //从隐藏域中去掉当前的检索条件
        for (var i = 0; i < totalSelectConds.length; i++) {
            if (totalSelectConds[i] != currentCondtion) {
                var tmp = totalSelectConds[i] + "[_]";
                tempCond += tmp;
            }
        }
        if (tempCond.length > 0) {
            tempCond = tempCond.substring(0, tempCond.length - 3);
        }
        selectConditionDom.value = tempCond;
        //取消已排除条件的复选框	
        $(".cluster-group :checkbox").each(function () {
            if (this.value === currentCondtion)
                this.checked = false;
        });
        //移除选中的聚类查询条件
        contNum--;
        $(thisCon).parent().remove();
        setSelectCondNum();
    };
    //设置选中条件个数
    function setSelectCondNum() {
        $("#selectNumber").text(contNum < 0 ? 0 : contNum);
    }
    /** 回填复选框*/
    function fillBack() {
        if (!document.getElementById("selectCondition")) return;
        var condition = document.getElementById("selectCondition").value;
        if (condition) {
            var strhtml = "";
            var conditions = condition.split("[_]");
            //
            //取得选中的聚类的名称
            for (var i = 0; i < conditions.length; i++) {
                var showAllDiscriptorName = conditions[i].split("#")[1];
                //截段聚类的名称 
                var showTrimDiscriptorName = showAllDiscriptorName.trimToMaxLength(6, "...");
                //alert(conditions);
                var strType = $(".cluster-group input:checkbox[value^='" + conditions[i].split("#")[0].split("=")[0] + "=" + "']").closest("div").prev().text();
                strhtml += "<li cfield='{0}'><span class='type'>[{1}]</span><a href='javascript:void(0)' title='{2}'>{3}</a><a class='del' href='javascript:void(0)' onclick='g_RemoveCondition(this)'><img src='/images/selected-del.gif' width='16' height='16'  /></a></li>".format(conditions[i], strType, showAllDiscriptorName, showTrimDiscriptorName);
            }
            $("#cplist").html(strhtml);
        }

        
        if (conditions)
            contNum = conditions.length;
        setSelectCondNum();

        if (contNum > 0) {
            $("#selectNumber").show();
        } else {
            $("#selectNumber").hide();
        }
    }

    /**单选按钮功能*/
    function switchType(obj) {
        var url = g_addQueryString(/page=[^&#]+/i, "page=" + 1);
        location.href = g_addQueryString(/searchType=[^&#]*/i, "searchType=" + obj.value, url);
    }

    /**点击li中的span元素时直接发起请求，将其下的input中的值写入隐藏域，然后提交就可以了*/
    function singleCondtionRequest(obj) {
        //取得复选框中的条件
        var inputDom = $(obj).siblings("input").get(0);
        var condition = inputDom.value;
        var selectConditionDom = document.getElementById("selectCondition");
        if (!selectConditionDom) return;
        var totalSelectCond = selectConditionDom.value;
        //取复选框的反值
        inputDom.checked = !inputDom.checked;
        if (inputDom.checked) {
            contNum++;
            if (contNum > 5) {
                contNum--;
                alert("您所选的查询过滤条件不能超过5个！");
                inputDom.checked = false;
            } else {
                if (!validateMultipleTwoLevel(condition, totalSelectCond)) return;
                if (totalSelectCond != "") {
                    var cond1 = "[_]" + condition;
                    totalSelectCond += cond1;
                } else {
                    totalSelectCond += condition;
                }
                selectConditionDom.value = totalSelectCond;
            }
        } else {
            //排除查询条件中的非选中值
            var totalSelectConds = totalSelectCond.split("[_]");
            var tempCond = "";
            for (var i = 0; i < totalSelectConds.length; i++) {
                if (totalSelectConds[i] != condition) {
                    var tmp = totalSelectConds[i] + "[_]";
                    tempCond += tmp;
                }
            }
            if (tempCond.length > 0) {
                tempCond = tempCond.substring(0, tempCond.length - 3);
            }
            selectConditionDom.value = tempCond;
        }
        selectConditionDom = inputDom = null;
        $("#submitExecute").click();
    }
    /**验证用户是否选中了同类型的多个二级条件*/
    function validateMultipleTwoLevel(singleCondition, conditions) {
        if (!conditions) return true;
        var arrCondtions = conditions.split("[_]");
        var newFieldName = singleCondition.split("#")[0].split("=")[0]; //当前选中条件的字段名
        var newId = singleCondition.split("#")[0].split("=")[1];
        var newPrefix = newId.substring(0, newId.lastIndexOf("_"));
        for (var i = 0; i < arrCondtions.length; i++) {
            var id = arrCondtions[i].split("#")[0].split("=")[1];
            //只有id个数大于1才去处理
            if (id.split("_").length > 1 && newId.split("_").length > 1) {
                var orginalFieldName = arrCondtions[i].split("#")[0].split("=")[0];
                //字段名相同再判断是否同一个上级的子级，若不是就要弹出警告
                if (orginalFieldName === newFieldName) {
                    if (id.substring(0, id.lastIndexOf("_")) !== newPrefix) {
                        alert("您已选中同类型的二级" + fieldMap[newFieldName] + "，请先取消重新选择!");
                        return false;
                    }
                }

            }
        }
        return true;
    }

    $(function () {
        //绑定结果中搜索的回车事件
        $("#resultAdd").on("keydown", ":text", function (event) {
            if (event.keyCode == 13)
                g_resultAdd();
        });
        fillBack();
        //绑定清空所有查询条件按钮
        $("#clearAllCondition").click(function () {
            //清空隐藏域中的条件
            document.getElementById("selectCondition").value = "";
            //删除显示的条件列表
            $("#cplist").empty();
            //复选框中全部非选中状态
            $(".cluster-group input[type=checkbox]").each(function () {
                this.checked = false;
            });
            //总数清零
            contNum = 0;
            setSelectCondNum();

            //$("#submitExecute").click();
        });

        //检索报告
        $("#btnRetrieveReport").on("click", function () {
            if (btnClusterToolClickTimes > 0) {
                btnClusterToolClickTimes = 0;
                return false;
            } else {
                btnClusterToolClickTimes++;
            }

            var url = location.href;
            var index = url.indexOf('?');
            //没有查询字符串
            if (index == -1) {
                window.open("/zk/atlas.aspx");
            } else {
                var p = url.substring(index);
                window.open("/zk/atlas.aspx" + p);
            }
        });

        //对比
        $("#btnGoToCompare").on("click", function () {
            var otype = $("#hfldObjectType").val().toLocaleLowerCase();
            if (otype == "journal") {

                var ids = [];
                var k = 0;
                $("#contentDiv>div>dl>dd.check").each(function (a, b) {
                    if ($(b).children("input:checkbox").prop("checked") == true) {
                        //ids[k] = $(b).children("input:checkbox").attr("gchdata").toLocaleLowerCase();
                        //k++;
                        ids[k] = $(b).children("input:checkbox").attr("value").toLocaleLowerCase();
                        k++;
                    }
                });
                if (ids.length > 5) {
                    alert("对比条件不能超过5个");
                }
                else {
                    var str = "";
                    for (var i = 0; i < ids.length; i++) {
                        str = str + ids[i] + "|";
                    }
                    var strids = encodeURIComponent(str);
                    var url = "ids=" + strids + "&type=" + otype;
                    location.href = "/compare/journalcompare.aspx?" + url;
                }

            } else {
                var ids = $("#hfldSelectedIds").val();
                ids = ids.substring(1, ids.length - 1);
                if (ids) {
                    ids = ids.split(",");
                    if (ids.length > 5) {
                        alert("对比条件不能超过5个");
                    } else {
                        var str = "";
                        for (var i = 0; i < ids.length; i++) {
                            str = str + ids[i] + "|";
                        }
                        str = encodeURIComponent(str);
                        var url = "ids=" + str + "&type=" + otype;
                        if (otype == "writer") {
                            location.href = "/compare/writercompare.aspx?" + url;
                        }
                        else if (otype == "subject") {
                            location.href = "/compare/subjectcompare.aspx?" + url;
                        }
                        else if (otype == "organ") {
                            location.href = "/compare/organcompare.aspx?" + url;
                        }
                        else if (otype == "fund") {
                            location.href = "/compare/fundcompare.aspx?" + url;
                        }
                    }
                } else {
                    var str = "";
                    str = encodeURIComponent(str);
                    var url = "ids=" + str + "&type=" + otype;
                    if (otype == "writer") {
                        location.href = "/compare/writercompare.aspx?" + url;
                    }
                    else if (otype == "subject") {
                        location.href = "/compare/subjectcompare.aspx?" + url;
                    }
                    else if (otype == "organ") {
                        location.href = "/compare/organcompare.aspx?" + url;
                    }
                    else if (otype == "fund") {
                        location.href = "/compare/fundcompare.aspx?" + url;
                    }
                }
            }
        });

        $("#btnClusterTool").on("mouseenter", function () {
            if ($("#divClusterTool").is(":hidden")) {
                $("#btnClusterTool").attr("title", "点击当前位置将显示聚类选择信息！");
            } else {
                $("#btnClusterTool").attr("title", "点击当前位置将隐藏聚类选择信息！");
            }
        });

        //绑定隐藏聚类工具栏按钮
        $("#btnClusterTool").on("click", function () {
            if (btnClusterToolClickTimes > 0) {
                btnClusterToolClickTimes = 0;
                return false;
            } else {
                btnClusterToolClickTimes++;
            }

            if ($("#divClusterTool").is(":hidden")) {
                $("#divClusterTool").show();
                $("#btnClusterTool").attr("title", "点击当前位置将显示聚类选择信息！").addClass("current");
            } else {
                $("#divClusterTool").hide();
                $("#btnClusterTool").attr("title", "点击当前位置将隐藏聚类选择信息！").removeClass("current");
            }
            if (contNum > 0) {
                $("#selectNumber").show();
            } else {
                $("#selectNumber").hide();
            }
        });

        $("#hideCondition").click(function () {
            $("#divClusterTool").hide();
            $("#btnClusterTool").attr("title", "点击当前位置将隐藏聚类选择信息！").removeClass("current");
            if (contNum > 0) {
                $("#selectNumber").show();
            } else {
                $("#selectNumber").hide();
            }
        });

        //点击对学科，地区，机构下一级聚类
        $("#ctl00_classcluster,#ctl00_areacluster,#ctl00_organcluster").on("click.cluster", "li a", function (event) {

            var jqSelf = $(this);
            var jqParentLi = jqSelf.parent();
            var jqChildrenUl = jqSelf.siblings("ul");
            //如果已经有数据了就不再去请求了
            if (jqChildrenUl.length) {
                if (jqSelf.hasClass("show")) {
                    jqChildrenUl.show();
                    jqSelf.removeClass("show").addClass("hide");
                } else {
                    jqChildrenUl.hide();
                    jqSelf.removeClass("hide").addClass("show");
                }

            } else {
                //添加加载状态样式
                jqSelf.removeClass("show").addClass("loading");
                var clusterValue = $(this).prev().attr("dataitem");
                $.ajax({
                    url: "/ajax/clusterextendhandler.ashx",//clusterValue
                    data: { clusterValue: clusterValue, type: document.getElementById("hfldObjectType").value, relField: $("#hfldRelField").val(), relValue: $("#hfldRelFieldValue").val(), searchKey: GetQueryString("key"), clusterFilter: GetQueryString("cf"), resultFilter: GetQueryString("rf"), strIds: GetQueryString("strIds"), sType: GetQueryString("sType"), keyid: $("#hfldSearchConditionValue").val(), filterValue: $("#hfldFilterValue").val(), year: GetQueryString("year") },
                    dataType: "json",
                    type: "post",
                    timeout: 15000
                }).done(function (data) {
                    if (data.Flag) {
                        //简单处理，防止用户因为服务迟迟没有返回而多次点击
                        if (jqSelf.siblings("ul").length) return;
                        //无数据填充提示信息
                        if (!data.ResultHtml)
                            //                            data.ResultHtml = "<ul class='level1'><li>没有下级聚类信息</li></ul>";
                            data.ResultHtml = "<ul class='level1'><li></li></ul>";
                    } else
                        data.ResultHtml = "<ul class='level1'><li>{0}</li></ul>".format(data.Msg);
                    jqParentLi.append(data.ResultHtml);
                    //无论服务器端是否出错这里都要处理样式 
                    jqSelf.removeClass("loading").addClass("hide");
                    var jqRoot = jqSelf.closest(".cluster-group");
                    //添加当前聚类列表打开样式
                    jqRoot.children("h3").find("a").removeClass("close").addClass("open");
                    //移出当前聚类列表的样式
                    jqRoot.children("div").children("ul").removeAttr("style");
                }).fail(function (jqXhr, statusText, e) {
                    if (statusText == "timeout") {
                        jqParentLi.append("<ul class='level1'><li>请求超时，请稍等再请求</li></ul>");
                        jqSelf.siblings("ul").fadeOut(5000, function () {
                            jqSelf.removeClass("loading").addClass("show");
                            jqSelf.siblings("ul").remove();
                        });
                    }
                });
            }
            return false;

        });
        //处理聚类点击事件
        $(".cluster-group").on("click.filter", "li input,li span[type!='zkbycount']", function (event) {
            var nodeName = event.target.nodeName.toLowerCase();
            //如果不是需要响应的元素直接退出
            if (nodeName !== "input" && nodeName !== "span")
                return false;
            //如果用户点击的是span元素，那么直接发起请求
            if (nodeName === "span") {
                return singleCondtionRequest(event.target);
            }
            //单选按钮事件 
            if (event.target.type.toLowerCase() === "radio")
                return switchType(event.target);

            $("#divClusterTool").show();
            $("#btnClusterTool").addClass("current");

            var strType;              //聚类类型描述名
            var showTrimDiscriptorName;    //显示在列表中的截取后的聚类名称
            var showAllDiscriptorName;   //聚类完全描述名，放在title属性中
            var strhtml;                  //组装后的聚类工具栏信息
            //取得隐藏域dom
            var selectConditionDom = document.getElementById("selectCondition");
            //取得当前点击的类型的描述名称，如：机构，学科等
            strType = $(this).closest("div").prev().text().replace("图谱分析", "");
            //取得当前复选框中的条件
            var condition = this.value;

            //取得选中的聚类的名称
            showAllDiscriptorName = condition.split("#")[1];
            //截段聚类的名称 
            showTrimDiscriptorName = showAllDiscriptorName.trimToMaxLength(6, "...");
            //聚得隐藏域中的条件
            var totalSelectCond = selectConditionDom.value;
            //处理复选框点击事件，根据选中状态执行不同操作
            if (this.checked) {
                contNum++;
                if (contNum > 5) {
                    contNum--;
                    alert("您所选的查询过滤条件不能超过5个！");
                    this.checked = false;
                } else {
                    if (!validateMultipleTwoLevel(condition, totalSelectCond)) {
                        contNum--;
                        return false;
                    }
                    if (totalSelectCond != "") {
                        var cond1 = "[_]" + condition;
                        totalSelectCond += cond1;
                    } else {
                        totalSelectCond += condition;
                    }
                    selectConditionDom.value = totalSelectCond;

                    strhtml = "<li cfield='{0}'><span class='type'>[{1}]</span><a href='javascript:void(0)' title='{2}'>{3}</a><a class='del' href='javascript:void(0)' onclick='g_RemoveCondition(this)'><img src='/images/selected-del.gif' width='16' height='16'  /></a></li>".format(condition, strType, showAllDiscriptorName, showTrimDiscriptorName);
                    $("#cplist").append(strhtml);

                    setSelectCondNum();
                }
            } else {
                //排除查询条件中的非选中值
                var totalSelectConds = totalSelectCond.split("[_]");
                var tempCond = "";
                for (var i = 0; i < totalSelectConds.length; i++) {
                    if (totalSelectConds[i] != condition) {
                        var tmp = totalSelectConds[i] + "[_]";
                        tempCond += tmp;
                    }
                }
                if (tempCond.length > 0) {
                    tempCond = tempCond.substring(0, tempCond.length - 3);
                }
                selectConditionDom.value = tempCond;
                //移出列表的li元素
                $("li[cfield='" + condition + "']").remove();
                contNum--;
                setSelectCondNum();
            }

            if (contNum > 0) {
                $("#selectNumber").show();
            } else {
                $("#selectNumber").hide();
            }

            event.stopPropagation();
            selectConditionDom = null;

        });

        //执行按钮响应事件
        $("#submitExecute").click(function () {
            var sCond = $("#selectCondition").val();
            var url = g_addQueryString(/page=\d*/i, "page=1");
            if (sCond == "") {
                //alert("您尚未选择任何聚类条件，请选择后再点击执行！");
                location.href = g_addQueryString(/(&)?cf=[^&#]*/i, "", url);
                return false;
            }
            var clusterFilter = sCond;
            clusterFilter = encodeURIComponent(clusterFilter);
            var newUrl = g_addQueryString(/cf=[^&#]*/i, "cf=" + clusterFilter, url);
            if (newUrl.length > 1000) {
                alert("检索表达式位数超出");
                return false;
            }
            if ($("#hifOutObjectType").val() == "JournalGuide") {
                var index = newUrl.indexOf('?');
                //提取查询串
                var queryString = newUrl.substring(index + 1); //不包括?
                newUrl = "/journal/journalsearch.aspx?" + queryString;
            }
            location.href = newUrl;
            if (window.event)
                window.event.returnValue = false; //ie6fix
        });

        if ($(".bycountrange").text()) {
            var size = $(".bycountrange").next("div").find("li").size();
            if (size == 1) {
                var chk = $(".bycountrange").next("div").find("li:eq(0)").find("input");
                if (!chk.is(":checked") && chk.val().trim() == "BRCC=0#0") {
                    $(".bycountrange").parent(".cluster-group").hide();
                }
            }
        }

        //合并检索
        var pageType = $("#hfldObjectType").val();
        if (pageType == "organ") {
            var request = g_getUrlParms();
            var type = request["searchType"];
            if (type && type == "5") {
                $("#btnMergeTool").show();
                $("#btnClusterTool").hide();
            } else {
                $("#btnMergeTool").hide();
                $("#btnClusterTool").show();
            }
        }
        $("#btnMergeSearch").on("click", function () {
            var strEx = "", request = g_getUrlParms();
            var id = request["id"] + "#" + $("#hidOrganName").val();
            $("#ulMergeSearch li").each(function (i, item) {
                var value = $(this).attr("mid") + "#" + $(this).find("a").html();
                if (value) {
                    if (i == 0) {
                        strEx = "O=" + value + "";
                    } else {
                        strEx += "[+]O=" + value;
                    }
                }
            });
            if (strEx == "") {
                alert("请先选中合并检索的内容");
            } else {
                strEx += "[+]O=" + id;
                location.href = "/zk/search.aspx?rf=" + encodeURIComponent(strEx);
            }
        });
    });
})();
