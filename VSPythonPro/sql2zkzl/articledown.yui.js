﻿(function () {
    function showdown(dinfo) {
        checkUserLogin(showDownAfterLogin, [dinfo]);
        //showDownAfterLogin();
    }

    function showzipdown(dinfo) {
        return checkUserLogin(showZipDownAfterLogin, [dinfo]);
        //return showZipDownAfterLogin();
    }

    function closedown() {
        $("#downloadinfo div:eq(1)").remove();
        $(".layer-bg").hide();
        $("#downloadlayer").hide();
    }

    function rebacklogin() {
        closedown();
        var result = true;
        return result;
    }

    function showDownAfterLogin(dinfo) {
        $("#downloadinfo div:eq(1)").remove();
        $(".layer-bg").show();
        $("#downloadlayer").setWindowCenter().show();
        getdowninfo(dinfo);
    }

    function getdowninfo(dinfo) {
        $("#downloadinfo").html('');
        $.ajax({
            dataType: "html",
            url: "/article/download.aspx",
            data: { info: dinfo, ts: new Date().getTime() },
            beforeSend: function () {
                $("#downloadinfo").append('<div id="PretreatmentInfo" class="layer-cnt download-detail" ><p><img src="/images/loading.gif" width="16" height="16"> 正在载入数据...</p></div>');
            }
        }).done(function (json) {
            $('#PretreatmentInfo').hide();
            $("#downloadinfo").append(json);
        });
    }

    function showZipDownAfterLogin(dinfo) {
        $.ajax({
            dataType: "json",
            async: false,
            url: "/article/zipdownload.aspx",
            data: { info: dinfo, ts: new Date().getTime() }
        }).done(function (json) {
            if (json.path) {
                $("#hidepdfpath").val(json.path);
            }
        });
    }

    $(function () {
        window.showdown = showdown;
        window.closedown = closedown;
        window.rebacklogin = rebacklogin;
        window.showzipdown = showzipdown;
    });

})();