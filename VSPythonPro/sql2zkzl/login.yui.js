﻿//检查用户是否登录，参数是登录成功后的回调函数
//如果没登录返回false并弹出登录界面
var callBackFunction = "";
var callParams = [];
function checkUserLogin(callback, params) {
    var logined = true;
    $.ajax(
    {
        url: '/ajax/userlogincheck.ashx?' + Math.random(),
        dataType: 'json',
        async: false,
        success: function (json) { logined = json.isLogined; },
        error: function () { logined = false; }
    });    
    if (!logined && $('#divLoginBox').is(":hidden")) {
        var pathname = window.location.pathname.toLowerCase();
        if (pathname != "/index.aspx" && pathname != "/refreshcache.aspx") {
            window.location.href = "/index.aspx";
            return;
        }

        $(".layer-bg").show();
        $("#divLoginBox").find("#txtLPassword").val('');
        $("#divLoginBox").setWindowCenter().show();
        callBackFunction = (callback || loginAfter);
        callParams = (params || []);
    } else {
        if (callback) {
            var p = "";
            if (params) {
                for (var i = 0; i < params.length; i++) {
                    var temp = params[i];
                    if (isNaN(params[i])) {
                        temp = "'" + params[i] + "'";
                    }
                    if (p == "") {
                        p = temp;
                    } else {
                        p += "," + temp;
                    }
                }
            }
            eval("callback(" + p + ")");
        }
    }
    return logined;
}

function normalLogin() {
    var username = $("#txtLUsername").val().trim();
    var passwpord = $("#txtLPassword").val().trim();
    if (username == "") {
        $("#divLoginError").html("请输入用户名！");
        return false;
    }
    if (passwpord == "") {
        $("#divLoginError").html("请输入密码！");
        return false;
    }

    $.getJSON("/user/login.aspx?" + Math.random(), { operate: "NormalLogin", username: username, password: passwpord }, function (json) {
        if (json.error) {
            $("#divLoginError").html(json.error);
            return false;
        }
        $("#divLoginError").html("");
        $("#divLoginBox").hide();
        $("#liTraceData").hide();
        $("#liIndexData").hide();
        $("#liByData").hide();
        $("#liTraceData2").hide();
        $("#liIndexData2").hide();
        $("#liByData2").hide();
        $(".layer-bg").hide();
        
        if (json.TraceRight) {
            $("#liTraceData").show().find("a").attr("href",json.TraceUrl);
        } else {
            $("#liTraceData2").show();
        }
        if (json.IndexRight) {
            $("#liIndexData").show().find("a").attr("href", json.IndexUrl);
        } else {
            $("#liIndexData2").show();
        }
        if (json.ByRight) {
            $("#liByData").show().find("a").attr("href", json.ByUrl);
        } else {
            $("#liByData2").show();
        }
        
        userpermissions();
        if (callBackFunction) {
            var p = "";
            if (callParams) {
                for (var i = 0; i < callParams.length; i++) {
                    var temp = callParams[i];
                    if (isNaN(callParams[i])) {
                        temp = "'" + callParams[i] + "'";
                    }
                    if (p == "") {
                        p = temp;
                    } else {
                        p += "," + temp;
                    }
                }
            }

            if (callBackFunction == loginAfter) {
                loginAfter();
            } else {
                loginAfter();
                eval("callBackFunction(" + p + ")");
            }
        }

        return true;
    });

    // 加载userlist
    $.get("/ajax/loaduserlist.ashx?" + Math.random(), function (response) {
        $(".header-organ-nav").remove();
        $("#header > .top-nav > .w-width > .left-nav").append(response);
    });
}

function ipLogin() {
    $.getJSON("/user/login.aspx?" + Math.random(), { operate: "IPLogin" }, function (json) {
        if (json.error) {
            $("#divLoginError").html(json.error);
            return false;
        }
        $("#divLoginError").html("");
        $("#divLoginBox").hide();
        $("#liTraceData").hide();
        $("#liIndexData").hide();
        $("#liByData").hide();
        $("#liTraceData2").hide();
        $("#liIndexData2").hide();
        $("#liByData2").hide();
        $(".layer-bg").hide();

        if (json.TraceRight) {
            $("#liTraceData").show().find("a").attr("href", json.TraceUrl);
        } else {
            $("#liTraceData2").show();
        }
        if (json.IndexRight) {
            $("#liIndexData").show().find("a").attr("href", json.IndexUrl);
        } else {
            $("#liIndexData2").show();
        }
        if (json.ByRight) {
            $("#liByData").show().find("a").attr("href", json.ByUrl);
        } else {
            $("#liByData2").show();
        }

        // 加载userlist
        $.get("/ajax/loaduserlist.ashx?" + Math.random(), function (response) {
            $(".header-organ-nav").remove();
            $("#header > .top-nav > .w-width > .left-nav").append(response);
        });

        userpermissions();
        if (callBackFunction) {
            var p = "";
            if (callParams) {
                for (var i = 0; i < callParams.length; i++) {
                    var temp = callParams[i];
                    if (isNaN(callParams[i])) {
                        temp = "'" + callParams[i] + "'";
                    }
                    if (p == "") {
                        p = temp;
                    } else {
                        p += "," + temp;
                    }
                }
            }
            if (callBackFunction == loginAfter) {
                loginAfter();
            } else {
                loginAfter();
                eval("callBackFunction(" + p + ")");
            }
        }
        return true;
    });
}

function loginAfter() {
    $("#loginLoading").html("正在加载，请稍候...").load("/user/loginheader.aspx?" + Math.random());
}

//权限刷新
function userpermissions() {
    if ($("#hifOutObjectType").length == 1 && $("#hifOutObjectType").val() == "Articles") {
        var load = "<img src='/images/zoomloader.gif' alt='正在加载，请稍候...' />";
        $(".permissinshow").html(load);
        var ids = "", ls = GetQueryString("ls");
        $(".search-result input:checkbox").each(function () {
            ids += $(this).val() + ",";
        });
        if (ids) {
           
        }
    }
}

$(document).ready(function () {
    $("#divLoginBox").find(":input").keydown(function (event) {
        var e = $.event.fix(event);
        if (e.keyCode == 13) {
            return normalLogin();
        }
    });

    $("#btnLoginClose").click(function () {
        $("#divLoginError").html("");
        $("#divLoginBox").find("#txtLPassword").val('');
        $("#divLoginBox").hide();
        $(".layer-bg").hide();
    });
});