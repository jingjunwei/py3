#coding=utf-8 
#!/usr/bin/env python
#-------------------------------------------------------------------------------
# Name: pymssqlTest.py
# Purpose: 测试 pymssql库，该库到这里下载：http://www.lfd.uci.edu/~gohlke/pythonlibs/#pymssql
#
# Author: scott
#
# Created: 04/02/2012
#-------------------------------------------------------------------------------
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

import pymssql
import time
import solr  
s = solr.SolrConnection('http://192.168.10.53:8080/solr/core_zk')

ISOTIMEFORMAT = '%Y-%m-%d %X'
class MSSQL:
    """
    对pymssql的简单封装
    pymssql库，该库到这里下载：http://www.lfd.uci.edu/~gohlke/pythonlibs/#pymssql
    使用该库时，需要在Sql Server Configuration Manager里面将TCP/IP协议开启

    用法：

    """

    def __init__(self,host,user,pwd,db):
        self.host = host
        self.user = user
        self.pwd = pwd
        self.db = db

    def __GetConnect(self):
        """
        得到连接信息
        返回: conn.cursor()
        """
        if not self.db:
            raise(NameError,"没有设置数据库信息")
        self.conn = pymssql.connect(host=self.host,user=self.user,password=self.pwd,database=self.db,charset="utf8")
        cur = self.conn.cursor()
        if not cur:
            raise(NameError,"连接数据库失败")
        else:
            return cur

    def ExecQuery(self,sql):
        """
        执行查询语句
        返回的是一个包含tuple的list，list的元素是记录行，tuple的元素是每行记录的字段

        调用示例：
                ms = MSSQL(host="localhost",user="sa",pwd="123456",db="PythonWeiboStatistics")
                resList = ms.ExecQuery("SELECT id,NickName FROM WeiBoUser")
                for (id,NickName) in resList:
                    print str(id),NickName
        """
        cur = self.__GetConnect()
        cur.execute(sql)
        resList = cur.fetchall()

        #查询完毕后必须关闭连接
        self.conn.close()
        return resList

    def ExecNonQuery(self,sql):
        """
        执行非查询语句

        调用示例：
            cur = self.__GetConnect()
            cur.execute(sql)
            self.conn.commit()
            self.conn.close()
        """
        cur = self.__GetConnect()
        cur.execute(sql)
        self.conn.commit()
        self.conn.close()

def main():
## ms =
## MSSQL(host="localhost",user="sa",pwd="123456",db="PythonWeiboStatistics")
## #返回的是一个包含tuple的list，list的元素是记录行，tuple的元素是每行记录的字段
## ms.ExecNonQuery("insert into WeiBoUser values('2','3')")
    ms = MSSQL(host="192.168.10.53",user="sa",pwd="vipproject",db="newMetrail")

    file = open("fenleidata.txt")
 
    while 1:
        lines = file.readlines(100000)
        if not lines:
            break
        fenlei = u''
        fenleiName = u''
        subfenlei = u''
        subfenleiName = u''
        num = 0
        idx = u''
        fenleiname = u''
        fenlei = u''
        keywords = u''

        for line in lines:
            print line
            idx = line.index(' ')
            fid = line[0 : idx]
            fname = line[idx+1 : len(line)]
            if line.find('.') < 0:
                fenleiname = fname
                fenlei = fid
                fenlei = fenlei.strip().decode('GBK')
                fenleiname = fenleiname.strip().decode('GBK')
            elif line.find('.') >= 0:
                subfenleiname = fname
                subfenlei = fid

                q = 'fenlei:"' + subfenlei + '"'
                result = s.query(q, rows=2000, fields='KeyWord_C')
                if result.numFound > 0:
                    keywords = ''
                    for re in result:
                        #keyword:噪声;粗晶材料;超声检测;信号处理;小波变换
                        try:
                            kwc = re['KeyWord_C'].decode('utf-8').strip()
                            if kwc.find(';') != -1:
                                idx = kwc.index(';')
                                kwc = kwc[0:idx]
                                if keywords.find(kwc) == -1:
                                    keywords += (kwc + ';')
                        except Exception,ex:
                            continue
                        counttemp = keywords.count(';')
                        if counttemp >= 60:
                            break;
                    #print keywords
                    try:
                        subfenlei = subfenlei.strip().decode('GBK')
                        subfenleiname = subfenleiname.strip().decode('GBK')
                        keywords = keywords.strip().replace("'",'"');
                        sql = u"INSERT INTO main0_zkfenleikeyword VALUES ('%s','%s','%s','%s','%s')" % (fenlei,fenleiname,subfenlei,subfenleiname,keywords)
                        #print sql
                        #raw_input()
                        ms.ExecNonQuery(sql)
                    except Exception,ex:
                        print Exception,":",ex
                        print 'currentLine: %s' % line
                        raw_input()
                        
                    
    file.close()
    

if __name__ == '__main__':
    print time.strftime(ISOTIMEFORMAT, time.localtime()) + u"开始"
    main()
    print time.strftime(ISOTIMEFORMAT, time.localtime()) + u"结束完成"
    raw_input(u"回车退出。")
