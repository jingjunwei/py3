﻿(function ($) {
    $(function () {
        //下拉框显示
        $("#dropSort,#dropListStyle").easySelectBox({ speed: 300 });
        //点击排序下拉框，执行检索
        $("#dropSort").change(function () {
            var url = g_addQueryString(/so=\w+/i, "so=" + encodeURIComponent($(this).val()));
            url = g_addQueryString(/page=\d*/i, "page=1", url);
            if (url.indexOf("#search-result-list") == -1) {
                url += "#search-result-list";
            }
            location.href = url;
        });

        //点击列表样式展示
        $("#dropListStyle").change(function () {
            var url = g_addQueryString(/ls=\d*/i, "ls=" + encodeURIComponent($(this).val()));
            if (url.indexOf("#search-result-list") == -1) {
                url += "#search-result-list";
            }
            location.href = url;
        });

        $('#backtop').click(function () {
            $('html,body').animate({ scrollTop: 0 }, 200);
        });

        $("#selLanguage").easySelectBox({ speed: 300 }).next().on("click", "div ul a", function (event) {
            var lanType = $(this).attr("rel");
            if (lanType == "english") {
                window.open("http://en.cqvip.com");
            } else if (lanType == "japan") {
                window.open("http://jp.cqvip.com");
            }
        });

        $.get("/ajax/stateview.ashx", {
            "info": $("#hifBaseLog").val(),
            "_t": +new Date
        });
    });
})(jQuery);

$(document).ready(function () {
    SetBodyWidthByWindow();
});
$(window).resize(function () {
    ResizeBodyWidthByWindow();
});
function SetBodyWidthByWindow() {
    var windowWidth = $(window).width();
    if (windowWidth >= 1500) {
        $(document.body).removeClass().addClass("s-1200");
    } else {
        $(document.body).removeClass().addClass("s-980");
    }
}
function ResizeBodyWidthByWindow() {
    // 什么都不做
    //var windowWidth = $(window).width();
    //if (windowWidth >= 1500) {
    //    $(document.body).removeClass().addClass("w-1200");
    //    if ($(".search-year-graph").length > 0) {
    //        $(".search-year-graph").width("926");
    //    }
    //} else {
    //    $(document.body).removeClass().addClass("w-980");
    //    if ($(".search-year-graph").length > 0) {
    //        $(".search-year-graph").width("710");
    //    }
    //}
}

function formatNum(num) {
    if (!/^(\+|-)?\d+(\.\d+)?$/.test(num)) {
        alert("wrong!");
        return num;
    }
    var re = /(\d)(\d{3})(,|\.|$)/;
    num += "";
    while (re.test(num))
        num = num.replace(re, "$1,$2$3");
    return num;
}

function GetBrowserParam(paraList) {
    var paras = "";
    var keys = paraList.split(/\[\*]/g);
    for (var i in keys) {
        if (keys[i] != "") {
            var inputValue = keys[i].split("=")[1];
            if (inputValue != "") {
                if (paras != "") {
                    paras += "," + inputValue;
                } else {
                    paras = inputValue;
                }
            }
        }
    }
    return paras;
}

//处理用户访问日志表
$(function () {
    $(document).on("click", ".btnTitle", function () {
        //var keyword = $("#hidTokens").val().split(",").join("");//高亮隐藏域
        var request = g_getUrlParms();
        var keyword = request["key"], rf = request["rf"], cf = request["cf"];
        var searchType = getCookie("MainSearch");
        if (keyword) {
            var s = GetBrowserParam(keyword);
            if (s)
                keyword = s.replace(/(0x005b)/g, "[").replace(/(0x005d)/g, "]").replace(/(0x005a)/g, "=").replace(/(‘)/g, "'").replace(/(’)/g, "'").replace(/(\“)/g, "\"").replace(/(\”)/g, "\"").replace(/\[|\]/g, "");
        }
        if (keyword != "" && searchType == "true" && (!rf) && (!cf)) {
            var articleid = "";
            if ($(this).attr("articleid")) {
                articleid = $(this).attr("articleid");
            }
            var myDate = new Date();
            var tableName = $("#hfldObjectType").val();
            var pageSize = $("#hidPageSize").val();
            var pageIndex = $("#hidPageIndex").val();
            var recordCount = $("#hidRecordCount").val();
            var sort = $("#dropSort").find("option:selected").attr("sortfield");
            var position = $(this).attr("index");
            var rules = $("#hfldSearchRules").val();
            var filterrules = $("#hfldSearchFilterRules").val();

            //alert("行为分析参数："+keyword + "+" + tableName + "+" + pageSize + "+" + pageIndex + "+" + recordCount + "+" + sort + "+" + position);
            $.ajax({
                type: "post",
                url: "/ajax/BehaviorAnalysis.ashx",
                data: { type: "click", articleid: articleid, keyword: keyword, tableName: tableName, pageSize: pageSize, pageIndex: pageIndex, recordCount: recordCount, sort: sort, position: position, rules: rules, filterrules: filterrules, time: myDate.getMilliseconds() },
                success: function () {
                }
            });
        }
    });

    $(document).on("click", ".btnIsView", function () {
        $.get("/ajax/stateview.ashx", {
            "info": $("#hifBaseLog").val(),
            "_t": +new Date
        });
    });
});

// 记录用户的搜索记录
function searchHistoryRecord(url) {
    // $.get("/ajax/SearchHistoryRecords.ashx?op=create&url=" + url);
    //var random=Math.ceil(Math.random()*100);
    //$.get("/ajax/SearchHistoryRecords.ashx?op=create&url=" + url + "&random=" + random);
    $.ajax({
        type: "post",
        async: false,
        url: "/ajax/SearchHistoryRecords.ashx",
        data: { op: "create", url: url },
        success: function (data) {
            //alert("ss");
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert(XMLHttpRequest.status); alert(textStatus); alert(errorThrown);
        }
    })
}

// 获取 url 参数
function getQueryString(name) {
    try {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = decodeURIComponent(window.location.search.substr(1)).match(reg);
        if (r != null) return unescape(r[2]); return "";
    }
    catch (e) {
        var query = unescape(window.location.search);
        if (query != "")
            query = query.substr(1);
        var parms = query.split("&");
        for (var i = 0; i < parms.length; i++) {
            var items = parms[0].split("=");
            if (items.length > 1)
                if (items[0] == name)
                    return unescape(items[1]);
        }
        return "";
    }
}