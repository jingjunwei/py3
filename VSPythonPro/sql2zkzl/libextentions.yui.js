﻿/*
对jquery类的扩展
功能说明：
//扩展事件，以下方法封装了IE和FF之间的差异，使其保持与FF的标准一致
formatEvent:主要是格式化IE中的事件对象event，
getEvent：在事件处理函数中取得事件对象
addEventHandler:对DOM元素添加事件处理函数
removeEventHandler:移出DOM元素中的事件处理函数
*/
/*
添加几个类实用类
StringBuilder
ArrayList
Hashtable
*/
/*
扩展String类
添加方法
trimToMaxLength
format
htmlEncode
htmlDecode
*/
(function () {
    //扩展事件
    jQuery.fn.extend({
        formatEvent: function (oEvent) {
            if (! -[1, ]) {
                oEvent.charCode = (oEvent.type == "keypress") ? oEvent.keyCode : 0;
                oEvent.eventPhase = 2;
                oEvent.isChar = (oEvent.charCode > 0);
                oEvent.pageX = oEvent.clientX + document.body.scrollLeft;
                oEvent.pageY = oEvent.clientY + document.body.scrollTop;
                oEvent.preventDefault = function () {
                    this.returnValue = false;
                };
                if (oEvent.type == "mouseout") {
                    oEvent.relatedTarget = oEvent.toElement;
                } else if (oEvent.type == "mouseover") {
                    oEvent.relatedTarget = oEvent.fromElement;
                }
                oEvent.stopPropagation = function () {
                    this.cancelBubble = true;
                };
                oEvent.target = oEvent.srcElement;
                oEvent.time = (new Date).getTime();
            }
            return oEvent;
        },
        getEvent: function () {
            if (window.event) {
                return this.formatEvent(window.event);
            } else {
                return this.getEvent.caller.arguments[0];
            }
        },
        addEventHandler: function (oTarget, sEventType, fnHandler) {
            if (oTarget.addEventListener) {
                oTarget.addEventListener(sEventType, fnHandler, false);
            } else if (oTarget.attachEvent) {
                oTarget.attachEvent("on" + sEventType, fnHandler);
            } else {
                oTarget["on" + sEventType] = fnHandler;
            }
        },
        removeEventHandler: function (oTarget, sEventType, fnHandler) {
            if (oTarget.removeEventListener) {
                oTarget.removeEventListener(sEventType, fnHandler, false);
            } else if (oTarget.detachEvent) {
                oTarget.detachEvent("on" + sEventType, fnHandler);
            } else {
                oTarget["on" + sEventType] = null;
            }
        },
        setWindowCenter: function () {
            var _scrollHeight = $(document).scrollTop(), //获取当前窗口距离页面顶部高度 
                _windowHeight = $(window).height(), //获取当前窗口高度 
                _windowWidth = $(window).width(), //获取当前窗口宽度 
                _popupHeight = $(this).height(), //获取弹出层高度 
                _popupWidth = $(this).width(); //获取弹出层宽度 
            var _posiTop = (_windowHeight - _popupHeight) / 2 + _scrollHeight;
            var _posiLeft = (_windowWidth - 800) / 2;
            if($(window).width() < 800) {$(this).css({ "left": 0 + "px", "top": _posiTop + "px" });} //设置position 
            else {$(this).css({ "left": _posiLeft + "px", "top": _posiTop + "px" });} //设置position 
            return $(this);
        }
    });
    //将顔色的rgb转换成16进制
    $.fn.getHexBackgroundColor = function (color) {
        var rgb = $(this).css(color);
        //ie处理
        if (!! -[1, ]) {
            rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
            function hex(x) { return ("0" + parseInt(x).toString(16)).slice(-2); }
            rgb = "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
        }
        return rgb;
    };
})();

//增加实用类
(function () {
    //
    if (window.ExtentionLib) throw new Error("The namespace 'ExtentionLib' has exists!");
    ExtentionLib = {};
    // StringBuffer.  
    function StringBuilder() {
        this.array = new Array();
    }
    StringBuilder.prototype.append = function (content) {
        this.array[this.array.length] = content;
    };
    StringBuilder.prototype.toString = function (separator) {
        return this.array.join(separator ? separator : "");
    }; // ArrayList.  
    function ArrayList() {
        this.index = -1;
        this.array = new Array();
    }
    ArrayList.prototype.add = function (obj) {
        this.index = this.index + 1;
        this.array[this.index] = obj;
    };
    ArrayList.prototype.get = function (index) { return this.array[index]; };
    ArrayList.prototype.size = function () { return this.index + 1; };
    ArrayList.prototype.remove = function (index) {
        var j = 0;
        var arrThis = this.array;
        var arrTemp = new Array();
        for (w = 0; w < arrThis.length; w++) {
            if (eval(index) != eval(w)) {
                arrTemp[j] = arrThis[w];
                j++;
            }
        }
        this.array = arrTemp;
        this.index = eval(j - 1);
    }; // HashTable Object
    function Hashtable() {
        this._data = new Object();
        this.__typeName = 'Hash';
    }
    Hashtable.prototype.add = function (key, value) {
        if (!value || !key) return;  //如果键或值为空,则返回     
        this._data[key] = value;    //用新的值覆盖旧的值,或赋予新的键-值对.
    };
    Hashtable.prototype.addRange = function (hashtable) {
        for (var key in hashtable._data) {
            var value = hashtable._data[key];
            if (!!value) {
                this.add(key, value);
            }
        }
    };
    Hashtable.prototype.remove = function (key) {
        if (!key) return null;       //如果键为空,则返回
        var value = this._data[key];
        delete this._data[key];     //删除此hash中的此键
        return value;               //返回找到的值
    };
    Hashtable.prototype.removeAt = function (index) {
        if (isNaN(index)) return null;
        var i = 0;
        for (var key in this._data) {
            if (i == index) {
                return this.remove(key);
            }
            i++;
        }
        return null;
    };
    Hashtable.prototype.removeRange = function (startIndex, endIndex) {
        if (isNaN(startIndex) || isNaN(endIndex)) return null;
        var i = 0;
        var h = new Hash();
        for (var key in this._data) {
            if (i >= startIndex && i <= endIndex) {
                h.add(key, this.remove(key));
            }
            i++;
        }
        return h;
    };
    Hashtable.prototype.count = function () {
        var i = 0;
        for (var key in this._data) i++;
        return i;
    };
    Hashtable.prototype.items = function (key) {
        if (!key) return null;       //如果键为空,则返回
        var value = this._data[key];
        if (!!value) {               //如果有值,则返回
            return this._data[key];
        } else {                      //没有值的话,把"动态"增加的属性给删除.
            delete this._data[key];
            return null;
        }
    };
    Hashtable.prototype.forEach = function (method, instance) {
        var i = 0;
        for (var key in this._data) {
            var value = this._data[key];
            if (typeof (value) !== 'undefined') {
                method.call(instance, value, key, i, this);
                i++;
            }
        }
    };
    Hashtable.prototype.getKeys = function () {
        var arr = new Array();
        for (var key in this._data) {
            arr.push(key);
        }
        return arr;
    };
    Hashtable.prototype.getValues = function () {
        var arr = new Array();
        for (var key in this._data) {
            arr.push(this._data[key]);
        }
        return arr;
    };
    Hashtable.prototype.clear = function () {
        for (var k in this._data)
        { delete this._data[k]; }
        return true;
    };
    Hashtable.prototype.contains = function (key) {
        return typeof (this._data[key]) != "undefined";
    };
    ExtentionLib.StringBuilder = StringBuilder;
    ExtentionLib.ArrayList = ArrayList;
    ExtentionLib.Hashtable = Hashtable;
})();
/*为String类添加方法*/
(function () {
    String.prototype.trimToMaxLength = function (maxLength, suffix) {
        if (suffix == null)
            return (this == null || this.length <= maxLength) ? this : this.substring(0, maxLength + 1);
        else
            return (this == null || this.length <= maxLength) ? this : this.substring(0, maxLength + 1) + suffix;
    };
    String.prototype.format = function () {
        var tt = this;
        for (var n = 0; n < arguments.length; n++) {
            var reg;
            reg = new RegExp("\\{" + n + "\\}", "g");
            tt = tt.replace(reg, arguments[n]);
        }
        return tt;
    };
    String.prototype.htmlEncode = function () {
        var str = this.toString();
        var s = "";
        if (str.length == 0) return "";
        s = str.replace(/&/g, "&amp;");
        s = s.replace(/</g, "&lt;");
        s = s.replace(/>/g, "&gt;");
        s = s.replace(/\'/g, "&#39;");
        s = s.replace(/\"/g, "&quot;");
        return s;
    };
    String.prototype.htmlDecode = function () {
        var str = this.toString();
        var s = "";
        if (str.length == 0) return "";
        s = str.replace(/&amp;/g, "&");
        s = s.replace(/&lt;/g, "<");
        s = s.replace(/&gt;/g, ">");
        s = s.replace(/&#39;/g, "\'");
        s = s.replace(/&quot;/g, "\"");
        return s;
    };
    String.prototype.trim = function () {
        return this.replace(/(^\s*)|(\s*$)/g, "");
    };
    String.prototype.lTrim = function () {
        return this.replace(/(^\s*)/g, "");
    };
    String.prototype.rTrim = function () {
        return this.replace(/(\s*$)/g, "");
    };

    String.prototype.endWith = function (str) {
        if (str == "" || str == null) {
            return false;
        }
        if (this.length < str.length) {
            return false;
        } else if (this == str) {
            return true;
        } else if (this.substring(this.length - str.length) == str) {
            return true;
        }
        return false;
    };

    String.prototype.startWith = function(str) {
        if (str == "" || str == null) {
            return false;
        }
        if (this.length < str.length) {
            return false;
        } else if (this == str) {
            return true;
        } else if (this.substr(0, str.length) == str) {
            return true;
        }
        return false;
    };
})();
/**
获取查询字符串的方法：g_getUrlParms
添加查询字符串：g_addQueryString
移除查询字符串：g_removeQueryString
*/
(function () {
    /*
   获取查询字符串的方法，对值全部进行解码
   */
    function getUrlParms() {
        var args = new Object();
        var query = location.search.substring(1);//获取查询串  
        var pairs = query.split("&");//在逗号处断开  
        for (var i = 0; i < pairs.length; i++) {
            var pos = pairs[i].indexOf('=');//查找name=value  
            if (pos == -1) continue;//如果没有找到就跳过  
            var argname = pairs[i].substring(0, pos);//提取name  
            var value = pairs[i].substring(pos + 1);//提取value  
            //args[argname] = unescape(value);//存为属性 
            try{
                args[argname] = decodeURIComponent(value);//存为属性  
            }
            catch (e) {
                args[argname] = unescape(value);//存为属性 
            }
        }
        return args;
    }    
    /*
    添加查询字符串
    参数url可选的，参数regex和nameValue是必需的
    regex:可验证的正则表达式,nameValue:需要添加的名值对参数
    */
    function addQueryString(regex, nameValue, path/*如果此参数为空则取location.href*/) {
        var url = path ? path : location.href;
        //如果匹配到模式，直接替换
        if (regex.test(url)) {
            var temp = url.replace(regex, nameValue);
            return temp;
        }
        else {
            var index = url.indexOf('?');
            //没有查询字符串，则直接添加
            if (index == -1)
                return url + "?" + nameValue;
                //可能有查询字符串
            else {
                //提取查询串
                var queryString = url.substring(index + 1); //不包括?
                //提取主机路径 
                var hostname = url.substring(0, index); //不包括?
                //?号后面有内容查询字符中内容
                if (queryString) {
                    //有查询字符串，再判断是否有锚点
                    var hashIndex = queryString.indexOf('#');
                    //无锚点直接拼接返回
                    if (hashIndex == -1)
                        return url + "&" + nameValue;
                    else
                        return hostname + "?" + queryString.substring(0, hashIndex) + "&" + nameValue + queryString.substring(hashIndex);

                }
                else
                    return url + nameValue;




            }

        }
    }
    /**
    移出与正则匹配的查询字符串，用replaceValue值替换
    */
    function removeQueryString(regex, replaceValue, path/*可选参数*/) {
        var url = path ? path : location.href;
        if (regex.test(url)) {
            var temp = url.replace(regex, replaceValue);
            return temp;
        }
        return url;
    }
    window.g_getUrlParms = getUrlParms;
    window.g_addQueryString = addQueryString;
    window.g_removeQueryString = removeQueryString;

    //去掉字符串的前后空格
    String.prototype.trim = function () {
        return this.replace(/^\s+/, "").replace(/\s+$/, "");
    }
})();
