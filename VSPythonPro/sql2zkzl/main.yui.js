﻿


//获得跳转链接
function geturlMain(type, str, rf, index) {
    var url = "", key = "";
    if (str) {
        str = encodeURIComponent(str);
        key = "?key=" + str;
    }
    if (rf) {
        rf = encodeURIComponent(rf);
        if (key.indexOf("?") != -1) {
            key += "&rf=" + rf;
        } else {
            key += "?rf=" + rf;
        }
    }
    if (index) {
        if (key.indexOf("?") != -1) {
            key += "&uf=" + index;
        } else {
            key += "?uf=" + index;
        }
    }
    if (key.length > 2000) {
        alert("检索表达式超长");
        return false;
    }
    switch (type) {
        case "writer":
            url = "/writer/writersearch.aspx";
            break; //人物
        case "organ":
            url = "/organ/organsearch.aspx";
            break; //机构
        case "subject":
            url = "/subject/subjectsearch.aspx";
            break; //主题
        case "journal":
            url = "/journal/journalsearch.aspx";
            break; //期刊
        case "fund":
            url = "/fund/fundsearch.aspx";
            break; //资助
        case "domain":
            url = "/domain/domainsearch.aspx";
            break; //学科
        case "area":
            url = "/area/areasearch.aspx";
            break; //地区
        case "articles":
            url = "/zk/search.aspx";
            break; //文献
        default:
            url = "/zk/search.aspx";
            break;
    }
    searchHistoryRecord(url + key); // 记录查询历史
    location.href = url + key;
}

/**查询处理函数*/
function searchMain() {
    var keysValue = $("#txtMainSearchType").val().replace(/(\;|\,|\<|\>)/g, "").replace(/(\))/g, "）").replace(/(\()/g, "（").replace(/(\[)/g, "0x005b").replace(/(\])/g, "0x005d").replace(/(\')/g, "‘").replace(/(\')/g, "’").replace(/(\")/g, "“").replace(/(\")/g, "”").replace(/(\=)/g, "0x005a").replace(/( AND )/g, "[*]").replace(/( OR )/g, "[+]").replace(/( NOT )/g, "[-]").replace(/( \* )/g, "[×]").replace(/( \+ )/g, "[＋]").replace(/( \- )/g, "[－]");
    if (keysValue && !$("#txtMainSearchType").hasClass('watermark')) {
        if (keysValue.length > 2000) {
            alert("检索表达式超长");
            return false;
        }
        if (/^\"+$/.test(keysValue)) { // 全是双引号的情况下，将其转换为全角符号
            var temp = "";
            for (var i = 0; i < keysValue.length; i++)
                temp += "＂";
            keysValue = temp;
        }
        var key = "U";
        var inputValues = [];
        var str1 = "";
        if (keysValue && keysValue != "") {
            inputValues = split(keysValue, /(\[[\*|\+|\-|\×|\＋|\－]\])/);
            if (inputValues.length > 1) {
                for (var put = 0; put < inputValues.length; put++) {
                    if (put % 2 == 0) {
                        str1 += key + "=" + inputValues[put];
                    } else {
                        str1 += inputValues[put];
                    }
                }
            } else {
                str1 += key + "=" + inputValues[0];
            }
        }

        var searchType = $("#stype").val();
        //选中文献
        if (searchType == "articles") {
            var subType = "0";
            var $typeOption = $("#searchTypeOptions > i.current");
            if ($typeOption.length == 1) {
                subType = $typeOption.attr("opt_type");
            }
            if (subType != "0") {
                str1 += "[*]LT=" + subType;
                //$.cookie("articlestype", subType);//设置cookie
            }
        }
        geturlMain(searchType, str1);
    }
    else {
        alert("请输入检索条件");
    }
}

//回车检索
function search_enterKeydown(event) {
    var e = $.event.fix(event);
    if (e.keyCode === 13) {
        searchMain();
    }
}

//设置前面搜索文献的种类样式
function setArticlestype()
{
    var articlesTypeValue = $.cookie('articlestype');
    if (articlesTypeValue != "" && articlesTypeValue!=null)
    {
        $("#searchTypeOptions>i").each(function () {
            if ($(this).attr("opt_type") == articlesTypeValue) {
                $(this).addClass("current");
            }
            else {
                $(this).removeClass("current");
            }
        });
    }
}

$(function () {

    //获取搜索参数值
    //var searchKeys = GetQueryString("key");
    var request = g_getUrlParms();
    var searchKeys = request["key"];
    if (searchKeys) {
        var uvalue = searchKeys.match(/(\[[|*\+\-×＋－]\])?U=[^\[]*/gi);
        if (uvalue && uvalue.length > 0) {
            // $("#txtMainSearchType").removeClass("watermark"); //移除检索框class
            //$("#txtMainSearchType").css("color", "black"); //移除检索框文字加入颜色
            var searchPut = "";
            for (var urlValue = 0; urlValue < uvalue.length; urlValue++) {
                searchPut += uvalue[urlValue].replace("U=", "").replace("[*]", " AND ").replace("[+]", " OR ").replace("[-]", " NOT ").replace("[×]", " * ").replace("[＋]", " + ").replace("[－]", " - ");
            }
            var searchvalue = searchPut.replace(/(0x005b)/g, "[").replace(/(0x005d)/g, "]").replace(/(0x005a)/g, "=").replace(/(‘)/g, "'").replace(/(’)/g, "'").replace(/(\“)/g, "\"").replace(/(\”)/g, "\"");
            $("#hidSearchKeysHidden").val(searchvalue); //隐藏检索赋值
            $("#txtMainSearchType").val(searchvalue); //检索框赋值
        }
    }
    //处理搜索关键字框获得焦点的情况，移除水印并清空水印值
    $("#txtMainSearchType").focusin(function () {
        $("#txtMainSearchType").removeClass("watermark");
        $("#txtMainSearchType").css("color", "black");
        if ($("#hidSearchKeysHidden").val()) {
            $("#txtMainSearchType").val($("#hidSearchKeysHidden").val());
        } else {
            $("#txtMainSearchType").val("");
        }
    });

    //处理搜索关键字框失去焦点的情况，判断是否该加水印
    $("#txtMainSearchType").focusout(function () {
        if ($(this).val()) {
            $("#hidSearchKeysHidden").val($(this).val());
        } else {
            setSearchTextboxVal();
        }
    });

    function setSearchTextboxVal() {
        var sType = $("#stype").val();
        if (sType.toLowerCase() == "articles") {
            sType = $("#searchTypeOptions>i.current").attr("opt_type").toLowerCase();
            switch (sType) {
                case "1":
                    sType = "journal";
                    break;
                case "2":
                    sType = "degree";
                    break;
                case "3":
                    sType = "conference";
                    break;
                case "4":
                    sType = "patent";
                    break;
                case "5":
                    sType = "standard";
                    break;
                case "6":
                    sType = "achievement";
                    break;
                case "7":
                    sType = "books";
                    break;
                case "8":
                    sType = "product";
                    break;
                case "9":
                    sType = "technicalReport";
                    break;
                case "10":
                    sType = "policy";
                    break;
                default:
                    sType = "all";
                    break;
            }
        }
        getWaterMark(sType);
    }

    //声明水印关键字
    //var searchTypeWordsMap = {
    //    all: "在\"全文献类型\"范围内搜索专业资源，提供2亿专业资源的搜索及分析筛选",
    //    journal: "在\"期刊文章\"范围内搜索专业资源",
    //    conference: "在\"会议论文\"范围内搜索专业资源",
    //    degree: "在\"学位论文\"范围内搜索专业资源",
    //    patent: "在\"专利\"范围内搜索专业资源",
    //    books: "在\"专著\"范围内搜索专业资源",
    //    standard: "在\"标准\"范围内搜索专业资源",
    //    achievement: "在\"科技成果\"范围内搜索专业资源",
    //    product: "在\"产品样本\"范围内搜索专业资源",
    //    technicalReport: "在\"科技报告\"范围内搜索专业资源",
    //    policy: "在\"政策法规\"范围内搜索专业资源",
    //    writer: "输入姓名、主题、机构等描述条件，都可以找到人物",
    //    subject: "输入名称、学科领域等描述条件，都可以找到研究主题",
    //    organ: "输入名称、主题等描述条件，都可以找到研究机构",
    //    fund: "输入名称、主题、机构等描述条件，都可以找到资质项目",
    //    media: "输入名称、主题等描述条件，都可以找到期刊出版物和学术会议目标"
    //};
    var searchTypeWordsMap = {
        all: "",
        journal: "",
        conference: "",
        degree: "",
        patent: "",
        books: "",
        standard: "",
        achievement: "",
        product: "",
        technicalReport: "",
        policy: "",
        writer: "",
        subject: "",
        organ: "",
        fund: "",
        media: ""
    };

    function searchType() {
        $("#searchTypeOptions").on("click.filter", "i", function (event) {
            $(this).addClass('current').siblings().removeClass('current');
            var sType = $(this).attr("opt_type");
            $("#articlestype").val(sType);//设定选中的文章类型
            if ($("#hidSearchKeysHidden").val() == "")
                GetWaterMark(sType);
        });
    }

    $(function () {
        searchType();
        //installTopCss();
        //setArticlestype();
        //处理导航顶部选项卡点击事件
        $(".head_nav").on("click.filter", "li a", function (event) {
            var item = $(this).parent();
            if (item.attr("id") == "navType1") {
                //如果选中的是文献
                $("#ArticleSearch").show();
                $("#ObjectSearch").hide();
            } else {
                //如果选中的是其他5个对象
                $("#ArticleSearch").hide();
                $("#ObjectSearch").show();
            }

            item.children().addClass('current').parent().siblings().children().removeClass('current'); //为当前选中选项卡添加current样式，同时清除其他选项卡current样式

            var idNum = item.attr("id"); //取到当前选项控件的id值
            if (!idNum) { //如果不存在直接返回
                return;
            } else { //存在取id的最后一位编号，用来切换选项卡中的内容
                //$("#sontype").val("");
                var itemNum = idNum.charAt(idNum.length - 1);
                //$('#selType' + itemNum).show().siblings().hide();
                if (itemNum == 1) {
                    $(".w-width>h1").empty();
                    $(".w-width>h1").append("<b class='t-ico'></b>查找文献");
                    $("#stype").val("articles");
                    $("#searchTypeOptions i:first").addClass('current').siblings().removeClass('current');
                    var div = "<div class='searchMain-sel' id='searchTypeOptions'><i class='current' opt_type='0'>全部文献</i><i opt_type='1'>期刊文章</i><i opt_type='2'>学位论文</i>                        <i opt_type='3'>会议论文</i><i opt_type='4'>专利</i><i opt_type='5'>标准</i><i opt_type='7'>专著</i><i opt_type='6'>科技成果</i><i opt_type='8'>产品样本</i>         <i opt_type='9'>科技报告</i></div>"
                    $("#ArticleSearch").empty();
                    $("#ArticleSearch").append(div);
                    $("#searchTypeDiv").addClass("s-article");
                    searchType();
                } else if (itemNum == 2) {
                    $(".w-width>h1").empty();
                    $(".w-width>h1").append("<b class='t-ico rw'></b>查找人物");
                    $("#stype").val(item.attr("opt_type"));
                    $("#searchTypeDiv").removeClass("s-article");
                } else if (itemNum == 3) {
                    $(".w-width>h1").empty();
                    $(".w-width>h1").append("<b class='t-ico zt'></b>查找主题");
                    $("#stype").val(item.attr("opt_type"));
                    $("#searchTypeDiv").removeClass("s-article");
                } else if (itemNum == 4) {
                    $(".w-width>h1").empty();
                    $(".w-width>h1").append("<b class='t-ico jg'></b>查找机构");
                    $("#stype").val(item.attr("opt_type"));
                    $("#searchTypeDiv").removeClass("s-article");
                } else if (itemNum == 5) {
                    $(".w-width>h1").empty();
                    $(".w-width>h1").append("<b class='t-ico zz'></b>查找资助");
                    $("#stype").val(item.attr("opt_type"));
                    $("#searchTypeDiv").removeClass("s-article");
                } else if (itemNum == 6) {
                    $(".w-width>h1").empty();
                    $(".w-width>h1").append("<b class='t-ico cm'></b>查找传媒");
                    $("#stype").val(item.attr("opt_type"));
                    $("#searchTypeDiv").removeClass("s-article");
                }
               
                var searchkey = $("#txtMainSearchType").val();//得到检索条件
                if (searchkey && searchkey != "" && searchkey != "在\"期刊文章\"范围内搜索专业资源"
                    && searchkey != "输入姓名、主题、机构等描述条件，都可以找到人物"
                    && searchkey != "输入名称、学科领域等描述条件，都可以找到研究主题"
                    && searchkey != "输入名称、主题等描述条件，都可以找到研究机构"
                    && searchkey != "输入名称、主题、机构等描述条件，都可以找到资质项目"
                    && searchkey != "输入名称、主题等描述条件，都可以找到期刊出版物和学术会议目标"
                    && searchkey != searchTypeWordsMap["all"]
                    && searchkey != searchTypeWordsMap["conference"]
                    && searchkey != searchTypeWordsMap["degree"]
                    && searchkey != searchTypeWordsMap["patent"]
                    && searchkey != searchTypeWordsMap["books"]
                    && searchkey != searchTypeWordsMap["standard"]
                    && searchkey != searchTypeWordsMap["achievement"]
                    && searchkey != searchTypeWordsMap["product"]
                    && searchkey != searchTypeWordsMap["technicalReport"]
                    && searchkey != searchTypeWordsMap["policy"]
                    ) {
                    var key = "U";
                    var inputValues = [];
                    var str1 = "";
                    inputValues = split(searchkey, /(\[[\*|\+|\-|\×|\＋|\－]\])/);
                    if (inputValues.length > 1) {
                        for (var put = 0; put < inputValues.length; put++) {
                            if (put % 2 == 0) {
                                str1 += key + "=" + inputValues[put];
                            } else {
                                str1 += inputValues[put];
                            }
                        }
                    } else {
                        str1 += key + "=" + inputValues[0];
                    }
                    geturlMain($("#stype").val(), str1);
                }
                else {
                    //var num = $(this).children("i");
                   // if (num.length>0) {
                    var urlkey = request["key"];
                    if (urlkey!=""&& urlkey!=null&& urlkey.length > 0) {
                        //去掉除基本检索外的所有表达式
                        urlkey = urlkey.replace(/(\[\*]|\[\+]|\[-]|\[×]|\[＋]|\[－])?(\()?(QK|ZY|YY|UT|CID)=(\w+)?(-\w+)?([^u4e00-u9fa5]+)?(\))?/g, "");
                        var ivalue = urlkey.substring(0, 3);
                        if (ivalue == "[*]" || ivalue == "[+]" || ivalue == "[-]" || ivalue == "[×]" || ivalue == "[＋]" || ivalue == "[－]") {
                            urlkey = urlkey.substring(3, urlkey.length);
                        }
                        //跳转替换U并去除所有的精确检索
                        urlkey = urlkey.replace(/\w+?=/g, "U=").replace(/(\[|\])/g, "").replace(/(\*|\+|\-|×|＋|－)/g, "[$1]");
                        geturlMain($("#stype").val(), urlkey);
                    }
                   // }
                }

                getWaterMark($("#stype").val());
                
            }
        });
    });



 


    //搜索按钮点击事件
    $("#searchBtn").click(function () {
        searchMain();
    });
});

