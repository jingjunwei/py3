#! /usr/bin/python

import hashlib
import hmac
import time

from base64 import b64encode

class HashSignature:
    MD5 = 'md5'
    SHA1 = 'sha1'
    
    def __init__(self, key, stype = 'sha1'):
        self._key = key
        self._signMethod = getattr(hashlib, stype)
        self.name = stype
    
    def sign(self, data):
        signature = hmac.new(self._key, data, self._signMethod).digest()
        return b64encode(signature)

    def verify(self, rawData, signature):
        try:
            ret = self.sign(rawData)
            if ret != signature:
                return False
        except Exception, e:
            print e
            return False
        return True
    
    def setKey(self, key):
        self._key = key
        
class Sha1Signature(HashSignature):
    def __init__(self, key):
        HashSignature.__init__(self, key, 'sha1')
        

class SecurityKey:
    def __init__(self, accessId, sckey):
        self.accessId = accessId
        self.sckey = sckey
        self._listeners = []
        self._exits = {}
    
    def addKeyChangeListener(self, func):
        if self._exits.has_key(func):
            return
        assert callable(func), '[SecurityKey]: Not callable'
        self._listeners.append(func)
        self._exits[func] = True
    
    def removeKeyChangeListener(self, func):
        if not self._exits.has_key(func):
            return
        self._exits.pop(func)
        self._listeners.remove(func)
    
    def setKeys(self, accessId, sckey):
        self.accessId = accessId
        self.sckey = sckey
        for listener in self._listeners:
            listener(self)
    
    def keys(self):
        return self.accessId, self.sckey

class RpcSigner:
    def __init__(self, securityKey, acExpire = 300):
        self._securityKey = securityKey
        self._acExpire = acExpire
        self._signer = Sha1Signature(securityKey.sckey)
        self._securityKey.addKeyChangeListener(self._onKeyChange)
        
    def _onKeyChange(self, event):
        self._signer.setKey(event.sckey)
    
    def __call__(self, uri, params):
        now = int(time.time())
        exptime = now + self._acExpire
        qs = []
        keys = params.keys()
        keys.sort()
        for k in keys:
            if isinstance(k, unicode):
                k = k.encode('utf-8')
            v = params[k]
            if isinstance(v, unicode):
                v = v.encode('utf-8')
            qs.append('%s=%s' % (k, v))
        qs = '&'.join(qs)
        if qs:
            qs += '&'
        plaintext = qs + 'exptime=%d' % exptime
        plaintext += '&uri=%s' % uri.strip('/')
        print "plaintext:\t\t" + plaintext
        sig = self._signer.sign(plaintext)
        signInfo = {}
        signInfo['Kss-Sig'] = sig
        signInfo['Kss-Sig-Exptime'] = exptime
        signInfo['Kss-Accessid'] = self._securityKey.accessId
        signInfo['Kss-SignType'] = 'sha1'
        
        return signInfo

if __name__ == "__main__":
    DEV_ACCESSID = '22e50a4f751378c2860b09234e4b51fd'
    DEV_SCKEY    = '1405d70f6ec901995d050aa68049f313'
    
    keys = SecurityKey(DEV_ACCESSID, DEV_SCKEY)
    print keys.keys()
    
    signer = RpcSigner(keys)
    print signer
    signInfo = signer('/login', {})
    print "Kss-Sig:\t\t" + signInfo['Kss-Sig']
    
    print 
    signInfo = signer('/login', {'username': 'test1', 'password': '111111', 'client': 'pc-4.0.0.0'})
    print "Kss-Sig:\t\t" + signInfo['Kss-Sig']
    
    
    
