# -*- coding: utf-8 -*-
import sys
import random
import os

reload(sys)
sys.setdefaultencoding("utf-8")


class Player:
    __name = ""
    __gold = 0
    def __init__(self, name):
        self.__name = name
        self.__gold = 1000

    def showPlayerInfo(self):
        print '--------------------------------\n'
        print 'Name: ' + self.__name + '\n'
        print 'Gold: ' + str(self.__gold) + '\n'
        print '--------------------------------\n'
    def getGold(self, gold):
        self.__gold += gold
    def loseGold(self, gold):
        self.__gold -= gold
    def gold(self):
        return self.__gold

class Card:
    __colorGroup = (u"Diamonds", u"Clubs", u"Hearts", u"Spades")
    __numGroup = ("2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A")
    __color = ""
    __num = ""
    def __init__(self):
        self.__color = random.choice(self.__colorGroup)
        self.num = random.choice(self.__numGroup)
    def exist(self,cardComputer):
        if self.__colorGroup.index(self.__color) == self.__colorGroup.index(cardComputer.__color) and self.__numGroup.index(self.num) == self.__numGroup.index(cardComputer.num):
            return True
    def playerWin(self, cardComputer):
        if self.__numGroup.index(self.num) == self.__numGroup.index(cardComputer.num):
            if self.__colorGroup.index(self.__color) > self.__colorGroup.index(cardComputer.__color):
                return True
            return False
        elif self.__numGroup.index(self.num) > self.__numGroup.index(cardComputer.num):
            return True
        return False
    def show(self):
        print self.__color, self.num

class Game:
    player = ""
    playerCard = ""
    computerCard = ""
    playGold = 0
    def __init__(self,playerName):
        self.player = Player(playerName)
        self.player.showPlayerInfo()
    def start(self):
        while 1:
            if self.player.gold() <= 0:
                print
                print 'Your have no Gold! Game Over!'
                print
                break
            key = raw_input("E+Enter To Exit, Enter ToPlay: \n")
            if key == "E":
                break
            if key == "GIVEMEGOLD":
                self.player.getGold(9999999)
            os.system("cls")
            self.player.showPlayerInfo()
            self.playGold = 0
            while self.playGold <= 0 or self.playGold > self.player.gold():
                try:
                    print
                    self.playGold = input("Play Gold:")
                    self.player.loseGold(1)
                    print
                except:
                    continue
            os.system("cls")
            self.playerCard = Card()
            print "Cost: 1 gold."
            print 'Your Card:'
            self.playerCard.show()
            
            print "Computer's Card:"
            self.computerCard = Card()
            while self.playerCard.exist(self.computerCard):
                self.computerCard = Card()
            self.computerCard.show()
            if self.playerCard.playerWin(self.computerCard):
                print 'You Win!'
                self.player.getGold(self.playGold)
            else:
                print 'You Lose!'
                self.player.loseGold(self.playGold)
            self.player.showPlayerInfo()


if __name__ == "__main__":
    while raw_input("E+Enter To Exit, Enter ToPlay: \n") != "E":
        os.system("cls")
        print "Welcome to THE GAME!\n"
        playerName = raw_input(u"Please Input Your NickName: ")
        game = Game(playerName)
        game.start()
