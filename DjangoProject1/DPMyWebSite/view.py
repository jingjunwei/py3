from django.http import HttpResponse
import datetime
def hello(req):
    return HttpResponse("<h1>Hello World!</h1>")

def homepage(req):
    return HttpResponse("<h1>THIS IS HOMEPAGE!</h1>")

def now(req):
    crtime = datetime.datetime.now()
    txt = "It's %s." % crtime
    return HttpResponse("<h1>" + txt + "</h1>")

def getparam(req, param):
    print req
    txt = "The param is [%s]" % param
    return HttpResponse("<h1>" + txt + "</h1>")