﻿from django.conf.urls import patterns, include, url
from DPMyWebSite.view import hello, homepage, now, getparam

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    #url(r'^$', 'MyApp.views.home', name='home'),
    #url(r'^DPMyWebSite/', include('DPMyWebSite.DPMyWebSite.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    #url(r'^admin/', include(admin.site.urls)),
    url(r'^$', homepage),
    url(r'^hello/$', hello),
    url(r'^now/$', now),
    url(r'^getparam/([0-9a-zA-Z/]{1,20})', getparam),
)
