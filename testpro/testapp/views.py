#coding:utf-8
from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return HttpResponse(u"第一个Django App")

def add2(request,a,b):
    c = int(a) + int(b)
    return HttpResponse(str(c))

