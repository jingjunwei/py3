# coding: utf-8
# 打开测试文件
# fp = open("test.txt", 'w')

fp = open("test.txt", 'r')
lines = fp.readlines();
# 表的第一行
head = 0
# 表的最后一行
lastEnd = 0
# 表名
tableName = ''
lastTbName = ''
# 索引字段
idField = ''
# 是否打印
isPrinted = False

def createColumn(id, type):
    if isPrinted:
        print '[' + id + '] ' + type + ' ,'
    return '[' + id + '] ' + type + ' ,'

def createIdColumn(id, type):
    if isPrinted:
        print '[' + id + '] ' + type + ' IDENTITY(1, 1) NOT NULL,'
    return '[' + id + '] ' + type + ' IDENTITY(1, 1) NOT NULL,'

def createTableHead(tableName):
    if isPrinted:
        print '\ngo\nCREATE TABLE ' +  tableName + ' (' 
    return '\ngo\nCREATE TABLE ' +  tableName + ' (' 

def createIndexAndTableEnd(tbName, idField):
    if isPrinted:
        print 'CONSTRAINT [PK_' + tbName + '] PRIMARY KEY CLUSTERED ( [' + idField + '] ASC )' + ''' WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO\n'''
    return 'CONSTRAINT [PK_' + tbName + '] PRIMARY KEY CLUSTERED ( [' + idField + '] ASC )' + ''' WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO\n'''

def createTbDesc(tbName, desc):
    if isPrinted:
        print "\nEXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'" + desc + "', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'"+ tbName +"' \nGO"
    return "\nEXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'" + desc + "', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'"+ tbName +"' \nGO"

def createColumnDesc(tbName, colName, desc):
    if isPrinted:
        print "\nEXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'" + desc + "', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'"+ tbName +"', @level2type = N'COLUMN', @level2name = N'" + colName + "' \nGO"
    return "\nEXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'" + desc + "', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'"+ tbName +"', @level2type = N'COLUMN', @level2name = N'" + colName + "' \nGO"

dbName = raw_input('请输入数据库名称(直接回车为KnowledgeDB):')
if(dbName == ''):
    dbName = 'KnowledgeDB'
print
# 文件名
filename = dbName + '.sql'
fpSql = open(filename, 'w')
fpSql.write("-- set DB master to access Table sysdatabases\n");
fpSql.write("USE master \n");
fpSql.write("go\n\n");
fpSql.write("-- If EXISTS, Delete It\n");
fpSql.write("IF EXISTS ( SELECT  *\n");
fpSql.write("FROM    sysdatabases\n");
fpSql.write(" WHERE   name = '" + dbName + "' )\n");
fpSql.write(" DROP DATABASE " + dbName + '\n');
fpSql.write("go\n\n");
fpSql.write("-- Create DB\n");
fpSql.write("CREATE DATABASE " + dbName + '\n');
fpSql.write("go\n");
fpSql.write("USE " + dbName + '\n');
# 循环文件的行
for l in range(0, len(lines)):
    if len(lines[l]) > 0:
        # 表名判断
        if(len(lines[l].split('\t')) == 2):
            lastEnd = l
            if(lastEnd != 0):
                fpSql.write(createIndexAndTableEnd(tableName, idField))
                for i in range(head, lastEnd):
                    if(len(lines[i].split('\t')) == 2):
                        lastTbName = lines[i].split('\n')[0].split('\t')[0]
                        tbDesc = lines[i].split('\n')[0].split('\t')[1]
                        fpSql.write('-- ' + lastTbName + 'Create Table Description\n')
                        fpSql.write(createTbDesc(lastTbName, tbDesc))
                    else:
                        colName = lines[i].split('\n')[0].split('\t')[0]
                        desc = lines[i].split('\n')[0].split('\t')[2]
                        fpSql.write(createColumnDesc(lastTbName, colName, desc))
            head = l
            tableName = lines[l].split('\n')[0].split('\t')[0]
            fpSql.write('\n--*****************************************************\n');
            fpSql.write('-- Create Table' + tableName + lines[l].split('\n')[0].split('\t')[1] + '\n')
            fpSql.write(createTableHead(tableName))
            
        else:
            id = lines[l].split('\t')[0]
            type = lines[l].split('\t')[1]
            if(lines[l].split('\t')[0].upper() == 'ID'):
                fpSql.write(createIdColumn(id, type))
                idField = id
            else:
                fpSql.write(createColumn(id, type))
    if(l == len(lines)-1):
        fpSql.write(createIndexAndTableEnd(tableName, idField))
        for i in range(head, len(lines)):
            if(len(lines[i].split('\t')) == 2):
                lastTbName = lines[i].split('\n')[0].split('\t')[0]
                tbDesc = lines[i].split('\n')[0].split('\t')[1]
                fpSql.write('\n--*****************************************************\n');
                fpSql.write('-- ' + lastTbName + 'Create Table Description\n')
                fpSql.write(createTbDesc(lastTbName, tbDesc))
            else:
                colName = lines[i].split('\n')[0].split('\t')[0]
                desc = lines[i].split('\n')[0].split('\t')[2]
                fpSql.write(createColumnDesc(lastTbName, colName, desc))

print '完成。\n'
dbName = raw_input('回车退出。')
fp.close()
fpSql.close()
