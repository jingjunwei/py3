-- set DB master to access Table sysdatabases
USE master 
go

-- If EXISTS, Delete It
IF EXISTS ( SELECT  *
FROM    sysdatabases
 WHERE   name = 'KnowledgeDB' )
 DROP DATABASE KnowledgeDB
go

-- Create DB
CREATE DATABASE KnowledgeDB
go
USE KnowledgeDB

--*****************************************************
-- Create TableT_User用户表

go
CREATE TABLE T_User ([Id] int IDENTITY(1, 1) NOT NULL,[Name] nvarchar(50) ,[NickName] nvarchar(50) ,[Email] nvarchar(80) ,[Sex] Char(1) ,[Birthday] Datetime ,[Mobile] nvarchar(32) ,[Tel] nvarchar(32) ,[QQ] nvarchar(20) ,[Address] nvarchar(500) ,[Photo] nvarchar(500) ,[Enabled] int ,[Signature] nvarchar(800) ,[Description] nvarchar(800) ,[CompanyId] int ,[CreatedUId] int ,[CreatedTime] Datetime ,[ModifiedUId] int ,[ModifiedTime] Datetime ,[DelUId] int ,[DelTime] Datetime ,[DelFlag] int ,[Field1] nvarchar(1) ,[Field2] nvarchar(1) ,CONSTRAINT [PK_T_User] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_UserCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'用户表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'姓名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'Name' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'昵称', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'NickName' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'电邮', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'Email' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'性别 M男 F女', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'Sex' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'生日', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'Birthday' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'手机', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'Mobile' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'座机', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'Tel' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'QQ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'QQ' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'地址', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'Address' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'头像', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'Photo' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'有效 1是 0否', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'Enabled' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'个性签名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'Signature' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'个人描述', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'Description' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'公司 T_Company主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'CompanyId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'CreatedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'CreatedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'ModifiedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'ModifiedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'DelUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'DelTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除标记 1是 0否', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'DelFlag' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'备用字段1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'Field1' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'备用字段2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User', @level2type = N'COLUMN', @level2name = N'Field2' 
GO
--*****************************************************
-- Create TableT_Account账户表

go
CREATE TABLE T_Account ([Id] int IDENTITY(1, 1) NOT NULL,[UserId] int ,[EmailAccount] nvarchar(100) ,[ABCAccount] nvarchar(30) ,[MobileAccount] nvarchar(20) ,[Pwd] nvarchar(100) ,[CreatedUId] int ,[CreatedTime] Datetime ,[ModifiedUId] int ,[ModifiedTime] Datetime ,[DelUId] int ,[DelTime] Datetime ,[DelFlag] int ,[Field1] nvarchar(1) ,[Field2] nvarchar(1) ,CONSTRAINT [PK_T_Account] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_AccountCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'账户表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Account' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Account', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'用户主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Account', @level2type = N'COLUMN', @level2name = N'UserId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'邮箱账户', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Account', @level2type = N'COLUMN', @level2name = N'EmailAccount' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'字母账户', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Account', @level2type = N'COLUMN', @level2name = N'ABCAccount' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'手机账户', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Account', @level2type = N'COLUMN', @level2name = N'MobileAccount' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'密码(加密后)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Account', @level2type = N'COLUMN', @level2name = N'Pwd' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Account', @level2type = N'COLUMN', @level2name = N'CreatedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Account', @level2type = N'COLUMN', @level2name = N'CreatedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Account', @level2type = N'COLUMN', @level2name = N'ModifiedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Account', @level2type = N'COLUMN', @level2name = N'ModifiedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Account', @level2type = N'COLUMN', @level2name = N'DelUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Account', @level2type = N'COLUMN', @level2name = N'DelTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除标记 1是 0否', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Account', @level2type = N'COLUMN', @level2name = N'DelFlag' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'备用字段1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Account', @level2type = N'COLUMN', @level2name = N'Field1' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'备用字段2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Account', @level2type = N'COLUMN', @level2name = N'Field2' 
GO
--*****************************************************
-- Create TableT_Role角色表

go
CREATE TABLE T_Role ([Id] int IDENTITY(1, 1) NOT NULL,[Name] nvarchar(50) ,[Type] int ,[CompanyId] int ,[Description] nvarchar(200) ,[CreatedUId] int ,[CreatedTime] Datetime ,[ModifiedUId] int ,[ModifiedTime] Datetime ,[DelUId] int ,[DelTime] Datetime ,[DelFlag] int ,CONSTRAINT [PK_T_Role] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_RoleCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'角色表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Role' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Role', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'角色名称', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Role', @level2type = N'COLUMN', @level2name = N'Name' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'根据系统自定义（企业角色、内置角色等）', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Role', @level2type = N'COLUMN', @level2name = N'Type' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'公司ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Role', @level2type = N'COLUMN', @level2name = N'CompanyId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'描述', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Role', @level2type = N'COLUMN', @level2name = N'Description' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Role', @level2type = N'COLUMN', @level2name = N'CreatedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Role', @level2type = N'COLUMN', @level2name = N'CreatedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Role', @level2type = N'COLUMN', @level2name = N'ModifiedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Role', @level2type = N'COLUMN', @level2name = N'ModifiedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Role', @level2type = N'COLUMN', @level2name = N'DelUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Role', @level2type = N'COLUMN', @level2name = N'DelTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除标记 1是 0否', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Role', @level2type = N'COLUMN', @level2name = N'DelFlag' 
GO
--*****************************************************
-- Create TableT_User_Role用户角色表

go
CREATE TABLE T_User_Role ([Id] int IDENTITY(1, 1) NOT NULL,[UserId] int ,[RoleId] int ,CONSTRAINT [PK_T_User_Role] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_User_RoleCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'用户角色表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User_Role' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User_Role', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'用户ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User_Role', @level2type = N'COLUMN', @level2name = N'UserId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'角色ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_User_Role', @level2type = N'COLUMN', @level2name = N'RoleId' 
GO
--*****************************************************
-- Create TableT_Limit基础数据权限表

go
CREATE TABLE T_Limit ([Id] int IDENTITY(1, 1) NOT NULL,[Name] nvarchar(50) ,[LimitTypeId] int ,[Value] Bigint ,[Description] nvarchar(200) ,[CreatedUId] int ,[CreatedTime] Datetime ,[ModifiedUId] int ,[ModifiedTime] Datetime ,[DelUId] int ,[DelTime] Datetime ,[DelFlag] int ,CONSTRAINT [PK_T_Limit] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_LimitCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'基础数据权限表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Limit' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Limit', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'权限名称', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Limit', @level2type = N'COLUMN', @level2name = N'Name' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'权限类型id', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Limit', @level2type = N'COLUMN', @level2name = N'LimitTypeId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'权限值', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Limit', @level2type = N'COLUMN', @level2name = N'Value' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'描述', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Limit', @level2type = N'COLUMN', @level2name = N'Description' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Limit', @level2type = N'COLUMN', @level2name = N'CreatedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Limit', @level2type = N'COLUMN', @level2name = N'CreatedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Limit', @level2type = N'COLUMN', @level2name = N'ModifiedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Limit', @level2type = N'COLUMN', @level2name = N'ModifiedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Limit', @level2type = N'COLUMN', @level2name = N'DelUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Limit', @level2type = N'COLUMN', @level2name = N'DelTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除标记 1是 0否', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Limit', @level2type = N'COLUMN', @level2name = N'DelFlag' 
GO
--*****************************************************
-- Create TableT_Role_Limit角色权限表

go
CREATE TABLE T_Role_Limit ([Id] int IDENTITY(1, 1) NOT NULL,[RoleId] int ,[LimitValues] int ,[ModuleActionId] int ,CONSTRAINT [PK_T_Role_Limit] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_Role_LimitCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'角色权限表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Role_Limit' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Role_Limit', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'角色Id', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Role_Limit', @level2type = N'COLUMN', @level2name = N'RoleId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'权限集合值', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Role_Limit', @level2type = N'COLUMN', @level2name = N'LimitValues' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'用于角色在菜单下控件的权限 0/控件id', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Role_Limit', @level2type = N'COLUMN', @level2name = N'ModuleActionId' 
GO
--*****************************************************
-- Create TableT_Menu菜单基础表

go
CREATE TABLE T_Menu ([Id] int IDENTITY(1, 1) NOT NULL,[Name] nvarchar(50) ,[Url] nvarchar(200) ,[Sort] int ,[Type] int ,[ParentId] int ,[Icon] nvarchar(200) ,[ActionValues] int ,[Description] nvarchar(200) ,[CreatedUId] int ,[CreatedTime] Datetime ,[ModifiedUId] int ,[ModifiedTime] Datetime ,[DelUId] int ,[DelTime] Datetime ,[DelFlag] int ,CONSTRAINT [PK_T_Menu] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_MenuCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'菜单基础表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Menu' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Menu', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'菜单名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Menu', @level2type = N'COLUMN', @level2name = N'Name' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'菜单路径', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Menu', @level2type = N'COLUMN', @level2name = N'Url' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'页面排序', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Menu', @level2type = N'COLUMN', @level2name = N'Sort' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'类型（预留）0前台1后台', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Menu', @level2type = N'COLUMN', @level2name = N'Type' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'上级菜单（父Id）', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Menu', @level2type = N'COLUMN', @level2name = N'ParentId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'菜单图标（预留）', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Menu', @level2type = N'COLUMN', @level2name = N'Icon' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'操作集合（二进制权限控制）', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Menu', @level2type = N'COLUMN', @level2name = N'ActionValues' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'描述', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Menu', @level2type = N'COLUMN', @level2name = N'Description' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Menu', @level2type = N'COLUMN', @level2name = N'CreatedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Menu', @level2type = N'COLUMN', @level2name = N'CreatedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Menu', @level2type = N'COLUMN', @level2name = N'ModifiedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Menu', @level2type = N'COLUMN', @level2name = N'ModifiedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Menu', @level2type = N'COLUMN', @level2name = N'DelUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Menu', @level2type = N'COLUMN', @level2name = N'DelTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除标记 1是 0否', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Menu', @level2type = N'COLUMN', @level2name = N'DelFlag' 
GO
--*****************************************************
-- Create TableT_Action页面（模块、菜单下）基础操作表

go
CREATE TABLE T_Action ([Id] int IDENTITY(1, 1) NOT NULL,[Name] nvarchar(50) ,[Value] int ,[Action] nvarchar(50) ,[Description] nvarchar(200) ,CONSTRAINT [PK_T_Action] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_ActionCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'页面（模块、菜单下）基础操作表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Action' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Action', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'功能名称', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Action', @level2type = N'COLUMN', @level2name = N'Name' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'权限值', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Action', @level2type = N'COLUMN', @level2name = N'Value' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'操作', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Action', @level2type = N'COLUMN', @level2name = N'Action' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'描述', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Action', @level2type = N'COLUMN', @level2name = N'Description' 
GO
--*****************************************************
-- Create TableT_Company公司表

go
CREATE TABLE T_Company ([Id] int IDENTITY(1, 1) NOT NULL,[Name] nvarchar(50) ,[RegDate] Datetime ,[RegAddress] nvarchar(200) ,[RegFund] nvarchar(50) ,[Type] int ,[ContactPerson] nvarchar(50) ,[Tel] nvarchar(50) ,[EmployeeNum] int ,[DeveloperNum] int ,[HighGradeNum] int ,[MiddleGradeNum] int ,[LowGradeNum] int ,[LastYearRevenue] nvarchar(50) ,[Corporate] nvarchar(20) ,[CorporateBirthDay] datetime ,[CorporateDegree] nvarchar(50) ,[CorporateTitle] nvarchar(50) ,[CorporatePhone] nvarchar(20) ,[CorporateEmail] nvarchar(200) ,[Industry] nvarchar(200) ,[MajorProduct] nvarchar(200) ,[AreaCode] nvarchar(200) ,[Logo] nvarchar(200) ,[Description] nvarchar(200) ,[CreatedUId] int ,[CreatedTime] Datetime ,[ModifiedUId] int ,[ModifiedTime] Datetime ,[DelUId] int ,[DelTime] Datetime ,[DelFlag] int ,CONSTRAINT [PK_T_Company] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_CompanyCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'公司表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'公司名称', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'Name' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'注册时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'RegDate' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'注册地址', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'RegAddress' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'注册资金', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'RegFund' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'企业类型', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'Type' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'联系人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'ContactPerson' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'联系电话', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'Tel' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'员工数', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'EmployeeNum' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'研发人员数', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'DeveloperNum' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'高级职称人员数', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'HighGradeNum' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'中级职称人员数', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'MiddleGradeNum' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'初级职称人员数', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'LowGradeNum' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'去年营收', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'LastYearRevenue' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'企业法人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'Corporate' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'法人出生年月', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'CorporateBirthDay' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'法人学历', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'CorporateDegree' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'法人职称', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'CorporateTitle' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'法人联系电话', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'CorporatePhone' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'法人电邮', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'CorporateEmail' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'产品行业', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'Industry' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主要产品', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'MajorProduct' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'区域代码', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'AreaCode' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'LOGO图片路径', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'Logo' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'描述', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'Description' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'CreatedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'CreatedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'ModifiedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'ModifiedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'DelUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'DelTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除标记 1是 0否', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company', @level2type = N'COLUMN', @level2name = N'DelFlag' 
GO
--*****************************************************
-- Create TableT_Company_Follow公司关注表

go
CREATE TABLE T_Company_Follow ([Id] int IDENTITY(1, 1) NOT NULL,[CompanyId] int ,[Type] int ,[Value] nvarchar(50) ,[Name] nvarchar(50) ,CONSTRAINT [PK_T_Company_Follow] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_Company_FollowCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'公司关注表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company_Follow' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company_Follow', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'公司ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company_Follow', @level2type = N'COLUMN', @level2name = N'CompanyId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'关注类型(机构、竞争对手机构、专家、标准)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company_Follow', @level2type = N'COLUMN', @level2name = N'Type' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'关注对象的ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company_Follow', @level2type = N'COLUMN', @level2name = N'Value' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'关注对象的名称', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company_Follow', @level2type = N'COLUMN', @level2name = N'Name' 
GO
--*****************************************************
-- Create TableT_Company_Follow_Web公司关注网站表

go
CREATE TABLE T_Company_Follow_Web ([Id] int IDENTITY(1, 1) NOT NULL,[CompanyId] int ,[Url] nvarchar(50) ,[Name] nvarchar(50) ,[Section] nvarchar(200) ,[VerifyStatus] int ,CONSTRAINT [PK_T_Company_Follow_Web] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_Company_Follow_WebCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'公司关注网站表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company_Follow_Web' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company_Follow_Web', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'公司ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company_Follow_Web', @level2type = N'COLUMN', @level2name = N'CompanyId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'关注对象的ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company_Follow_Web', @level2type = N'COLUMN', @level2name = N'Url' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'关注对象的名称', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company_Follow_Web', @level2type = N'COLUMN', @level2name = N'Name' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'关注栏目', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company_Follow_Web', @level2type = N'COLUMN', @level2name = N'Section' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'审核状态0待审核1审核通过 2审核未通过', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company_Follow_Web', @level2type = N'COLUMN', @level2name = N'VerifyStatus' 
GO
--*****************************************************
-- Create TableT_Company_Ip公司可访问IP段表

go
CREATE TABLE T_Company_Ip ([Id] int IDENTITY(1, 1) NOT NULL,[CompanyId] int ,[StartIp] nvarchar(30) ,[EndIp] nvarchar(30) ,CONSTRAINT [PK_T_Company_Ip] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_Company_IpCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'公司可访问IP段表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company_Ip' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company_Ip', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'公司ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company_Ip', @level2type = N'COLUMN', @level2name = N'CompanyId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'起始IP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company_Ip', @level2type = N'COLUMN', @level2name = N'StartIp' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'结束IP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Company_Ip', @level2type = N'COLUMN', @level2name = N'EndIp' 
GO
--*****************************************************
-- Create TableT_KnowledgeDirectory知识目录

go
CREATE TABLE T_KnowledgeDirectory ([Id] int IDENTITY(1, 1) NOT NULL,[CompanyId] int ,[Name] varchar(200) ,[ParentId] int ,[Sort] int ,[Subjects] text ,[Description] nvarchar(200) ,[CreatedUId] int ,[CreatedTime] Datetime ,[ModifiedUId] int ,[ModifiedTime] Datetime ,[DelUId] int ,[DelTime] Datetime ,[DelFlag] int ,CONSTRAINT [PK_T_KnowledgeDirectory] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_KnowledgeDirectoryCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'知识目录', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_KnowledgeDirectory' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_KnowledgeDirectory', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'公司ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_KnowledgeDirectory', @level2type = N'COLUMN', @level2name = N'CompanyId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'名称', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_KnowledgeDirectory', @level2type = N'COLUMN', @level2name = N'Name' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'父ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_KnowledgeDirectory', @level2type = N'COLUMN', @level2name = N'ParentId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'排序', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_KnowledgeDirectory', @level2type = N'COLUMN', @level2name = N'Sort' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主题集合', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_KnowledgeDirectory', @level2type = N'COLUMN', @level2name = N'Subjects' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'描述', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_KnowledgeDirectory', @level2type = N'COLUMN', @level2name = N'Description' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_KnowledgeDirectory', @level2type = N'COLUMN', @level2name = N'CreatedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_KnowledgeDirectory', @level2type = N'COLUMN', @level2name = N'CreatedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_KnowledgeDirectory', @level2type = N'COLUMN', @level2name = N'ModifiedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_KnowledgeDirectory', @level2type = N'COLUMN', @level2name = N'ModifiedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_KnowledgeDirectory', @level2type = N'COLUMN', @level2name = N'DelUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_KnowledgeDirectory', @level2type = N'COLUMN', @level2name = N'DelTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除标记 1是 0否', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_KnowledgeDirectory', @level2type = N'COLUMN', @level2name = N'DelFlag' 
GO
--*****************************************************
-- Create TableT_Role_Knowledge角色与知识目录关系表

go
CREATE TABLE T_Role_Knowledge ([Id] int IDENTITY(1, 1) NOT NULL,[RoleId] int ,[KnowledgeId] int ,CONSTRAINT [PK_T_Role_Knowledge] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_Role_KnowledgeCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'角色与知识目录关系表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Role_Knowledge' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Role_Knowledge', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'角色ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Role_Knowledge', @level2type = N'COLUMN', @level2name = N'RoleId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'知识目录ID（一级目录的）', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Role_Knowledge', @level2type = N'COLUMN', @level2name = N'KnowledgeId' 
GO
--*****************************************************
-- Create TableT_Bulletin快报表

go
CREATE TABLE T_Bulletin ([Id] int IDENTITY(1, 1) NOT NULL,[Title] nvarchar(100) ,[BulletinTypeId] int ,[Keywords] nvarchar(50) ,[SubTime] datetime ,[Years] nvarchar(20) ,[Issue] nvarchar(50) ,[Cover] varchar(100) ,[CompanyId] int ,[Description] nvarchar(200) ,[CreatedUId] int ,[CreatedTime] Datetime ,[ModifiedUId] int ,[ModifiedTime] Datetime ,[DelUId] int ,[DelTime] Datetime ,[DelFlag] int ,CONSTRAINT [PK_T_Bulletin] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_BulletinCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'快报表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Bulletin' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Bulletin', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'标题', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Bulletin', @level2type = N'COLUMN', @level2name = N'Title' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'快报类型ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Bulletin', @level2type = N'COLUMN', @level2name = N'BulletinTypeId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'关键字', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Bulletin', @level2type = N'COLUMN', @level2name = N'Keywords' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'发布时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Bulletin', @level2type = N'COLUMN', @level2name = N'SubTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'年份', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Bulletin', @level2type = N'COLUMN', @level2name = N'Years' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'期次', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Bulletin', @level2type = N'COLUMN', @level2name = N'Issue' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'封面', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Bulletin', @level2type = N'COLUMN', @level2name = N'Cover' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'公司自动生成时所用字段', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Bulletin', @level2type = N'COLUMN', @level2name = N'CompanyId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'描述', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Bulletin', @level2type = N'COLUMN', @level2name = N'Description' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Bulletin', @level2type = N'COLUMN', @level2name = N'CreatedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Bulletin', @level2type = N'COLUMN', @level2name = N'CreatedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Bulletin', @level2type = N'COLUMN', @level2name = N'ModifiedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Bulletin', @level2type = N'COLUMN', @level2name = N'ModifiedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Bulletin', @level2type = N'COLUMN', @level2name = N'DelUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Bulletin', @level2type = N'COLUMN', @level2name = N'DelTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除标记 1是 0否', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Bulletin', @level2type = N'COLUMN', @level2name = N'DelFlag' 
GO
--*****************************************************
-- Create TableT_BulletinType快报类型表

go
CREATE TABLE T_BulletinType ([Id] int IDENTITY(1, 1) NOT NULL,[Name] nvarchar(50) ,CONSTRAINT [PK_T_BulletinType] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_BulletinTypeCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'快报类型表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_BulletinType' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_BulletinType', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'快报类型', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_BulletinType', @level2type = N'COLUMN', @level2name = N'Name' 
GO
--*****************************************************
-- Create TableT_BulletinContent快报内容表

go
CREATE TABLE T_BulletinContent ([Id] int IDENTITY(1, 1) NOT NULL,[BullTitle] nvarchar(50) ,[BulletinId] int ,[BullArticleId] nvarchar(MAX) ,[Method] int ,CONSTRAINT [PK_T_BulletinContent] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_BulletinContentCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'快报内容表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_BulletinContent' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_BulletinContent', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'快报类型', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_BulletinContent', @level2type = N'COLUMN', @level2name = N'BullTitle' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'快报ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_BulletinContent', @level2type = N'COLUMN', @level2name = N'BulletinId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'快报文章', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_BulletinContent', @level2type = N'COLUMN', @level2name = N'BullArticleId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'生成方式1自动生成 2手动添加', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_BulletinContent', @level2type = N'COLUMN', @level2name = N'Method' 
GO
--*****************************************************
-- Create TableT_BulletinPush快报推送表

go
CREATE TABLE T_BulletinPush ([Id] int IDENTITY(1, 1) NOT NULL,[BulletinId] int ,[ObjectType] int ,[ObjectId] int ,[Description] nvarchar(200) ,[CreatedUId] int ,[CreatedTime] Datetime ,[ModifiedUId] int ,[ModifiedTime] Datetime ,[DelUId] int ,[DelTime] Datetime ,[DelFlag] int ,CONSTRAINT [PK_T_BulletinPush] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_BulletinPushCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'快报推送表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_BulletinPush' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_BulletinPush', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'快报ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_BulletinPush', @level2type = N'COLUMN', @level2name = N'BulletinId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'推送对象类型', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_BulletinPush', @level2type = N'COLUMN', @level2name = N'ObjectType' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'推送对象ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_BulletinPush', @level2type = N'COLUMN', @level2name = N'ObjectId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'描述', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_BulletinPush', @level2type = N'COLUMN', @level2name = N'Description' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_BulletinPush', @level2type = N'COLUMN', @level2name = N'CreatedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_BulletinPush', @level2type = N'COLUMN', @level2name = N'CreatedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_BulletinPush', @level2type = N'COLUMN', @level2name = N'ModifiedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_BulletinPush', @level2type = N'COLUMN', @level2name = N'ModifiedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_BulletinPush', @level2type = N'COLUMN', @level2name = N'DelUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_BulletinPush', @level2type = N'COLUMN', @level2name = N'DelTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除标记 1是 0否', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_BulletinPush', @level2type = N'COLUMN', @level2name = N'DelFlag' 
GO
--*****************************************************
-- Create TableT_UploadArticle上传文章

go
CREATE TABLE T_UploadArticle ([Id] int IDENTITY(1, 1) NOT NULL,[Title] nvarchar(100) ,[Writer] nvarchar(50) ,[Source] nvarchar(200) ,[Url] nvarchar(200) ,[Content] nvarchar(MAX) ,[Method] int ,[Description] nvarchar(200) ,[CreatedUId] int ,[CreatedTime] Datetime ,[ModifiedUId] int ,[ModifiedTime] Datetime ,[DelUId] int ,[DelTime] Datetime ,[DelFlag] int ,CONSTRAINT [PK_T_UploadArticle] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_UploadArticleCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'上传文章', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UploadArticle' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UploadArticle', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'标题', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UploadArticle', @level2type = N'COLUMN', @level2name = N'Title' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'作者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UploadArticle', @level2type = N'COLUMN', @level2name = N'Writer' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'来源', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UploadArticle', @level2type = N'COLUMN', @level2name = N'Source' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'链接', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UploadArticle', @level2type = N'COLUMN', @level2name = N'Url' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'内容', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UploadArticle', @level2type = N'COLUMN', @level2name = N'Content' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'上传途径', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UploadArticle', @level2type = N'COLUMN', @level2name = N'Method' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'描述', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UploadArticle', @level2type = N'COLUMN', @level2name = N'Description' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UploadArticle', @level2type = N'COLUMN', @level2name = N'CreatedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UploadArticle', @level2type = N'COLUMN', @level2name = N'CreatedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UploadArticle', @level2type = N'COLUMN', @level2name = N'ModifiedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UploadArticle', @level2type = N'COLUMN', @level2name = N'ModifiedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UploadArticle', @level2type = N'COLUMN', @level2name = N'DelUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UploadArticle', @level2type = N'COLUMN', @level2name = N'DelTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除标记 1是 0否', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UploadArticle', @level2type = N'COLUMN', @level2name = N'DelFlag' 
GO
--*****************************************************
-- Create TableT_Attachment附件表

go
CREATE TABLE T_Attachment ([Id] int IDENTITY(1, 1) NOT NULL,[Name] nvarchar(50) ,[Path] nvarchar(200) ,[ArticleId] int ,[Description] nvarchar(200) ,[CreatedUId] int ,[CreatedTime] Datetime ,[ModifiedUId] int ,[ModifiedTime] Datetime ,[DelUId] int ,[DelTime] Datetime ,[DelFlag] int ,CONSTRAINT [PK_T_Attachment] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_AttachmentCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'附件表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Attachment' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Attachment', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'名称', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Attachment', @level2type = N'COLUMN', @level2name = N'Name' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'路径', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Attachment', @level2type = N'COLUMN', @level2name = N'Path' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'文章ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Attachment', @level2type = N'COLUMN', @level2name = N'ArticleId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'描述', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Attachment', @level2type = N'COLUMN', @level2name = N'Description' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Attachment', @level2type = N'COLUMN', @level2name = N'CreatedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Attachment', @level2type = N'COLUMN', @level2name = N'CreatedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Attachment', @level2type = N'COLUMN', @level2name = N'ModifiedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Attachment', @level2type = N'COLUMN', @level2name = N'ModifiedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Attachment', @level2type = N'COLUMN', @level2name = N'DelUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Attachment', @level2type = N'COLUMN', @level2name = N'DelTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除标记 1是 0否', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Attachment', @level2type = N'COLUMN', @level2name = N'DelFlag' 
GO
--*****************************************************
-- Create TableT_loginAccess登录访问统计表

go
CREATE TABLE T_loginAccess ([Id] int IDENTITY(1, 1) NOT NULL,[Account] nvarchar(100) ,[UserId] nvarchar(50) ,[CompanyId] nvarchar(50) ,[Ip] nvarchar(30) ,[LoginTime] datetime ,CONSTRAINT [PK_T_loginAccess] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_loginAccessCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'登录访问统计表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_loginAccess' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_loginAccess', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'用户账号', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_loginAccess', @level2type = N'COLUMN', @level2name = N'Account' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'用户Id', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_loginAccess', @level2type = N'COLUMN', @level2name = N'UserId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'公司Id', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_loginAccess', @level2type = N'COLUMN', @level2name = N'CompanyId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'登录者的IP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_loginAccess', @level2type = N'COLUMN', @level2name = N'Ip' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'登录时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_loginAccess', @level2type = N'COLUMN', @level2name = N'LoginTime' 
GO
--*****************************************************
-- Create TableT_IndexAccess网站首页访问统计表

go
CREATE TABLE T_IndexAccess ([Id] int IDENTITY(1, 1) NOT NULL,[AccessCount] int ,[AccessTime] datetime ,CONSTRAINT [PK_T_IndexAccess] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_IndexAccessCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'网站首页访问统计表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_IndexAccess' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_IndexAccess', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'访问次数（按天统计）', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_IndexAccess', @level2type = N'COLUMN', @level2name = N'AccessCount' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'访问日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_IndexAccess', @level2type = N'COLUMN', @level2name = N'AccessTime' 
GO
--*****************************************************
-- Create TableT_SectionAccess模块访问统计表

go
CREATE TABLE T_SectionAccess ([Id] int IDENTITY(1, 1) NOT NULL,[MenuId] int ,[CompanyId] int ,[AccessCount] int ,[AccessTime] datetime ,CONSTRAINT [PK_T_SectionAccess] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_SectionAccessCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'模块访问统计表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_SectionAccess' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_SectionAccess', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'栏目ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_SectionAccess', @level2type = N'COLUMN', @level2name = N'MenuId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'企业ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_SectionAccess', @level2type = N'COLUMN', @level2name = N'CompanyId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'访问次数', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_SectionAccess', @level2type = N'COLUMN', @level2name = N'AccessCount' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'访问时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_SectionAccess', @level2type = N'COLUMN', @level2name = N'AccessTime' 
GO
--*****************************************************
-- Create TableT_ArticleAccess文章访问统计表

go
CREATE TABLE T_ArticleAccess ([Id] int IDENTITY(1, 1) NOT NULL,[ArticleId] nvarchar(100) ,[Title] nvarchar(200) ,[Type] int ,[AccessCount] int ,[AccessTime] datetime ,CONSTRAINT [PK_T_ArticleAccess] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_ArticleAccessCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'文章访问统计表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_ArticleAccess' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_ArticleAccess', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'文章ID（智立方、其他）', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_ArticleAccess', @level2type = N'COLUMN', @level2name = N'ArticleId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'文章标题', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_ArticleAccess', @level2type = N'COLUMN', @level2name = N'Title' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'文章类型（智立方、其他）', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_ArticleAccess', @level2type = N'COLUMN', @level2name = N'Type' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'访问次数', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_ArticleAccess', @level2type = N'COLUMN', @level2name = N'AccessCount' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'访问时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_ArticleAccess', @level2type = N'COLUMN', @level2name = N'AccessTime' 
GO
--*****************************************************
-- Create TableT_ZlfArticleDownload智立方文章下载统计表

go
CREATE TABLE T_ZlfArticleDownload ([Id] int IDENTITY(1, 1) NOT NULL,[ArticleId] nvarchar(100) ,[Title] nvarchar(200) ,[DownCount] int ,[DownTime] datetime ,CONSTRAINT [PK_T_ZlfArticleDownload] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_ZlfArticleDownloadCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'智立方文章下载统计表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_ZlfArticleDownload' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_ZlfArticleDownload', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'文章ID（智立方）', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_ZlfArticleDownload', @level2type = N'COLUMN', @level2name = N'ArticleId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'文章标题', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_ZlfArticleDownload', @level2type = N'COLUMN', @level2name = N'Title' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'下载次数', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_ZlfArticleDownload', @level2type = N'COLUMN', @level2name = N'DownCount' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'下载时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_ZlfArticleDownload', @level2type = N'COLUMN', @level2name = N'DownTime' 
GO
--*****************************************************
-- Create TableT_AttachDownload自定义文章附件下载统计表

go
CREATE TABLE T_AttachDownload ([Id] int IDENTITY(1, 1) NOT NULL,[ArticleId] int ,[Title] nvarchar(200) ,[AttachId] int ,[DownCount] int ,[DownTime] datetime ,CONSTRAINT [PK_T_AttachDownload] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_AttachDownloadCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'自定义文章附件下载统计表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AttachDownload' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AttachDownload', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'文章ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AttachDownload', @level2type = N'COLUMN', @level2name = N'ArticleId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'文章标题', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AttachDownload', @level2type = N'COLUMN', @level2name = N'Title' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'附件ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AttachDownload', @level2type = N'COLUMN', @level2name = N'AttachId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'下载次数', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AttachDownload', @level2type = N'COLUMN', @level2name = N'DownCount' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'下载时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AttachDownload', @level2type = N'COLUMN', @level2name = N'DownTime' 
GO
--*****************************************************
-- Create TableT_AskService提问服务表

go
CREATE TABLE T_AskService ([Id] int IDENTITY(1, 1) NOT NULL,[UserId] int ,[Phone] nvarchar(20) ,[Email] nvarchar(100) ,[Title] nvarchar(500) ,[Content] nvarchar(500) ,[Type] int ,[AskParentId] int ,[ParentId] int ,[Description] nvarchar(200) ,[CreatedUId] int ,[CreatedTime] Datetime ,[ModifiedUId] int ,[ModifiedTime] Datetime ,[DelUId] int ,[DelTime] Datetime ,[DelFlag] int ,CONSTRAINT [PK_T_AskService] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_AskServiceCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'提问服务表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AskService' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AskService', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'用户id（如果是问题就是提问用户id，如果是回答就是回答用户id）', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AskService', @level2type = N'COLUMN', @level2name = N'UserId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'提问者电话', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AskService', @level2type = N'COLUMN', @level2name = N'Phone' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'提问者邮箱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AskService', @level2type = N'COLUMN', @level2name = N'Email' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'提问标题', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AskService', @level2type = N'COLUMN', @level2name = N'Title' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'内容(问题/回答/追问)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AskService', @level2type = N'COLUMN', @level2name = N'Content' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'0为问题1为回答2追问 ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AskService', @level2type = N'COLUMN', @level2name = N'Type' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主问题ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AskService', @level2type = N'COLUMN', @level2name = N'AskParentId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'上级ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AskService', @level2type = N'COLUMN', @level2name = N'ParentId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'描述', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AskService', @level2type = N'COLUMN', @level2name = N'Description' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AskService', @level2type = N'COLUMN', @level2name = N'CreatedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AskService', @level2type = N'COLUMN', @level2name = N'CreatedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AskService', @level2type = N'COLUMN', @level2name = N'ModifiedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AskService', @level2type = N'COLUMN', @level2name = N'ModifiedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AskService', @level2type = N'COLUMN', @level2name = N'DelUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AskService', @level2type = N'COLUMN', @level2name = N'DelTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除标记 1是 0否', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_AskService', @level2type = N'COLUMN', @level2name = N'DelFlag' 
GO
--*****************************************************
-- Create TableT_UserFeedback用户反馈统计（快报，文章的点赞。评论，收藏）

go
CREATE TABLE T_UserFeedback ([Id] int IDENTITY(1, 1) NOT NULL,[Type] int ,[Source] int ,[Title] nvarchar(200) ,[ObjectId] nvarchar(100) ,[UserId] int ,[IsPraise] int ,[IsCollection] int ,[IsDiscuss] int ,CONSTRAINT [PK_T_UserFeedback] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_UserFeedbackCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'用户反馈统计（快报，文章的点赞。评论，收藏）', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UserFeedback' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UserFeedback', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'反馈的类型（快报，文章等）', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UserFeedback', @level2type = N'COLUMN', @level2name = N'Type' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'文章来源（1，智立方2，本地）', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UserFeedback', @level2type = N'COLUMN', @level2name = N'Source' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'文章、快报标题', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UserFeedback', @level2type = N'COLUMN', @level2name = N'Title' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'快报、文章id', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UserFeedback', @level2type = N'COLUMN', @level2name = N'ObjectId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'用户ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UserFeedback', @level2type = N'COLUMN', @level2name = N'UserId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'是否点赞（0未赞，1已赞）', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UserFeedback', @level2type = N'COLUMN', @level2name = N'IsPraise' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'是否收藏（0未收藏，1已收藏）', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UserFeedback', @level2type = N'COLUMN', @level2name = N'IsCollection' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'是否评论（0未评论，1已评论）', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UserFeedback', @level2type = N'COLUMN', @level2name = N'IsDiscuss' 
GO
--*****************************************************
-- Create TableT_Talk说说

go
CREATE TABLE T_Talk ([Id] int IDENTITY(1, 1) NOT NULL,[Content] nvarchar(500) ,[Description] nvarchar(200) ,[CreatedUId] int ,[CreatedTime] Datetime ,[ModifiedUId] int ,[ModifiedTime] Datetime ,[DelUId] int ,[DelTime] Datetime ,[DelFlag] int ,CONSTRAINT [PK_T_Talk] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_TalkCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'说说', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Talk' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Talk', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'说说内容', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Talk', @level2type = N'COLUMN', @level2name = N'Content' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'描述', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Talk', @level2type = N'COLUMN', @level2name = N'Description' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Talk', @level2type = N'COLUMN', @level2name = N'CreatedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Talk', @level2type = N'COLUMN', @level2name = N'CreatedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Talk', @level2type = N'COLUMN', @level2name = N'ModifiedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Talk', @level2type = N'COLUMN', @level2name = N'ModifiedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Talk', @level2type = N'COLUMN', @level2name = N'DelUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Talk', @level2type = N'COLUMN', @level2name = N'DelTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除标记 1是 0否', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_Talk', @level2type = N'COLUMN', @level2name = N'DelFlag' 
GO
--*****************************************************
-- Create TableT_TalkCollection说说收藏

go
CREATE TABLE T_TalkCollection ([Id] int IDENTITY(1, 1) NOT NULL,[TalkId] int ,[UserId] int ,[CollectionTime] Datetime ,CONSTRAINT [PK_T_TalkCollection] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_TalkCollectionCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'说说收藏', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkCollection' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkCollection', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'说说Id', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkCollection', @level2type = N'COLUMN', @level2name = N'TalkId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'收藏人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkCollection', @level2type = N'COLUMN', @level2name = N'UserId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'收藏时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkCollection', @level2type = N'COLUMN', @level2name = N'CollectionTime' 
GO
--*****************************************************
-- Create TableT_TalkPraise说说点赞

go
CREATE TABLE T_TalkPraise ([Id] int IDENTITY(1, 1) NOT NULL,[TalkId] int ,[UserId] int ,[PraiseTime] datetime ,CONSTRAINT [PK_T_TalkPraise] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_TalkPraiseCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'说说点赞', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkPraise' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkPraise', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'说说Id', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkPraise', @level2type = N'COLUMN', @level2name = N'TalkId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'点赞用户', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkPraise', @level2type = N'COLUMN', @level2name = N'UserId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'点赞时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkPraise', @level2type = N'COLUMN', @level2name = N'PraiseTime' 
GO
--*****************************************************
-- Create TableT_TalkPic说说配图

go
CREATE TABLE T_TalkPic ([Id] int IDENTITY(1, 1) NOT NULL,[TalkId] int ,[Path] nvarchar(200) ,[Size] nvarchar(50) ,[CreatedTime] datetime ,CONSTRAINT [PK_T_TalkPic] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_TalkPicCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'说说配图', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkPic' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkPic', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'说说Id', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkPic', @level2type = N'COLUMN', @level2name = N'TalkId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'图片路径', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkPic', @level2type = N'COLUMN', @level2name = N'Path' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'图片大小', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkPic', @level2type = N'COLUMN', @level2name = N'Size' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkPic', @level2type = N'COLUMN', @level2name = N'CreatedTime' 
GO
--*****************************************************
-- Create TableT_TalkComment说说评论表

go
CREATE TABLE T_TalkComment ([Id] int IDENTITY(1, 1) NOT NULL,[Content] nvarchar(240) ,[ParentId] int ,[TalkId] int ,[CreatedUId] int ,[CreatedTime] Datetime ,[ModifiedUId] int ,[ModifiedTime] Datetime ,[DelUId] int ,[DelTime] Datetime ,[DelFlag] int ,CONSTRAINT [PK_T_TalkComment] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_TalkCommentCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'说说评论表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkComment' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkComment', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'评论内容', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkComment', @level2type = N'COLUMN', @level2name = N'Content' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'父ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkComment', @level2type = N'COLUMN', @level2name = N'ParentId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'说说ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkComment', @level2type = N'COLUMN', @level2name = N'TalkId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkComment', @level2type = N'COLUMN', @level2name = N'CreatedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'创建时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkComment', @level2type = N'COLUMN', @level2name = N'CreatedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkComment', @level2type = N'COLUMN', @level2name = N'ModifiedUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'更改时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkComment', @level2type = N'COLUMN', @level2name = N'ModifiedTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkComment', @level2type = N'COLUMN', @level2name = N'DelUId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkComment', @level2type = N'COLUMN', @level2name = N'DelTime' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'删除标记 1是 0否', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_TalkComment', @level2type = N'COLUMN', @level2name = N'DelFlag' 
GO
--*****************************************************
-- Create TableT_UserFollow关注服务表

go
CREATE TABLE T_UserFollow ([Id] int IDENTITY(1, 1) NOT NULL,[UserId] int ,[ObjId] Varchar(50) ,[Time] Datetime ,[ObjType] int ,CONSTRAINT [PK_T_UserFollow] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_UserFollowCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'关注服务表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UserFollow' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UserFollow', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'关注人ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UserFollow', @level2type = N'COLUMN', @level2name = N'UserId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'被关注对象ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UserFollow', @level2type = N'COLUMN', @level2name = N'ObjId' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'关注时间', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UserFollow', @level2type = N'COLUMN', @level2name = N'Time' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'被关注对象类型0表示本地人 1智立方人 2 ......', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_UserFollow', @level2type = N'COLUMN', @level2name = N'ObjType' 
GO
--*****************************************************
-- Create TableT_WebConfig网站配置

go
CREATE TABLE T_WebConfig ([Id] int IDENTITY(1, 1) NOT NULL,[Name] Varchar(50) ,[Value] Varchar(200) ,[Type] int ,[Description] Varchar(200) ,[Size] Varchar(10) ,CONSTRAINT [PK_T_WebConfig] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO
-- T_WebConfigCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'网站配置', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_WebConfig' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_WebConfig', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'配置名称', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_WebConfig', @level2type = N'COLUMN', @level2name = N'Name' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'配置值', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_WebConfig', @level2type = N'COLUMN', @level2name = N'Value' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'配置类型', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_WebConfig', @level2type = N'COLUMN', @level2name = N'Type' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'描述', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_WebConfig', @level2type = N'COLUMN', @level2name = N'Description' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'配置文件大小', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_WebConfig', @level2type = N'COLUMN', @level2name = N'Size' 
GO
--*****************************************************
-- Create TableT_SystemConfig系统配置

go
CREATE TABLE T_SystemConfig ([Id] int IDENTITY(1, 1) NOT NULL,[Name] Varchar(50) ,[Value] Varchar(200) ,[Description] Varchar(200) ,CONSTRAINT [PK_T_SystemConfig] PRIMARY KEY CLUSTERED ( [Id] ASC ) WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
    IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
    ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
    )
    ON [PRIMARY]
    GO

--*****************************************************
-- T_SystemConfigCreate Table Description

EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'系统配置', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_SystemConfig' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'主键', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_SystemConfig', @level2type = N'COLUMN', @level2name = N'Id' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'配置名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_SystemConfig', @level2type = N'COLUMN', @level2name = N'Name' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'配置值', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_SystemConfig', @level2type = N'COLUMN', @level2name = N'Value' 
GO
EXEC sys.sp_addextendedproperty @name = N'MS_Description', @value = N'描述', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'T_SystemConfig', @level2type = N'COLUMN', @level2name = N'Description' 
GO